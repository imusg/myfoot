<?php

use Illuminate\Database\Seeder;
use App\Domain\Cycles\Database\Seeds\CycleSeeder;
use App\Domain\Meal\Database\Seeds\MealsTableSeed;
use App\Domain\Unit\Database\Seeds\UnitsTableSeed;
use App\Domain\Users\Database\Seeds\UsersTableSeed;
use App\Domain\Users\Database\Seeds\RolesTableSeeder;
use App\Domain\Dishies\Database\Seeds\DishTableSeeder;
use App\Domain\Manager\Database\Seeds\TaskTableSeeder;
use App\Domain\Clients\Database\Seeds\ClientTableSeeder;
use App\Domain\Branches\Database\Seeds\BranchesTableSeed;
use App\Domain\Storage\Database\Seeds\StorageTableSeeder;
use App\Domain\Addition\Database\Seeds\AdditionsTableSeed;
use App\Domain\Manager\Database\Seeds\TaskTypeTableSeeder;
use App\Domain\Category\Database\Seeds\CategoriesTableSeed;
use App\Domain\Users\Database\Seeds\PermissionsTableSeeder;
use App\Domain\Clients\Database\Seeds\ClientMessageTableSeeder;
use App\Domain\Dishies\Database\Seeds\DirectoryDishTableSeeder;
use App\Domain\Ingredients\Database\Seeds\IngredientTableSeeder;
use App\Domain\Clients\Database\Seeds\InteractionTypesTableSeeder;
use App\Domain\Dishies\Database\Seeds\DishIngredientsTableSeeder;
use App\Domain\Clients\Database\Seeds\ClientDeliveryAddressTableSeeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call(InteractionTypesTableSeeder::class);
        $this->call(BranchesTableSeed::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeed::class);
        $this->call(CategoriesTableSeed::class);
        $this->call(MealsTableSeed::class);
        $this->call(StorageTableSeeder::class);
        $this->call(DishTableSeeder::class);
        $this->call(ClientTableSeeder::class);
        $this->call(TaskTypeTableSeeder::class);
        $this->call(TaskTableSeeder::class);
    }
}
