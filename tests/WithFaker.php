<?php
namespace Tests;

use Faker\Generator;
use Illuminate\Foundation\Testing\WithFaker as BaseWithFaker;

trait WithFaker
{
    use BaseWithFaker {
        makeFaker as baseMakeFaker;
    }

    protected function makeFaker($locale = 'ru_RU')
    {
        return $this->app->make(Generator::class, [$locale]);
    }

}
