<?php

namespace Tests;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Application\Exceptions\Handler;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use AttachJwtToken;

    protected function disableExceptionHandling(): void
    {
        $this->app->instance(ExceptionHandler::class, new class extends Handler
        {
            public function __construct()
            {
            }

            public function report($exception)
            {
            }

            public function render($request, $exception)
            {
                throw $exception;
            }
        });
    }
}
