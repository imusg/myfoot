<?php

namespace App\Infrastructure\Abstracts;

use App\Domain\Users\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Model;

abstract class ModelPolicy
{
    use HandlesAuthorization;

    abstract protected function getModelClass(): string;

    public function viewAny(User $user): bool
    {
        return $user->can('view any ' . $this->getModelClass());
    }

    public function view(User $user, Model $model): bool
    {
        return $user->can('view ' . $this->getModelClass());
    }

    public function create(User $user) : bool
    {
        return $user->can('create ' . $this->getModelClass());
    }

    public function update(User $user, Model $model) : bool
    {
        return $user->can('update ' . $this->getModelClass());
    }

    public function delete(User $user, Model $model) : bool
    {
        return $user->can('delete ' . $this->getModelClass());
    }
}
