<?php

namespace App\Interfaces\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *    title="CRM My Eat",
 *    version="1.0.0",
 *    @OA\Contact(
 *     email="gor.mihal@gmail.com"
 *    )
 * )
 * @OA\Schemes(format="http")
 * @OA\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      in="header",
 *      name="bearerAuth",
 *      type="http",
 *      scheme="bearer",
 *      bearerFormat="JWT",
 * )
 * @OA\Server(url="http://myfoot-b1.amics-tech.ru/api/v1", description="Testing Server API")
 * @OA\Server(url="http://localhost/api/v1", description="Local Server API")
 * @OA\Tag(
 *     name="Users",
 *     description="Users endpoints",
 * )
 * @OA\Tag(
 *     name="Units",
 *     description="Units endpoints",
 * )
 * @OA\Tag(
 *     name="Dishes",
 *     description="Dishes endpoints",
 * )
 * @OA\Tag(
 *     name="Manager",
 *     description="Manager endpoints",
 * )
 * @OA\Tag(
 *     name="Clients",
 *     description="Clients endpoints",
 * )
 * @OA\Tag(
 *     name="Orders",
 *     description="Orders endpoints",
 * )
 * @OA\Tag(
 *     name="Semifinisheds",
 *     description="Semifinisheds endpoints",
 * )
 * @OA\Tag(
 *     name="Additions",
 *     description="Additions endpoints",
 * )
 * @OA\Schema(
 *    schema="Meta",
 *    type="object",
 *    @OA\Property(
 *      property="current_page",
 *      type="integer",
 *      description="Current page",
 *      example="1",
 *    ),
 *    @OA\Property(
 *      property="from",
 *      type="integer",
 *      description="From",
 *      example="1",
 *    ),
 *    @OA\Property(
 *      property="last_page",
 *      type="integer",
 *      description="Last Page",
 *      example="1",
 *    ),
 *    @OA\Property(
 *      property="per_page",
 *      type="integer",
 *      description="Per Page",
 *      example="1",
 *    ),
 *    @OA\Property(
 *      property="to",
 *      type="integer",
 *      description="To",
 *      example="2",
 *    ),
 *    @OA\Property(
 *      property="total",
 *      type="integer",
 *      description="Total",
 *      example="2",
 *    ),
 *    @OA\Property(
 *      property="timestamp",
 *      type="integer",
 *      description="Timestamp",
 *      example="1608272304509",
 *    )
 * )
 */
abstract class Controller extends BaseController
{
    use AuthorizesRequests;
    use ResponseTrait;

    protected function resourceAbilityMap(): array
    {
        return [
            'index'   => 'viewAny',
            'show'    => 'view',
            'create'  => 'create',
            'store'   => 'create',
            'edit'    => 'update',
            'update'  => 'update',
            'destroy' => 'delete',
        ];
    }
}
