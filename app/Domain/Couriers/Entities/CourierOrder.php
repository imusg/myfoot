<?php
namespace App\Domain\Couriers\Entities;

use Illuminate\Database\Eloquent\Model;

class CourierOrder extends Model
{
    protected $table = 'courier_order';

    protected $fillable = [
        'courier_id',
        'order_id',
        'ord',
        'isDelivered'
    ];
    public $timestamps = false;
}
