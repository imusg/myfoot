<?php
namespace App\Domain\Couriers\Entities;

use App\Domain\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;

class CourierFile extends Model
{
    protected $fillable = [
        'url',
        'branch_id',
        'user_id',
        'courier_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
