<?php
namespace App\Domain\Couriers\Entities;

use App\Domain\Orders\Entities\Order;
use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $fillable = [
        'name',
        'branch_id'
    ];

    public function orders()
    {
        return $this->belongsToMany(
            Order::class
        )->with('clients')
            ->withPivot(['ord', 'isDelivered']);
    }

    public function files()
    {
        return $this->hasMany(CourierFile::class, 'courier_id', 'id');
    }
}
