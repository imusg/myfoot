<?php

namespace App\Domain\Couriers\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Couriers\Services\CourierFileService;
use App\Domain\Couriers\Http\Resources\CourierFileResource;
use App\Domain\Couriers\Http\Resources\CourierFileCollection;

class CourierFileController extends Controller
{
    private CourierFileService $courierFileService;

    public function __construct(CourierFileService $courierFileService)
    {
        $this->courierFileService = $courierFileService;
        $this->resourceCollection = CourierFileCollection::class;
        $this->resourceItem = CourierFileResource::class;
    }

    public function generate(int $id)
    {
        $preview = request()->preview === 'true' ? true : false;
        return $this->respondWithCustomData($this->courierFileService->generateFile($id, $preview));
    }

    public function index()
    {
        return $this->respondWithCollection($this->courierFileService->getAllFiles());
    }
    public function destroy(int $id)
    {
        return $this->respondWithCustomData($this->courierFileService->destroy($id));
    }
}
