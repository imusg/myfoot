<?php
namespace App\Domain\Couriers\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Couriers\Services\CourierService;
use App\Domain\Couriers\Http\Resources\CourierResource;
use App\Domain\Couriers\Http\Resources\CourierCollection;
use App\Domain\Couriers\Http\Requests\CourierStoreRequest;
use App\Domain\Couriers\Http\Requests\CourierUpdateRequest;
use App\Domain\Couriers\Http\Requests\CourierBindingRequest;

class CouriersController extends Controller
{
    private CourierService $courierService;

    public function __construct(CourierService $courierService)
    {
        $this->courierService = $courierService;
        $this->resourceCollection = CourierCollection::class;
        $this->resourceItem = CourierResource::class;
    }

    public function index()
    {
        $branchId = request()->user()->branch_id;
        $limit = request()->limit ? request()->limit : 20;
        $response = $this->courierService->getAll($branchId, $limit);
        return $this->respondWithCollection($response);
    }

    public function show(int $id)
    {
        $response = $this->courierService->getOneCourier($id);
        return $this->respondWithItem($response);
    }

    public function store(CourierStoreRequest $courierStoreRequest)
    {
        $data = $courierStoreRequest->validated();
        $data['branch_id'] = request()->user()->branch_id;
        $response = $this->courierService->store($data);
        return $this->respondWithItem($response);
    }

    public function update(int $id, CourierUpdateRequest $courierUpdateRequest)
    {
        $data = $courierUpdateRequest->validated();
        $response = $this->courierService->update($id, $data);
        return $this->respondWithItem($response);
    }

    public function destroy(int $id)
    {
        $response = $this->courierService->destroy($id);
        return $this->respondWithItem($response);
    }

    public function courierBinding(CourierBindingRequest $courierBindingRequest)
    {
        $data = $courierBindingRequest->validated();
        $response = $this->courierService->courierBinding($data);
        return $this->respondWithCustomData($response);
    }

    public function courierUnbinding(int $courierId, int $orderId)
    {
        $response = $this->courierService->courierUnbinding($courierId, $orderId);
        return $this->respondWithCustomData($response);
    }
}
