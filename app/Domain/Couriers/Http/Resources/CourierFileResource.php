<?php

namespace App\Domain\Couriers\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourierFileResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'url' => $this->url,
           'created_at' => $this->created_at->locale('ru_RU')->isoFormat('DD MMMM YYYY H:m')
        ];
    }
}
