<?php

namespace App\Domain\Couriers\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CourierCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => CourierResource::collection($this->collection)
        ];
    }
}
