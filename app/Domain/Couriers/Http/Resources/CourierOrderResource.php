<?php

namespace App\Domain\Couriers\Http\Resources;

use App\Domain\Clients\Http\Resources\ClientResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CourierOrderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'client' => ClientResource::make($this->clients[0]),
            'orderId' => $this->id,
            'ord' => $this->pivot->ord,
            'isDelivered' => $this->pivot->isDelivered === 1,
        ];
    }
}
