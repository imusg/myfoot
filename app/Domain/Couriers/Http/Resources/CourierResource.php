<?php

namespace App\Domain\Couriers\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourierResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'orders' => CourierOrderResource::collection($this->orders),
            'files' => CourierFileResource::collection($this->files),
        ];
    }
}
