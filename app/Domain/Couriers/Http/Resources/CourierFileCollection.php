<?php

namespace App\Domain\Couriers\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CourierFileCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => CourierFileResource::collection($this->collection)
        ];
    }
}
