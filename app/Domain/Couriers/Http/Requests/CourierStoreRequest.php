<?php

namespace App\Domain\Couriers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourierStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
        ];
    }
}
