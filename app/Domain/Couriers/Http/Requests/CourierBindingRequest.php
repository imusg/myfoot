<?php

namespace App\Domain\Couriers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourierBindingRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'bindingsArray' => 'required|array'
        ];
    }
}
