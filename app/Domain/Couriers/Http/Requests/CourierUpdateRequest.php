<?php

namespace App\Domain\Couriers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourierUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'name' => 'string|nullable',
            'ords' => 'array|nullable',
            'isDelivered' => 'nullable|boolean',
            'order_id' => 'nullable'
        ];
    }
}
