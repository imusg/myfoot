<?php

namespace App\Domain\Couriers\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
    }

    public function provides(): array
    {
        return [];
    }
}
