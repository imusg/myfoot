<?php
namespace App\Domain\Couriers\Providers;

use Illuminate\Routing\Router;
use App\Domain\Couriers\Http\Controllers\CouriersController;
use App\Domain\Couriers\Http\Controllers\CourierFileController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesCouriers($router);
                $this->mapRoutesCourierFiles($router);
            });
    }

    private function mapRoutesCouriers(Router $router) : void
    {
        $router->apiResource('couriers', CouriersController::class)
            ->only(['index', 'show', 'store', 'update', 'destroy']);
        $router->post('couriers/binding', [CouriersController::class, 'courierBinding']);

        $router->delete(
            'couriers/binding/{courierId}/order/{orderId}',
            [CouriersController::class, 'courierUnbinding']
        );
    }

    private function mapRoutesCourierFiles(Router $router) : void
    {
        $router->get('couriers/{id}/file', [CourierFileController::class, 'generate']);
        $router->get('couriers/files', [CourierFileController::class, 'index']);
        $router->delete('couriers/files/{id}', [CourierFileController::class, 'destroy']);
    }
}
