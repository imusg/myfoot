<?php
namespace App\Domain\Couriers\Providers;

use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'couriers';
    protected bool $hasMigrations = true;
    protected bool $hasTranslations = true;
    protected bool $hasFactories = true;
    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
    ];

    protected array $policies = [];
    protected array $factories = [];
}
