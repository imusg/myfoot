<?php
namespace App\Domain\Couriers\Services;

use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Couriers\Entities\Courier;
use App\Domain\Couriers\Entities\CourierOrder;

class CourierService
{
    public function getAll($branchId, $limit)
    {
        return QueryBuilder::for(Courier::class)
            ->where('branch_id', $branchId)
            ->paginate($limit);
    }

    public function getOneCourier(int $id)
    {
        $courier = Courier::findOrFail($id);
        $courier->orders = $courier->orders
            ->where('date', request()->date)
            ->sortBy('pivot.ord');
        return $courier;
    }

    public function store(array $data)
    {
        $courier = Courier::create($data);
        return $courier;
    }

    public function update(int $id, array $data)
    {
        $courier = Courier::find($id);
        if (isset($data['ords'])) {
            foreach ($data['ords'] as $value) {
                $courier->orders()
                    ->where('order_id', $value['order_id'])
                    ->where('courier_id', $value['courier_id'])
                    ->update(['ord' => $value['ord']]);
            }
        }
        if (isset($data['isDelivered'])) {
            $courier->orders()
                ->where('order_id', $data['order_id'])
                ->update(['isDelivered' => $data['isDelivered']]);
        }
        return tap($courier)->update($data);
    }

    public function destroy(int $id)
    {
        $courier = Courier::find($id);
        return tap($courier)->delete();
    }

    public function courierBinding(array $data)
    {
        $courierBinding = CourierOrder::insert($data['bindingsArray']);
        return $courierBinding;
    }

    public function courierUnbinding(int $courierId, int $orderId)
    {
        $courierBinding = CourierOrder::where('courier_id', $courierId)
            ->where('order_id', $orderId);
        return tap($courierBinding)->delete();
    }
}
