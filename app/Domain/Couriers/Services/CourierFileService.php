<?php

namespace App\Domain\Couriers\Services;

use Carbon\Carbon;
use App\Domain\Orders\Entities\Order;
use Spatie\QueryBuilder\QueryBuilder;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Domain\Couriers\Entities\Courier;
use App\Domain\Couriers\Entities\CourierFile;
use Storage;

class CourierFileService
{
    public function generateFile(int $id, bool $preview)
    {
        $date = request()->date ? request()->date : Carbon::now()->format('Y-m-d');
        $branchId = request()->user()->branch_id;

        $generalData = collect([]);

        $courier = Courier::find($id);
        $orders = $courier->orders()
            ->where('date', $date)
            ->where('branch_id', $branchId)
            ->with(['positions', 'clients.address'])
            ->get();

        $orders->each(function ($order) use ($generalData) {
            $clientAddressFind = $order->clients[0]->address->where('isMain', 1);

            $city = $order->clients[0] ? $order->clients[0]->address[0]->city : null;
            $street = $order->clients[0]->address[0]->street;
            $house = $order->clients[0]->address[0]->house;
            $apartment = $order->clients[0]->address[0]->apartment;
            $entrance = $order->clients[0]->address[0]->entrance;
            $comment = $order->clients[0]->address[0]->comment;
            if (!$clientAddressFind->isEmpty()) {
                $city = $clientAddressFind->first()->city;
                $street = $clientAddressFind->first()->street;
                $house = $clientAddressFind->first()->house;
                $apartment = $clientAddressFind->first()->apartment;
                $entrance = $clientAddressFind->first()->entrance;
                $comment = $clientAddressFind->first()->comment;
            }

            $ord = $order->pivot->ord;
            $isDelivered = $order->pivot->isDelivered;
            // Формирование информации о клиенте
            $clientName = $order->clients[0]->name;
            $clientAddress = "{$city}, {$street}, д.{$house}, подъезд {$entrance}, кв. {$apartment}";
            $clientComment = $comment;
            $timeDelivery = isset($order->clients[0]->timeDelivery) ? implode(' - ',json_decode($order->clients[0]->timeDelivery)) : '-';
            $clientPhone = $order->clients[0]->phone;

            $client = [
                'clientName' => $clientName,
                'clientAddress' => $clientAddress,
                'clientComment' => $clientComment,
                'timeDelivery' => $timeDelivery,
                'clientPhone' => $clientPhone,
            ];

            $general = collect([]);
            $additions = collect([]);

            $order->positions->each(function ($position) use ($general, $additions) {
                if ($position->addition) {
                    $additions->push([
                        'additionalName' => $position->addition->title,
                        'additionalCount' => $position->quantity
                    ]);
                    return;
                }
                $positionCategory = $position->data->directory->dish->meal->title;
                $positionCount = $position->quantity;
                $positionName = $position->data->directory->dish->title;

                $general->push([
                    'positionCategory' => $positionCategory,
                    'positionCount' => $positionCount,
                    'positionName' => $positionName
                ]);
            });
            $generalData->push([
                'client' => $client,
                'general' => $general,
                'additions' => $additions,
                'ord' => $ord,
                'isDelivered' => $isDelivered
            ]);
        });
        $userId = request()->user()->id;
        $path = $this->generateDocFile($generalData->where('isDelivered', 0)->sortBy('ord')->values()->toArray(), $courier->name, $date);
        if (!$preview) {
            CourierFile::create([
                'url' => $path,
                'branch_id' => $branchId,
                'user_id' => $userId,
                'courier_id' => $id
            ]);
        }
        return [
            'url' => $path
        ];
    }

    public function destroy(int $id)
    {
        $file = CourierFile::find($id);
        return $file->delete();
    }
    public function getAllFiles()
    {
        $branchId = request()->user()->branch_id;
        return QueryBuilder::for(CourierFile::class)
            ->where('branch_id', $branchId)
            ->defaultSort('-created_at')
            ->paginate(20);
    }

    private function transliterateen($input)
    {
        $gost = array(
            "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
            "е"=>"e", "ё"=>"yo","ж"=>"j","з"=>"z","и"=>"i",
            "й"=>"i","к"=>"k","л"=>"l", "м"=>"m","н"=>"n",
            "о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t",
            "у"=>"y","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
            "ш"=>"sh","щ"=>"sh","ы"=>"i","э"=>"e","ю"=>"u",
            "я"=>"ya",
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
            "Е"=>"E","Ё"=>"Yo","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"I","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"Y","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
            "Ш"=>"Sh","Щ"=>"Sh","Ы"=>"I","Э"=>"E","Ю"=>"U",
            "Я"=>"Ya",
            "ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"",
            "ї"=>"j","і"=>"i","ґ"=>"g","є"=>"ye",
            "Ї"=>"J","І"=>"I","Ґ"=>"G","Є"=>"YE"
            );
            return strtr($input, $gost);
    }

    private function generateDocFile(array $generalData, string $courierName, string $d)
    {
        $name = explode(' ', $courierName);
        foreach ($name as $key => $n) {
            $name[$key] = $this->transliterateen($n);
        }
        $name = implode('_', $name);

        $templateProcessor = new TemplateProcessor(storage_path('app/templates/template_courier.docx'));
        $date = Carbon::now()->format('d.m.Y-H.i.s');
        $path = "route_sheet_{$name}_{$date}.docx";
        $pathToSave = public_path($path);

        $templateProcessor->cloneBlock('clientBlock', count($generalData), true, true);
        $templateProcessor->setValue('courierName', $courierName);
        $templateProcessor->setValue('date', Carbon::make($d)->format('d.m.Y'));

        foreach ($generalData as $key => $value) {
            $columnNumber = $key + 1;

            $templateProcessor->setValue("clientName#{$columnNumber}", $value['client']['clientName']);
            $templateProcessor->setValue("clientAddress#{$columnNumber}", $value['client']['clientAddress']);
            $templateProcessor->setValue("clientComment#{$columnNumber}", $value['client']['clientComment']);
            $templateProcessor->setValue("timeDelivery#{$columnNumber}", $value['client']['timeDelivery']);
            $templateProcessor->setValue("clientPhone#{$columnNumber}", $value['client']['clientPhone']);
        }

        $file = $templateProcessor->save();
        Storage::disk('minio')->put("files/couriers/$path", file_get_contents($file));
        return Storage::disk('minio')->url("files/couriers/$path");
    }
}
