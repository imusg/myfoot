<?php

namespace App\Domain\Orders\Tests\Feature;

use App\Domain\Users\Entities\User;
use Tests\TestCase;

class OrderControllerTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function testStoreOrders()
    {
        $reqData = [
            'date' => '2020-12-21',
            'client_id' => 1
        ];

        $this->actingAs($this->user)->post(route('orders.store'), $reqData)
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'meal',
                    'directory_dish'
                ],
                'meta'
            ]);
    }
}
