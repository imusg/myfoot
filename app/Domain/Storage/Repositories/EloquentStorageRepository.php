<?php
namespace App\Domain\Storage\Repositories;

use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Storage\Entities\StorageHistory;
use App\Domain\Storage\Contracts\StorageRepository;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentStorageRepository extends EloquentRepository implements StorageRepository
{
    private string $defaultSort = 'id';

    private array $defaultSelect = [
        'id',
        'count',
        'cost_price',
        'ingredient_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'branch',
        'ingredient.title',
        'ingredient.unit.title',
        'ingredient.category.title'
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];

    private array $allowedIncludes = [
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Storage::class)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function store(array $data) : Model
    {
        foreach ($data['ingredients'] as $key => $item) {
            StorageHistory::create([
                'date' => $data['date'],
                'provider' => $data['provider'],
                'cost_price' => $item['price'],
                'count' => $item['count'],
                'one_price' => $item['one'],
                'ingredient_id' => $item['id'],
                'branch_id' => request()->user()->branch_id
            ]);
            $storage = Storage::where('ingredient_id', $item['id'])
                ->when(request()->user()->branch_id, function ($query) {
                    return $query->where('branch_id', request()->user()->branch_id);
                })
                ->first();
            if (!$storage) {
                Storage::create([
                    'ingredient_id'=> $item['id'],
                    'branch_id' => request()->user()->branch_id,
                    'count' => $item['count'],
                    'cost_price' => $item['one']
                ]);
            } else {
                $storage->update([
                    'count' => $storage->count + $item['count'],
                    'cost_price' => (($storage->cost_price * $storage->count) + ($item['one'] * $item['count'])) / ($storage->count + $item['count'])
                ]);
            }
        }
        return $this->model;
    }

    public function getStorageHistory(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(StorageHistory::class)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function destroyHistory(int $id): bool
    {
        $storageHistory = StorageHistory::find($id);
        $storage = Storage::where('ingredient_id', $storageHistory->ingredient_id)
                ->when(request()->branch_id, function ($query) {
                    return $query->where('branch_id', request()->branch_id);
                })
                ->first();
        if ((int)$storage->count - (int)$storageHistory->count <= 0) {
            $storage->update([
                'count' => 0,
                'cost_price' => 0
            ]);
        } else {
            $storage->update([
                'count' => $storage->count - $storageHistory->count > 0 ? $storage->count - $storageHistory->count : 0,
                'cost_price' => (($storage->cost_price * $storage->count) - (($storageHistory->cost_price / $storageHistory->count) * $storageHistory->count)) / ($storage->count - $storageHistory->count)
            ]);
        }

        $storageHistory->delete();
        return true;
    }
}
