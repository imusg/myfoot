<?php
namespace App\Domain\Storage\Entities;

use Illuminate\Database\Eloquent\Model;

class StorageReason extends Model
{
    protected $fillable = ['title'];
    public $timestamps = false;
}
