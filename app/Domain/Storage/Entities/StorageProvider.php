<?php
namespace App\Domain\Storage\Entities;

use Illuminate\Database\Eloquent\Model;

class StorageProvider extends Model
{
    protected $fillable = ['name', 'comment', 'branch_id'];
}
