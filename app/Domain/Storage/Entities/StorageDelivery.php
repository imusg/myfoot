<?php
namespace App\Domain\Storage\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Entities\StorageProvider;

class StorageDelivery extends Model
{
    protected $fillable = [
        'date',
        'provider_id',
        'cost_price',
        'ingredient_id',
        'count',
        'branch_id',
        'one_price'
    ];

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function provider()
    {
        return $this->hasOne(StorageProvider::class, 'id', 'provider_id');
    }
}
