<?php
namespace App\Domain\Storage\Entities;

use App\Domain\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Storage\Entities\StorageReason;
use App\Domain\Ingredients\Entities\Ingredient;

class StorageWriteOff extends Model
{
    protected $fillable = [
        'user_id',
        'date',
        'ingredient_id',
        'branch_id',
        'reason_id',
        'sum',
        'count',
        'comment'
    ];

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function reason()
    {
        return $this->hasOne(StorageReason::class, 'id', 'reason_id');
    }

    public function storage()
    {
        return $this->hasOne(Storage::class, 'ingredient_id', 'ingredient_id')
            ->where('branch_id', request()->user()->branch_id);
    }
}
