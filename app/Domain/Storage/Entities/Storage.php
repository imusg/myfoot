<?php
namespace App\Domain\Storage\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Semis\Entities\SemifinishedIngredient;

class Storage extends Model
{
    protected $fillable = ['ingredient_id', 'count', 'cost_price', 'branch_id'];
    protected $table = 'storage';

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function ingredientInSemis()
    {
        return $this->hasMany(SemifinishedIngredient::class, 'ingredient_id', 'ingredient_id');
    }

    public function ingredientInDish()
    {
        return $this->hasMany(DishIngredient::class, 'ingredient_id', 'ingredient_id');
    }
}
