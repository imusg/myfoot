<?php

namespace App\Domain\Storage\Providers;

use Illuminate\Support\ServiceProvider;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Storage\Contracts\StorageRepository;
use App\Domain\Orders\Repositories\EloquentOrderRepository;
use App\Domain\Storage\Repositories\EloquentStorageRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(StorageRepository::class, function () {
            return new EloquentStorageRepository(new Storage());
        });
    }

    public function provides(): array
    {
        return [
            StorageRepository::class
        ];
    }
}
