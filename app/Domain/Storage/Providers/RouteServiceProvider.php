<?php
namespace App\Domain\Storage\Providers;

use Illuminate\Routing\Router;
use App\Domain\Storage\Http\Controllers\RemainsController;
use App\Domain\Storage\Http\Controllers\StorageController;
use App\Domain\Storage\Http\Controllers\ProviderController;
use App\Domain\Storage\Http\Controllers\StorageReasonsController;
use App\Domain\Storage\Http\Controllers\StorageDeliveryController;
use App\Domain\Storage\Http\Controllers\StorageWriteOffController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
                $this->mapRoutesRemains($router);
                $this->mapRoutesProvider($router);
                $this->mapRoutesStorageDelivery($router);
                $this->mapRoutesStorageReasons($router);
                $this->mapRoutesStorageWriteOff($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->apiResource('storage', StorageController::class)->only([
            'index', 'store', 'destroy'
        ]);
        $router->get('storage/history', [StorageController::class, 'getHistory']);
    }

    private function mapRoutesRemains(Router $router) : void
    {
        $router->get('storage/remains', [RemainsController::class, 'findRemainsByFilters']);
    }

    private function mapRoutesStorageReasons(Router $router) : void
    {
        $router->get('storage/reasons', [StorageReasonsController::class, 'indexStorageReasons']);
        $router->post('storage/reasons', [StorageReasonsController::class, 'createStorageReasons']);
    }

    private function mapRoutesProvider(Router $router) : void
    {
        $router->apiResource('storage/providers', ProviderController::class)
            ->only(['index','store','update','destroy']);
    }

    private function mapRoutesStorageDelivery(Router $router) : void
    {
        $router->apiResource('storage/delivery', StorageDeliveryController::class)
            ->only(['index','store','destroy']);
    }

    private function mapRoutesStorageWriteOff(Router $router) : void
    {
        $router->apiResource('storage/writeoffs', StorageWriteOffController::class)
            ->only(['index','store','update','destroy']);
    }
}
