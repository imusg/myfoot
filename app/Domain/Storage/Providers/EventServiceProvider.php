<?php

namespace App\Domain\Storage\Providers;

use App\Domain\Storage\Entities\Storage;
use App\Domain\Storage\Entities\StorageDelivery;
use App\Domain\Storage\Entities\StorageWriteOff;
use App\Domain\Storage\Listeners\StorageObserver;
use App\Domain\Storage\Listeners\StorageDeliveryObserver;
use App\Domain\Storage\Listeners\StorageWriteOffObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        Storage::observe(StorageObserver::class);
        StorageDelivery::observe(StorageDeliveryObserver::class);
        StorageWriteOff::observe(StorageWriteOffObserver::class);
    }
}
