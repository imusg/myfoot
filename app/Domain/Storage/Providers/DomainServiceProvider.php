<?php
namespace App\Domain\Storage\Providers;

use App\Infrastructure\Abstracts\ServiceProvider;
use App\Domain\Storage\Database\Factories\StorageFactory;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'storage';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        StorageFactory::class,
    ];
}
