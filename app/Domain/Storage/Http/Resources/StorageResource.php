<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Ingredients\Http\Resources\IngredientResource;

class StorageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'count' => round($this->count, 3),
            'cost_price' => $this->cost_price,
            'ingredient' => IngredientResource::make($this->ingredient),
        ];
    }
}
