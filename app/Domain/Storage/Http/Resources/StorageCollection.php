<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StorageCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => StorageResource::collection($this->collection),
        ];
    }
}
