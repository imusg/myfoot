<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StorageDeliveryCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => StorageDeliveryResource::collection($this->collection),
        ];
    }
}
