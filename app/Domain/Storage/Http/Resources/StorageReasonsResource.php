<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StorageReasonsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'value' => $this->id,
            'label' => $this->title
        ];
    }
}
