<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RemainsCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => RemainsResource::collection($this->collection),
        ];
    }
}
