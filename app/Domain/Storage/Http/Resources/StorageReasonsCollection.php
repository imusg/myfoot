<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StorageReasonsCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => StorageReasonsResource::collection($this->collection),
        ];
    }
}
