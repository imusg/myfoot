<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProviderCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => ProviderResource::collection($this->collection),
        ];
    }
}
