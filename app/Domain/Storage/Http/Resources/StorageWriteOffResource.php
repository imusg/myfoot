<?php

namespace App\Domain\Storage\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class StorageWriteOffResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ingredient' => $this->ingredient->title,
            'unit' => $this->ingredient->units->title,
            'count' => $this->count,
            'reason' => $this->reason->title,
            'date' => $this->date,
            'date_format' => Carbon::make($this->date)
                ->locale('ru_RU')
                ->isoFormat('DD MMMM YYYY'),
            'user' => $this->user->name,
            'sum' => $this->sum,
        ];
    }
}
