<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StorageWriteOffCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => StorageWriteOffResource::collection($this->collection),
        ];
    }
}
