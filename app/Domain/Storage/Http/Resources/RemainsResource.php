<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Ingredients\Http\Resources\IngredientResource;

class RemainsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'title' => $this->ingredient->title,
            'price' => $this->cost_price,
            'count' => $this->count,
            'unit' => $this->ingredient->units->title,
            'sum' => $this->count * $this->cost_price
        ];
    }
}
