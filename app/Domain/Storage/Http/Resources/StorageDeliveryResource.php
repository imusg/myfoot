<?php

namespace App\Domain\Storage\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Ingredients\Http\Resources\IngredientResource;

class StorageDeliveryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'count' => $this->count,
            'cost_price' => $this->cost_price,
            'date' => $this->date,
            'date_format' => Carbon::make("{$this->date} {$this->time}")
                ->locale('ru_RU')
                ->isoFormat('DD MMMM YYYY'),
            'provider' => ProviderResource::make($this->provider),
            'ingredient' => IngredientResource::make($this->ingredient),
        ];
    }
}
