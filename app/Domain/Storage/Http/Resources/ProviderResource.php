<?php

namespace App\Domain\Storage\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Ingredients\Http\Resources\IngredientResource;

class ProviderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'comment' => $this->comment
        ];
    }
}
