<?php
namespace App\Domain\Storage\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Storage\Services\ProviderService;
use App\Domain\Storage\Http\Resources\ProviderResource;
use App\Domain\Storage\Http\Resources\ProviderCollection;
use App\Domain\Storage\Http\Requests\ProviderStoreRequest;
use App\Domain\Storage\Http\Requests\ProviderUpdateRequest;

class ProviderController extends Controller
{
    private ProviderService $providerService;

    public function __construct(ProviderService $providerService)
    {
        $this->providerService = $providerService;
        $this->resourceItem = ProviderResource::class;
        $this->resourceCollection = ProviderCollection::class;
    }

    public function index(Request $request)
    {
        $branchId = $request->user()->branch_id;
        $response = $this->providerService->getAllByFilters($branchId);
        return $this->respondWithCollection($response);
    }

    public function store(ProviderStoreRequest $providerStoreRequest)
    {
        $data = $providerStoreRequest->validated();
        $data['branch_id'] = $providerStoreRequest->user()->branch_id;
        $response = $this->providerService->createProvider($data);
        return $this->respondWithItem($response);
    }

    public function update(int $id, ProviderUpdateRequest $providerUpdateRequest)
    {
        $response = $this->providerService->updateProvider($id, $providerUpdateRequest->validated());
        return $this->respondWithItem($response);
    }

    public function destroy(int $id)
    {
        $response = $this->providerService->deleteProvider($id);
        return $this->respondWithItem($response);
    }
}
