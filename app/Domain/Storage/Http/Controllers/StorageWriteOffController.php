<?php
namespace App\Domain\Storage\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Storage\Services\StorageWriteOffService;
use App\Domain\Storage\Http\Resources\StorageWriteOffResource;
use App\Domain\Storage\Http\Resources\StorageWriteOffCollection;
use App\Domain\Storage\Http\Requests\StorageWriteOffStoreRequest;

class StorageWriteOffController extends Controller
{
    private StorageWriteOffService $storageWriteOffService;

    public function __construct(StorageWriteOffService $storageWriteOffService)
    {
        $this->storageWriteOffService = $storageWriteOffService;
        $this->resourceItem = StorageWriteOffResource::class;
        $this->resourceCollection = StorageWriteOffCollection::class;
    }

    public function index(Request $request)
    {
        $branchId = $request->user()->branch_id;
        $export = $request->export === 'true' ? true : false;
        $response = $this->storageWriteOffService->getAllByFilters($branchId, $export);
        if ($export) {
            return $this->respondWithCustomData($response);
        }
        return $this->respondWithCollection($response);
    }

    public function store(StorageWriteOffStoreRequest $storageWriteOffStoreRequest)
    {
        $data = $storageWriteOffStoreRequest->validated();
        $data['branch_id'] = $storageWriteOffStoreRequest->user()->branch_id;
        $data['user_id'] = $storageWriteOffStoreRequest->user()->id;
        $response = $this->storageWriteOffService->create($data);
        return $this->respondWithItem($response);
    }


    public function destroy(int $id)
    {
        $response = $this->storageWriteOffService->destroy($id);
        return $this->respondWithItem($response);
    }
}
