<?php
namespace App\Domain\Storage\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Storage\Services\StorageDeliveryService;
use App\Domain\Storage\Http\Resources\StorageDeliveryResource;
use App\Domain\Storage\Http\Resources\StorageDeliveryCollection;
use App\Domain\Storage\Http\Requests\StorageDeliveryStoreRequest;

class StorageDeliveryController extends Controller
{
    private StorageDeliveryService $storageDeliveryService;

    public function __construct(StorageDeliveryService $storageDeliveryService)
    {
        $this->storageDeliveryService = $storageDeliveryService;
        $this->resourceItem = StorageDeliveryResource::class;
        $this->resourceCollection = StorageDeliveryCollection::class;
    }

    public function index(Request $request)
    {
        $branchId = $request->user()->branch_id;
        $export = $request->export === 'true' ? true : false;
        $response = $this->storageDeliveryService->getAllByFilters($branchId, $export);
        if ($export) {
            return $this->respondWithCustomData($response);
        }
        return $this->respondWithCollection($response);
    }

    public function store(StorageDeliveryStoreRequest $storageDeliveryStoreRequest)
    {
        $data = $storageDeliveryStoreRequest->validated();
        $data['branch_id'] = $storageDeliveryStoreRequest->user()->branch_id;
        $response = $this->storageDeliveryService->createStorageDelivery($data);
        return $this->respondWithCollection($response);
    }

    public function destroy(int $id)
    {
        $response = $this->storageDeliveryService->deleteStorageDelivery($id);
        return $this->respondWithItem($response);
    }
}
