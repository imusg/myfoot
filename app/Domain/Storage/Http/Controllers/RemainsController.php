<?php
namespace App\Domain\Storage\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain\Storage\Services\RemainsService;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Storage\Http\Resources\RemainsResource;
use App\Domain\Storage\Http\Resources\RemainsCollection;

class RemainsController extends Controller
{
    private RemainsService $remainsService;

    public function __construct(RemainsService $remainsService)
    {
        $this->remainsService = $remainsService;
        $this->resourceItem = RemainsResource::class;
        $this->resourceCollection = RemainsCollection::class;
    }

    public function findRemainsByFilters(Request $request)
    {
        $branchId = $request->user()->branch_id;
        $export = $request->export === 'true' ? true : false;
        $response = $this->remainsService->getAllByFilters($branchId, $export);
        if ($export) {
            return $this->respondWithCustomData($response);
        }
        return $this->respondWithCollection($response);
    }
}
