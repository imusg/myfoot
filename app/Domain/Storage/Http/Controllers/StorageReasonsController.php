<?php
namespace App\Domain\Storage\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Storage\Services\StorageReasonsService;
use App\Domain\Storage\Http\Resources\StorageReasonsResource;
use App\Domain\Storage\Http\Resources\StorageReasonsCollection;
use App\Domain\Storage\Http\Requests\StorageReasonsStoreRequest;

class StorageReasonsController extends Controller
{
    private StorageReasonsService $storageReasonsService;

    public function __construct(StorageReasonsService $storageReasonsService)
    {
        $this->storageReasonsService = $storageReasonsService;
        $this->resourceItem = StorageReasonsResource::class;
        $this->resourceCollection = StorageReasonsCollection::class;
    }

    public function indexStorageReasons()
    {
        $response = $this->storageReasonsService->getAll();
        return $this->respondWithCollection($response);
    }

    public function createStorageReasons(StorageReasonsStoreRequest $storageReasonsStoreRequest)
    {
        $data = $storageReasonsStoreRequest->validated();
        $response = $this->storageReasonsService->createStorageReasons($data);
        return $this->respondWithItem($response);
    }
}
