<?php

namespace App\Domain\Storage\Http\Controllers;

use Illuminate\Http\Response;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Storage\Contracts\StorageRepository;
use App\Domain\Storage\Http\Resources\StorageResource;
use App\Domain\Storage\Http\Resources\StorageCollection;
use App\Domain\Storage\Http\Requests\StorageStoreRequest;
use App\Domain\Storage\Http\Resources\StorageHistoryCollection;

class StorageController extends Controller
{
    private StorageRepository $storageRepository;

    public function __construct(StorageRepository $storageRepository)
    {
        $this->storageRepository = $storageRepository;
        $this->resourceItem = StorageResource::class;
        $this->resourceCollection = StorageCollection::class;
    }

    public function index()
    {
        $collection = $this->storageRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function store(StorageStoreRequest $request)
    {
        $data = $request->validated();
        $storage = $this->storageRepository->store($data);
        return $this->respondWithCustomData(StorageResource::make($storage), Response::HTTP_CREATED);
    }

    public function update(StorageStoreRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->storageRepository->update(
            $this->storageRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy(int $id)
    {
        $response = $this->storageRepository->destroyHistory($id);
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function getHistory()
    {
        $response = $this->storageRepository->getStorageHistory();
        return new StorageHistoryCollection($response);
    }
}
