<?php

namespace App\Domain\Storage\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class ProviderUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'name' => 'string|nullable',
            'comment' => 'string|nullable',
        ];
    }
}
