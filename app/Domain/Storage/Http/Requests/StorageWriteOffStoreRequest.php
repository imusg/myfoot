<?php

namespace App\Domain\Storage\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class StorageWriteOffStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'date' => 'required|string',
            'ingredient_id' => 'required|exists:ingredients,id',
            'reason_id' => 'required|exists:storage_reasons,id',
            'comment' => 'string|nullable',
            'count' => 'string|required'
        ];
    }
}
