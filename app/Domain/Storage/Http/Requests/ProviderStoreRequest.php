<?php

namespace App\Domain\Storage\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class ProviderStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'comment' => 'string|nullable',
        ];
    }
}
