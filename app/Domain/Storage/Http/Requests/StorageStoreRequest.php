<?php

namespace App\Domain\Storage\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

/**
 * App\Domain\Semis\Entities\Semifinished
 * @OA\Schema (
 *    schema="SemisRequest",
 *    type="object",
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Название полуфабриката",
 *      example="Пельмени",
 *    ),
 *     @OA\Property (
 *      property="cooking_process",
 *      type="string",
 *      description="Процесс приготовления",
 *      example="Берем мсясо ....",
 *    ),
 *    @OA\Property (
 *      property="cooking_time",
 *      type="string",
 *      example="14:20",
 *    )
 * )
*/
class StorageStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'date' => 'required|string',
            'provider' => 'required|string',
            'ingredients' => 'required|array'
        ];
    }
}
