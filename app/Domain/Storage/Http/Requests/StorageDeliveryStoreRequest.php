<?php

namespace App\Domain\Storage\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class StorageDeliveryStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'date' => 'required|string',
            'provider_id' => 'required|exists:storage_providers,id',
            'ingredients' => 'required|array'
        ];
    }
}
