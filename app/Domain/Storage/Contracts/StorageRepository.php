<?php
namespace App\Domain\Storage\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface StorageRepository extends BaseRepository
{
}
