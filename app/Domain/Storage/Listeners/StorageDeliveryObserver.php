<?php

namespace App\Domain\Storage\Listeners;

use App\Domain\Storage\Entities\Storage;
use App\Domain\Storage\Entities\StorageDelivery;

class StorageDeliveryObserver
{
    public function created(StorageDelivery $storageDelivery)
    {
        $storage = Storage::where('ingredient_id', $storageDelivery->ingredient_id)
            ->where('branch_id', $storageDelivery->branch_id)
            ->first();
        if (!$storage) {
            Storage::create([
                'ingredient_id'=> $storageDelivery->ingredient_id,
                'branch_id' =>  $storageDelivery->branch_id,
                'count' => $storageDelivery->count,
                'cost_price' => $storageDelivery->one_price
            ]);
            return;
        }

        $sumPriceStorageDelivery = $storageDelivery->one_price * $storageDelivery->count;
        $allCount = $storage->count + $storageDelivery->count;
        $storagePrice = $storage->cost_price * $storage->count;

        $storage->update([
            'count' => $storage->count + $storageDelivery->count,
            'cost_price' => ($storagePrice + $sumPriceStorageDelivery) / ($allCount)
        ]);
    }

    public function deleted(StorageDelivery $storageDelivery)
    {
        $storage = Storage::where('ingredient_id', $storageDelivery->ingredient_id)
                ->where('branch_id', $storageDelivery->branch_id)
                ->first();
        if ($storage->count - $storageDelivery->count <= 0) {
            $storage->update([
                'count' => 0,
                'cost_price' => 0
            ]);
        } else {
            $storagePrice = $storage->cost_price * $storage->count;
            $storageDeliveryPrice = $storageDelivery->one_price * $storageDelivery->count;
            $storage->update([
                'count' => $storage->count - $storageDelivery->count > 0 ? $storage->count - $storageDelivery->count : 0,
                'cost_price' => ($storagePrice - $storageDeliveryPrice) / ($storage->count - $storageDelivery->count)
            ]);
        }
    }
}
