<?php

namespace App\Domain\Storage\Listeners;

use Exception;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Storage\Entities\StorageWriteOff;

class StorageWriteOffObserver
{
    public function creating(StorageWriteOff $storageWriteOff)
    {
        if ($storageWriteOff->storage->count - (float) $storageWriteOff->count <= 0) {
            throw new Exception('Недосточное количество ингредиента на складе');
        }

        $storageWriteOff->storage()->update([
            'count' => $storageWriteOff->storage->count - (float) $storageWriteOff->count
        ]);

        $storageWriteOff->sum = (float) $storageWriteOff->count * $storageWriteOff->storage->cost_price;
    }

    public function deleting(StorageWriteOff $storageWriteOff)
    {
        $storageWriteOff->storage()->update([
            'count' => $storageWriteOff->storage->count + (float) $storageWriteOff->count
        ]);
    }
}
