<?php

namespace App\Domain\Storage\Listeners;

use App\Domain\Storage\Entities\Storage;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;

class StorageObserver
{
    public function updated(Storage $storage)
    {
        if ($storage->isDirty('cost_price')) {
            $dishIds= DishIngredient::where('ingredient_id', $storage->ingredient_id)
                ->get()
                ->map(function ($dishIngredient) {
                    return $dishIngredient->dish_id;
                });

            DirectoryDish::whereIn('dish_id', $dishIds)
                ->where('branch_id', $storage->branch_id)
                ->get()
                ->each(function ($directoryDish) use ($storage) {
                    $prices = collect([]);
                    $dishIngredients = DishIngredient::where('dish_id', $directoryDish->dish_id)->get();
                    $dishIngredients->each(function ($dishIngredient) use ($storage, $prices, $directoryDish) {
                        $storageIngredient = Storage::where('ingredient_id', $dishIngredient->ingredient_id)
                            ->where('branch_id', $directoryDish->branch_id)
                            ->first();
                        if ($storageIngredient) {
                            $price = $storageIngredient->cost_price * $dishIngredient->netto;
                            $prices->push($price);
                        }
                    });
                    if (!$prices->count()) {
                        $directoryDish->update([
                            'price_custom' => 'нет данных'
                        ]);
                    } else {
                        $directoryDish->update([
                            'price_custom' => $prices->sum()
                        ]);
                    }
                });
        }
    }
}
