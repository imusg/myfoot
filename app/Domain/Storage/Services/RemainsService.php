<?php
namespace App\Domain\Storage\Services;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Storage\Filters\FilterTypeRemains;
use App\Domain\Storage\Filters\FilterNameStorageIngredient;

class RemainsService
{
    public function getAllByFilters($branchId, $export)
    {
        $storage = QueryBuilder::for(Storage::class)
            ->where('branch_id', $branchId)
            ->allowedFilters([
                AllowedFilter::custom('type', new FilterTypeRemains),
                AllowedFilter::custom('title', new FilterNameStorageIngredient)
            ]);

        if ($export) {
            $list = collect([]);
            $storageArray = $storage->with('ingredient.units')->get()->toArray();
            foreach ($storageArray as $item) {
                $list->push([
                    $item['ingredient']['title'],
                    str_replace('.', ',', $item['count']),
                    $item['ingredient']['units']['title'],
                    str_replace('.', ',', $item['cost_price']),
                    str_replace('.', ',', $item['count']*$item['cost_price']),
                ]);
            }
            $header = ['Название', 'Количество', 'Ед измерения', 'Cредневзвешенная себестоимость', 'Cумма'];
            $list->prepend($header);
            $filename = '/upload/export_remains_'.time().'.csv';
            $fp = fopen(public_path().$filename, 'w');
            fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
            foreach ($list->unique()->toArray() as $fields) {
                $this->myFputcsv($fp, $fields);
            }
            fclose($fp);
            return ['url' => 'http://myfoot-b1.amics-tech.ru'.$filename];
        }

        return $storage->paginate(20);
    }

    private function myFputcsv($fp, $csv_arr, $delimiter = ';', $enclosure = '"')
    {
        if (!is_array($csv_arr)) {
            return(false);
        }
        for ($i = 0, $n = count($csv_arr); $i < $n; $i ++) {
            if (!is_numeric($csv_arr[$i])) {
                $csv_arr[$i] =  $enclosure.str_replace($enclosure, $enclosure.$enclosure, $csv_arr[$i]).$enclosure;
            }
            if (($delimiter == '.') && (is_numeric($csv_arr[$i]))) {
                $csv_arr[$i] =  $enclosure.$csv_arr[$i].$enclosure;
            }
        }
        $str = implode($delimiter, $csv_arr)."\n";
        fwrite($fp, $str);
        return strlen($str);
    }
}
