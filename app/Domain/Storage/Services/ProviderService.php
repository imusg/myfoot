<?php
namespace App\Domain\Storage\Services;

use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Storage\Entities\StorageProvider;

class ProviderService
{
    public function getAllByFilters($branchId)
    {
        $providers = QueryBuilder::for(StorageProvider::class)
            ->where('branch_id', $branchId)
            ->paginate(20);
        return $providers;
    }

    public function createProvider(array $data)
    {
        $provider = StorageProvider::create($data);
        return $provider;
    }

    public function updateProvider(int $id, array $data)
    {
        $provider = StorageProvider::find($id);
        return tap($provider)->update($data);
    }

    public function deleteProvider(int $id)
    {
        $provider = StorageProvider::find($id);
        return tap($provider)->delete();
    }
}
