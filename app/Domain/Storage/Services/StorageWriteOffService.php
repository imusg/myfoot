<?php
namespace App\Domain\Storage\Services;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Domain\Storage\Entities\Provider;
use App\Domain\Storage\Entities\StorageDelivery;
use App\Domain\Storage\Entities\StorageWriteOff;
use App\Domain\Storage\Filters\FilterNameStorageIngredient;

class StorageWriteOffService
{
    public function getAllByFilters($branchId, $export)
    {
        $storageWriteOff = QueryBuilder::for(StorageWriteOff::class)
            ->where('branch_id', $branchId)
            ->allowedFilters([
                'date',
                AllowedFilter::exact('reasonId', 'reason_id'),
                AllowedFilter::exact('userId', 'user_id'),
                AllowedFilter::custom('title', new FilterNameStorageIngredient),
            ])
            ->defaultSort('-date');
        if ($export) {
            $list = collect([]);

            $storageWriteOffArray = $storageWriteOff->with([
                'ingredient',
                'ingredient',
                'reason',
                'user'
            ])
                ->get()
                ->toArray();

            foreach ($storageWriteOffArray as $item) {
                $list->push([
                    $item['ingredient']['title'],
                    $item['date'],
                    str_replace('.', ',', $item['sum']),
                    $item['user']['name'],
                    $item['reason']['title']
                ]);
            }
            $header = [
                'Наименование',
                'Дата',
                'Сумма',
                'Сотрудник',
                'Причина'
            ];
            $list->prepend($header);
            $filename = '/upload/export_storage_write_offs_'.time().'.csv';
            $fp = fopen(public_path().$filename, 'w');
            fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
            foreach ($list->unique()->toArray() as $fields) {
                $this->myFputcsv($fp, $fields);
            }
            fclose($fp);
            return ['url' => 'http://myfoot-b1.amics-tech.ru'.$filename];
        }
        return $storageWriteOff->paginate(20);
    }

    private function myFputcsv($fp, $csv_arr, $delimiter = ';', $enclosure = '"')
    {
        if (!is_array($csv_arr)) {
            return(false);
        }

        for ($i = 0, $n = count($csv_arr); $i < $n; $i++) {
            if (!is_numeric($csv_arr[$i])) {
                $csv_arr[$i] =  $enclosure.str_replace($enclosure, $enclosure.$enclosure, $csv_arr[$i]).$enclosure;
            }
            if (($delimiter == '.') && (is_numeric($csv_arr[$i]))) {
                $csv_arr[$i] =  $enclosure.$csv_arr[$i].$enclosure;
            }
        }
        $str = implode($delimiter, $csv_arr)."\n";
        fwrite($fp, $str);
        return strlen($str);
    }

    public function create(array $data)
    {
        $storageWriteOff = StorageWriteOff::create($data);
        return $storageWriteOff;
    }

    public function destroy(int $id)
    {
        $storageWriteOff = StorageWriteOff::find($id);
        return tap($storageWriteOff)->delete();
    }
}
