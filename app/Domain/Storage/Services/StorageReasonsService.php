<?php
namespace App\Domain\Storage\Services;

use App\Domain\Storage\Entities\StorageReason;

class StorageReasonsService
{
    public function getAll()
    {
        $storageReasons = StorageReason::get();
        return $storageReasons;
    }

    public function createStorageReasons(array $data)
    {
        $storageReason = StorageReason::create($data);
        return $storageReason;
    }
}
