<?php
namespace App\Domain\Storage\Services;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Domain\Storage\Entities\Provider;
use App\Domain\Storage\Entities\StorageDelivery;
use App\Domain\Storage\Filters\FilterNameStorageIngredient;
use App\Domain\Storage\Http\Resources\StorageDeliveryCollection;

class StorageDeliveryService
{
    public function getAllByFilters($branchId, $export)
    {
        $storageDelivery = QueryBuilder::for(StorageDelivery::class)
            ->where('branch_id', $branchId)
            ->allowedFilters([
                'date',
                AllowedFilter::exact('providerId', 'provider_id'),
                AllowedFilter::exact('category', 'ingredient.category.id'),
                AllowedFilter::custom('title', new FilterNameStorageIngredient),
            ])
            ->defaultSort('-date');
        if ($export) {
            $list = collect([]);

            $storageDeliveryArray = $storageDelivery->with([
                'ingredient.units',
                'ingredient.category',
                'provider'
            ])
                ->get()
                ->toArray();

            foreach ($storageDeliveryArray as $item) {
                $provider = $item['provider'] ? $item['provider']['name'] : 'Не указано';
                $list->push([
                    $item['id'],
                    $item['ingredient']['title'],
                    $item['ingredient']['category']['title'],
                    $item['ingredient']['units']['title'],
                    str_replace('.', ',', $item['count']),
                    str_replace('.', ',', $item['cost_price']),
                    $provider,
                    $item['date']
                ]);
            }
            $header = [
                '#',
                'Наименование',
                'Категория',
                'Ед измерения',
                'Количество',
                'Себестоимость',
                'Поставщик',
                'Дата'
            ];
            $list->prepend($header);
            $filename = '/upload/export_storage_delivery_'.time().'.csv';
            $fp = fopen(public_path().$filename, 'w');
            fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
            foreach ($list->unique()->toArray() as $fields) {
                $this->myFputcsv($fp, $fields);
            }
            fclose($fp);
            return ['url' => 'http://myfoot-b1.amics-tech.ru'.$filename];
        }
        return $storageDelivery->paginate(20);
    }

    private function myFputcsv($fp, $csv_arr, $delimiter = ';', $enclosure = '"')
    {
        if (!is_array($csv_arr)) {
            return(false);
        }
        for ($i = 0, $n = count($csv_arr); $i < $n; $i ++) {
            if (!is_numeric($csv_arr[$i])) {
                $csv_arr[$i] =  $enclosure.str_replace($enclosure, $enclosure.$enclosure, $csv_arr[$i]).$enclosure;
            }
            if (($delimiter == '.') && (is_numeric($csv_arr[$i]))) {
                $csv_arr[$i] =  $enclosure.$csv_arr[$i].$enclosure;
            }
        }
        $str = implode($delimiter, $csv_arr)."\n";
        fwrite($fp, $str);
        return strlen($str);
    }

    public function createStorageDelivery(array $data)
    {
        $collection = collect([]);
        foreach ($data['ingredients'] as $item) {
            $storageDelivery = StorageDelivery::create([
                'date' => $data['date'],
                'provider_id' => $data['provider_id'],
                'cost_price' => $item['price'],
                'count' => $item['count'],
                'one_price' => $item['one'],
                'ingredient_id' => $item['id'],
                'branch_id' => $data['branch_id']
            ]);
            $collection->push($storageDelivery);
        }
        return new StorageDeliveryCollection($collection);
    }

    public function deleteStorageDelivery(int $id)
    {
        $storageDelivery = StorageDelivery::find($id);
        return tap($storageDelivery)->delete();
    }
}
