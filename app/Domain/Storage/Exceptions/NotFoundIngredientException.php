<?php

namespace App\Domain\Storage\Exceptions;

use Exception;
use Illuminate\Http\Response;

class NotFoundIngredientException extends Exception
{
    public function report()
    {
    }

    public function render($request)
    {
        return response([
            'message' => 'Ингредиент не найден на складе',
            'errors' => [$this->getMessage()],
            'type' => class_basename($this),
        ], Response::HTTP_BAD_REQUEST);
    }
}
