<?php

namespace App\Domain\Storage\Exceptions;

use Exception;
use Illuminate\Http\Response;

class NotEnoughException extends Exception
{
    public function report()
    {
    }

    public function render($request)
    {
        return response([
            'message' => 'Не досточное кол-во ингредиента на складе',
            'errors' => [$this->getMessage()],
            'type' => class_basename($this),
        ], Response::HTTP_BAD_REQUEST);
    }
}
