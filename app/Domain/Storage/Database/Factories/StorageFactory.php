<?php

namespace App\Domain\Storage\Database\Factories;

use App\Domain\Storage\Entities\Storage;
use App\Infrastructure\Abstracts\ModelFactory;

class StorageFactory extends ModelFactory
{
    protected string $model = Storage::class;

    public function fields()
    {
        return [];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
