<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorageHistoryTable extends Migration
{
    public function up()
    {
        Schema::create('storage_history', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('provider');
            $table->decimal('cost_price');
            $table->integer('count');
            $table->foreignId('ingredient_id');
            $table->foreign('ingredient_id')
                ->references('id')
                ->on('ingredients')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('storage_history');
    }
}
