<?php

namespace App\Domain\Storage\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Domain\Unit\Entities\Unit;
use App\Domain\Storage\Entities\Storage;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Ingredients\Entities\IngredientCategory;

class StorageTableSeeder extends Seeder
{
    public function run()
    {
        $fileName = storage_path('app/import/import_ingredients_storage.xls');
        $reader = new Xls();

        $spreadsheet = $reader->load($fileName)->getActiveSheet();
        $collectIngredients = collect($spreadsheet->toArray());
        /*
        array:12 [
            0 => null
            1 => "Название"
            2 => "Категория"
            3 => "Ед. измерения"
            4 => "% потерь при очистке"
            5 => "% потерь при варке"
            6 => "% потерь при жарке"
            7 => "% потерь при тушении"
            8 => "% потерь при запекании"
            9 => "Остатки на складах"
            10 => "Себестоимость"
            11 => "Сумма остатков"
            ]
        */
        $collectIngredients->filter(function ($value) {
            return $value[1] !== null && $value[1] !== 'Название';
        })->each(function ($item) {
            $category = IngredientCategory::firstOrCreate([
                'title' => $this->mbUcfirst($item[2])
            ]);
            $unit = Unit::firstOrCreate([
                'title' => substr($item[3], -1) === '.' ? substr($item[3], 0, -1) : $item[3]
            ]);
            $ingredient = Ingredient::create([
                'title' => $this->mbUcfirst($item[1]),
                'category_id' => $category->id,
                'unit_id' => $unit->id
            ]);
            Storage::create([
                'ingredient_id' => $ingredient->id,
                'count' => 0,
                'cost_price' => $item[10]
            ]);
        });
    }

    private function mbUcfirst($str)
    {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }
}
