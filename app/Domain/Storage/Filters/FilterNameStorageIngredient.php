<?php
namespace App\Domain\Storage\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterNameStorageIngredient implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereHas('ingredient', function (Builder $query) use ($value, $property) {
            $query->where($property, 'LIKE', "%{$value}%");
        });
    }
}
