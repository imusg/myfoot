<?php
namespace App\Domain\Storage\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterTypeRemains implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        switch ($value) {
            case 'semis':
                    $query->whereHas('ingredientInSemis');
                break;
            case 'ingredient':
                    $query->doesntHave('ingredientInSemis');
                break;
            case 'dish':
                    $relations = [
                        'ingredientInDish',
                        'dishInDirectory',
                        'dishInCycleData',
                        'cycleDataInOrderPositions',
                        'positionsInKitchens'
                    ];
                    $query->whereHas(implode('.', $relations), function (Builder $query) {
                        $query->doesntHave('kitchenInDelivery');
                    });
                break;
            default:
                break;
        }
    }
}
