<?php

namespace App\Domain\Cycles\Listeners\Observers;

use App\Domain\Cycles\Entities\Cycle;

class CycleObserver
{
    public function created(Cycle $cycle)
    {
    }
}
