<?php

namespace App\Domain\Cycles\Database\Factories;

use App\Domain\Cycles\Entities\Cycle;
use App\Infrastructure\Abstracts\ModelFactory;
use Illuminate\Database\Eloquent\Factories\Factory;

class CycleFactory extends ModelFactory
{
    protected string $model = Cycle::class;

    public function fields()
    {
        return [
            'start' => $this->faker->dateTimeBetween('now', '+1days')->format('Y-m-d'),
            'end' => $this->faker->dateTimeBetween('now', '+28days')->format('Y-m-d'),
            'branch_id' => 1,
            'current' => 1,
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
