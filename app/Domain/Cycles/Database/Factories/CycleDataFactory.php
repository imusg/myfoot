<?php

namespace App\Domain\Cycles\Database\Factories;

use App\Domain\Cycles\Entities\CycleData;
use App\Infrastructure\Abstracts\ModelFactory;

class CycleDataFactory extends ModelFactory
{
    protected string $model = CycleData::class;

    public function fields(): array
    {
        return [
            'day' => $this->faker->numberBetween(1, 7),
            'week' => $this->faker->numberBetween(1, 52),
            'cycle_id' => $this->faker->numberBetween(1, 100),
            'directory_dish_id' => $this->faker->numberBetween(1, 50),
        ];
    }

    public function states()
    {
    }
}
