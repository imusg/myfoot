<?php

namespace App\Domain\Cycles\Database\Seeds;

use App\Domain\Cycles\Entities\Cycle;
use Illuminate\Database\Seeder;

class CycleSeeder extends Seeder
{
    public function run()
    {
        factory(Cycle::class)->create();
    }
}
