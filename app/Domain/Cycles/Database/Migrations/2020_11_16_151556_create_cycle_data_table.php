<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCycleDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cycle_data', function (Blueprint $table) {
            $table->id();
            $table->integer('day')->nullable();
            $table->integer('week')->nullable();
            $table->date('date')->nullable();
            $table->foreignId('cycle_id');
            $table->foreign('cycle_id')->references('id')->on('cycles')->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreignId('directory_dish_id');
            $table->foreign('directory_dish_id')->references('id')->on('directory_dishes')->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cycle_data');
    }
}
