<?php

namespace App\Domain\Category\Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Entities\User;
use App\Domain\Cycles\Entities\Cycle;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Category\Entities\Category;

/*
    [x] Отображение циклов
    [x] Быстрое редактирование цикла
    [x] Удаление цикла
    [ ] Создание цикла с блюдами
    [ ] Создание цикла без блюд
    [ ] Получение конкретного цикла
    [ ] Получение конкретного цикла на выбранный филлиал
    [ ] Получение блюд на конкретный день цикла
    [ ] Добавление блюд на конкретный день цикла
    [ ] Удаление блюд на конкретный день цикла
*/

class CycleControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testGeatAllCycles(): void
    {
        factory(Cycle::class)->create();
        $response = $this->get(route('cycles.index'))
            ->assertSuccessful();
        $data = $response->json('data');
        $equalCount = Cycle::where('excluded', 0)->count();
        $this->assertEquals(count($data), $equalCount);
    }

    public function testUpdateCycle(): void
    {
        $end = '2020-01-02';
        $start = '2020-02-02';
        $current = false;

        $cycle = factory(Cycle::class)->create();
        $this->put(
            route('cycles.update', ['cycle' => $cycle->id]),
            ['current' => $current, 'end' => $end, 'start' => $start]
        )
            ->assertSuccessful();

        $this->assertDatabaseHas(
            'cycles',
            ['id' => $cycle->id, 'current' => $current, 'end' => $end, 'start' => $start]
        );
    }

    public function testDeleteCycle() : void
    {
        $cycle = factory(Cycle::class)->create();

        $this->delete(route('cycles.destroy', ['cycle' => $cycle->id]))
            ->assertSuccessful();

        $this->assertDatabaseMissing('cycles', ['id' => $cycle->id]);
    }

    public function testCreateCycleWithoutDishes() : void
    {
        factory(Branch::class, 1)->create();
        $count = Branch::get()->count() + 1;
        $data = [
            'start' => '2020-01-02',
            'end' => '2020-01-05',
            'current' => true
        ];

        $response = $this->post(route('cycles.store'), $data)->assertSuccessful();
        $this->assertDatabaseHas('cycles', $data);

        $this->assertEquals(Cycle::where('start', $data['start'])->count(), $count);
    }

    // public function testStoreCategory() : void
    // {
    //     $response = $this->post(
    //         route('category.store'),
    //         ['title' => $this->faker->jobTitle]
    //     )
    //         ->assertSuccessful()
    //         ->assertJsonStructure([
    //             'data' => [
    //                 'id',
    //                 'title'
    //             ],
    //             'meta',
    //         ]);
    //     $id = $response->json('data.id');
    //     $this->assertDatabaseHas('categories', ['id' => $id]);
    // }

    // public function testUpdateCategory() : void
    // {
    //     $Category = factory(Category::class)
    //         ->create();

    //     $response = $this->put(route('category.update', $Category->id))
    //         ->assertSuccessful()
    //         ->assertJsonStructure([
    //             'data' => [
    //                 'id',
    //                 'title'
    //             ],
    //             'meta',
    //         ]);

    //     $data = $response->json('data');
    //     $this->assertDatabaseHas('categories', $data);
    // }

    // public function testDestroyCategory() : void
    // {
    //     $Category = factory(Category::class)
    //         ->create();

    //     $this->delete(route('category.destroy', $Category->id))
    //         ->assertSuccessful()
    //         ->assertJsonStructure([
    //             'data' => [
    //                 'deleted',
    //             ],
    //             'meta',
    //         ]);
    //     $this->assertDatabaseMissing('categories', ['id' => $Category->id]);
    // }
}
