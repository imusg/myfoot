<?php
namespace App\Domain\Cycles\Repositories;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\Domain\Cycles\Entities\Cycle;
use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Cycles\Entities\CycleData;
use App\Domain\Addition\Entities\Addition;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Cycles\Contracts\CycleRepository;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Clients\Entities\ClientExcludeIngredient;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentCycleRepository extends EloquentRepository implements CycleRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'end',
        'start',
        'current',
        'excluded',
        'branch_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'id',
        'data.day',
        'end',
        'start',
        'current'
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];

    private array $allowedIncludes = [
        'data',
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;
        return QueryBuilder::for(Cycle::class)
            ->select($this->defaultSelect)
            ->where('excluded', 0)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function findOneByIdWithData(int $id): Collection
    {
        $cyclesIds = Cycle::where('owner_cycle_id', $id)->get()->map(function ($item) {
            return $item->id;
        });

        if (request()->date) {
            if (request()->branch_id) {
                $cycle = Cycle::where('branch_id', request()->branch_id)
                    ->where('current', 1)
                    ->first();
                $data = CycleData::where('cycle_id', $cycle->id)
                    ->where('date', request()->date)
                    ->with(['directory.dish', 'directory.dish.meal', 'directory.branch'])
                    ->get();
                return $data;
            }

            $data = CycleData::whereIn('cycle_id', $cyclesIds)
                    ->where('date', request()->date)
                    ->with(['directory.dish', 'directory.dish.meal', 'directory.branch'])
                    ->get();
            return $data;
        }

        $data = CycleData::whereIn('cycle_id', $cyclesIds)
            ->with(['directory', 'directory.dish', 'directory.dish.meal'])
            ->get()
            ->groupBy('date');
        $keys = $data->keys();
        $resp = collect([]);
        $keys->each(function ($key) use ($data, $resp) {
            $resp->push($data[$key]->map(function ($item) use ($data, $key) {
                $content = '<div class="content-events"><span class="count-food">'.$data[$key]->count().'</span>
                    <i class="el-icon-food"></i>
                    </div>';
                $PCREpattern  =  '/\r\n|\r|\n/u';
                return [
                    'start' => $item->date,
                    'end' => $item->date,
                    'background' => true,
                    'allDay' => true,
                    'class' => 'food-events',
                    'content' => preg_replace($PCREpattern, '', $content)
                ];
            })[0]);
        });
        return $resp;
    }

    public function findOneByQuery(int $id)
    {
        $cyclesIds = Cycle::where('owner_cycle_id', $id)
            ->select('id')
            ->get();
        $data = CycleData::whereIn('cycle_id', $cyclesIds)
            ->with(['directory', 'directory.dish', 'directory.dish.meal'])
            ->get()
            ->groupBy('date');
        $keys = $data->keys();
        $resp = collect([]);
        $keys->each(function ($key) use ($data, $resp) {
            $resp->push($data[$key]->map(function ($item) use ($data, $key) {
                $carbon = Carbon::parse($item->date);
                $dataCycle = collect($data[$key])->map(function ($cycle) {
                    return [
                        'price' => $cycle->directory->price,
                        'title' => $cycle->directory->dish->title,
                        'meal' => $cycle->directory->dish->meal ? $cycle->directory->dish->meal->title : 'Не указано',
                    ];
                });
                $dayName = mb_convert_case($carbon->dayName, MB_CASE_TITLE, "UTF-8");
                return [
                    'dayName' => "{$dayName} ({$carbon->format('d.m.Y')})",
                    'date' => $item->date,
                    'count' => $data[$key]->count(),
                    'cycleData' => $dataCycle
                ];
            })[0]);
        });
        return $resp;
    }

    public function findMenu()
    {
        $branch_id = request()->user()->branch_id;
        $client = request()->client;
        $clientModel = Client::find(request()->client);

        $excludedIngredient = ClientExcludeIngredient::where('client_id', $client)->with('ingredient')->get();
        $cycle = Cycle::where('branch_id', null)
            ->where('owner_cycle_id', null)
            ->get()->map(function ($item) {
                return $item->id;
            });


        $cyclesIds = Cycle::whereIn('owner_cycle_id', $cycle)
            ->where('branch_id', $clientModel->branch_id)
            ->get()
            ->map(function ($item) {
                return $item->id;
            });

        if (!$cycle) {
            return null;
        }

        $cycleData = CycleData::query();

        $cycleData->when($clientModel->branch_id, function ($query) use ($cyclesIds) {
            return $query->whereIn('cycle_id', $cyclesIds);
        });

        $cycleData->where('date', request()->date)
            ->with(['directory.dish', 'directory.dish.meal', 'directory.dish.ingredients']);

        $menu = $cycleData->get();

        $menu = $menu->map(function ($item) use ($excludedIngredient) {
            $diff = collect([]);
            $excludedIngredient->each(function ($value) use ($item, $diff) {
                $key = $item->directory->dish->ingredients->search(function ($v, $key) use ($value) {
                    return $v->id === $value->ingredient_id;
                });
                if ($key) {
                    $diff->push($item->directory->dish->ingredients[$key]->id);
                }
            });
            if ($diff->isNotEmpty()) {
                $ingredients = $item->directory->dish->ingredients->map(function ($item) use ($diff) {
                    $ingredients = collect([]);
                    $diff->each(function ($value) use ($item, $ingredients) {
                        if ($value === $item->id) {
                            $ingredients->push([
                                'id' => $item->id,
                                'title' => $item->title,
                                'exclude' => true
                            ]);
                        } else {
                            $ingredients->push([
                                'id' => $item->id,
                                'title' => $item->title,
                                'exclude' => false
                            ]);
                        }
                    });
                    return $ingredients[0];
                });
            } else {
                $ingredients = $item->directory->dish->ingredients->map(function ($item) {
                    return [
                        'id' => $item->id,
                        'title' => $item->title,
                        'exclude' => false
                    ];
                });
            }
            return [
                'title' => $item->directory->dish->title,
                'branch_id' => $item->directory->branch_id,
                'date' => $item->date,
                'cycleDataId' => $item->id,
                'directoryId' => $item->directory->id,
                'dishId' => $item->directory->dish->id,
                'image' => $item->directory->dish->image,
                'meal' => $item->directory->dish->meal ? $item->directory->dish->meal->title : 'не указано',
                'mealId' => $item->directory->dish->meal ? $item->directory->dish->meal->id : null,
                'price' => $item->directory->price,
                'isSelected' => false,
                'ingredients' => $ingredients
            ];
        })->where('branch_id', $clientModel->branch_id)->values()->all();
        $additions = Addition::where('branch_id', $clientModel->branch_id)->get()->map(function ($addition) {
            return [
                'title' => $addition->title,
                'price' => $addition->price,
                'id' => $addition->id,
                'isSelected' => false
            ];
        });

        return ['date' => request()->date, 'sale' => null, 'positions' => $menu, 'additions' => $additions];
    }

    public function store(array $data): Model
    {
        $data['branch_id'] = request()->user()->branch_id;
        $cycle = parent::store($data);
        $branches = Branch::all();
        foreach ($branches as $branch) {
            $newCycle = $cycle->replicate()->fill([
                'owner_cycle_id' => $cycle->id,
                'current' => true,
                'branch_id' => $branch->id,
                'excluded' => true,
            ]);
            $newCycle->save();
            if (isset($data['cycles'])) {
                foreach ($data['cycles'] as $key => $item) {
                    $newCycle->data()->firstOrCreate([
                            'date' => $item['date'],
                            'directory_dish_id' => $item['id'],
                        ]);
                }
            }
        }
        if (isset($data['cycles'])) {
            foreach ($data['cycles'] as $key => $item) {
                $cycle->data()->create([
                    'day' => Carbon::make($item['date'])->dayOfWeek,
                    'date' => $item['date'],
                    'directory_dish_id' => $item['id'],
                ]);
            }
        }
        return $cycle;
    }

    public function getChildrenCycle($cylcleId)
    {
        return Cycle::with('branch')->where('owner_cycle_id', $cylcleId)->get();
    }

    public function update(Model $model, array $data): Model
    {
        $cycle = parent::update($model, $data);
        if (isset($data['cycle'])) {
            $this->updateCyclesWithData($cycle, $data['cycles']);
        }
        return $cycle;
    }

    public function updateCyclesWithData($model, $cycles)
    {
        $start = Carbon::make($model->start);
        $end = Carbon::make($model->end);
        $diff = $end->diffInWeeks($start);
        foreach ($cycles as $key => $item) {
            for ($i = 0; $i < $diff; $i++) {
                foreach ($item['days'] as $day) {
                    $model->data()->where('day', $day)->update([
                        'day' => $day,
                        'week' => $start->week + $i,
                        'directory_dish_id' => $item['directory_id'],
                    ]);
                }
            }
        }
    }

    public function destroy(Model $model): bool
    {
        try {
            $model->data()->delete();
            Cycle::where('owner_cycle_id', $model->id)->delete();
            return parent::destroy($model);
        } catch (Exception $e) {
            throw new Exception('В этом цикле есть заказы');
        }

    }

    public function storeDataWithCycle(array $data): Model
    {
        $dish = DirectoryDish::find($data['directory_dish_id']);
        $branch_id = $dish->branch->id;
        $cycle = Cycle::where('owner_cycle_id', $data['cycle_id'])
            ->where('branch_id', $branch_id)->first();
        $data['cycle_id'] = $cycle->id;
        $data = CycleData::create($data);
        return $data;
    }

    public function updateDataWithCycle(array $data, $id)
    {
        $data = tap(CycleData::find($id))->update($data);
        return $data;
    }

    public function deleteDataWithCycle($id)
    {
        return tap(CycleData::find($id))->delete();
    }

    public function findCurrentCycle()
    {
        $branch = !auth()->user()->branch_id ? null : auth()->user()->branch_id;
        $cycles =  Cycle::where([
            ['branch_id', '=', $branch],
            ['current', '=', 1]
        ])->get();
        $starts = $cycles->map(function ($cycle) {
            return $cycle->start;
        });
        $ends = $cycles->map(function ($cycle) {
            return $cycle->end;
        });

        $ids = $cycles->map(function ($cycle) {
            return $cycle->id;
        });

        return [
            'start' => $starts->first(),
            'end' => $ends->last(),
            'id' => $ids->sort()->first()
        ];
    }

    public function copy($id, $date)
    {
        $start = Carbon::make($date[0]);
        $end = Carbon::make($date[1]);
        $diff = $start->diffInDays($end);
        $dates = [];
        for ($i=0; $i <= $diff; $i++) {
            array_push($dates, Carbon::make($date[0])->addDays($i)->format('Y-m-d'));
        }
        $c = Cycle::find($id);
        $newC = $c->replicate()->fill(['start' => $date[0], 'end' => $date[1]]);
        $newC->push();
        $cycles = Cycle::where('owner_cycle_id', $id)->get();

        $cycles->each(function ($item) use ($newC, $date, $dates) {
            $cycle = Cycle::find($item->id)
                ->replicate()
                ->fill([
                        'owner_cycle_id' => $newC->id,
                        'start' => $date[0],
                        'end' => $date[1]
                    ]);
            $cycle->push();

            $dates2 = collect($item->data)->map(function ($item) {
                return $item->date;
            })->unique()->sort()->values();

            collect($item->data)->each(function ($value, $index) use ($cycle, $dates, $dates2) {
                $cycleData = CycleData::find($value->id);
                $index = $dates2->search($cycleData->date);
                $newCy = $cycleData
                        ->replicate()
                        ->fill(['cycle_id' => $cycle->id, 'date' => $dates[$index]]);
                $newCy->push();
            });
        });
    }
}
