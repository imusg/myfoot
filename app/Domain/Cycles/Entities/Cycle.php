<?php

namespace App\Domain\Cycles\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Branches\Entities\Branch;

class Cycle extends Model
{
    protected $fillable = ['end', 'start', 'owner_cycle_id', 'current', 'branch_id', 'excluded'];

    public function data()
    {
        return $this->hasMany(CycleData::class, 'cycle_id', 'id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    public function scopeCurrent($query)
    {
        $branch_id = request()->user()->branch_id;
        return $query
            ->where('branch_id', $branch_id)
            ->where('current', 1);
    }

    /* Local scope */

    public function getIdParentsCycle()
    {
        return $this->where([
            ['branch_id', '=', null],
            ['owner_cycle_id', '=', null],
        ])
            ->select(['id'])
            ->get();
    }

    public function getCycleByBranchId($branchId)
    {
        return $this->where([
            ['current', '=', 1],
            ['branch_id', '=', $branchId],
        ]);
    }
}
