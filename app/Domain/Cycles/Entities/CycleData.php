<?php
namespace App\Domain\Cycles\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Orders\Entities\OrderPosition;
use App\Domain\Dishies\Entities\DirectoryDish;

class CycleData extends Model
{
    protected $fillable = [
        'week',
        'month',
        'day',
        'date',
        'year',
        'cycle_id',
        'directory_dish_id',
    ];

    protected $table = 'cycle_data';

    public function directory()
    {
        return $this->hasOne(DirectoryDish::class, 'id', 'directory_dish_id');
    }

    public function cycleDataInOrderPositions()
    {
        return $this->hasMany(OrderPosition::class, 'cycle_data_id', 'id');
    }

    public function scopeDate($query, $data)
    {
        return $query
            ->where('date', $data['date'])
            ->where('cycle_id', $data['cycle_id']);
    }
}
