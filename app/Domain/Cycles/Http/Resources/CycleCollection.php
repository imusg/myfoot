<?php


namespace App\Domain\Cycles\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CycleCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
           'data' => CycleResource::collection($this->collection)
       ];
    }
}
