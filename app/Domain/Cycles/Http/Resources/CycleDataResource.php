<?php

namespace App\Domain\Cycles\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Dishies\Http\Resources\DirectoryResource;

class CycleDataResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'week' => $this->week,
            'directory' => DirectoryResource::make($this->directory),
            'isSelected' => false
        ];
    }
}
