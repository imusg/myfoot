<?php


namespace App\Domain\Cycles\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CycleDataCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
           'data' => CycleDataResource::collection($this->collection)
       ];
    }
}
