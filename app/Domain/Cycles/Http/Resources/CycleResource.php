<?php


namespace App\Domain\Cycles\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CycleResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'start' => $this->start,
           'end' => $this->end,
           'current' => $this->current,
           'excluded' => $this->excluded,
           'branch_id' => $this->branch_id,
           'data' => CycleDataResource::collection($this->whenLoaded('data'))
       ];
    }
}
