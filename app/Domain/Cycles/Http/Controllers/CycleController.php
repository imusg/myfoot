<?php

namespace App\Domain\Cycles\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Cycles\Contracts\CycleRepository;
use App\Domain\Cycles\Http\Resources\CycleResource;
use App\Domain\Cycles\Http\Resources\CycleCollection;
use App\Domain\Cycles\Http\Requests\CycleStoreRequest;
use App\Domain\Cycles\Http\Requests\CycleUpdateRequest;
use App\Domain\Cycles\Http\Resources\CycleDataResource;
use App\Domain\Cycles\Http\Requests\CycleDataStoreRequest;

class CycleController extends Controller
{
    private CycleRepository $cycleRepository;

    public function __construct(CycleRepository $cycleRepository)
    {
        $this->cycleRepository = $cycleRepository;
        $this->resourceItem = CycleResource::class;
        $this->resourceCollection = CycleCollection::class;
    }

    public function index(): CycleCollection
    {
        $collection = $this->cycleRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function getCurrentCycle()
    {
        $response = $this->cycleRepository->findCurrentCycle();
        if ($response) {
            return $this->respondWithCustomData($response);
        }
        return $this->respondWithNoContent();
    }

    public function show(int $id): JsonResponse
    {
        $type = request()->query('type');
        if ($type === 'table') {
            $response = $this->cycleRepository->findOneByQuery($id);
            return $this->respondWithCustomData($response);
        }
        $response = $this->cycleRepository->findOneByIdWithData($id);
        return $this->respondWithCustomData($response);
    }

    public function store(CycleStoreRequest $request): JsonResponse
    {
        $response = $this->cycleRepository->store($request->validated());
        return $this->respondWithCustomData($response, Response::HTTP_CREATED);
    }

    public function update(CycleUpdateRequest $request, int $id): CycleResource
    {
        $data = $request->validated();
        $response = $this->cycleRepository->update(
            $this->cycleRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy($id): JsonResponse
    {
        $response = $this->cycleRepository->destroy($this->cycleRepository->findOneById($id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function storeDataWithCycle(CycleDataStoreRequest $request): CycleDataResource
    {
        $data = $request->validated();
        $response = $this->cycleRepository->storeDataWithCycle($data);
        return CycleDataResource::make($response);
    }

    public function updateDataWithCycle(CycleDataStoreRequest $request, $id): CycleDataResource
    {
        $data = $request->validated();
        $response = $this->cycleRepository->updateDataWithCycle($data, $id);
        return CycleDataResource::make($response);
    }

    public function deleteDataWithCycle($id): JsonResponse
    {
        $response = $this->cycleRepository->deleteDataWithCycle($id);
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function getCycleMenu()
    {
        $response = $this->cycleRepository->findMenu();
        if (!$response) {
            return $this->respondWithNoContent();
        }
        return $this->respondWithCustomData($response, Response::HTTP_OK);
    }

    public function childrenCycle()
    {
        try {
            $collection = $this->cycleRepository->getChildrenCycle(request()->cycleId);
            return $this->respondWithCustomData($collection);
        } catch (\Throwable $th) {
            return $this->respondWithCustomData($th->getMessage());
        }
    }

    public function copy(Request $request)
    {
        $id = $request->id;
        $date = $request->date;
        return $this->respondWithCustomData($this->cycleRepository->copy($id, $date));
    }
}
