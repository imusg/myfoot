<?php

namespace App\Domain\Cycles\Http\Controllers;

use App\Domain\Cycles\Contracts\CycleRepository;
use App\Domain\Cycles\Http\Requests\CycleStoreRequest;
use App\Domain\Cycles\Http\Resources\CycleCollection;
use App\Domain\Cycles\Http\Resources\CycleResource;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CycleDataController extends Controller
{
    private CycleRepository $cycleRepository;

    public function __construct(CycleRepository $cycleRepository)
    {
        $this->cycleRepository = $cycleRepository;
        $this->resourceItem = CycleResource::class;
        $this->resourceCollection = CycleCollection::class;
    }

    public function index()
    {
        $collection = $this->cycleRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function store(CycleStoreRequest $request)
    {
        $response = $this->cycleRepository->store($request->validated());
        return $this->respondWithCustomData($response, Response::HTTP_CREATED);
    }

    public function update(CycleStoreRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->cycleRepository->update(
            $this->cycleRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy($id)
    {
        $response = $this->cycleRepository->destroy($this->cycleRepository->findOneById($id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }
}
