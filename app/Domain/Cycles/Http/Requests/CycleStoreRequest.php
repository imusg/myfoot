<?php


namespace App\Domain\Cycles\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CycleStoreRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'start' => 'required|string',
            'end' => 'required|string',
            'current' => 'boolean',
            'cycles' => 'array'
        ];
    }
}
