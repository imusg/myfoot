<?php


namespace App\Domain\Cycles\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CycleDataStoreRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'day' => 'integer',
            'week' => 'integer',
            'date' => 'required',
            'cycle_id' => 'required|exists:cycles,id',
            'directory_dish_id' => 'required|exists:directory_dishes,id'
        ];
    }
}
