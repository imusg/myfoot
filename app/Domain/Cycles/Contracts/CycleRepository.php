<?php

namespace App\Domain\Cycles\Contracts;

use App\Infrastructure\Contracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface CycleRepository extends BaseRepository
{
}
