<?php
namespace App\Domain\Cycles\Providers;

use App\Domain\Cycles\Database\Factories\CycleDataFactory;
use App\Domain\Cycles\Database\Factories\CycleFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'cycles';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class,
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        CycleDataFactory::class,
        CycleFactory::class
    ];
}
