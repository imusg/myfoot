<?php

namespace App\Domain\Cycles\Providers;

use App\Domain\Cycles\Http\Controllers\CycleController;
use App\Domain\Cycles\Http\Controllers\CycleDataController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->apiResource('cycles', CycleController::class)
            ->only(['index', 'store', 'update', 'show', 'destroy'])
            ->names('cycles');

        $router->post('cycle/data', [CycleController::class, 'storeDataWithCycle']);

        $router->get('cycle/children', [CycleController::class, 'childrenCycle']);

        $router->put('cycle/data/{id}', [CycleController::class, 'updateDataWithCycle']);

        $router->delete('cycle/data/{id}', [CycleController::class, 'deleteDataWithCycle']);

        $router->get('menu', [CycleController::class, 'getCycleMenu']);

        $router->get('cycle/current', [CycleController::class, 'getCurrentCycle']);

        $router->post('cycles/copy', [CycleController::class, 'copy']);
    }
}
