<?php


namespace App\Domain\Cycles\Providers;

use App\Domain\Cycles\Contracts\CycleRepository;
use App\Domain\Cycles\Entities\Cycle;
use App\Domain\Cycles\Repositories\EloquentCycleRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(CycleRepository::class, function () {
            return new EloquentCycleRepository(new Cycle());
        });
    }

    public function provides(): array
    {
        return [
            CycleRepository::class,
        ];
    }
}
