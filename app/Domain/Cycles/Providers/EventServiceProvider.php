<?php

namespace App\Domain\Cycles\Providers;

use App\Domain\Cycles\Entities\Cycle;
use App\Domain\Cycles\Listeners\Observers\CycleObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        Cycle::observe(CycleObserver::class);
    }
}
