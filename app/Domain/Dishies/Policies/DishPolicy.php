<?php

namespace App\Domain\Dishes\Policies;

use App\Domain\Users\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DishPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('view any dishes');
    }

    public function create(User $user)
    {
        return $user->can('create dishes');
    }
}

