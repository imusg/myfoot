<?php

namespace App\Domain\Dishies\Providers;

use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Dishies\Listeners\Observers\DishIngredientObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        DishIngredient::observe(DishIngredientObserver::class);
    }
}
