<?php


namespace App\Domain\Dishies\Providers;

use App\Domain\Dishies\Entities\Dish;
use Illuminate\Support\ServiceProvider;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Dishies\Contracts\DishRepository;
use App\Domain\Dishies\Entities\DishIngredients;
use App\Domain\Dishies\Contracts\DirectoryRepository;
use App\Domain\Dishies\Contracts\DishIngredientRepository;
use App\Domain\Dishies\Repositories\EloquentDishRepository;
use App\Domain\Dishies\Repositories\EloquentDirectoryRepository;
use App\Domain\Dishies\Repositories\EloquentDishIngredientRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(DishRepository::class, function () {
            return new EloquentDishRepository(new Dish());
        });
        $this->app->singleton(DirectoryRepository::class, function () {
            return new EloquentDirectoryRepository(new DirectoryDish());
        });
        $this->app->singleton(DishIngredientRepository::class, function () {
            return new EloquentDishIngredientRepository(new DishIngredient());
        });
    }

    public function provides(): array
    {
        return [
            DishRepository::class,
            DirectoryRepository::class,
            DishIngredientRepository::class
        ];
    }
}
