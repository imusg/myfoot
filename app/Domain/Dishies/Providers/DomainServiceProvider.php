<?php
namespace App\Domain\Dishies\Providers;

use App\Domain\Dishes\Policies\DishPolicy;
use App\Domain\Dishies\Database\Factories\DirectoryDishFactory;
use App\Domain\Dishies\Database\Factories\DishFactory;
use App\Domain\Dishies\Database\Factories\DishIngredientsFactory;
use App\Domain\Dishies\Entities\Dish;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'dishes';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class,
    ];

    protected array $policies = [
        Dish::class => DishPolicy::class
    ];

    protected array $factories = [
        DirectoryDishFactory::class,
        DishFactory::class,
        DishIngredientsFactory::class
    ];
}
