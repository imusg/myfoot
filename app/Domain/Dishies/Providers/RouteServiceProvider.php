<?php

namespace App\Domain\Dishies\Providers;

use App\Domain\Dishies\Http\Controllers\DirectoryDishController;
use App\Domain\Dishies\Http\Controllers\DishesController;
use App\Domain\Dishies\Http\Controllers\DishIngredientController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->get('test', [DishesController::class, 'test']);
        $router->get('cost/calculation', [DirectoryDishController::class, 'calculationCostPrice']);

        $router->apiResource('dishes', DishesController::class)
            ->only(['index','store','update', 'show', 'destroy'])
            ->names('dish');

        $router->post('dishes/copy', [DishesController::class, 'copy']);

        $router->post('dishes/{dish}/ingredient', [DishIngredientController::class, 'store'])
            ->name('dishes.store.ingredient');

        $router->post('upload', [DishesController::class, 'upload']);

        $router->put('dishes/{dish}/ingredient/{ingredient}', [DishIngredientController::class, 'update'])
            ->name('dishes.update.ingredient');

        $router->delete('dishes/{dish}/ingredient/{ingredient}', [DishIngredientController::class, 'destroy'])
            ->name('dishes.destroy.ingredient');

        $router->apiResource('directory', DirectoryDishController::class)
            ->names('directory');

        $router->get('dishes/{id}/file', [DishesController::class, 'generateFile']);
    }

}
