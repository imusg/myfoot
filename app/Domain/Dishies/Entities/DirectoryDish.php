<?php

namespace App\Domain\Dishies\Entities;

use OwenIt\Auditing\Auditable;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Cycles\Entities\Cycle;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Cycles\Entities\CycleData;
use App\Domain\Orders\Entities\OrderPosition;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class DirectoryDish extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'price',
        'price_custom',
        'dish_id',
        'branch_id',
        'meal_id',
    ];

    public function dish()
    {
        return $this->hasOne(Dish::class, 'id', 'dish_id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    public function meal()
    {
        return $this->hasOne(Meal::class, 'id', 'meal_id');
    }

    public function dishInCycleData()
    {
        return $this->hasMany(CycleData::class, 'directory_dish_id', 'id');
    }

    public function scopeMeals($query, $value)
    {
        return $query->where('meal_id', $value);
    }
}
