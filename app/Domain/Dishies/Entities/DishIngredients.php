<?php

namespace App\Domain\Dishies\Entities;

use App\Domain\Ingredients\Entities\Ingredient;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Dishies\Entities\DishIngredients
 *
 * @property int $id
 * @property int $dish_id
 * @property int $ingredient_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Ingredient|null $ingredient
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients query()
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients whereIngredientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishIngredients whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DishIngredient extends Model
{
    protected $fillable = ['dish_id', 'ingredient_id',
        'count', 'losses_cleaning_checked',
        'losses_frying_checked', 'losses_cooking_checked',
        'losses_baking_checked',
        'losses_stew_checked'
    ];

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }
}
