<?php

namespace App\Domain\Dishies\Entities;

use App\Domain\Meal\Entities\Meal;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Category\Entities\Category;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * App\Domain\Dishies\Entities\Dish
 *
 * @OA\Schema (
 *    schema="Dish",
 *    type="object",
 *    @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Название блюда",
 *      example="Пюре с котлетой",
 *    ),
 *     @OA\Property (
 *      property="barcode",
 *      type="integer",
 *      description="Штрихкод",
 *      example="4703",
 *    ),
 *    @OA\Property (
 *      property="workshop",
 *      type="string",
 *      description="Цех",
 *      example="Цех 1",
 *    ),
 *    @OA\Property (
 *      property="tax",
 *      type="integer",
 *      description="налог",
 *      example="40",
 *    ),
 *    @OA\Property (
 *      property="image",
 *      type="string",
 *      description="Фотография блюда",
 *      example="https://via.placeholder.com/640x480.png/003311?text=aperiam",
 *    ),
 *    @OA\Property (
 *      property="color",
 *      type="string",
 *      description="Цвет блюда",
 *      example="Военно-воздушный синий",
 *    ),
 *    @OA\Property (
 *      property="cooking_process",
 *      type="string",
 *      description="Процесс приготовления блюда",
 *      example="Процесс приготовления блюда",
 *    ),
 *   @OA\Property (
 *      property="cooking_time",
 *      type="string",
 *      description="Процесс приготовления блюда",
 *      example="00:42:00",
 *    ),
 *    @OA\Property (
 *      property="category",
 *      ref="#/components/schemas/Category"
 *    ),
 *    @OA\Property (
 *      property="ingredients",
 *      type="array",
 *      @OA\Items(
 *          type="object",
 *          ref="#/components/schemas/Ingredient"
 *      )
 *    )
 * )
 * @property int $id
 * @property string $title
 * @property string $barcode
 * @property string $workshop
 * @property string $tax
 * @property string $image
 * @property string $color
 * @property string $cooking_process
 * @property string $cooking_time
 * @property int $category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|Ingredient[] $ingredients
 * @property-read int|null $ingredients_count
 * @method static \Illuminate\Database\Eloquent\Builder|Dish newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dish newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dish query()
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCookingProcess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCookingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereWorkshop($value)
 * @mixin \Eloquent
 */
class Dish extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'title',
        'barcode',
        'workshop',
        'tax',
        'image',
        'calories',
        'proteins',
        'fats',
        'carbohydrates',
        'color',
        'netto', 'brutto',
        'cooking_process',
        'cooking_time',
        'category_id',
        'meal_id'
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function ingredients()
    {
        return $this->belongsToMany(
            Ingredient::class,
            'dish_ingredients',
            'dish_id',
            'ingredient_id'
        )->withPivot(['netto', 'brutto']);
    }

    public function semis()
    {
        return $this->belongsToMany(
            Semifinished::class,
            'dish_ingredients',
            'dish_id',
            'semis_id'
        );
    }
    public function meal()
    {
        return $this->hasOne(Meal::class, 'id', 'meal_id');
    }

    public function counts()
    {
        return $this->hasMany(DishIngredient::class, 'dish_id', 'id');
    }
}
