<?php

namespace App\Domain\Dishies\Entities;

use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Ingredients\Entities\Ingredient;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class DishIngredient extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'dish_id',
        'ingredient_id',
        'count',
        'losses_cleaning_checked',
        'losses_frying_checked',
        'losses_cooking_checked',
        'losses_baking_checked',
        'losses_stew_checked',
        'brutto',
        'netto',
        'semis_id'
    ];

    public function dish()
    {
        return $this->hasOne(Dish::class, 'id', 'dish_id');
    }

    public function dishInDirectory()
    {
        return $this->hasOne(DirectoryDish::class, 'dish_id', 'dish_id');
    }

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function semis()
    {
        return $this->hasOne(Semifinished::class, 'id', 'semis_id');
    }
}
