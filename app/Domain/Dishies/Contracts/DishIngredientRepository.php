<?php


namespace App\Domain\Dishies\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface DishIngredientRepository extends BaseRepository
{
}
