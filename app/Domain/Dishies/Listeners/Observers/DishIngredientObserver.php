<?php

namespace App\Domain\Dishies\Listeners\Observers;

use App\Domain\Dishies\Entities\Dish;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Exceptions\NotEnoughException;
use App\Domain\Storage\Exceptions\NotFoundIngredientException;

class DishIngredientObserver
{
    public function updating(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $netto = $dishIngredient->netto;
            $brutto = $dishIngredient->brutto;
            $ingredient = Ingredient::find($dishIngredient->ingredient_id);
            $body = request()->all();
            foreach ($body as $nameMethod => $valueMethod) {
                switch ($nameMethod) {
                    case 'losses_cleaning_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_cleaning / 100));
                            $dishIngredient->dish()->update([
                                'netto' => $dishIngredient->dish->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_cleaning / 100));
                        $dishIngredient->dish()->update([
                                'netto' => $dishIngredient->dish->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);
                        break;
                    case 'losses_frying_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_frying / 100));
                            $dishIngredient->dish()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_frying / 100));
                        $dishIngredient->dish()->update([
                                'netto' => $dishIngredient->dish->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);

                        break;
                    case 'losses_cooking_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_cooking / 100));
                            $dishIngredient->dish()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_cooking / 100));
                        $dishIngredient->dish()->update([
                                'netto' => $dishIngredient->dish->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);

                        break;
                    case 'losses_baking_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_baking / 100));
                            $dishIngredient->dish()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_baking / 100));
                        $dishIngredient->dish()->update([
                                'netto' => $dishIngredient->dish->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);

                        break;
                    case 'losses_stew_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_stew / 100));
                            $dishIngredient->dish()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_stew / 100));
                        $dishIngredient->dish()->update([
                                'netto' => $dishIngredient->dish->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);
                        break;
                    default:
                        return null;
                }
            }
        }
        if ($dishIngredient->semis_id) {
            $netto = $dishIngredient->netto;
            $brutto = $dishIngredient->brutto;
            $ingredient = Semifinished::find($dishIngredient->semis_id);
            $body = request()->all();
            foreach ($body as $nameMethod => $valueMethod) {
                switch ($nameMethod) {
                    case 'losses_cleaning_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_cleaning / 100));
                            $dishIngredient->semis()->update([
                                'netto' => $dishIngredient->semis->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_cleaning / 100));
                        $dishIngredient->semis()->update([
                                'netto' => $dishIngredient->semis->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);
                        break;
                    case 'losses_frying_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_frying / 100));
                            $dishIngredient->semis()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_frying / 100));
                        $dishIngredient->semis()->update([
                                'netto' => $dishIngredient->semis->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);

                        break;
                    case 'losses_cooking_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_cooking / 100));
                            $dishIngredient->semis()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_cooking / 100));
                        $dishIngredient->semis()->update([
                                'netto' => $dishIngredient->semis->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);

                        break;
                    case 'losses_baking_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_baking / 100));
                            $dishIngredient->semis()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_baking / 100));
                        $dishIngredient->semis()->update([
                                'netto' => $dishIngredient->semis->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);

                        break;
                    case 'losses_stew_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_stew / 100));
                            $dishIngredient->semis()->update([
                                'netto' =>  $dishIngredient->netto  - $dishIngredient->getOriginal('netto') + $dishIngredient->dish->netto
                            ]);
                            return;
                        }
                        $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_stew / 100));
                        $dishIngredient->semis()->update([
                                'netto' => $dishIngredient->semis->netto - $dishIngredient->getOriginal('netto') + $dishIngredient->netto
                        ]);
                        break;
                    default:
                        return null;
                }
            }
        }
    }

    public function creating(DishIngredient $dishIngredient)
    {
        $dishIngredient->netto = $dishIngredient->netto / 1000;
        $dishIngredient->brutto = $dishIngredient->brutto / 1000;
    }

    public function created(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $dish = Dish::find($dishIngredient->dish_id);
            $netto = $dishIngredient->netto * 1000;

            [
                'calories' => $calories,
                'proteins' => $proteins,
                'fats' => $fats,
                'carbohydrates' => $carbohydrates,
            ] = $this->countingKBZU($dishIngredient->ingredient, $netto);

            $dish->update([
                'calories' => $dish->calories + $calories,
                'proteins' => $dish->proteins + $proteins,
                'fats' => $dish->fats + $fats,
                'carbohydrates' => $dish->carbohydrates + $carbohydrates,
                'netto' => $dish->netto - ($dishIngredient->getOriginal('netto') / 1000) + ($netto / 1000)
            ]);
        }
        if ($dishIngredient->semis_id) {
            $dish = Dish::find($dishIngredient->dish_id);
            $netto = $dishIngredient->netto * 1000;

            [
                'calories' => $calories,
                'proteins' => $proteins,
                'fats' => $fats,
                'carbohydrates' => $carbohydrates,
            ] = $this->countingKBZU($dishIngredient->semis, $netto);

            $dishIngredient->dish()->update([
                'calories' => $dishIngredient->dish->calories + $calories,
                'proteins' => $dishIngredient->dish->proteins + $proteins,
                'fats' => $dishIngredient->dish->fats + $fats,
                'carbohydrates' => $dishIngredient->dish->carbohydrates + $carbohydrates,
                'netto' => $dishIngredient->dish->netto - ($dishIngredient->getOriginal('netto') / 1000) + ($netto / 1000)
            ]);
        }
    }

    private function countingKBZU($ingredient, $netto): array
    {
        $oneCalories = $ingredient->calories / 100; // кол-во калорий в 1 грамме
        $oneProteins = $ingredient->proteins / 100; // кол-во белка в 1 грамме
        $oneFats = $ingredient->fats / 100; // кол-во жиров в 1 грамме
        $oneСarbohydrates = $ingredient->carbohydrates / 100; // кол-во углеводов в 1 грамме

        return [
            'calories' => $oneCalories * $netto,
            'proteins' => $oneProteins  * $netto,
            'fats' => $oneFats  * $netto,
            'carbohydrates' => $oneСarbohydrates  * $netto
        ];
    }

    public function updated(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            if ($dishIngredient->isDirty('netto')) {
                $netto = $dishIngredient->netto * 1000;
                $initNetto = $dishIngredient->getOriginal('netto') * 1000;

                [
                    'calories' => $calories,
                    'proteins' => $proteins,
                    'fats' => $fats,
                    'carbohydrates' => $carbohydrates,
                ] = $this->countingKBZU($dishIngredient->ingredient, $netto);

                [
                    'calories' => $initCalories,
                    'proteins' => $initProteins,
                    'fats' => $initFats,
                    'carbohydrates' => $initCarbohydrates,
                ] = $this->countingKBZU($dishIngredient->ingredient, $initNetto);

                $dishIngredient->dish()->update([
                    'calories' => $dishIngredient->dish->calories - $initCalories + $calories,
                    'proteins' => $dishIngredient->dish->proteins - $initProteins + $proteins,
                    'fats' => $dishIngredient->dish->fats - $initFats + $fats,
                    'carbohydrates' => $dishIngredient->dish->carbohydrates - $initCarbohydrates + $carbohydrates,
                    'netto' =>  $dishIngredient->dish->netto - ($initNetto / 1000) + ($netto / 1000)
                ]);
                $directoryDish = DirectoryDish::where('dish_id', $dishIngredient->dish_id)
                        ->get();
                $directoryDish->each(function ($item) {
                    $prices = collect([]);
                    DishIngredient::where('dish_id', $item->dish_id)->get()
                    ->each(function ($value) use ($prices, $item) {
                        $storageIngredient = Storage::where('ingredient_id', $value->ingredient_id)
                                    ->where('branch_id', $item->branch_id)
                                    ->first();
                        if ($storageIngredient) {
                            $price = $storageIngredient->cost_price * $value->netto;
                            $prices->push($price);
                        }
                    });

                    if ($prices->count() > 0) {
                        $item->update(['price_custom' => $prices->sum()]);
                    }
                });
            }
        }
        if ($dishIngredient->semis_id) {
            if ($dishIngredient->isDirty('netto')) {
                $netto = $dishIngredient->netto * 1000;
                $initNetto = $dishIngredient->getOriginal('netto') * 1000;

                [
                    'calories' => $calories,
                    'proteins' => $proteins,
                    'fats' => $fats,
                    'carbohydrates' => $carbohydrates,
                ] = $this->countingKBZU($dishIngredient->semis, $netto);

                [
                    'calories' => $initCalories,
                    'proteins' => $initProteins,
                    'fats' => $initFats,
                    'carbohydrates' => $initCarbohydrates,
                ] = $this->countingKBZU($dishIngredient->semis, $initNetto);

                $dishIngredient->dish()->update([
                    'calories' => $dishIngredient->dish->calories - $initCalories + $calories,
                    'proteins' => $dishIngredient->dish->proteins - $initProteins + $proteins,
                    'fats' => $dishIngredient->dish->fats - $initFats + $fats,
                    'carbohydrates' => $dishIngredient->dish->carbohydrates - $initCarbohydrates + $carbohydrates,
                    'netto' =>  $dishIngredient->dish->netto - ($initNetto / 1000) + ($netto / 1000)
                ]);
            }
        }
    }

    public function deleted(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $netto = $dishIngredient->netto * 1000;

            [
                'calories' => $calories,
                'proteins' => $proteins,
                'fats' => $fats,
                'carbohydrates' => $carbohydrates,
            ] = $this->countingKBZU($dishIngredient->ingredient, $netto);


            $dishIngredient->dish()->update([
                'calories' => $dishIngredient->dish->calories - $calories < 0 ? 0 : $dishIngredient->dish->calories - $calories,
                'proteins' => $dishIngredient->dish->proteins - $proteins < 0 ? 0 : $dishIngredient->dish->proteins - $proteins,
                'fats' => $dishIngredient->dish->fats - $fats < 0 ? 0 : $dishIngredient->dish->fats - $fats,
                'carbohydrates' => $dishIngredient->dish->carbohydrates - $carbohydrates < 0 ? 0 : $dishIngredient->dish->carbohydrates - $carbohydrates,
                'netto' => $dishIngredient->dish->netto - $netto /1000 < 0 ? 0 : $dishIngredient->dish->netto - $netto / 1000
            ]);
        }
        if ($dishIngredient->semis_id) {
            $netto = $dishIngredient->netto * 1000;

            [
                'calories' => $calories,
                'proteins' => $proteins,
                'fats' => $fats,
                'carbohydrates' => $carbohydrates,
            ] = $this->countingKBZU($dishIngredient->semis, $netto);

            $dishIngredient->dish()->update([
                'calories' => $dishIngredient->dish->calories - $calories < 0 ? 0 : $dishIngredient->dish->calories - $calories,
                'proteins' => $dishIngredient->dish->proteins - $proteins < 0 ? 0 : $dishIngredient->dish->proteins - $proteins,
                'fats' => $dishIngredient->dish->fats - $fats < 0 ? 0 : $dishIngredient->dish->fats - $fats,
                'carbohydrates' => $dishIngredient->dish->carbohydrates - $carbohydrates < 0 ? 0 : $dishIngredient->dish->carbohydrates - $carbohydrates,
                'netto' => $dishIngredient->dish->netto - $netto /1000 < 0 ? 0 : $dishIngredient->dish->netto - $netto / 1000
            ]);
        }
    }
}
