<?php

namespace App\Domain\Dishies\Listeners\Observers;

use App\Domain\Storage\Entities\Storage;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Exceptions\NotEnoughException;
use App\Domain\Storage\Exceptions\NotFoundIngredientException;

class DirectoryDishObserver
{
}
