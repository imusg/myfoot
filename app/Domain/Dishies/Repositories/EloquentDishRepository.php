<?php

namespace App\Domain\Dishies\Repositories;

use App\Domain\Dishies\Entities\Dish;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Dishies\Contracts\DishRepository;
use App\Domain\Dishies\Entities\DishIngredients;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentDishRepository extends EloquentRepository implements DishRepository
{
    private string $defaultSort = 'id';

    private array $defaultSelect = [
        'id',
        'title',
        'barcode',
        'workshop',
        'tax',
        'image',
        'color',
        'netto',
        'cooking_process',
        'cooking_time',
        'category_id',
        'meal_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'title',
        'barcode',
        'workshop',
        'color',
        'meal.title'
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'category',
        'ingredients',
        'counts',
        'meal'
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Dish::class)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function findOneById(int $id): Model
    {
        $this->with(['category', 'ingredients', 'counts']);
        return parent::findOneById($id);
    }

    public function store(array $data): Model
    {
        $dish = parent::store($data);
        DirectoryDish::create([
            'dish_id' => $dish->id,
        ]);
        return $dish;
    }
    public function copy($id)
    {
        Dish::withoutEvents(function () use ($id) {
            $dish = $this->findOneById($id);
            $newDish = $dish->replicate();
            $newDish->push();
            DirectoryDish::create([
                'dish_id' => $newDish->id,
            ]);
            $dishIngredients = DishIngredient::where('dish_id', $id)->get();
            $dishIngredients->each(function ($item) use ($newDish) {
                DishIngredient::withoutEvents(function () use ($item, $newDish) {
                    $ingr = DishIngredient::find($item->id)->replicate()->fill(['dish_id' => $newDish->id]);
                    $ingr ->push();
                });
            });
        });
        return true;
    }
}
