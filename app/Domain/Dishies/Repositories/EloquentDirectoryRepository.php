<?php

namespace App\Domain\Dishies\Repositories;

use Carbon\Carbon;
use App\Domain\Cycles\Entities\Cycle;
use App\Domain\Dishies\Entities\Dish;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Cycles\Entities\CycleData;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Dishies\Contracts\DirectoryRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentDirectoryRepository extends EloquentRepository implements DirectoryRepository
{
    private string $defaultSort = 'id';

    private array $defaultSelect = [
        'id',
        'price',
        'price_custom',
        'dish_id',
        'branch_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'branch.title',
        'dish.meal.title',
        'dish.title'
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'dish',
        'branch',
        'meal',
        'cycles'
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(DirectoryDish::class)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function store(array $data): Model
    {
        $branch_id = $data['branch_id'];
        $dishIngredients = DishIngredient::where('dish_id', $data['dish_id'])->get();
        $prices = collect([]);
        $dishIngredients->each(function ($dishIngredient) use ($branch_id, $prices, $data) {
            $storageIngredient = Storage::where('ingredient_id', $dishIngredient->ingredient_id)
                ->where('branch_id', $branch_id)
                ->first();
            if (!$storageIngredient) {
                return;
            }
            $price = ($storageIngredient->cost_price * ($dishIngredient->netto * 1000)) / 1000;
            $prices->push($price);
        });
        if (!$prices->count()) {
            $data['price_custom'] = 'нет данных';
            $data['price'] = isset($data['price']) ? $data['price'] : $data['price_custom'];
            return parent::store($data);
        }
        $data['price_custom'] = $prices->sum();
        $data['price'] = isset($data['price']) ? $data['price'] :$prices->sum();
        return parent::store($data);
    }

    public function findOneByIdFilter($id): Model
    {
        $this->with(['dish', 'branch', 'meal']);
        $branch = request()->branch ? request()->branch : null;
        return $this->findOneBy(['dish_id' => $id, 'branch_id' => $branch]);
    }

    public function storeWithCycle(array $data): Model
    {
        $directory = $this->store($data);
        $this->storeCycle(1, $data['cycles']);
        return $directory;
    }

    private function storeCycle($directory_id, $data)
    {
        foreach ($data as $key => $item) {
            $start = Carbon::create($item['start']);
            $end = Carbon::create($item['end']);
            $diffWeek = $end->week - $start->week;
            foreach ($item['days'] as $day) {
                $start = Carbon::create($item['start']);
                $start->addDays($day);
                for ($i=0; $i <= $diffWeek; $i++) {
                    Cycle::create([
                       'week' => $start->week + $i,
                       'month' => $start->month,
                       'day' => $start->day,
                       'year' => $start->year,
                       'directory_dish_id' => $directory_id
                    ]);
                    $start->addDays(7);
                }
            }
        }
    }

    public function getWithParams()
    {
        $params = request()->only([
            'day', 'week', 'meal_id', 'category_id'
        ]);

        $dishIds = CycleData::where('day', $params['day'])
            ->where('week', $params['week'])
            ->select('directory_dish_id')
            ->get();

        return DirectoryDish::whereIn('id', $dishIds)
            ->where('meal_id', $params['meal_id'])
            ->get();
    }

    public function calculationCostPrice()
    {
        try {
            $directoryDish = DirectoryDish::get();
            $directoryDish->each(function ($item) {
                $prices = collect([]);
                DishIngredient::where('dish_id', $item->dish_id)
                    ->get()
                    ->each(function ($value) use ($prices, $item) {
                        $storageIngredient = Storage::where('ingredient_id', $value->ingredient_id)
                        ->where('branch_id', $item->branch_id)
                        ->first();
                        if ($storageIngredient) {
                            $price = $storageIngredient->cost_price * $value->netto;
                            $prices->push($price);
                        }
                    });
                if ($prices->count() > 0) {
                    $item->update(['price_custom' => $prices->sum()]);
                }
            });
            return true;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage);
        }
    }
}
