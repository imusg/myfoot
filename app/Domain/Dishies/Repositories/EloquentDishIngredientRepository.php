<?php

namespace App\Domain\Dishies\Repositories;

use App\Domain\Dishies\Entities\Dish;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Dishies\Entities\DishIngredients;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Domain\Dishies\Contracts\DishIngredientRepository;

class EloquentDishIngredientRepository extends EloquentRepository implements DishIngredientRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'title',
        'barcode',
        'workshop',
        'tax',
        'image',
        'color',
        'cooking_process',
        'cooking_time',
        'category_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'title',
        'barcode',
        'workshop',
        'color',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'category',
        'ingredients'
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Dish::class)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function findOneById(int $id): Model
    {
        $this->with(['category', 'ingredients']);
        return parent::findOneById($id);
    }

    public function store(array $data) : Model
    {
        $data['brutto'] = str_replace(',', '.', $data['brutto']);
        $data['netto'] = str_replace(',', '.', $data['netto']);
        $data['count'] = str_replace(',', '.', $data['count']);
        return parent::store($data);
    }

    public function update(Model $model, array $data) : Model
    {
        return parent::update($model, $data);
    }

    public function destroy($model) : bool
    {
        return parent::destroy($model);
    }
}
