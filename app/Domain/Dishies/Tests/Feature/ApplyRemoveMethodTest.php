<?php

namespace App\Domain\Dishes\Tests\Feature;

use Tests\TestCase;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Users\Entities\User;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Exceptions\NotEnoughException;

class ApplyRemoveMethodTest extends TestCase
{
    /*
        [x] Применение метода отчистки для ингредиента
        [x] Применение метода жарки для ингредиента
        [x] Применение метода варки для ингредиента
        [x] Применение метода тушения для ингредиента
        [x] Применение метода запекания для ингредиента
        [x] Удаление метода отчистки для ингредиента
        [x] Удаление метода жарки для ингредиента
        [x] Удаление метода варки для ингредиента
        [x] Удаление метода тушения для ингредиента
        [x] Удаление метода запекания для ингредиента
    */

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testApplyMethodCleaningInIngredient() : void
    {
        $losses_cleaning_checked = true;

        $losses_cleaning = 20;
        $netto = 100;

        $equalNetto = ($netto + $netto * ($losses_cleaning / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cleaning' => $losses_cleaning]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cleaning_checked' => $losses_cleaning_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);

        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_cleaning_checked, $dishIngr->losses_cleaning_checked);
    }

    public function testApplyMethodFryingInIngredient() : void
    {
        $losses_frying_checked = true;

        $losses_frying = 50;
        $netto = 100;

        $equalNetto = ($netto + $netto * ($losses_frying / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_frying' => $losses_frying]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_frying_checked' => $losses_frying_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_frying_checked, $dishIngr->losses_frying_checked);
    }

    public function testApplyMethodCookingInIngredient() : void
    {
        $losses_cooking_checked = true;

        $losses_cooking = 55;
        $netto = 100;

        $equalNetto = ($netto + $netto * ($losses_cooking / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cooking' => $losses_cooking]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cooking_checked' => $losses_cooking_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_cooking_checked, $dishIngr->losses_cooking_checked);
    }

    public function testApplyMethodStewInIngredient() : void
    {
        $losses_stew_checked = true;

        $losses_stew = 15;
        $netto = 100;

        $equalNetto = ($netto +($netto * ($losses_stew / 100))) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_stew' => $losses_stew]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_stew_checked' => $losses_stew_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_stew_checked, $dishIngr->losses_stew_checked);
    }

    public function testApplyMethodBakingInIngredient() : void
    {
        $losses_baking_checked = true;

        $losses_baking = 15;
        $netto = 100;

        $equalNetto = ($netto + $netto * ($losses_baking / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_baking' => $losses_baking]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_baking_checked' => $losses_baking_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_baking_checked, $dishIngr->losses_baking_checked);
    }

    public function testRemoveMethodCleaningInIngredient() : void
    {
        $losses_cleaning_checked = false;

        $losses_cleaning = 20;
        $brutto = 100;
        $initNetto = 100 + $brutto * ($losses_cleaning / 100);

        $equalNetto = ($initNetto - $brutto * ($losses_cleaning / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cleaning' => $losses_cleaning]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $initNetto,
                'losses_cleaning_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cleaning_checked' => $losses_cleaning_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_cleaning_checked, $dishIngr->losses_cleaning_checked);
    }

    public function testRemoveMethodFryingInIngredient() : void
    {
        $losses_frying_checked = false;

        $losses_frying = 50;
        $brutto = 100;
        $initNetto = 100 + $brutto * ($losses_frying / 100);

        $equalNetto = ($initNetto - $brutto * ($losses_frying / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_frying' => $losses_frying]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $initNetto,
                'losses_frying_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_frying_checked' => $losses_frying_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_frying_checked, $dishIngr->losses_frying_checked);
    }

    public function testRemoveMethodCookingInIngredient() : void
    {
        $losses_cooking_checked = false;

        $losses_cooking = 55;
        $brutto = 100;
        $initNetto = 100 + $brutto * ($losses_cooking / 100);

        $equalNetto = ($initNetto - $brutto * ($losses_cooking / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cooking' => $losses_cooking]
        );
        factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 1,
            'cost_price' => 2
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $initNetto,
                'losses_cooking_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cooking_checked' => $losses_cooking_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_cooking_checked, $dishIngr->losses_cooking_checked);
    }

    public function testRemoveMethodStewInIngredient() : void
    {
        $losses_stew_checked = false;

        $losses_stew = 15;
        $brutto = 100;
        $initNetto = 100 + $brutto * ($losses_stew / 100);

        $equalNetto = ($initNetto - $brutto * ($losses_stew / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_stew' => $losses_stew]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $initNetto,
                'losses_stew_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_stew_checked' => $losses_stew_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_stew_checked, $dishIngr->losses_stew_checked);
    }

    public function testRemoveMethodBakingInIngredient() : void
    {
        $losses_baking_checked = false;

        $losses_baking = 15;
        $brutto = 100;
        $initNetto = 100 + $brutto * ($losses_baking / 100);

        $equalNetto = ($initNetto - $brutto * ($losses_baking / 100)) / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_baking' => $losses_baking]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $initNetto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_baking_checked' => $losses_baking_checked]
        );

        $dishIngr = DishIngredient::find($dishIngredient->id);
        $this->assertEquals($equalNetto, $dishIngr->netto);
        $this->assertEquals($losses_baking_checked, $dishIngr->losses_baking_checked);
    }
}
