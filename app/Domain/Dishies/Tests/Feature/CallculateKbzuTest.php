<?php

namespace App\Domain\Dishes\Tests\Feature;

use Tests\TestCase;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Users\Entities\User;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Exceptions\NotEnoughException;

class CallculateKbzuTest extends TestCase
{
    /*
        [x] Проверка расчета каллорий блюда при добавлении ингредиентов
        [x] Проверка расчета белков блюда при добавлении ингредиентов
        [x] Проверка расчета жиров блюда при добавлении ингредиентов
        [x] Проверка расчета углеводов блюда при добавлении ингредиентов
        [x] Проверка расчета углеводов , каллорий, белков, жиров блюда при добавлении ингредиентов
        [x] Проверка расчета каллорий блюда при добавлении 2-x и более ингредиентов
        [x] Проверка расчета белков блюда при добавлении 2-x и более ингредиентов
        [x] Проверка расчета жиров блюда при добавлении 2-x и более ингредиентов
        [x] Проверка расчета углеводов блюда при добавлении 2-x и более ингредиентов
    */

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testCalculateCaloriesAddIngredeint() : void
    {
        $calories = 22.5;
        $netto = 80;

        $equalClories = ($netto * $calories) / 100;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);

        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'calories' => $calories]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $findDish = Dish::find($dish->id);
        $this->assertEquals($equalClories, $findDish->calories);
    }

    public function testCalculateProteinsAddIngredeint() : void
    {
        $proteins = 25.5;
        $netto = 80;

        $equalProteins = ($netto * $proteins) / 100;
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'proteins' => $proteins]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $findDish = Dish::find($dish->id);
        $this->assertEquals($equalProteins, $findDish->proteins);
    }

    public function testCalculateFatsAddIngredeint() : void
    {
        $fats = 25.5;
        $netto = 80;

        $equalFats = ($netto * $fats) / 100;
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'fats' => $fats]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $findDish = Dish::find($dish->id);
        $this->assertEquals($equalFats, $findDish->fats);
    }

    public function testCalculateCarbohydratesAddIngredeint() : void
    {
        $carbohydrates = 25.5;
        $netto = 80;

        $equalCarbohydrates = ($netto * $carbohydrates) / 100;
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'carbohydrates' => $carbohydrates]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $findDish = Dish::find($dish->id);
        $this->assertEquals($equalCarbohydrates, $findDish->carbohydrates);
    }

    public function testCalculateCarbohydratesFatsProteinsCalloriesAddIngredeint() : void
    {
        $carbohydrates = 45.5;
        $proteins = 15.4;
        $fats = 65.2;
        $calories = 55.7;

        $initCarbohydrates = 42;
        $initProteins = 15;
        $initFats = 6;
        $initCalories = 5;

        $netto = 80;

        $equalCarbohydrates = $initCarbohydrates + ($netto * $carbohydrates) / 100;
        $equalProteins = $initProteins + ($netto * $proteins) / 100;
        $equalFats = $initFats+ ($netto * $fats) / 100;
        $equalCalories = $initCalories + ($netto * $calories) / 100;

        $dish = factory(Dish::class)->create([
            'title' => 'Foo dish',
            'carbohydrates' => $initCarbohydrates,
            'calories' => $initCalories,
            'fats' => $initFats,
            'proteins' => $initProteins
        ]);

        $ingredient = factory(Ingredient::class)->create(
            [
                'title' => 'Foo ingredient',
                'carbohydrates' => $carbohydrates,
                'calories' => $calories,
                'fats' => $fats,
                'proteins' => $proteins
            ]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $findDish = Dish::find($dish->id);

        $this->assertEquals($equalCarbohydrates, $findDish->carbohydrates);
        $this->assertEquals($equalProteins, $findDish->proteins);
        $this->assertEquals($equalFats, $findDish->fats);
        $this->assertEquals($equalCalories, $findDish->calories);
    }

    public function testCalculateCaloriesAddTwoIngredeints() : void
    {
        $calories1 = 22.5;
        $calories2 = 12;

        $netto = 80;

        $equalClories = ($netto * $calories1) / 100;
        $equalClories = $equalClories + ($netto * $calories2) / 100;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);

        $ingredient1 = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'calories' => $calories1]
        );

        $ingredient2 = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'calories' => $calories2]
        );


        $storage1 = factory(Storage::class)->create([
            'ingredient_id' => $ingredient1->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $storage2 = factory(Storage::class)->create([
            'ingredient_id' => $ingredient2->id,
            'count' => 2,
            'cost_price' => 1
        ]);


        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient1->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient2->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );


        $findDish = Dish::find($dish->id);

        $this->assertEquals($equalClories, $findDish->calories);
    }

    public function testCalculateCaloriesRemoveIngredeints() : void
    {
        $fats = 25.5;
        $netto = 80;

        $equalFats = 0;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'fats' => $fats]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $this->delete(route(
            'dishes.destroy.ingredient',
            ['dish' => $dish->id, 'ingredient' => $ingredient->id]
        ));

        $findDish = Dish::find($dish->id);
        $this->assertEquals($equalFats, $findDish->fats);
    }
}
