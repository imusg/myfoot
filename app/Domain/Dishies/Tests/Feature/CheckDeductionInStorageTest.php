<?php

namespace App\Domain\Dishes\Tests\Feature;

use Tests\TestCase;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Users\Entities\User;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Exceptions\NotEnoughException;

class CheckDeductionInStorageTest extends TestCase
{
    /*
        [x] Проверка вычета веса ингредиента из склада при добавлении ингредиента
        [x] Проверка когда не хватает ингредиента
        [x] Проверка вычета веса ингредиента из склада при отчистке
        [x] Проверка вычета веса ингредиента из склада при жарке
        [x] Проверка вычета веса ингредиента из склада при варке
        [x] Проверка вычета веса ингредиента из склада при тушении
        [x] Проверка вычета веса ингредиента из склада при запекании
        [x] Проверка пересчета на складе после удаление метода отчистки для ингредиента
        [x] Проверка пересчета на складе после удаление метода жарки для ингредиента
        [x] Проверка пересчета на складе после удаление метода варки для ингредиента
        [x] Проверка пересчета на складе после удаление метода тушения для ингредиента
        [x] Проверка пересчета на складе после удаление метода запекания для ингредиента
        [x] Проверка добавления веса на складе после удаления ингредиента
    */

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testDeductionInStorageWithAddIngredient() : void
    {
        $countStorage = 2;
        $netto = 200;

        $equalCountStorage = $countStorage - $netto / 1000;
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);

        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient']
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $countStorage,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
        $storage->delete();
    }

    public function testNotEonoughtInStorageWithAddIngredient() : void
    {
        $countStorage = 2;
        $netto = 5000;

        $equalCountStorage = $countStorage - $netto;
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);

        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient']
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $countStorage,
            'cost_price' => 1
        ]);

        $this->expectException(NotEnoughException::class);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => $netto,
                'netto' => $netto
            ]
        );

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
        $storage->delete();
    }

    public function testDeductionInStorageWithAddMethodCleaningIngredient() : void
    {
        $losses_cleaning_checked = true;

        $countStorage = 2;
        $losses_cleaning = 50;
        $netto = 100;
        $equalNetto = $netto + $netto * ($losses_cleaning / 100);

        $equalCountStorage = $countStorage - $equalNetto / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cleaning' => $losses_cleaning]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $countStorage,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cleaning_checked' => $losses_cleaning_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithAddMethodFryingIngredient() : void
    {
        $losses_frying_checked = true;

        $countStorage = 2;
        $losses_frying = 30;
        $netto = 100;
        $equalNetto = $netto + $netto * ($losses_frying / 100);

        $equalCountStorage = $countStorage - $equalNetto / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_frying' => $losses_frying]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $countStorage,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_frying_checked' => $losses_frying_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);
    }

    public function testDeductionInStorageWithAddMethodCookingIngredient() : void
    {
        $losses_cooking_checked = true;

        $countStorage = 2;
        $losses_cooking = 70;
        $netto = 100;
        $equalNetto = $netto + $netto * ($losses_cooking / 100);

        $equalCountStorage = $countStorage - $equalNetto / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cooking' => $losses_cooking]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $countStorage,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cooking_checked' => $losses_cooking_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithAddMethodStewIngredient() : void
    {
        $losses_stew_checked = true;

        $countStorage = 2;
        $losses_stew = 90;
        $netto = 100;
        $equalNetto = $netto + $netto * ($losses_stew / 100);

        $equalCountStorage = $countStorage - $equalNetto / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_stew' => $losses_stew]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $countStorage,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_stew_checked' => $losses_stew_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithAddMethodBakingIngredient() : void
    {
        $losses_baking_checked = true;

        $countStorage = 2;
        $losses_baking = 50;
        $netto = 100;
        $equalNetto = $netto + $netto * ($losses_baking / 100);

        $equalCountStorage = $countStorage - $equalNetto / 1000;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_baking' => $losses_baking]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $countStorage,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => $netto
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_baking_checked' => $losses_baking_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithRemoveMethodCleaningIngredient() : void
    {
        $losses_cleaning_checked = false;

        $brutto = 0.1;
        $netto = 0.15;
        $countStorage = 2 - $netto;
        $losses_cleaning = 50;
        $initNetto = 100;

        $t = $netto - $brutto * ($losses_cleaning / 100);
        $equalCountStorage = $countStorage + $netto - $t;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cleaning' => $losses_cleaning]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 150,
                'losses_cleaning_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cleaning_checked' => $losses_cleaning_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithRemoveMethodFryingIngredient() : void
    {
        $losses_frying_checked = false;

        $brutto = 0.1;
        $netto = 0.15;
        $countStorage = 2 - $netto;
        $losses_frying = 50;
        $initNetto = 100;

        $t = $netto - $brutto * ($losses_frying / 100);
        $equalCountStorage = $countStorage + $netto - $t;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_frying' => $losses_frying]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 150,
                'losses_frying_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_frying_checked' => $losses_frying_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithRemoveMethodCookingIngredient() : void
    {
        $losses_cooking_checked = false;

        $brutto = 0.1;
        $netto = 0.15;
        $countStorage = 2 - $netto;
        $losses_cooking = 50;
        $initNetto = 100;

        $t = $netto - $brutto * ($losses_cooking / 100);
        $equalCountStorage = $countStorage + $netto - $t;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_cooking' => $losses_cooking]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 150,
                'losses_cooking_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_cooking_checked' => $losses_cooking_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithRemoveMethodStewIngredient() : void
    {
        $losses_stew_checked = false;

        $brutto = 0.1;
        $netto = 0.15;
        $countStorage = 2 - $netto;
        $losses_stew = 50;
        $initNetto = 100;

        $t = $netto - $brutto * ($losses_stew / 100);
        $equalCountStorage = $countStorage + $netto - $t;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_stew' => $losses_stew]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 150,
                'losses_stew_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_stew_checked' => $losses_stew_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testDeductionInStorageWithRemoveMethodBakingIngredient() : void
    {
        $losses_baking_checked = false;

        $brutto = 0.1;
        $netto = 0.15;
        $countStorage = 2 - $netto;
        $losses_baking = 50;
        $initNetto = 100;

        $t = $netto - $brutto * ($losses_baking / 100);
        $equalCountStorage = $countStorage + $netto - $t;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(
            ['title' => 'Foo ingredient', 'losses_baking' => $losses_baking]
        );

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 2,
            'cost_price' => 1
        ]);

        $dishIngredient = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 150,
                'losses_baking_checked' => true
            ]
        );

        $this->put(
            route('dishes.update.ingredient', ['dish' => $dish->id, 'ingredient' => $ingredient->id]),
            ['losses_baking_checked' => $losses_baking_checked]
        );

        $storageFind = Storage::find($storage->id);

        $this->assertEquals($equalCountStorage, $storageFind->count);

        $dish->delete();
        $ingredient->delete();
        $dishIngredient->delete();
    }

    public function testAddWeightStorageWithRemoveIngredient() : void
    {
        $storageCount = 2;

        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(['title' => 'Foo ingredient']);

        $storage = factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => $storageCount,
            'cost_price' => 1
        ]);

        factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 1,
            'cost_price' => 2
        ]);

        $dishSemis = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 100
            ]
        );

        $this->delete(route(
            'dishes.destroy.ingredient',
            ['dish' => $dish->id, 'ingredient' => $ingredient->id]
        ));


        $this->assertDatabaseMissing(
            'dish_ingredients',
            ['dish_id' => $dish->id, 'ingredient_id' => $ingredient->id, 'brutto' => 100, 'netto' => 100]
        );

        $storageFind = Storage::find($storage->id);
        $this->assertEquals($storageCount, $storageFind->count);
    }
}
