<?php

namespace App\Domain\Unit\Tests\Feature;

use Tests\TestCase;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Users\Entities\User;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Dishies\Entities\DirectoryDish;

/*
    [x] Получение всех блюд из справочника
    [x] Фильтр по названию, филиалу, категории
    [x] Создание справочника блюда
    [x] Редактирование справочника блюда
    [x] Удаление справочника блюда
*/

class DirectoryControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testGetAllGuides(): void
    {
        $response = $this->get(route('directory.index'))
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertEquals(count($data), 20);
    }

    public function testGetGuidesByFilterName() : void
    {
        $title = 'search dish';
        $dish = factory(Dish::class)->create(['title' => $title]);
        $directory = factory(DirectoryDish::class)->create(['dish_id' => $dish->id]);

        $response = $this->get(route('directory.index', ['filter[dish.title]' => $title]))
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertEquals(count($data), 1);

        $dish->delete();
        $directory->delete();
    }

    public function testGetGuidesByFilterBranch() : void
    {
        $title = 'search branch';
        $dish = factory(Dish::class)->create(['title' => 'search dish']);
        $branch = factory(Branch::class)->create(['title' => $title]);
        $directory = factory(DirectoryDish::class)->create(['dish_id' => $dish->id, 'branch_id' => $branch->id]);

        $response = $this->get(route('directory.index', ['filter[branch.title]' => $title]))
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertEquals(count($data), 1);

        $dish->delete();
        $directory->delete();
        $branch->delete();
    }

    public function testGetGuidesByFilterMeal() : void
    {
        $title = 'search meal';
        $meal = factory(Meal::class)->create(['title' => $title]);
        $dish = factory(Dish::class)->create(['title' => 'search dish', 'meal_id' => $meal->id]);
        $directory = factory(DirectoryDish::class)->create(['dish_id' => $dish->id]);

        $response = $this->get(route('directory.index', ['filter[dish.meal.title]' => $title]))
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertEquals(count($data), 1);

        $dish->delete();
        $directory->delete();
        $meal->delete();
    }

    public function testCreateDirectory() : void
    {
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $branch = factory(Branch::class)->create(['title' => 'Foo branch']);

        $response = $this->post(
            route('directory.store'),
            [
                'price' => 1,
                'dish_id' => $dish->id,
                'branch_id' => $branch->id,
            ]
        )
            ->assertSuccessful();

        $id = $response->json('data.id');
        $this->assertDatabaseHas('directory_dishes', ['id' => $id, 'branch_id' => $branch->id, 'dish_id' => $dish->id]);
    }

    public function testUpdateDirectory() : void
    {
        $directory = factory(DirectoryDish::class)
            ->create();

        $response = $this->put(route('directory.update', $directory->id), ['price' => 200])
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertDatabaseHas('directory_dishes', ['price' => 200, 'id' => $directory->id]);
    }

    public function testDeleteDirectory() : void
    {
        $directory = factory(DirectoryDish::class)
            ->create();
        $this->delete(route('directory.destroy', $directory->id))
            ->assertSuccessful();

        $this->assertDatabaseMissing('directory_dishes', ['id' => $directory->id]);
    }
}
