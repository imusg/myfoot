<?php

namespace App\Domain\Dishes\Tests\Feature;

use Tests\TestCase;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Users\Entities\User;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Exceptions\NotEnoughException;

class DishControllerTest extends TestCase
{
    /*
        [x] Получение всех тех карт блюда
        [x] Фильтр по названию
        [x] Фильтр по категории
        [x] Создание блюда
        [x] Создание с ошибками валидации
        [x] Обновление тех карты блюда
        [x] Получение одно блюда
        [x] Удаление блюда
        [x] Добавление полуфабриката в блюдо
        [x] Добавление ингредиента в блюдо
        [x] Удаление ингредиента из блюда
        [x] Удаление полуфабриката из блюда
    */

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testGetAllDishes(): void
    {
        $response = $this->get(route('dish.index'))
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertEquals(count($data), 20);
    }

    public function testGetDishesByFilterName(): void
    {
        $title = 'search dish';
        $dish = factory(Dish::class)->create(['title' => $title]);

        $response = $this->get(route('dish.index', ['filter[title]' => $title]))
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertEquals(count($data), 1);

        $dish->delete();
    }

    public function testGetDishesByFilterCategory(): void
    {
        $title = 'search meal';

        $meal = factory(Meal::class)->create(['title' => $title]);
        $dish = factory(Dish::class)->create(['title' => 'foo dish', 'meal_id' => $meal->id]);

        $response = $this->get(route('dish.index', ['filter[meal.title]' => $title]))
            ->assertSuccessful();

        $data = $response->json('data');

        $this->assertEquals(count($data), 1);

        $dish->delete();
        $meal->delete();
    }

    public function testCreateAllFieldFill() : void
    {
        $data = [
            'title' => 'Foo dish',
            'barcode' => 'barcode',
            'workshop' => 'workshop',
            'tax' => 10,
            'image' => 'image',
            'color' => 'color',
            'cooking_process' => 'cooking_process',
            'cooking_time' => '10:20',
            'meal_id' => 1,
            'calories' => 100,
            'proteins' => 100,
            'fats' => 100,
            'carbohydrates' => 100,
        ];

        $response = $this->post(route('dish.store'), $data)
         ->assertSuccessful();

        $id = $response->json('data.id');
        $this->assertDatabaseHas('dishes', ['id' => $id]);
    }

    public function testCreateWithErrorTitle() : void
    {
        $data = [
            'barcode' => 'barcode',
            'workshop' => 'workshop',
            'tax' => 10,
            'image' => 'image',
            'color' => 'color',
            'cooking_process' => 'cooking_process',
            'cooking_time' => '10:20',
            'meal_id' => 1,
            'calories' => 100,
            'proteins' => 100,
            'fats' => 100,
            'carbohydrates' => 100,
        ];

        $response = $this->post(route('dish.store'), $data)
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'title' => [
                        'validation.required'
                    ]
                ]
            ]);
    }

    public function testUpdateDish() : void
    {
        $dish = factory(Dish::class)
            ->create();

        $response = $this->put(route('dish.update', $dish->id), ['title' => 'title'])
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertDatabaseHas('dishes', ['title' => $data['title']]);
    }

    public function testGetDish(): void
    {
        $dish = factory(Dish::class)
            ->create();

        $response = $this->get(route('dish.show', $dish->id))
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertDatabaseHas('dishes', ['id' => $data['id']]);
    }

    public function testDeleteDish() : void
    {
        $dish = factory(Dish::class)
            ->create();

        $this->delete(route('dish.destroy', $dish->id))
            ->assertSuccessful();

        $this->assertDatabaseMissing('dishes', ['id' => $dish->id]);
    }

    public function testAddSemisInDish() : void
    {
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $semis = factory(Semifinished::class)->create(['title' => 'Foo semis']);

        $response = $this->post(route('dishes.store.ingredient', $dish->id), [
            'semis_id' => $semis->id,
            'brutto' => 100,
            'netto' => 100
        ]);

        $this->assertDatabaseHas(
            'dish_ingredients',
            ['dish_id' => $dish->id, 'semis_id' => $semis->id, 'brutto' => 100, 'netto' => 100 ]
        );

        $dish->delete();
        $semis->delete();
    }

    public function testAddIngredientInDish() : void
    {
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(['title' => 'Foo ingredient']);

        factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 1,
            'cost_price' => 2
        ]);

        $response = $this->post(route('dishes.store.ingredient', $dish->id), [
            'ingredient_id' => $ingredient->id,
            'brutto' => 100,
            'netto' => 100
        ])->assertSuccessful();

        $this->assertDatabaseHas(
            'dish_ingredients',
            ['dish_id' => $dish->id, 'ingredient_id' => $ingredient->id, 'brutto' => 100/1000, 'netto' => 100/1000]
        );
    }

    public function testDeleteSemisInDish() : void
    {
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $semis = factory(Semifinished::class)->create(['title' => 'Foo semis']);

        $dishSemis = factory(DishIngredient::class)->create(
            [
                'semis_id' => $semis->id,
                'ingredient_id' => null,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 100
            ]
        );

        $this->delete(route(
            'dishes.destroy.ingredient',
            ['dish' => $dish->id, 'ingredient' => $semis->id, 'action' => 'semis']
        ));

        $this->assertDatabaseMissing(
            'dish_ingredients',
            ['dish_id' => $dish->id, 'semis_id' => $semis->id, 'brutto' => 100, 'netto' => 100 ]
        );

        $dish->delete();
        $semis->delete();
    }

    public function testDeleteIngredientInDish() : void
    {
        $dish = factory(Dish::class)->create(['title' => 'Foo dish']);
        $ingredient = factory(Ingredient::class)->create(['title' => 'Foo ingredient']);

        factory(Storage::class)->create([
            'ingredient_id' => $ingredient->id,
            'count' => 1,
            'cost_price' => 2
        ]);

        $dishSemis = factory(DishIngredient::class)->create(
            [
                'semis_id' => null,
                'ingredient_id' => $ingredient->id,
                'dish_id' => $dish->id,
                'brutto' => 100,
                'netto' => 100
            ]
        );

        $this->delete(route(
            'dishes.destroy.ingredient',
            ['dish' => $dish->id, 'ingredient' => $ingredient->id]
        ));


        $this->assertDatabaseMissing(
            'dish_ingredients',
            ['dish_id' => $dish->id, 'ingredient_id' => $ingredient->id, 'brutto' => 100, 'netto' => 100]
        );

        $dish->delete();
        $ingredient->delete();
    }
}
