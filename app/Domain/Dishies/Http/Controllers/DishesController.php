<?php

namespace App\Domain\Dishies\Http\Controllers;

use App\Domain\Couriers\Entities\CourierFile;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Kitchens\Entities\KitchenFile;
use App\Domain\Kitchens\Entities\PickerFile;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Dishies\Contracts\DishRepository;
use App\Domain\Dishies\Services\GenerateDishFile;
use App\Domain\Dishies\Http\Resources\DishResource;
use App\Domain\Dishies\Http\Resources\DishCollection;
use App\Domain\Dishies\Http\Requests\DishStoreRequest;
use App\Domain\Dishies\Http\Requests\DishUpdateRequest;
use Storage;

class DishesController extends Controller
{
    private DishRepository $dishRepository;
    private GenerateDishFile $generateDishFile;

    public function __construct(DishRepository $dishRepository, GenerateDishFile $generateDishFile)
    {
        $this->dishRepository = $dishRepository;
        $this->generateDishFile = $generateDishFile;
        $this->resourceItem = DishResource::class;
        $this->resourceCollection = DishCollection::class;
    }

    public function test()
    {
        $courierFiles = CourierFile::all();
        $pickerFile = PickerFile::all();
        $kitchenFile = KitchenFile::all();

        $courierFiles->each(function ($item) {
            $item->update([
                'url' =>   str_replace(
                    'http://myfoot-b1.amics-tech.ru/tasks/courier/',
                    'https://static.manager.moyaeda34.ru/production/files/couriers/',
                    $item->url
                )
            ]);
        });

        $pickerFile->each(function ($item) {
            $item->update([
                'url' =>   str_replace(
                    'http://myfoot-b1.amics-tech.ru/tasks/picker/',
                    'https://static.manager.moyaeda34.ru/production/files/pickers/',
                    $item->url
                )
            ]);
        });

        $kitchenFile->each(function ($item) {
            $item->update([
                'url' =>   str_replace(
                    'http://myfoot-b1.amics-tech.ru/tasks/kitchen/',
                    'https://static.manager.moyaeda34.ru/production/files/kitchens/',
                    $item->url
                )
            ]);
        });
    }

    public function index()
    {
        $collection = $this->dishRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function show($id)
    {
        $response = $this->dishRepository->findOneById($id);
        return $this->respondWithItem($response);
    }

    public function store(DishStoreRequest $request): JsonResponse
    {
        $data = $request->validated();
        $dish = $this->dishRepository->store($data);
        return $this->respondWithCustomData(DishResource::make($dish), Response::HTTP_CREATED);
    }

    public function update(DishUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->dishRepository->update(
            $this->dishRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy(int $id)
    {
        $response = $this->dishRepository->destroy($this->dishRepository->findOneById($id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function upload(Request $request)
    {
        if ($request->file()) {
            $fileName = time().'.'.$request->file('file')->getClientOriginalExtension();
            Storage::disk('minio')->put("dishes/$fileName", $request->file('file')->get());
            $file_path = Storage::disk('minio')->url("dishes/$fileName");
            return $this->respondWithCustomData([
                'status' => 'Success',
                'path' => $file_path
            ], Response::HTTP_OK);
        }
    }

    public function copy(Request $request)
    {
        $id = $request->id;
        $response = $this->dishRepository->copy($id);
        return $this->respondWithCustomData($response, Response::HTTP_OK);
    }

    public function generateFile(int $id)
    {
        $response = $this->generateDishFile->generateFile($id);
        return $this->respondWithCustomData($response);
    }
}
