<?php

namespace App\Domain\Dishies\Http\Controllers;

use App\Domain\Dishies\Contracts\DirectoryRepository;
use App\Domain\Dishies\Http\Requests\DirectoryStoreRequest;
use App\Domain\Dishies\Http\Requests\DirectoryUpdateRequest;
use App\Domain\Dishies\Http\Resources\DirectoryCollection;
use App\Domain\Dishies\Http\Resources\DirectoryResource;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DirectoryDishController extends Controller
{
    private DirectoryRepository $directoryRepository;

    public function __construct(DirectoryRepository $directoryRepository)
    {
        $this->directoryRepository = $directoryRepository;
        $this->resourceItem = DirectoryResource::class;
        $this->resourceCollection = DirectoryCollection::class;
    }

    public function index()
    {
        $collection = $this->directoryRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function show($id)
    {
        $response = $this->directoryRepository->findOneByIdFilter($id);
        return $this->respondWithItem($response);
    }

    public function store(DirectoryStoreRequest $request): JsonResponse
    {
        $data = $request->validated();
        $dish = $this->directoryRepository->store($data);
        return $this->respondWithCustomData(
            DirectoryResource::make($dish),
            Response::HTTP_CREATED
        );
    }

    public function update(DirectoryUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->directoryRepository->update(
            $this->directoryRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy($id)
    {
        $response = $this->directoryRepository->destroy($this->directoryRepository->findOneById($id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function calculationCostPrice() {
        $response = $this->directoryRepository->calculationCostPrice();
        return $this->respondWithCustomData($response);
    }
}
