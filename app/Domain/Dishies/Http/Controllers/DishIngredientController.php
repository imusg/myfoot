<?php


namespace App\Domain\Dishies\Http\Controllers;

use Illuminate\Http\Response;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Semis\Http\Resources\SemisResource;
use App\Domain\Dishies\Contracts\DishIngredientRepository;
use App\Domain\Ingredients\Http\Resources\IngredientResource;
use App\Domain\Dishies\Http\Requests\DishIngredientStoreRequest;
use App\Domain\Dishies\Http\Requests\DishIngredientUpdateRequest;

class DishIngredientController extends Controller
{
    private DishIngredientRepository $dishIngredientsRepository;

    public function __construct(DishIngredientRepository $dishIngredientsRepository)
    {
        $this->dishIngredientsRepository = $dishIngredientsRepository;
    }

    /**
     * @OA\Post(
     * path="/dishes/{dish}/ingredient",
     * summary="Добавление ингредиента в тех карту блюда",
     * description="Добавление ингредиента в тех карту блюда",
     * operationId="storeIngredientsDishes",
     * tags={"Dishes"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные об ингредиентах в тех карты блюда",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/DishIngredientsRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление ингредиента в тех карты блюда",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Ingredient"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  DishIngredientStoreRequest  $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DishIngredientStoreRequest $request, $id): \Illuminate\Http\JsonResponse
    {
        $data = $request->validated();
        $data['dish_id'] = $id;
        $response = $this->dishIngredientsRepository->store($data);
        return $this->respondWithCustomData(
            true,
            Response::HTTP_CREATED
        );
    }

    /**
     * @OA\Put(
     * path="/dishes/{dish}/ingredient/{ingredient}",
     * summary="Обновление ингредиента в тех карты блюда",
     * description="Обновление ингредиента в тех карты блюда",
     * operationId="updateIngredientDishes",
     * tags={"Dishes"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="dish",
     *  in="path",
     *  required=true,
     *  description="Dish id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Parameter(
     *  name="ingredient",
     *  in="path",
     *  required=true,
     *  description="Ingredient id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для обновления ингредиента в тех карте блюда",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/DishIngredientsRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная обновление ингредеиента в тех карте блюда",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Ingredient"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  DishIngredientStoreRequest  $request
     * @param $dishId
     * @param $ingredientId
     * @return mixed
     */
    public function update(DishIngredientUpdateRequest $request, $dishId, $ingredientId)
    {
        $data = $request->validated();
        if (isset($data['netto'])) {
            $data['netto'] = $data['netto'] / 1000;
        }

        if (isset($data['action'])) {
            if ($data['action'] === 'semis') {
                $find = $this->dishIngredientsRepository->findOneBy([
                    'semis_id' => $ingredientId,
                    'dish_id' => $dishId
                ]);
                $response = $this->dishIngredientsRepository->update(
                    $find,
                    $data
                );
                return $this->respondWithCustomData(
                    SemisResource::make($response->semis),
                    Response::HTTP_OK
                );
            }
        }

        $find = $this->dishIngredientsRepository->findOneBy([
                'ingredient_id' => $ingredientId,
                'dish_id' => $dishId
        ]);

        $response = $this->dishIngredientsRepository->update(
            $find,
            $data
        );

        return $this->respondWithCustomData(
            $response,
            Response::HTTP_OK
        );
    }

    /**
     * @OA\Delete (
     * path="/dishes/{dish}/ingredient/{ingredient}",
     * summary="Удаление ингредиента из тех карты блюда",
     * description="Удаление ингредиента из тех карты блюда",
     * operationId="deleteIngredientDishes",
     * tags={"Dishes"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="dish",
     *  in="path",
     *  required=true,
     *  description="Dish id в котором удаляется ингредиент",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Parameter(
     *  name="ingredient",
     *  in="path",
     *  required=true,
     *  description="Ingredient id который удаляем",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная удаление ингредеиента в тех карте блюда",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            @OA\Property(
     *              property="deleted",
     *              type="boolean",
     *              example="true"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param $dishId
     * @param $ingredientId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($dishId, $ingredientId)
    {
        if (request()->action === 'semis') {
            $model = $this->dishIngredientsRepository->findOneBy([
                'semis_id' => $ingredientId,
                'dish_id' => $dishId
            ]);
        } else {
            $model = $this->dishIngredientsRepository->findOneBy([
                'ingredient_id' => $ingredientId,
                'dish_id' => $dishId
            ]);
        }
        $response = $this->dishIngredientsRepository->destroy(
            $model
        );

        return $this->respondWithCustomData(
            ['deleted' => true],
            Response::HTTP_OK
        );
    }
}
