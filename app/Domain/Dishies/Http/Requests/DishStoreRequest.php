<?php

namespace App\Domain\Dishies\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema (
 *    schema="DishRequest",
 *    type="object",
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Название блюда",
 *      example="Пюре с котлетой",
 *    ),
 *     @OA\Property (
 *      property="barcode",
 *      type="integer",
 *      description="Штрихкод",
 *      example="4703",
 *    ),
 *    @OA\Property (
 *      property="workshop",
 *      type="string",
 *      description="Цех",
 *      example="Цех 1",
 *    ),
 *    @OA\Property (
 *      property="tax",
 *      type="integer",
 *      description="налог",
 *      example="40",
 *    ),
 *    @OA\Property (
 *      property="image",
 *      type="string",
 *      description="Фотография блюда",
 *      example="https://via.placeholder.com/640x480.png/003311?text=aperiam",
 *    ),
 *    @OA\Property (
 *      property="color",
 *      type="string",
 *      description="Цвет блюда",
 *      example="Военно-воздушный синий",
 *    ),
 *    @OA\Property (
 *      property="cooking_process",
 *      type="string",
 *      description="Процесс приготовления блюда",
 *      example="Процесс приготовления блюда",
 *    ),
 *   @OA\Property (
 *      property="cooking_time",
 *      type="string",
 *      description="Процесс приготовления блюда",
 *      example="00:42:00",
 *    ),
 *    @OA\Property (
 *      property="category_id",
 *      type="integer",
 *      example="1",
 *    ),
 *    @OA\Property (
 *      property="ingredients",
 *      type="array",
 *      @OA\Items(
 *          type="object",
 *          @OA\Property (
 *             property="id",
 *             type="integer",
 *             description="Id ингредиента",
 *             example="1"
 *          )
 *      )
 *    )
 * )
 */

class DishStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => 'string|required',
            'barcode' => 'string|nullable',
            'workshop' => 'string|nullable',
            'tax' => 'numeric|nullable',
            'image' => 'string|nullable',
            'color' => 'string|nullable',
            'cooking_process' => 'string|nullable',
            'cooking_time' => 'date_format:H:i|nullable',
            'ingredients' => 'array|nullable',
            'meal_id' => 'nullable|exists:meals,id',
            'calories' => 'nullable',
            'proteins' => 'nullable',
            'fats' => 'nullable',
            'carbohydrates' => 'nullable',
        ];
    }
}
