<?php

namespace App\Domain\Dishies\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema (
 *    schema="DishUpdateIngredientsRequest",
 *    type="object",
 *    @OA\Property (
 *      property="ingredient_id",
 *      type="integer",
 *      description="ID ингредиента",
 *      example="1",
 *    ),
 *     @OA\Property (
 *      property="dish_id",
 *      type="integer",
 *      description="ID блюда",
 *      example="1",
 *    )
 * )
 */

class DishIngredientUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'action' => 'nullable',
            'count' => 'nullable',
            'brutto' => 'nullable',
            'netto' => 'nullable',
            'losses_cleaning_checked' => 'boolean',
            'losses_frying_checked' => 'boolean',
            'losses_cooking_checked' => 'boolean',
            'losses_baking_checked' => 'boolean',
            'losses_stew_checked' => 'boolean'
        ];
    }
}
