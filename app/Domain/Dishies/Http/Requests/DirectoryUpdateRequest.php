<?php

namespace App\Domain\Dishies\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DirectoryUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'price' => 'numeric',
            'dish_id' => 'exists:dishes,id',
            'branch_id' => 'integer|nullable',
            'meal_id' => 'exists:meals,id',
        ];
    }
}
