<?php

namespace App\Domain\Dishies\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DirectoryStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'price' => 'nullable|numeric',
            'dish_id' => 'nullable|exists:dishes,id',
            'branch_id' => 'nullable|exists:branches,id',
        ];
    }
}
