<?php

namespace App\Domain\Dishies\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DishUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'title' => 'string|nullable',
            'barcode' => 'string|nullable',
            'workshop' => 'string|nullable',
            'tax' => 'numeric|nullable',
            'image' => 'string|nullable',
            'color' => 'string|nullable',
            'cooking_process' => 'string|nullable',
            'cooking_time' => 'string|nullable',
            'category_id' => 'exists:categories,id|nullable',
            'meal_id' => 'nullable|exists:meals,id',
            'calories' => 'nullable',
            'proteins' => 'nullable',
            'fats' => 'nullable',
            'carbohydrates' => 'nullable',
        ];
    }
}
