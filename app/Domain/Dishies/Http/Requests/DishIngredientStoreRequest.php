<?php

namespace App\Domain\Dishies\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema (
 *    schema="DishIngredientsRequest",
 *    type="object",
 *    @OA\Property (
 *      property="ingredient_id",
 *      type="integer",
 *      description="ID ингредиента",
 *      example="1",
 *    )
 * )
 */

class DishIngredientStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'ingredient_id' => 'nullable',
            'semis_id' => 'nullable',
            'count' => 'nullable',
            'brutto' => 'nullable',
            'netto' => 'nullable',
        ];
    }
}
