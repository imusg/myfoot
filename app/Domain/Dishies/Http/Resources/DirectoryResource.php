<?php

namespace App\Domain\Dishies\Http\Resources;

use App\Domain\Branches\Http\Resources\BranchResource;
use App\Domain\Cycles\Http\Resources\CycleResource;
use App\Domain\Meal\Http\Resources\MealResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DirectoryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'price_custom' => round($this->price_custom, 2),
            'dish' => DishResource::make($this->dish),
            'branch' => BranchResource::make($this->branch),
            'meal' => MealResource::make($this->meal),
            'cycles' => CycleResource::collection($this->whenLoaded('cycles'))
        ];
    }
}
