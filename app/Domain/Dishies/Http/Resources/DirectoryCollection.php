<?php

namespace App\Domain\Dishies\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DirectoryCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => DirectoryResource::collection($this->collection),
        ];
    }
}
