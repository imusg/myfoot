<?php

namespace App\Domain\Dishies\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DishCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => DishResource::collection($this->collection),
        ];
    }
}
