<?php

namespace App\Domain\Dishies\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Semis\Http\Resources\SemisResource;
use App\Domain\Ingredients\Http\Resources\IngredientResource;

class IngredientCountResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'ingredient' => $this->ingredient ?
                IngredientResource::make($this->ingredient) :
                SemisResource::make($this->semis),
            'count' => $this->count,
            'netto' => $this->netto * 1000,
            'brutto' => $this->brutto * 1000,
            'losses_cleaning_checked' => $this->losses_cleaning_checked !== 0,
            'losses_frying_checked' => $this->losses_frying_checked !== 0,
            'losses_cooking_checked' => $this->losses_cooking_checked !== 0,
            'losses_baking_checked' => $this->losses_baking_checked !== 0,
            'losses_stew_checked' => $this->losses_stew_checked !== 0,
        ];
    }
}
