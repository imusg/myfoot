<?php

namespace App\Domain\Dishies\Http\Resources;

use App\Domain\Meal\Http\Resources\MealResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Category\Http\Resources\CategoryResource;
use App\Domain\Ingredients\Http\Resources\IngredientResource;

class DishResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'barcode' => $this->barcode,
            'workshop' => $this->workshop,
            'tax' => $this->tax,
            'image' => $this->image,
            'calories' => $this->calories,
            'proteins' => $this->proteins,
            'fats' => $this->fats,
            'netto' => ($this->netto * 1000)." гр.",
            'carbohydrates' => $this->carbohydrates,
            'cooking_process' => $this->cooking_process,
            'cooking_time' => $this->cooking_time,
            'category' => CategoryResource::make($this->category),
            'ingredients' => IngredientCountResource::collection($this->counts),
            'meal' => MealResource::make($this->meal),
        ];
    }
}
