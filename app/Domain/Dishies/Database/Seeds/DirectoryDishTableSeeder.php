<?php

namespace App\Domain\Dishies\Database\Seeds;

use App\Domain\Dishies\Entities\DirectoryDish;
use Illuminate\Database\Seeder;

class DirectoryDishTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(DirectoryDish::class, 100)->create();
    }
}
