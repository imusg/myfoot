<?php

namespace App\Domain\Dishies\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Storage\Entities\Storage;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;

class DishTableSeeder extends Seeder
{
    public function run(): void
    {
        $fileName = storage_path('app/import/import_dish.xls');
        $reader = new Xls();

        $spreadsheet = $reader->load($fileName)->getActiveSheet();
        $collectIngredients = collect($spreadsheet->toArray());
        /*
        array:17 [
            0 => null
            1 => "PosterID product_id (не менять!)"
            2 => "Название"
            3 => "Категория"
            4 => "Налог"
            5 => "Выход"
            6 => "Цена"
            7 => "Тип"
            8 => "Состав"
            9 => "Брутто"
            10 => "Нетто"
            11 => "Очистка"
            12 => "Варка"
            13 => "Жарка"
            14 => "Тушение"
            15 => "Запекание"
            16 => "Себестоимость составляющих"
            ]
        */
        $collectIngredients
            ->filter(function ($value) {
                return $value[1] !== null && $value[1] !== 'PosterID product_id (не менять!)';
            })->each(function ($item) {
                $dish = Dish::firstOrCreate([
                    'title' => $item[2]
                ]);
                if (!DirectoryDish::where('dish_id', $dish->id)->exists()) {
                    DirectoryDish::firstOrCreate([
                        'dish_id' => $dish->id,
                        'price' => $item[6]
                    ]);
                }
                $searchTerm =$item[8];

                $ingredient = Ingredient::where('title', 'LIKE', "%$searchTerm%")->first();
                DishIngredient::create([
                    'dish_id' => $dish->id,
                    'ingredient_id' => isset($ingredient->id) ? $ingredient->id : null,
                    'count' => floatval(substr($item[9], 0, -2))/1000,
                    'brutto' => floatval(substr($item[9], 0, -2))/1000,
                    'netto' => floatval(substr($item[9], 0, -2))/1000,
                ]);
            });
        $this->addProduct();
    }

    private function addProduct(): void
    {
        $fileName = storage_path('app/import/import_products.xls');
        $reader = new Xls();

        $spreadsheet = $reader->load($fileName)->getActiveSheet();
        $collectIngredients = collect($spreadsheet->toArray());

        $collectIngredients->filter(function ($value) {
            return $value[1] !== null && $value[1] !== 'Название';
        })->each(function ($item) {
            $dish = Dish::firstOrCreate([
                'title' => $item[1]
            ]);

            $searchTerm =$item[4];
            $ingredient = Ingredient::where('title', 'LIKE', "%$searchTerm%")->first();

            DishIngredient::create([
                'dish_id' => $dish->id,
                'ingredient_id' => isset($ingredient->id) ? $ingredient->id : null,
                'count' => floatval(substr($item[7], 0, -2))/1000,
                'brutto' => floatval(substr($item[7], 0, -2))/1000,
                'netto' => floatval(substr($item[7], 0, -2))/1000,
            ]);
        });
    }
}
