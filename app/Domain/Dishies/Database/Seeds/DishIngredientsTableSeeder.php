<?php

namespace App\Domain\Dishies\Database\Seeds;

use App\Domain\Dishies\Entities\DishIngredients;
use Illuminate\Database\Seeder;

class DishIngredientsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(DishIngredients::class, 100)->create();
    }
}
