<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDishesTable extends Migration
{
    public function up()
    {
        Schema::create('dishes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('barcode')->nullable();
            $table->string('workshop')->nullable();
            $table->decimal('tax')->nullable();
            $table->string('image')->nullable();
            $table->string('color')->nullable();
            $table->text('cooking_process')->nullable();
            $table->time('cooking_time')->nullable();
            $table->decimal('calories', 8, 5)->unsigned()->nullable();
            $table->decimal('netto', 8, 5)->unsigned()->nullable();
            $table->decimal('brutto', 8, 5)->unsigned()->nullable();
            $table->decimal('proteins', 8, 5)->unsigned()->nullable();
            $table->decimal('fats', 8, 5)->unsigned()->nullable();
            $table->decimal('carbohydrates', 8, 5)->unsigned()->nullable();
            $table->foreignId('meal_id')->nullable();
            $table->foreign('meal_id')
                ->references('id')
                ->on('meals')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreignId('category_id')->nullable();
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dishes');
    }
}
