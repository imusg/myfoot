<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectoryDishesTable extends Migration
{
    public function up()
    {
        Schema::create('directory_dishes', function (Blueprint $table) {
            $table->id();
            $table->decimal('price')->nullable();
            $table->decimal('price_custom')->nullable();
            $table->foreignId('dish_id')->nullable();
            $table->foreignId('branch_id')->nullable();
            $table->foreign('dish_id')
                ->references('id')
                ->on('dishes')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('branch_id')
                ->references('id')
                ->on('branches')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('directory_dishes');
    }
}
