<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDishIngredientsTable extends Migration
{
    public function up()
    {
        Schema::create('dish_ingredients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('dish_id')->nullable();
            $table->foreignId('ingredient_id')->nullable();
            $table->foreignId('semis_id')->nullable();
            $table->float('count', 8, 5)->nullable();
            $table->float('brutto', 8, 5)->nullable();
            $table->float('netto', 8, 5)->nullable();
            $table->boolean('losses_cleaning_checked')->default(false);
            $table->boolean('losses_frying_checked')->default(false);
            $table->boolean('losses_cooking_checked')->default(false);
            $table->boolean('losses_baking_checked')->default(false);
            $table->boolean('losses_stew_checked')->default(false);
        });
    }

    public function down()
    {
        Schema::dropIfExists('dish_ingredients');
    }
}
