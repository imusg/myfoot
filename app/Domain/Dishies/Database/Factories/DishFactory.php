<?php

namespace App\Domain\Dishies\Database\Factories;

use App\Domain\Dishies\Entities\Dish;
use App\Infrastructure\Abstracts\ModelFactory;

class DishFactory extends ModelFactory
{
    protected string $model = Dish::class;

    public function states()
    {
    }

    public function fields(): array
    {
        return [
            'title' => $this->faker->lexify(),
            'meal_id' => 1
        ];
    }
}
