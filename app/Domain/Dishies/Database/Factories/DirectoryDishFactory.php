<?php

namespace App\Domain\Dishies\Database\Factories;

use App\Domain\Dishies\Entities\DirectoryDish;
use App\Infrastructure\Abstracts\ModelFactory;

class DirectoryDishFactory extends ModelFactory
{
    protected string $model = DirectoryDish::class;

    public function fields(): array
    {
        return [];
    }

    public function states()
    {
    }
}
