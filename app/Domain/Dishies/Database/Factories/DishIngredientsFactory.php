<?php

namespace App\Domain\Dishies\Database\Factories;

use App\Domain\Dishies\Entities\DishIngredient;
use App\Infrastructure\Abstracts\ModelFactory;

class DishIngredientsFactory extends ModelFactory
{
    protected string $model = DishIngredient::class;

    public function fields(): array
    {
        return [
            'dish_id' => $this->faker->numberBetween(1, 50),
            'ingredient_id' => $this->faker->numberBetween(1, 50)
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
