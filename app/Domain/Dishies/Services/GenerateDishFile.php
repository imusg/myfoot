<?php

namespace App\Domain\Dishies\Services;

use Illuminate\Support\Carbon;
use App\Domain\Dishies\Entities\Dish;
use PhpOffice\PhpWord\TemplateProcessor;
use Storage;

class GenerateDishFile
{

    public function generateFile(int $id)
    {
        $dish = Dish::find($id);
        $generalData = [
            'calories' => round($dish->calories, 3),
            'proteins' => round($dish->proteins, 3),
            'fats' => round($dish->fats, 3),
            'carbohydrates' => round($dish->carbohydrates, 3),
            'netto' => ($dish->netto*1000)." гр.",
            'category' => $dish->meal ? $dish->meal->title : '-',
            'cooking_process' => $dish->cooking_process
        ];
        $ingredients = $dish->ingredients->map(function ($item) {
            $unit = $item->units ? $item->units->title : '';
            if ($unit === 'кг') {
                return [
                    'ingredientName' => $item->title,
                    'ingredientBrutto' => $item->pivot->brutto ? ($item->pivot->brutto*1000)." ".$unit."." : '-',
                    'ingredientNetto' => ($item->pivot->netto*1000)." гр.",
                ];
            }
            return [
                'ingredientName' => $item->title,
                'ingredientBrutto' => $item->pivot->brutto ? $item->pivot->brutto." ".$unit."." : '-',
                'ingredientNetto' => $item->pivot->netto." ".$unit.".",
            ];
        });
        $path = $this->generate($dish->title, $generalData, $ingredients->toArray());
        return [
            'url' => $path
        ];
    }

    private function transliterateen($input)
    {
        $gost = array(
            "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
            "е"=>"e", "ё"=>"yo","ж"=>"j","з"=>"z","и"=>"i",
            "й"=>"i","к"=>"k","л"=>"l", "м"=>"m","н"=>"n",
            "о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t",
            "у"=>"y","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
            "ш"=>"sh","щ"=>"sh","ы"=>"i","э"=>"e","ю"=>"u",
            "я"=>"ya",
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
            "Е"=>"E","Ё"=>"Yo","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"I","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"Y","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
            "Ш"=>"Sh","Щ"=>"Sh","Ы"=>"I","Э"=>"E","Ю"=>"U",
            "Я"=>"Ya",
            "ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"",
            "ї"=>"j","і"=>"i","ґ"=>"g","є"=>"ye",
            "Ї"=>"J","І"=>"I","Ґ"=>"G","Є"=>"YE"
            );
            return strtr($input, $gost);
    }

    private function generate(string $technicalCardName, array $generalData, array $ingredients)
    {
        $name = explode(' ', $technicalCardName);
        foreach ($name as $key => $n) {
            $name[$key] = $this->transliterateen($n);
        }
        $name = implode('_', $name);

        $templateProcessor = new TemplateProcessor(storage_path('app/templates/template_technical_card.docx'));
        $date = Carbon::now()->format('d.m.Y-H:i');
        $path = "technical_card_{$name}_{$date}.docx";

        $templateProcessor->setValue('name', $technicalCardName);
        $templateProcessor->setValue('cllories', $generalData['calories']);
        $templateProcessor->setValue('proteins', $generalData['proteins']);
        $templateProcessor->setValue('carbohydrates', $generalData['carbohydrates']);
        $templateProcessor->setValue('fats', $generalData['fats']);
        $templateProcessor->setValue('netto', $generalData['netto']);
        $templateProcessor->setValue('cookingProcess', $generalData['cooking_process']);
        $templateProcessor->setValue('category', $generalData['category']);

        $templateProcessor->cloneRowAndSetValues('ingredientName', $ingredients);

        $file = $templateProcessor->save();
        Storage::disk('minio')->put("files/technicalCards/$path", file_get_contents($file));
        return Storage::disk('minio')->url("files/technicalCards/$path");
    }
}
