<?php

namespace App\Domain\Manager\Database\Factories;

use App\Domain\Manager\Entities\Task;
use App\Infrastructure\Abstracts\ModelFactory;
use Carbon\Carbon;

class TaskFactory extends ModelFactory
{
    protected string $model = Task::class;

    public function fields(): array
    {
        return [
            'description' => $this->faker->realText(),
            'client_id' => 1,
            'user_id' => 1,
            'type_id' => 1,
            'time' => $this->faker->time('H:i'),
            'date' => $this->faker->dateTimeBetween(Carbon::now(), Carbon::now()->addDays(10))->format('Y-m-d')
        ];
    }

    public function states()
    {
    }

}
