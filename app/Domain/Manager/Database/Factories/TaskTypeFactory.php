<?php

namespace App\Domain\Manager\Database\Factories;

use App\Domain\Manager\Entities\TaskType;
use App\Infrastructure\Abstracts\ModelFactory;

class TaskTypeFactory extends ModelFactory
{
    protected string $model = TaskType::class;

    public function fields(): array
    {
        return [
            'title' => $this->faker->text(10),
        ];
    }

    public function states()
    {
    }

}
