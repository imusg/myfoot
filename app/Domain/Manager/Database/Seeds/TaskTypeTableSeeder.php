<?php

namespace App\Domain\Manager\Database\Seeds;

use App\Domain\Manager\Entities\TaskType;
use Illuminate\Database\Seeder;

class TaskTypeTableSeeder extends Seeder
{
    public function run()
    {
        factory(TaskType::class, 2)->create();
    }
}
