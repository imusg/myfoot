<?php

namespace App\Domain\Manager\Database\Seeds;

use App\Domain\Manager\Entities\Task;
use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    public function run()
    {
        factory(Task::class, 10)->create();
    }
}
