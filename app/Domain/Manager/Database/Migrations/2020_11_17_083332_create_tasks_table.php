<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();

            $table->text('description');
            $table->boolean('state')->default(false);
            $table->text('desc_state')->nullable();

            $table->foreignId('client_id');
            $table->foreignId('user_id');
            $table->foreignId('type_id');

            $table->time('time');
            $table->date('date');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('type_id')
                ->references('id')
                ->on('task_types')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
