<?php

namespace App\Domain\Manager\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ManagerTaskTypeResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title
        ];
    }
}
