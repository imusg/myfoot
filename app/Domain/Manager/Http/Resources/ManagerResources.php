<?php


namespace App\Domain\Manager\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class ManagerResources extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
