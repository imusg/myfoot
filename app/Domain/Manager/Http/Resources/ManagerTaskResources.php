<?php

namespace App\Domain\Manager\Http\Resources;

use App\Domain\Clients\Http\Resources\ClientResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ManagerTaskResources extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'state' => $this->state === 1,
            'desc_state' => $this->desc_state,
            'date' => $this->date,
            'time' => $this->time ? Carbon::make($this->time)->format('H:i') : $this->time,
            'client' => ClientResource::make($this->client),
            'manager' => ManagerResources::make($this->manager),
            'user' => $this->user,
            'type' => ManagerTaskTypeResource::make($this->type),
        ];
    }
}
