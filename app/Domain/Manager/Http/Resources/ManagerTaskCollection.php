<?php

namespace App\Domain\Manager\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ManagerTaskCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
           'data' => ManagerTaskResources::collection($this->collection)
        ];
    }
}
