<?php


namespace App\Domain\Manager\Http\Controllers;

use App\Domain\Manager\Contracts\ManagerTaskRepository;
use App\Domain\Manager\Http\Requests\ManagerTaskStoreRequest;
use App\Domain\Manager\Http\Requests\ManagerTaskUpdateRequest;
use App\Domain\Manager\Http\Resources\ManagerTaskCollection;
use App\Domain\Manager\Http\Resources\ManagerTaskResources;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ManagerTaskController extends Controller
{
    private ManagerTaskRepository $managerTaskRepository;

    public function __construct(
        ManagerTaskRepository $managerTaskRepository
    ) {
        $this->managerTaskRepository = $managerTaskRepository;
        $this->resourceCollection = ManagerTaskCollection::class;
        $this->resourceItem = ManagerTaskResources::class;
    }

    /**
     * @OA\Get(
     * path="/tasks",
     * summary="Получение задач менеджера",
     * description="Получение задач менеджера",
     * operationId="indexTasks",
     * tags={"Manager"},
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех задач менеджера",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Task"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *        )
     *  )
     * );
     */
    public function index()
    {
        $collection = $this->managerTaskRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    /**1
     * @OA\Get (
     * path="/tasks/{task}",
     * summary="Получение задачи менеджера",
     * description="Обновление задачи менеджера",
     * operationId="getOneTasks",
     * tags={"Manager"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="task",
     *  in="path",
     *  required=true,
     *  description="Task id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешное получение задачи менеджера",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Task"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $response = $this->managerTaskRepository->findOneById((int) $id);
        return $this->respondWithItem($response);
    }

    /**
     * @OA\Post(
     * path="/tasks",
     * summary="Создание задач менеджера",
     * description="Добавление задач менеджера",
     * operationId="storeTasks",
     * tags={"Manager"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для создания задач менеджера",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/TaskRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление задач менеджера",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Task"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  ManagerTaskStoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ManagerTaskStoreRequest $request)
    {
        $data = $request->validated();
        $data['branch_id'] = request()->user()->branch_id;
        $data['user_creator_id'] = request()->user()->id;
        $response = $this->managerTaskRepository->store($data);
        return $this->respondWithCustomData($response, Response::HTTP_CREATED);
    }

    /**
     * @OA\Put(
     * path="/tasks/{task}",
     * summary="Обновление задач менеджера",
     * description="Обновление задач менеджера",
     * operationId="updateTasks",
     * tags={"Manager"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="task",
     *  in="path",
     *  required=true,
     *  description="Task id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для обновления задач менеджера",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/TaskRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная обновление задач менеджера",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Task"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  ManagerTaskUpdateRequest  $request
     * @param $id
     * @return mixed
     */
    public function update(ManagerTaskUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->managerTaskRepository->update(
            $this->managerTaskRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    /**
     * @OA\Delete (
     * path="/tasks/{task}",
     * summary="Удаление задач менеджера",
     * description="Удаление задач менеджера",
     * operationId="deleteTasks",
     * tags={"Manager"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="task",
     *  in="path",
     *  required=true,
     *  description="Task id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная удаление задач менеджера",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            @OA\Property(
     *              property="deleted",
     *              type="boolean",
     *              example="true"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $response = $this->managerTaskRepository->destroy(
            $this->managerTaskRepository->findOneById($id)
        );
        return $this->respondWithCustomData([
            'deleted' => $response
        ]);
    }

    public function getTaskTypes()
    {
        return $this->respondWithCustomData($this->managerTaskRepository->getTaskTypes());
    }
}
