<?php


namespace App\Domain\Manager\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ManagerTaskUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'description' => 'string|nullable',
            'date' => 'string|nullable',
            'time' => 'string|nullable',
            'desc_state' => 'string|nullable',
            'state' => 'boolean',
            'client_id' => 'exists:clients,id',
            'user_id' => 'exists:users,id',
            'type_id' => 'exists:task_types,id',
        ];
    }

}
