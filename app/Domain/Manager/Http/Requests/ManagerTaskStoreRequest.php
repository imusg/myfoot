<?php


namespace App\Domain\Manager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema (
 *    schema="TaskRequest",
 *    type="object",
 *    @OA\Property (
 *      property="description",
 *      type="string",
 *      description="Описание задачи",
 *      example="Сделать звонок клиенту",
 *    ),
 *    @OA\Property (
 *      property="date",
 *      type="sting",
 *      description="Дата на которую сделанна задача",
 *      example="2020-12-20",
 *    ),
 *    @OA\Property (
 *      property="time",
 *      type="string",
 *      description="Время на которое созданна задача",
 *      example="16:18",
 *    ),
 *    @OA\Property (
 *      property="client_id",
 *      type="integer",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="user_id",
 *      type="integer",
 *      description="ID менеджера",
 *      example="1"
 *    ),
 *     @OA\Property (
 *      property="type_id",
 *      type="integer",
 *      description="ID Type",
 *      example="1"
 *    )
 * )
 * */
class ManagerTaskStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'description' => 'string|nullable',
            'date' => 'date_format:Y-m-d|nullable',
            'time' => 'date_format:H:i|nullable',
            'client_id' => 'exists:clients,id|nullable',
            'user_id' => 'exists:users,id|nullable',
            'type_id' => 'exists:task_types,id|nullable',
        ];
    }
}
