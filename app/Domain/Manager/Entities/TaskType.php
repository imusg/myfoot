<?php

namespace App\Domain\Manager\Entities;

use Illuminate\Database\Eloquent\Model;

class TaskType extends Model
{
    protected $fillable = ['title'];
}
