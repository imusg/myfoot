<?php

namespace App\Domain\Manager\Entities;

use App\Domain\Clients\Entities\Client;
use App\Domain\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'description',
        'state',
        'desc_state',
        'time',
        'date',
        'client_id',
        'user_id',
        'type_id',
        'branch_id',
        'user_creator_id'
    ];

    public function type()
    {
        return $this->hasOne(TaskType::class, 'id', 'type_id');
    }

    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_creator_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }
}
