<?php

namespace App\Domain\Manager\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface ManagerTaskRepository extends BaseRepository
{
}
