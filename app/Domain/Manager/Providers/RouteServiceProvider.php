<?php


namespace App\Domain\Manager\Providers;

use App\Domain\Manager\Http\Controllers\ManagerTaskController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
            });
    }

    private function mapRoutesWhenManager($router): void
    {
        $router->apiResource('tasks', ManagerTaskController::class)
            ->names('tasks')
            ->only(['index','store','update','destroy', 'show']);

        $router->get('task/types', [ManagerTaskController::class, 'getTaskTypes']);
    }
}
