<?php

namespace App\Domain\Manager\Providers;

use App\Domain\Manager\Database\Factories\TaskFactory;
use App\Domain\Manager\Database\Factories\TaskTypeFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'manager';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
    ];

    protected array $policies = [];

    protected array $factories = [
        TaskFactory::class,
        TaskTypeFactory::class
    ];
}
