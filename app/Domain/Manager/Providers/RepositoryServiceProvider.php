<?php

namespace App\Domain\Manager\Providers;

use App\Domain\Manager\Contracts\ManagerTaskRepository;
use App\Domain\Manager\Entities\Task;
use App\Domain\Manager\Repositories\EloquentManagerTaskRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(ManagerTaskRepository::class, function () {
            return new EloquentManagerTaskRepository(new Task());
        });
    }

    public function provides(): array
    {
        return [
            ManagerTaskRepository::class
        ];
    }
}
