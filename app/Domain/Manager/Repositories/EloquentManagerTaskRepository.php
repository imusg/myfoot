<?php

namespace App\Domain\Manager\Repositories;

use Carbon\Carbon;
use App\Domain\Manager\Entities\Task;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Manager\Entities\TaskType;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Manager\Contracts\ManagerTaskRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentManagerTaskRepository extends EloquentRepository implements ManagerTaskRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'description',
        'state',
        'desc_state',
        'client_id',
        'user_id',
        'type_id',
        'date',
        'time',
        'user_creator_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'id',
        'data.day',
        'end',
        'start',
        'current'
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'data',
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $userId = request()->userId;
        $date = request()->date;
        $deadline = request()->deadline;
        $created = request()->created;
        $branch = request()->user()->branch_id;
        $clientId = request()->clientId;

        return QueryBuilder::for(Task::class)
            ->when($clientId, function ($query) use ($clientId) {
                return $query->where('client_id', $clientId);
            })
            ->when($date, function ($query) use ($date) {
                return $query->whereBetween('date', [
                $date,
                Carbon::make($date)->addDays(2)
                    ->format('Y-m-d')
                ]);
            })
            ->when($created, function ($query) use ($created) {
                $arrCreated = explode(',', $created);
                return $query->whereBetween('created_at', [
                    $arrCreated[0],
                    $arrCreated[1]
                ]);
            })
            ->when($deadline, function ($query) use ($deadline){
                $arrDeadline = explode(',', $deadline);
                return $query->whereBetween('date', [
                    $arrDeadline[0],
                    $arrDeadline[1]
                ]);
            })
            ->when($branch, function ($query) use ($branch) {
                return $query->where('branch_id', $branch);
            })
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate(1000);
    }

    public function findOneById(int $id): Model
    {
        $this->with([
            'client',
            'type',
            'manager',
            'client.address',
            'client.orders',
            'client.histories'
        ]);
        return parent::findOneById($id);
    }

    public function getTaskTypes()
    {
        return QueryBuilder::for(TaskType::class)->get();
    }
}
