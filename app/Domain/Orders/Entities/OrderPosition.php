<?php

namespace App\Domain\Orders\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Cycles\Entities\CycleData;
use App\Domain\Kitchens\Entities\Kitchen;
use App\Domain\Addition\Entities\Addition;

class OrderPosition extends Model
{
    protected $fillable = ['order_id', 'addition_id', 'quantity', 'cycle_data_id', 'price', 'sale'];

    public $timestamps = false;

    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function addition()
    {
        return $this->hasOne(Addition::class, 'id', 'addition_id');
    }

    public function replace()
    {
        return $this->hasMany(OrderPositionReplaceIngredient::class, 'position_id', 'id');
    }

    public function data()
    {
        return $this->hasOne(CycleData::class, 'id', 'cycle_data_id');
    }

    public function positionsInKitchens()
    {
        return $this->hasMany(Kitchen::class, 'position_id', 'id');
    }
}
