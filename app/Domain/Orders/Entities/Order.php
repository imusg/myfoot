<?php

namespace App\Domain\Orders\Entities;

use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Couriers\Entities\Courier;
use App\Domain\Kitchens\Entities\Delivery;

class Order extends Model
{
    protected $fillable = [
        'amount',
        'paid',
        'date',
        'meal_id',
        'branch_id',
        'channel',
        'history',
        'frozen',
        'sale',
        'sumSale',
        'client_id'
    ];

    protected $attributes = ['paid' => false ];


    public function positions()
    {
        return $this->hasMany(OrderPosition::class, 'order_id', 'id');
    }

    public function delivery()
    {
        return $this->hasMany(Delivery::class, 'order_id', 'id');
    }

    public function clients()
    {
        return $this->belongsToMany(
            Client::class,
            'client_orders',
            'order_id',
            'client_id'
        );
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function couriers()
    {
        return $this->belongsToMany(
            Courier::class
        );
    }


    /* Local Scopes */

    public function findOrderWhereDateHasClient($date, $clientId)
    {
        return $this->where([
            ['date', '=', $date],
            ['client_id', '=', $clientId]
        ]);
    }

    public function getPositionsByCycleDataId(int $cycleDataId)
    {
        return $this->positions()->where('cycle_data_id', $cycleDataId);
    }

    public function findOrderByDateAndBranch(int $branchId, string $date)
    {
        return $this->where([
            ['branch_id', '=', $branchId],
            ['date', '=', $date]
        ]);
    }
}
