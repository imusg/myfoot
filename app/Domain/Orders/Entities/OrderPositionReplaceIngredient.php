<?php

namespace App\Domain\Orders\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Orders\Entities\OrderPosition;
use App\Domain\Ingredients\Entities\Ingredient;

class OrderPositionReplaceIngredient extends Model
{
    protected $fillable = [
        'before_ingredient_id',
        'after_ingredient_id',
        'position_id',
    ];

    protected $table = 'order_position_replace_ingredient';
    public $timestamps = false;

    public function position()
    {
        return $this->hasOne(OrderPosition::class, 'position_id', 'id');
    }

    public function afterIngredient()
    {
        return $this->hasOne(Ingredient::class, 'after_ingredient_id', 'id');
    }

    public function beforeIngredient()
    {
        return $this->hasOne(Ingredient::class, 'before_ingredient_id', 'id');
    }
}
