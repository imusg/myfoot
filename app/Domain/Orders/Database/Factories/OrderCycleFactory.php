<?php

namespace App\Domain\Orders\Database\Factories;

use App\Domain\Orders\Entities\OrderCycle;
use App\Infrastructure\Abstracts\ModelFactory;

class OrderCycleFactory extends ModelFactory
{
    protected string $model = OrderCycle::class;

    public function fields()
    {
        return [
            'day' => $this->faker->numberBetween(1, 7),
            'week' => $this->faker->numberBetween(1, 52),
            'cycle_id' => $this->faker->numberBetween(1, 10),
            'directory_dish_id' => $this->faker->numberBetween(1, 50),
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
