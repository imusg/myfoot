<?php

namespace App\Domain\Orders\Database\Factories;

use App\Domain\Orders\Entities\Order;
use App\Infrastructure\Abstracts\ModelFactory;
use Carbon\Carbon;

class OrderFactory extends ModelFactory
{
    protected string $model = Order::class;

    public function fields(): array
    {
        return [
            'amount' => $this->faker->randomNumber(1),
            'paid' => $this->faker->boolean,
            'day' => $this->faker->numberBetween(1,7),
            'week' => $this->faker->numberBetween(1,51),
            'date' => $this->faker->dateTimeBetween(Carbon::now(), Carbon::now()->addDays(3)),
            'cycle_data_id' => $this->faker->numberBetween(1, 100)
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
