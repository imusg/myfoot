<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderCyclesTable extends Migration
{
    public function up()
    {
        Schema::create('order_cycles', function (Blueprint $table) {
            $table->id();
            $table->integer('day');
            $table->integer('week');
            $table->foreignId('cycle_id');
            $table->foreign('cycle_id')
                ->references('id')
                ->on('cycles')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreignId('directory_dish_id');
            $table->foreign('directory_dish_id')
                ->references('id')
                ->on('directory_dishes')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_cycles');
    }
}
