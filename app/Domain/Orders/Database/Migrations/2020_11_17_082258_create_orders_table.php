<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount');
            $table->boolean('paid');
            $table->boolean('history')->default(false);
            $table->date('date');
            $table->string('channel')->nullable();
            $table->foreignId('branch_id')->nullable();
            $table->foreignId('meal_id')->nullable();
            $table->foreignId('cycle_data_id');
            $table->foreign('cycle_data_id')
                ->references('id')
                ->on('cycle_data')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('branch_id')
                ->references('id')
                ->on('branches')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('meal_id')
                ->references('id')
                ->on('meals')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
}
