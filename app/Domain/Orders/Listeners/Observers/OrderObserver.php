<?php
namespace App\Domain\Orders\Listeners\Observers;

use App\Domain\Orders\Entities\Order;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Clients\Entities\ClientTransaction;

class OrderObserver
{
    public function creating(Order $order)
    {
        $order->branch_id = request()->user()->branch_id;
    }

    public function created(Order $order)
    {
        if ($order->isDirty('paid')) {
            ClientOrder::create([
                'order_id' => $order->id,
                'client_id' => request()->client_id
            ]);
        }

        ClientOrder::withoutEvents(function () use ($order) {
            ClientOrder::create([
                'order_id' => $order->id,
                'client_id' => request()->client_id
            ]);
        });
    }

    public function deleting(Order $order)
    {
        $clientOrder = ClientOrder::where('order_id', $order->id)->first();
        $balance = $clientOrder->client->balance;
        $amount = $clientOrder->order->amount;
        $clientOrder->client()->update([
            'balance' => $balance + $amount
        ]);
        ClientTransaction::firstOrcreate([
            'type' => 'Заказ',
            'order_id' => $order->id,
            'value' => -$amount,
            'client_id' =>  $clientOrder->client->id
        ]);
    }

    public function updating(Order $order)
    {
        if ($order->isDirty('amount')) {
            $clientOrder = ClientOrder::where('order_id', $order->id)->firstOrFail();
            $clientOrder->client()->update([
                'balance' => $clientOrder->client->balance + $order->getOriginal('amount') - $order->amount,
            ]);
            ClientTransaction::firstOrcreate([
                'type' => 'Заказ',
                'order_id' => $order->id,
                'value' => $order->amount,
                'client_id' =>  $clientOrder->client->id
            ]);
        }
    }
}
