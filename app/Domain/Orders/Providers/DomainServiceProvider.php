<?php
namespace App\Domain\Orders\Providers;

use App\Domain\Orders\Database\Factories\OrderCycleFactory;
use App\Domain\Orders\Database\Factories\OrderFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'orders';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        OrderCycleFactory::class,
        OrderFactory::class
    ];
}
