<?php

namespace App\Domain\Orders\Providers;

use App\Domain\Orders\Entities\Order;
use App\Domain\Orders\Listeners\Observers\OrderObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        Order::observe(OrderObserver::class);
    }
}
