<?php

namespace App\Domain\Orders\Providers;

use Illuminate\Routing\Router;
use App\Domain\Orders\Http\Controllers\OrderController;
use App\Domain\Clients\Http\Controllers\ClientsController;
use App\Domain\Orders\Http\Controllers\StatiscticController;
use App\Domain\Clients\Http\Controllers\ClientOrderController;
use App\Domain\Clients\Http\Controllers\ClientAddressController;
use App\Domain\Clients\Http\Controllers\ClientHistoryController;
use App\Domain\Clients\Http\Controllers\ClientMessagesController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->post('orders', [OrderController::class, 'store'])
            ->name('orders.store');

        $router->get('order', [OrderController::class, 'getOrdersDay']);

        $router->get('orders', [OrderController::class, 'index'])
            ->name('orders.index');

        $router->put('orders/{id}', [OrderController::class, 'update'])
            ->name('orders.update');

        $router->delete('orders/{id}', [OrderController::class, 'destroy'])
            ->name('orders.destroy');

        $router->get('orders/history', [OrderController::class, 'getOrdersHistory']);

        $router->get('orders/longer', [OrderController::class, 'getOrdersLonger'])
            ->name('orders.longer.sum');

        $router->get('statistic', [StatiscticController::class, 'index']);

        $router->post('orders/ingredient/replace', [OrderController::class, 'replaceIngredient']);

        $router->delete('orders/{positionId}/ingredient/replace/{after}', [OrderController::class, 'destroyIngredient']);

        $router->post('orders/clear', [OrderController::class, 'clearOrder']);
    }
}
