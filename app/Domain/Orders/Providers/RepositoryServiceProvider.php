<?php

namespace App\Domain\Orders\Providers;

use App\Domain\Orders\Contracts\OrderRepository;
use App\Domain\Orders\Entities\Order;
use App\Domain\Orders\Repositories\EloquentOrderRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(OrderRepository::class, function() {
            return new EloquentOrderRepository(new Order());
        });
    }

    public function provides(): array
    {
        return [
            OrderRepository::class
        ];
    }
}
