<?php

namespace App\Domain\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Orders\Services\StatisticService;

class StatiscticController extends Controller
{
    protected $statisticService;

    public function __construct(StatisticService $statisticService)
    {
        $this->statisticService = $statisticService;
    }

    public function index()
    {
        return $this->respondWithCustomData($this->statisticService->findStat());
    }
}
