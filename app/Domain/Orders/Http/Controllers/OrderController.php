<?php

namespace App\Domain\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Orders\Services\OrderService;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Orders\Contracts\OrderRepository;
use App\Domain\Orders\Http\Resources\OrderResource;
use App\Domain\Orders\Http\Resources\OrderCollection;
use App\Domain\Orders\Http\Requests\OrderStoreRequest;
use App\Domain\Orders\Http\Requests\OrderUpdateRequest;
use App\Domain\Orders\Http\Requests\OrderReplaceStoreRequest;

class OrderController extends Controller
{
    private OrderRepository $orderRepository;
    private OrderService $orderService;

    public function __construct(OrderRepository $orderRepository, OrderService $orderService)
    {
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
        $this->resourceCollection = OrderCollection::class;
        $this->resourceItem = OrderResource::class;
    }

    public function index()
    {
        if (request()->mode) {
            $response = $this->orderRepository->findByClientHistory(request()->client);
        } elseif (request()->date) {
            $response = $this->orderRepository->findAllByDate(request()->date);
        } else {
            $response = $this->orderRepository->findAll(request()->client);
        }
        return $this->respondWithCustomData($response);
    }

    public function store(OrderStoreRequest $request)
    {
        $data = $request->validated();
        $response = $this->orderService->create($data);
        return $this->respondWithCustomData($response, Response::HTTP_CREATED);
    }

    public function update(OrderUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $model = $this->orderRepository->findOneById($id);
        $response = $this->orderRepository->update($model, $data);
        return $this->respondWithItem($response);
    }

    public function destroy($id)
    {
        $model = $this->orderRepository->findOneById($id);
        $response = $this->orderRepository->destroy($model);
        return $this->respondWithCustomData([
            'deleted' => $response
        ]);
    }

    public function getOrdersHistory()
    {
        $client_id = !request()->client_id ? '' : request()->client_id;
        $response = $this->orderRepository->findByClientHistory($client_id);
        return $this->respondWithCollection($response);
    }

    public function getOrdersDay(Request $request)
    {
        $resp = $this->orderRepository->findOrdersDay($request->input('date'));
        return $this->respondWithCustomData($resp);
    }

    public function getOrdersLonger()
    {
        if (request()->action === 'price') {
            $resp = $this->orderRepository
                ->getPriceLonger(
                    request()->date,
                    request()->sale,
                    request()->from,
                    json_decode(request()->positions),
                    json_decode(request()->additions)
                );
            return $this->respondWithCustomData(['sum' => $resp]);
        }
        return $this->respondWithNoContent();
    }

    public function replaceIngredient(OrderReplaceStoreRequest $orderReplaceStoreRequest)
    {
        $resp = $this->orderService->replaceIngredient($orderReplaceStoreRequest->validated());
        return $this->respondWithCustomData($resp);
    }

    public function destroyIngredient($positionId, $before)
    {
        $resp = $this->orderService->destroyReplaceIngredient($before, $positionId);
        return $this->respondWithCustomData($resp);
    }

    public function clearOrder(Request $request)
    {
        $resp = $this->orderService->clearOrder($request->client_id);
        return $this->respondWithCustomData($resp);
    }
}
