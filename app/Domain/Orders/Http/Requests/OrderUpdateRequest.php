<?php

namespace App\Domain\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'paid' => 'boolean',
            'col' => 'nullable',
            'meal_id' => 'exists:meals,id',
        ];
    }
}
