<?php

namespace App\Domain\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'date' => 'required|date_format:Y-m-d',
            'client_id' => 'required|exists:clients,id',
            'additions' => 'array',
            'positions' => 'array',
            'sale' => 'nullable',
            'longer' => 'nullable'
        ];
    }
}
