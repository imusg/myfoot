<?php

namespace App\Domain\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderReplaceStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'before_ingredient_id' => 'nullable',
            'after_ingredient_id' => 'nullable',
            'position_id' => 'nullable',
        ];
    }
}
