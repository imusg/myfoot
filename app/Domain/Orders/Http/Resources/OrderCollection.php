<?php

namespace App\Domain\Orders\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Domain\Cycles\Http\Resources\CycleDataResource;

class OrderCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return [
            'data' => OrderResource::collection($this->collection)
        ];
    }
}
