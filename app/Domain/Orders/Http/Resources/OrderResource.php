<?php

namespace App\Domain\Orders\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Cycles\Http\Resources\CycleDataResource;

class OrderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'paid' => $this->paid,
            'date' => $this->date,
            'channel' => $this->channel,
            'current' => Carbon::now()->format('Y-m-d') === $this->date,
            'data' => CycleDataResource::make($this->data),
        ];
    }
}
