<?php
namespace App\Domain\Orders\Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Entities\User;
use App\Domain\Clients\Entities\Client;

class OrdersCreateControllerTest extends TestCase
{
    protected $clientId = 2603;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(User::find(10));
    }

    /*
        [ ] Получение всех заказов клиента
        [ ] Получение всех заказов клиента за текущий день
        [ ] Получение истории заказов клиента
        [ ] Созадание заказа
        [ ] Обновление заказа
        [ ] Получение заказов за текущий день
        [ ] Получение суммы пролонгации
    */

    public function testGetSumLonger(): void
    {
        $action = 'price';
        $date = '2021-07-03';
        $from = '2021-06-28';
        $sale = null;
        $position = '[{"cycleDataId":4179,"quantity":1,"price":220,"sale":null}]';
        $additions = '[]';
        $response = $this->get(route('orders.longer.sum', [
            'action' => $action,
            'date' => $date,
            'sale' => $sale,
            'from' => $from,
            'additions' => $additions,
            'positions' => $position
        ]));
        $data = $response->json('data');
        dd($data);
    }
}
