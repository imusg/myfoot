<?php
namespace App\Domain\Orders\Repositories;

use Exception;
use Carbon\Carbon;
use App\Domain\Cycles\Entities\Cycle;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Orders\Entities\Order;
use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Cycles\Entities\CycleData;
use App\Domain\Addition\Entities\Addition;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Orders\Contracts\OrderRepository;
use App\Domain\Clients\Entities\ClientTransaction;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Clients\Entities\ClientExcludeIngredient;

class EloquentOrderRepository extends EloquentRepository implements OrderRepository
{
    public function storeWithGuides(array $data): bool
    {
        $client = Client::find($data['client_id']);

        $cycle = Cycle::where('current', 1)->where('branch_id', $client->branch_id)->first();
        if (!$cycle) {
            throw new Exception('Cycle not found', 404);
        }

        $date = [
            'date' => $data['date'],
            'cycle_id' => $cycle->id,
        ];

        foreach ($data['guides'] as $item) {
            $guide = DirectoryDish::find($item['id']);

            $cycleData = CycleData::date($date)
                ->where('directory_dish_id', $guide->id)
                ->first();

            if ($item['action'] === 'save') {
                $storeData = [
                    'amount' => $guide->price * $item['col'],
                    'date' => $data['date'],
                    'meal_id' => $guide->dish->meal_id,
                    'branch_id' => request()->user()->branch_id,
                    'cycle_data_id' => $cycleData->id,
                    'col' => isset($item['col']) ? $item['col'] : 0
                ];

                $order = $this->store($storeData);

                ClientOrder::create([
                    'order_id' => $order->id,
                    'client_id' => $data['client_id']
                ]);

                $client = Client::find($data['client_id']);

                $client->update([
                    'balance' => $client->balance - $guide->price * $item['col']
                ]);

                ClientTransaction::create([
                    'client_id' => $client->id,
                    'value' => -($guide->price * $item['col']),
                    'order_id' => $order->id
                ]);
            } else {
                $order = $this->model->where('cycle_data_id', $cycleData->id)->first();
                ClientOrder::where('order_id', $order->id)->delete();
                $client = Client::find($data['client_id']);
                $client->update([
                    'balance' => $client->balance + $guide->price
                ]);
                ClientTransaction::where('client_id', $client->id)
                    ->where('order_id', $order->id)
                    ->delete();
                $order->delete();
            }
        }

        if ($data['longer']['check']) {
            foreach ($data['longer']['meals'] as $value) {
                $dishesId = Dish::where('meal_id', $value['id'])->get()
                    ->map(function ($item) {
                        return $item->id;
                    });
                $directoriesId = DirectoryDish::whereIn('dish_id', $dishesId)
                    ->get()
                    ->map(function ($item) {
                        return $item->id;
                    });

                $cycleData = CycleData::where('cycle_id', $cycle->id)
                    ->with(['directory', 'directory.dish', 'directory.dish.meal'])
                    ->whereBetween(
                        'date',
                        [Carbon::make($data['date'])->addDays(1)->format('Y-m-d'),
                            Carbon::make($data['longer']['date'])->format('Y-m-d')]
                    )
                    ->whereIn('directory_dish_id', $directoriesId)
                    ->get();

                $cycleData->each(function ($item) use ($value, $data) {
                    $guide = DirectoryDish::find($item['directory_dish_id']);

                    $storeData = [
                        'amount' => $guide->price * $value['col'],
                        'date' => $item->date,
                        'meal_id' => $guide->dish->meal_id,
                        'branch_id' => request()->user()->branch_id,
                        'cycle_data_id' => $item->id,
                        'col' => $value['col']
                    ];

                    $order = $this->store($storeData);

                    ClientOrder::create([
                        'order_id' => $order->id,
                        'client_id' => $data['client_id']
                    ]);
                });
            }
        }

        return true;
    }

    public function findAllByDate($date)
    {
        $orders = Order::where('date', $date)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->with(['positions.data.directory.dish', 'clients', 'couriers'])
            ->get();
        if ($orders->count()) {
            return $orders->map(function ($item) {
                $addr = count($item->clients) ? $item->clients[0]->address->where('isMain', 1) : collect();
                return [
                    'client_id' =>  $item->clients->count() ? $item->clients[0]['id'] : null,
                    'client' => $item->clients->count() ? $item->clients[0]['name'] : null,
                    'address' => $addr->isNotEmpty() ? $addr->first() : null,
                    'district' => $addr->isNotEmpty() ? $addr->first()->districts : null,
                    'timeDelivery' => count($item->clients) ? $item->clients[0]->timeDelivery : '-',
                    'date' => $item['date'],
                    'orderId' => $item['id'],
                    'courierId' => count($item['couriers']) ? $item['couriers'][0]->id: null,
                    'position' => $item->positions->map(function ($val) {
                        return [
                            'quantity' => $val['quantity'],
                            'title' => isset($val['data']['directory']['dish']['title']) ? $val['data']['directory']['dish']['title'] : $val['addition']['title'],
                        ];
                    })
                ];
            })->unique('client_id')->filter(function ($item) {
                return $item['client'];
            })->values()->all();
        }
        return [];
    }

    public function update(Model $model, array $data): Model
    {
        $price = $model->data->directory->price;
        $data['amount'] = $price * $data['col'];
        $clientOrder = ClientOrder::where('order_id', $model->id)->first();
        $client = Client::find($clientOrder->client_id);
        $client->update([
            'balance' => $client->balance + $model->amount - $data['amount']
        ]);
        return parent::update($model, $data);
    }

    public function destroy($model): bool
    {
        $client_id = ClientOrder::where('order_id', $model->id)->first()->client_id;
        $client = Client::find($client_id);
        $client->update([
            'balance' => $client->balance + $model->amount
        ]);
        return parent::destroy($model);
    }

    public function findByDate(string $date, string $client_id)
    {
        $orderIds = ClientOrder::where('client_id', $client_id)->get()->map(function ($item) {
            return $item->order_id;
        });
        return $this->model
            ->whereIn('id', $orderIds)
            ->where('date', $date)
            ->with('data')
            ->get();
    }

    public function findByClientHistory(string $client_id)
    {
        $orderIds = ClientOrder::where('client_id', $client_id)->get()->map(function ($item) {
            return $item->order_id;
        });

        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $orders = Order::whereIn('id', $orderIds)->get()->groupBy('date');
        $keys = $orders->keys();

        $resp = collect([]);

        $keys->each(function ($key) use ($orders, $resp) {
            $resp->push($orders[$key]->map(function ($item) use ($orders, $key) {
                return [
                    'displayDate' => Carbon::parse($key)->locale('ru')->isoFormat('D MMMM YYYY'),
                    'date' => $key,
                    'id' => $item->id,
                    'count' => $item->positions->sum('quantity'),
                    'sum' => $item->amount,
                    'current' => Carbon::now()->format('Y-m-d') === $key
                ];
            })[0]);
        });

        return $resp->sortBy('date')->values()->all();
    }

    public function findAll(string $client_id)
    {
        $orders = Order::whereHas('clients', function ($query) use ($client_id) {
            return $query->where('client_id', $client_id);
        })->get()->groupBy('date');

        $keys = $orders->keys();

        $resp = collect([]);

        $keys->each(function ($key) use ($orders, $resp) {
            $resp->push($orders[$key]->map(function ($item) use ($orders, $key) {
                $content = '<div class="content-events">
                    <span class="price">'.intval($orders[$key][0]->amount).'</span>
                    <i class="el-icon-money"></i>
                    <span class="count-food">'.$orders[$key][0]->positions->count().'</span>
                    <i class="el-icon-food"></i>
                    </div>';
                $PCREpattern  =  '/\r\n|\r|\n/u';
                return [
                    'start' => $item->date,
                    'end' => $item->date,
                    'background' => true,
                    'allDay' => true,
                    'price' => intval($orders[$key][0]->amount),
                    'class' => 'food-events',
                    'content' => preg_replace($PCREpattern, '', $content)
                ];
            })[0]);
        });
        return $resp;
    }

    public function findOrdersDay($date)
    {
        $branch = request()->user()->branch_id;
        $client = Client::find(request()->client);

        $cylcleOwners = Cycle::where('branch_id', null)
            ->where('owner_cycle_id', null)
            ->select('id')
            ->get();

        $cycles = Cycle::where('branch_id', $client->branch_id)
            ->where('current', 1)
            ->whereIn('owner_cycle_id', $cylcleOwners)
            ->select('id')
            ->get();

        $orders = Order::where('date', $date)
            ->whereHas('clients', function ($query) {
                return $query->where('client_id', request()->client);
            })->first();


        $excludedIngredient = ClientExcludeIngredient::where('client_id', $client->id)->with('ingredient')->get();

        $menu = CycleData::whereIn('cycle_id', $cycles)
            ->where('date', $date)
            ->with(['directory.dish', 'directory.dish.meal', 'directory.dish.ingredients'])
            ->get()
            ->map(function ($item) use ($orders) {
                $isSelected = false;
                $order = $orders->positions->where('cycle_data_id', $item->id);
                $orderCol = $orders->positions->where('cycle_data_id', $item->id)->first();
                if ($order->isNotEmpty()) {
                    $isSelected = true;
                }
                $item['isSelected'] = $isSelected;
                if ($orderCol) {
                    $item['quantity'] = $orderCol->quantity;
                    $item['sale'] = $orderCol->sale;
                    $item['orderId'] = $orders->id;
                    $item['positionId'] = $orderCol->id;
                    $item['replace'] = $orderCol->replace;
                } else {
                    $item['quantity'] = 0;
                }
                return $item;
            });


        $menu = $menu->map(function ($item) use ($excludedIngredient) {
            $diff = collect([]);
            $excludedIngredient->each(function ($value) use ($item, $diff) {
                $key = $item->directory->dish->ingredients->search(function ($v, $key) use ($value) {
                    return $v->id === $value->ingredient_id;
                });
                if ($key !== false) {
                    $diff->push($item->directory->dish->ingredients[$key]->id);
                }
            });
            if ($diff->isNotEmpty()) {
                $ingredients = $item->directory->dish->ingredients->map(function ($v) use ($diff, $item) {
                    $ingredients = collect([]);
                    $diff->each(function ($value) use ($v, $ingredients, $item) {
                        if ($value === $v->id) {
                            $ingredients->push([
                                'id' => $v->id,
                                'title' => $v->title,
                                'exclude' => true,
                                'replace' => $item->replace ?
                                    $item->replace->where('before_ingredient_id', $v->id)->first() ?
                                        $item->replace->where('before_ingredient_id', $v->id)->first()->after_ingredient_id
                                        : null
                                    : null
                            ]);
                        } else {
                            $ingredients->push([
                                'id' => $v->id,
                                'title' => $v->title,
                                'exclude' => false,
                                'replace' =>  $item->replace ?
                                    $item->replace->where('before_ingredient_id', $v->id)->first() ?
                                        $item->replace->where('before_ingredient_id', $v->id)->first()->after_ingredient_id
                                        : null
                                    : null
                            ]);
                        }
                    });
                    return $ingredients[0];
                });
            } else {
                $ingredients = $item->directory->dish->ingredients->map(function ($v) use ($item) {
                    return [
                        'id' => $v->id,
                        'title' => $v->title,
                        'exclude' => false,
                        'replace' =>  $item->replace ?
                            $item->replace->where('before_ingredient_id', $v->id)->first() ?
                                $item->replace->where('before_ingredient_id', $v->id)->first()->after_ingredient_id
                                : null
                            : null
                    ];
                });
            }
            return [
                'title' => $item->directory->dish->title,
                'date' => $item->date,
                'col' => $item->quantity,
                'sale' => $item->sale,
                'orderId' => $item->orderId,
                'cycleDataId' => $item->id,
                'directoryId' => $item->directory->id,
                'positionId' => $item->positionId,
                'dishId' => $item->directory->dish->id,
                'branch_id' => $item->directory->branch_id,
                'image' => $item->directory->dish->image,
                'meal' => $item->directory->dish->meal ? $item->directory->dish->meal->title : 'не указано',
                'mealId' => $item->directory->dish->meal ? $item->directory->dish->meal->id : null,
                'price' => $item->directory->price,
                'isSelected' => $item->isSelected,
                'ingredients' => $ingredients
            ];
        })->where('branch_id', $client->branch_id)->values()->all();

        $additions = Addition::where('branch_id', $client->branch_id)->get()->map(function ($addition) use ($orders) {
            $orderCol = $orders->positions->where('addition_id', $addition->id)->first();
            if ($orderCol) {
                $item['quantity'] = $orderCol->quantity;
                $item['sale'] = $orderCol->sale;
                $item['orderId'] = $orders->id;
                return [
                    'title' => $addition->title,
                    'date' => $orders->date,
                    'col' => $orderCol->quantity,
                    'orderId' => $orders->id,
                    'sale' => $orderCol->sale,
                    'price' => $addition->price,
                    'id' => $addition->id,
                    'isSelected' => true
                ];
            }
            return [
                'title' => $addition->title,
                'date' => $orders->date,
                'orderId' => $orders->id,
                'price' => $addition->price,
                'id' => $addition->id,
                'isSelected' => false
            ];
        })->filter(function ($addition) {
            return $addition !== null;
        });

        return ['date' => $date, 'sale' => $orders->sale ,'positions' => $menu, 'additions' => $additions];
    }

    public function getPriceLonger($date, $sale, $from, $positions, $additions)
    {
        $additionsPrice = 0;
        $positionsPrice = 0;

        foreach ($positions as $position) {
            $cycleData = CycleData::find($position->cycleDataId);
            $mealId = $cycleData->directory->dish->meal_id;
            $cylcleOwner = Cycle::where('branch_id', null)
                ->where('owner_cycle_id', null)
                ->where('current', 1)
                ->select('id')
                ->get();

            $cycle = Cycle::whereIn('owner_cycle_id', $cylcleOwner)
                ->where('current', 1)
                ->where('branch_id', $cycleData->directory->branch_id)
                ->select('id')
                ->get();

            $price = CycleData::whereIn('cycle_id', $cycle)
                ->with(['directory', 'directory.dish', 'directory.dish.meal'])
                ->whereHas('directory.dish', function ($query) use ($mealId) {
                    $query->where('meal_id', $mealId);
                })
                ->whereBetween('date', [
                    Carbon::make($from)->addDays(1)->format('Y-m-d'),
                    Carbon::make($date)->format('Y-m-d')
                ])
                ->get();
            $positionsPrice = $positionsPrice + $price->sum('directory.price') * $position->quantity;
        }

        foreach ($additions as $addition) {
            $price = Addition::find($addition->id)->price * $addition->quantity;
            $additionsPrice = $additionsPrice + $price;
        }
        $saleAmount = (100 - (int) $sale) / 100;
        return ($positionsPrice + $additionsPrice) * $saleAmount;
    }

    private function save(int $guideId, string $date, int $cycleId, int $quantity): array
    {
        $guide = DirectoryDish::find($guideId);

        $cycleGuide = CycleData::date($date)
            ->where('directory_dish_id', $guideId)
            ->first();

        return [
            'amount' => $guide->price * $quantity,
            'date' => $date,
            'meal_id' => $guide->dish->meal_id,
            'branch_id' => request()->user()->branch_id,
            'cycle_data_id' => $cycleGuide->id,
            'col' => $quantity
        ];
    }
}
