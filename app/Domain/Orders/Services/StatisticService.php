<?php
namespace App\Domain\Orders\Services;

use Exception;
use Carbon\Carbon;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Kitchens\Entities\Kitchen;
use App\Domain\Kitchens\Entities\Delivery;

class StatisticService
{
    public function findStat()
    {
        $branches = Branch::get();

        $sales = collect([]);
        $kitchens = collect([]);
        $delivered = collect([]);

        if (request()->user()->hasRole('administrator')) {
            $delivery = Delivery::where('branch_id', request()->user()->branch_id)->where('state', 'complete')->get();

            $sales->push([
                'branch' => request()->user()->branch->title,
                'count' => $delivery->count(),
                'sum' => $delivery->sum('orders.amount'),
            ]);

            $kitchen = Kitchen::where('date', Carbon::now()->format('Y-m-d'))
                ->where('branch_id', request()->user()->branch_id)
                ->where('state', 'process')
                ->get();

            $kitchens->push([
                'branch' => request()->user()->branch->title,
                'count' => $kitchen->count(),
                'sum' => $kitchen->sum('orders.amount'),
            ]);

            $deliveryProcess = Delivery::where('branch_id', request()->user()->branch_id)->where('state', 'process')->get();

            $delivered->push([
                'branch' => request()->user()->branch->title,
                'count' => $deliveryProcess->count(),
                'sum' => $deliveryProcess->sum('orders.amount'),
            ]);
            return ['sales' => $sales, 'kitchens' => $kitchens, 'delivery' => $delivered];
        }

        $branches->each(function ($branch) use ($sales, $kitchens, $delivered) {
            $delivery = Delivery::where('branch_id', $branch->id)->where('state', 'complete')->get();

            $sales->push([
                'branch' => $branch->title,
                'count' => $delivery->count(),
                'sum' => $delivery->sum('orders.amount'),
            ]);

            $kitchen = Kitchen::where('date', Carbon::now()->format('Y-m-d'))
                ->where('branch_id', $branch->id)
                ->where('state', 'process')
                ->get();

            $kitchens->push([
                'branch' => $branch->title,
                'count' => $kitchen->count(),
                'sum' => $kitchen->sum('orders.amount'),
            ]);

            $deliveryProcess = Delivery::where('branch_id', $branch->id)->where('state', 'process')->get();

            $delivered->push([
                'branch' => $branch->title,
                'count' => $deliveryProcess->count(),
                'sum' => $deliveryProcess->sum('orders.amount'),
            ]);
        });

        return ['sales' => $sales, 'kitchens' => $kitchens, 'delivery' => $delivered];
    }
}
