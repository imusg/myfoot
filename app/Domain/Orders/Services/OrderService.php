<?php
namespace App\Domain\Orders\Services;

use Exception;
use Carbon\Carbon;
use App\Domain\Cycles\Entities\Cycle;
use App\Domain\Dishies\Entities\Dish;
use App\Domain\Orders\Entities\Order;
use App\Domain\Clients\Entities\Client;
use App\Domain\Cycles\Entities\CycleData;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Orders\Entities\OrderAddition;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Orders\Contracts\OrderRepository;
use App\Domain\Clients\Entities\ClientTransaction;
use App\Domain\Orders\Entities\OrderPositionReplaceIngredient;

class OrderService
{
    private OrderRepository $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function create(array $dataRequestOrder)
    {
        [
            'date' => $date,
            'client_id' => $clientId,
            'positions' => $positions,
            'additions' => $additions,
            'longer' => $longerDate,
            'sale' => $saleGlob
        ] = $dataRequestOrder;

        $orderByDay = Order::where('date', $date)
            ->whereHas('clients', function ($query) use ($clientId) {
                return $query->where('client_id', $clientId);
            })
            ->first();
        if (!$orderByDay) {
            $orderByDay = Order::create([
                'amount' => 0,
                'date' => $date,
                'sale' => $saleGlob
            ]);
        }
        $orderByDay->update(['sale' => $saleGlob]);

        $cycleIdss = Cycle::where('branch_id', null)
            ->where('owner_cycle_id', null)
            ->get()->map(function ($item) {
                return $item->id;
            });


        $cycle = Cycle::where('current', 1)
            ->where('branch_id', request()->user()->branch_id)
            ->firstOrFail();

        foreach ($positions as [
                 'cycleDataId' => $cycleDataId,
                 'price' => $price,
                 'quantity' => $quantity,
                 'sale' => $sale
        ]) {
            $saleP = $saleGlob ? 1 - ($saleGlob / 100): 1;
            $saleProc = $sale ? 1 - ($sale / 100) : 1;
            $sumSale = $sale ||  $saleGlob ? $saleP + ($saleProc===1 ? 0 : $saleProc): 1;
            if ($quantity > 0) {
                if ($longerDate) {
                    $cycleData = CycleData::find($cycleDataId);
                    $mealId = $cycleData->directory->dish->meal_id;
                    $dishIds = Dish::where('meal_id', $mealId)
                        ->get()
                        ->map(function ($item) {
                            return $item->id;
                        });

                    $directoryDishIds = DirectoryDish::whereIn('dish_id', $dishIds)
                        ->get()
                        ->map(function ($item) {
                            return $item->id;
                        });


                    $cycleIds = Cycle::whereIn('owner_cycle_id', $cycleIdss)
                        ->where('branch_id', request()->user()->branch_id)
                        ->where('current', 1)
                        ->get()
                        ->map(function ($item) {
                            return $item->id;
                        });

                    $cycleDatas = CycleData::whereIn('cycle_id', $cycleIds)
                        ->with(['directory', 'directory.dish', 'directory.dish.meal'])
                        ->whereBetween('date', [
                            Carbon::make($date)->addDays(1)->format('Y-m-d'),
                            Carbon::make($longerDate)->format('Y-m-d')
                        ])
                        ->whereIn('directory_dish_id', $directoryDishIds)
                        ->get()
                        ->where('directory.branch_id', request()->user()->branch_id);
                    $orderIds = ClientOrder::where('client_id', $clientId)->get()->map(function ($item) {
                        return $item->order_id;
                    });

                    $cycleDatas->each(function ($data, $index) use ($saleGlob, $orderByDay, $quantity, $orderIds,  $saleProc, $sale) {
                        $order = Order::whereIn('id', $orderIds)
                            ->where('date', $data->date)
                            ->where('branch_id', request()->user()->branch_id)->first();
                        if ($order) {
                            $order->positions()
                                ->firstOrCreate([
                                    'cycle_data_id' => $data->id,
                                ])
                                ->update([
                                    'quantity' => $quantity,
                                    'price' => (intval($data->directory->price) * $quantity) *  $saleProc,
                                    'sale' => $sale
                                ]);
                        } else {
                            $order = Order::create([
                                'date' => $data->date,
                                'sale' => $saleGlob,
                                'amount' => null,
                                'branch_id' => request()->user()->branch_id
                            ]);
                            $order->positions()
                                ->firstOrCreate([
                                    'cycle_data_id' => $data->id,
                                ])
                                ->update([
                                    'quantity' => $quantity,
                                    'price' => (intval($data->directory->price) * $quantity) *  $saleProc,
                                    'sale' => $sale
                                ]);
                        }
                    });
                } else {
                    $orderByDay->positions()->firstOrCreate([
                        'cycle_data_id' => $cycleDataId,
                    ])
                        ->update([
                            'quantity' => $quantity,
                            'price' => ($price * $quantity) * $saleProc,
                            'sale' => $sale
                        ]);
                }
            } else {
                $orderByDay->positions()->where('cycle_data_id', $cycleDataId)->delete();
            }
        }

        foreach ($additions as ['id' => $id, 'price' => $price, 'quantity' => $quantity, 'sale' => $sale]) {
            $saleProc = $sale ? 1 - ($sale / 100) : 0;
            if ($quantity > 0) {
                if ($longerDate) {
                    $diff = Carbon::make($date)->diffInDays(Carbon::make($longerDate)) + 1;
                    $orderIds = ClientOrder::where('client_id', $clientId)->get()->map(function ($item) {
                        return $item->order_id;
                    });

                    for ($i=1; $i < $diff; $i++) {
                        $order = Order::whereIn('id', $orderIds)
                            ->where('date', Carbon::make($date)->addDays($i)->format('Y-m-d'))->first();
                        if ($order) {
                            $order->positions()->firstOrCreate([
                                'addition_id' => $id,
                            ])->update(['quantity' => $quantity, 'price' => $price * $quantity]);
                        } else {
                            $order = Order::create([
                                'date' => Carbon::make($date)->addDays($i)->format('Y-m-d'),
                                'branch_id' => request()->user()->branch_id
                            ]);
                            $order->positions()->firstOrCreate([
                                'addition_id' => $id,
                            ])->update(['quantity' => $quantity, 'price' => $price * $quantity]);
                        }
                    }
                } else {
                    $orderByDay->positions()->firstOrCreate([
                        'addition_id' => $id,
                    ])->update(['quantity' => $quantity, 'price' => $price * $quantity, 'sale' => $sale]);
                }
            } else {
                $orderByDay->positions()->where('addition_id', $id)->delete();
            }
        }


        Order::whereHas('clients', function ($query) use ($clientId) {
            return $query->where('client_id', $clientId);
        })
            ->get()
            ->map(function ($item) {
                $saleAmount = (100 - $item->sale) / 100;
                if (!$item->positions->count()) {
                    $item->delete();
                    return;
                }
                $amountPosition = $item->positions()->whereNull('addition_id')->sum('price') * $saleAmount;
                $amountAddition = $item->positions()->whereNotNull('addition_id')->sum('price');
                $item->update([
                    'amount' => $amountPosition + $amountAddition
                ]);
            });

        $clientOrderIds = ClientOrder::where('client_id', $clientId)
            ->get()
            ->map(function ($item) {
                return $item->order_id;
            });

        $orders = Order::whereIn('id', $clientOrderIds)->where('date', '>', Carbon::now()->format('Y-m-d'))->get();
        if ($orders->count()) {
            $sumAmount = $orders->sum('amount');
            if ($sumAmount > 0) {
                $client = Client::find($clientId);
                $client->update([
                    'balance_forecast' => floor($client->balance / $sumAmount)
                ]);
            }
        }
        return true;
    }

    private function save($price, $quantity, $date, $mealId, $cycleDataId)
    {
        return $this->orderRepository->store([
            'amount' => $price * $quantity,
            'paid' => true,
            'date' => $date,
            'meal_id' => $mealId,
            'branch_id' => request()->user()->branch_id,
            'cycle_data_id' => $cycleDataId,
            'col' => $quantity
        ]);
    }

    public function delete($cycleDataId)
    {
        $order = $this->orderRepository->findOneBy(['cycle_data_id' => $cycleDataId]);
        $order->delete();
    }

    public function update($id, array $dataRequestOrder)
    {
        $order = $this->orderRepository->findOneById($id);
        $price = $order->data->directory->price;
        $dataRequestOrder['amount'] = $price * $dataRequestOrder['col'];
        return $this->orderRepository->update($order, $data);
    }

    public function replaceIngredient(array $data)
    {
        $replaceIngredient = OrderPositionReplaceIngredient::firstOrCreate([
            'position_id' => $data['position_id'],
            'before_ingredient_id' => $data['before_ingredient_id']
        ])->update(['after_ingredient_id' => $data['after_ingredient_id']]);
        return $replaceIngredient;
    }

    public function destroyReplaceIngredient($before, $positionId)
    {
        $replaceIngredient = OrderPositionReplaceIngredient::where('before_ingredient_id', $before)
            ->where('position_id', $positionId)
            ->delete();
        return $replaceIngredient;
    }

    public function clearOrder(int $clientId)
    {
        $client = Client::find($clientId);
        $sum = $client->orders->sum('amount');
        $client->orders()->delete();
        $client->update(['balance' => $client->balance + $sum]);
        ClientTransaction::firstOrcreate([
            'type' => 'Возврат',
            'value' => $sum,
            'client_id' =>  $clientId
        ]);
        return true;
    }
}
