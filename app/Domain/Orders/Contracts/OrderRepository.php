<?php

namespace App\Domain\Orders\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface OrderRepository extends BaseRepository
{
    public function storeWithGuides(array $data): bool;
}
