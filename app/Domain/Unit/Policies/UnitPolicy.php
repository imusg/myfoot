<?php

namespace App\Domain\Unit\Policies;

use App\Domain\Unit\Entities\Unit;
use App\Domain\Users\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UnitPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('view any units');
    }

    public function create(User $user)
    {
        return $user->can('create units');
    }

    public function update(User $user, Unit $unit)
    {
        return $user->can('update units');
    }

    public function delete(User $user, Unit $unit)
    {
        return $user->can('delete units');
    }
}
