<?php

namespace App\Domain\Unit\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UnitCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => UnitResource::collection($this->collection),
        ];
    }
}
