<?php

namespace App\Domain\Unit\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use App\Domain\Unit\Entities\Unit;
use App\Domain\Users\Entities\User;
use App\Domain\Unit\Contracts\UnitRepository;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Unit\Http\Resources\UnitResource;
use App\Domain\Unit\Http\Resources\UnitCollection;
use App\Domain\Unit\Http\Requests\UnitStoreRequests;
use App\Domain\Unit\Http\Requests\UnitUpdateRequests;

class UnitController extends Controller
{
    private UnitRepository $UnitRepository;

    public function __construct(UnitRepository $UnitRepository)
    {
        $this->UnitRepository = $UnitRepository;
        $this->resourceItem = UnitResource::class;
        $this->resourceCollection = UnitCollection::class;
    }

    /**
     * @OA\Get(
     * path="/units",
     * summary="Получение Ед. измерения",
     * description="Получение все ед.измерения",
     * operationId="indexUnits",
     * tags={"Units"},
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех ед. измерения",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Unit"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *        )
     *  )
     * );
     */

    public function index()
    {
        $this->authorize('viewAny', User::class);
        $collection = $this->UnitRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    /**
     * @OA\Post(
     * path="/units",
     * summary="Создание Ед. измерения",
     * description="Добавление ед.измерения",
     * operationId="storeUnits",
     * tags={"Units"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для создания ед.измерения",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/UnitRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление ед. измерения",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Unit"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  UnitStoreRequests  $request
     * @return mixed
     */
    public function store(UnitStoreRequests $request)
    {
        $this->authorize('create', User::class);
        $data = $request->validated();
        $response = $this->UnitRepository->store($data);
        return $this->respondWithItem($response);
    }

    /**
     * @OA\Put(
     * path="/units/{unit}",
     * summary="Обновление Ед. измерения",
     * description="Обновление ед.измерения",
     * operationId="updateUnits",
     * tags={"Units"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="unit",
     *  in="path",
     *  required=true,
     *  description="Unit id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для обновления ед.измерения",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/UnitRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная обновление ед. измерения",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Unit"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  UnitUpdateRequests  $request
     * @param  integer $id
     * @return mixed
     */
    public function update(UnitUpdateRequests $request, $id)
    {
        $unit = $this->UnitRepository->findOneById($id);
        $this->authorize('update', $unit);
        $data = $request->validated();
        $response = $this->UnitRepository->update(
            $unit,
            $data
        );
        return $this->respondWithItem($response);
    }

    /**
     * @OA\Delete (
     * path="/units/{unit}",
     * summary="Удаление Ед. измерения",
     * description="Удаление ед.измерения",
     * operationId="deleteUnits",
     * tags={"Units"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="unit",
     *  in="path",
     *  required=true,
     *  description="Unit id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная удаление ед. измерения",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Unit"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  integer  $id
     * @return mixed
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $unit = $this->UnitRepository->findOneById($id);
        $this->authorize('delete', $unit);
        $response = $this->UnitRepository->destroy($unit);
        return $this->respondWithCustomData([
            'deleted' => $response
        ]);
    }
}
