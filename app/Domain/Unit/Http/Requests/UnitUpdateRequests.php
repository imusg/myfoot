<?php

namespace App\Domain\Unit\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class UnitUpdateRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'string',
                'max:250',
            ]
        ];
    }
}
