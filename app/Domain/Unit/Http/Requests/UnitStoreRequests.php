<?php

namespace App\Domain\Unit\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

/**
 * @OA\Schema(
 *    schema="UnitRequest",
 *    type="object",
 * )
 * @OA\Property(
 *   property="title",
 *   type="string",
 *   description="Название ед измерения",
 *   example="шт.",
 * )
 */

class UnitStoreRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'max:250',
            ]
        ];
    }
}
