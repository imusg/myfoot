<?php

namespace App\Domain\Unit\Database\Seeds;

use App\Domain\Unit\Entities\Unit;
use Illuminate\Database\Seeder;

class UnitsTableSeed extends Seeder
{
    public function run()
    {
        $data = collect(['шт', 'кг']);
        $data->each(function ($item) {
            Unit::create([
                'title' => $item
            ]);
        });
    }
}
