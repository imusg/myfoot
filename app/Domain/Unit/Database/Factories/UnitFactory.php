<?php

namespace App\Domain\Unit\Database\Factories;

use App\Domain\Unit\Entities\Unit;
use App\Infrastructure\Abstracts\ModelFactory;

class UnitFactory extends ModelFactory
{
    protected string $model = Unit::class;

    public function fields(): array
    {
        return [
            'title' => $this->faker->jobTitle
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }


}
