<?php


namespace App\Domain\Unit\Providers;

use App\Domain\Unit\Contracts\UnitRepository;
use App\Domain\Unit\Entities\Unit;
use App\Domain\Unit\Repositories\EloquentUnitRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(UnitRepository::class, function () {
            return new EloquentUnitRepository(new Unit());
        });
    }

    public function provides(): array
    {
        return [
            UnitRepository::class
        ];
    }
}
