<?php
namespace App\Domain\Unit\Providers;

use App\Domain\Unit\Database\Factories\UnitFactory;
use App\Domain\Unit\Entities\Unit;
use App\Domain\Unit\Policies\UnitPolicy;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'units';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
    ];

    protected array $policies = [
        Unit::class => UnitPolicy::class
    ];

    protected array $factories = [
        UnitFactory::class
    ];
}
