<?php

namespace App\Domain\Unit\Tests\Feature;

use App\Domain\Unit\Entities\Unit;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class UnitControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testIndexUnit(): void
    {
        $this->get(route('units.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    ['id', 'title']
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    public function testStoreUnit() : void
    {
        $response = $this->post(
            route('units.store'),
            ['title' => $this->faker->jobTitle]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);
        $id = $response->json('data.id');
        $this->assertDatabaseHas('units', ['id' => $id]);
    }

    public function testUpdateUnit() : void
    {
        $Unit = factory(Unit::class)
            ->create();

        $response = $this->put(route('units.update', $Unit->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);

        $data = $response->json('data');
        $this->assertDatabaseHas('units', $data);
    }

    public function testDestroyUnit() : void
    {
        $Unit = factory(Unit::class)
            ->create();

        $this->delete(route('units.destroy', $Unit->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('units', ['id' => $Unit->id]);
    }

}
