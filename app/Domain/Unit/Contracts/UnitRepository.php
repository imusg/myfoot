<?php

namespace App\Domain\Unit\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface UnitRepository extends BaseRepository
{
}
