<?php

namespace App\Domain\Branches\Tests\Feature;

use App\Domain\Branches\Entities\Branch;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class BranchControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testIndexBranch(): void
    {
        $this->get(route('branches.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    ['id', 'title']
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    public function testStoreBranch() : void
    {
        $response = $this->post(
            route('branches.store'),
            ['title' => $this->faker->jobTitle]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);
        $id = $response->json('data.id');
        $this->assertDatabaseHas('branches', ['id' => $id]);
    }

    public function testUpdateBranch() : void
    {
        $branch = factory(Branch::class)
            ->create();

        $response = $this->put(route('branches.update', $branch->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);

        $data = $response->json('data');
        $this->assertDatabaseHas('branches', $data);
    }

    public function testDestroyBranch() : void
    {
        $branch = factory(Branch::class)
            ->create();

        $this->delete(route('branches.destroy', $branch->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('branches', ['id' => $branch->id]);
    }
}
