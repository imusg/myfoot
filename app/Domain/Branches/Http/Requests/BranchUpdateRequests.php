<?php

namespace App\Domain\Branches\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class BranchUpdateRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'string',
                'max:250',
            ]
        ];
    }
}
