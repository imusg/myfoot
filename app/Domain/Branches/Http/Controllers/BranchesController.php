<?php

namespace App\Domain\Branches\Http\Controllers;

use App\Domain\Branches\Contracts\BranchRepository;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Branches\Http\Requests\BranchStoreRequests;
use App\Domain\Branches\Http\Requests\BranchUpdateRequests;
use App\Domain\Branches\Http\Resources\BranchCollection;
use App\Domain\Branches\Http\Resources\BranchResource;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class BranchesController extends Controller
{
    private BranchRepository $branchRepository;

    public function __construct(BranchRepository $branchRepository)
    {
        $this->branchRepository = $branchRepository;
        $this->resourceItem = BranchResource::class;
        $this->resourceCollection = BranchCollection::class;
    }

    public function index()
    {
        $collection = $this->branchRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function store(BranchStoreRequests $request)
    {
        $data = $request->validated();
        $response = $this->branchRepository->storeBranch($data);
        return $this->respondWithItem($response);
    }

    public function update(BranchUpdateRequests $request, $id)
    {
        $data = $request->validated();
        $response = $this->branchRepository->update(
            $this->branchRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy($id): JsonResponse
    {
        $response = $this->branchRepository->destroy(
            $this->branchRepository->findOneById($id)
        );
        return $this->respondWithCustomData([
            'deleted' => $response
        ]);
    }
}
