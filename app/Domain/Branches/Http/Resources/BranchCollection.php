<?php

namespace App\Domain\Branches\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BranchCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => BranchResource::collection($this->collection),
        ];
    }
}
