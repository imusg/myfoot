<?php

namespace App\Domain\Branches\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface BranchRepository extends BaseRepository
{
}
