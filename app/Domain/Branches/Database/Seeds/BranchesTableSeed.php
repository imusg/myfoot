<?php

namespace App\Domain\Branches\Database\Seeds;

use App\Domain\Branches\Entities\Branch;
use Illuminate\Database\Seeder;

class BranchesTableSeed extends Seeder
{
    public function run()
    {
        collect([
            'Филиал 1 Тестовый'
        ])->each(function ($item) {
            Branch::create(['title' => $item]);
        });
    }
}
