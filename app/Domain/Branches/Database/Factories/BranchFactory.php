<?php

namespace App\Domain\Branches\Database\Factories;

use App\Domain\Branches\Entities\Branch;
use App\Infrastructure\Abstracts\ModelFactory;

class BranchFactory extends ModelFactory
{
    protected string $model = Branch::class;

    public function fields(): array
    {
        return [
            'title' => $this->faker->jobTitle
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
