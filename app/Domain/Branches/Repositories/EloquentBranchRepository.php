<?php


namespace App\Domain\Branches\Repositories;

use App\Domain\Cycles\Entities\Cycle;
use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Branches\Contracts\BranchRepository;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentBranchRepository extends EloquentRepository implements BranchRepository
{
    private string $defaultSort = 'id';

    private array $defaultSelect = [
        'id',
        'title',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'title',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Branch::class)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('id', request()->user()->branch_id);
            })
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function storeBranch(array $data)
    {
        $branch = $this->store($data);
        Cycle::where('owner_cycle_id', null)->get()->each(function ($item) use ($branch) {
            $cycle = Cycle::find($item->id)->replicate()->fill([
                'owner_cycle_id' => $item->id,
                'branch_id' => $branch->id,
                'excluded' => true
            ]);
            $cycle->push();
        });
        return $branch;
    }
}
