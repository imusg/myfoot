<?php

namespace App\Domain\Branches\Providers;

use App\Domain\Branches\Http\Controllers\BranchesController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenSuperUser($router);
            });
    }

    private function mapRoutesWhenSuperUser(Router $router) : void
    {
        $router->apiResource('branches', BranchesController::class)
            ->names('branches');
    }

}


