<?php


namespace App\Domain\Branches\Providers;

use App\Domain\Branches\Contracts\BranchRepository;
use App\Domain\Branches\Entities\Branch;
use App\Domain\Branches\Repositories\EloquentBranchRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(BranchRepository::class, function () {
            return new EloquentBranchRepository(new Branch());
        });
    }

    public function provides(): array
    {
        return [
            BranchRepository::class
        ];
    }
}
