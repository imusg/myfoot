<?php
namespace App\Domain\Branches\Providers;

use App\Domain\Branches\Database\Factories\BranchFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'branches';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        BranchFactory::class
    ];
}
