<?php

namespace App\Domain\Branches\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Branches\Entities\Branch
 * @OA\Schema (
 *    schema="Branch",
 *    type="object",
 *  @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *  @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Назавние филиала",
 *      example="Филиал 1"
 *    ),
 * )
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Branch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Branch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Branch query()
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Branch extends Model
{
    protected $fillable = ['title'];
}
