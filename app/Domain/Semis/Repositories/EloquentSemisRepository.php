<?php
namespace App\Domain\Semis\Repositories;

use App\Domain\Semis\Entities\SemifinishedIngredient;
use App\Domain\Storage\Entities\Storage;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Semis\Contracts\SemisRepository;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentSemisRepository extends EloquentRepository implements SemisRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'title',
        'cooking_process',
        'cooking_time',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'title',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];

    private array $allowedIncludes = [
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Semifinished::class)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function storeIngredient(array $data, $id): Model
    {
        $storage = Storage::where('ingredient_id', $data['ingredient_id'])->first();
        if ($storage) {
            Storage::where('ingredient_id', $data['ingredient_id'])->update([
                'count' => $storage->count - ($dish->count / 1000)
            ]);
        }
        return SemifinishedIngredient::create([
            'semifinished_id' => $id,
            'ingredient_id' => $data['ingredient_id']
        ]);
    }

    public function deleteIngredient($semis, $ingredient): bool
    {
        return SemifinishedIngredient::where('semifinished_id', $semis)
            ->where('ingredient_id', $ingredient)
            ->delete();
    }

    public function findOneById(int $id): Model
    {
        $this->with(['ingredients', 'counts']);
        return parent::findOneById($id);
    }
}
