<?php

namespace App\Domain\Semis\Repositories;

use App\Domain\Semis\Contracts\SemisIngredientRepository;
use App\Domain\Storage\Entities\Storage;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Database\Eloquent\Model;

class EloquentSemisIngredientRepository extends EloquentRepository implements SemisIngredientRepository
{
}
