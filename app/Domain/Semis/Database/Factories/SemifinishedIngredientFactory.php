<?php

namespace App\Domain\Semis\Database\Factories;

use App\Domain\Semis\Entities\SemifinishedIngredient;
use App\Infrastructure\Abstracts\ModelFactory;

class SemifinishedIngredientFactory extends ModelFactory
{
    protected string $model = SemifinishedIngredient::class;

    public function fields()
    {
        return [
            'semifinished_id' => $this->faker->numberBetween(1,50),
            'ingredient_id' => $this->faker->numberBetween(1,50)
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
