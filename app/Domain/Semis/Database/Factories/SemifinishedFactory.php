<?php

namespace App\Domain\Semis\Database\Factories;

use App\Domain\Semis\Entities\Semifinished;
use App\Infrastructure\Abstracts\ModelFactory;

class SemifinishedFactory extends ModelFactory
{
    protected string $model = Semifinished::class;

    public function fields()
    {
        return [
            'title' => $this->faker->jobTitle,
            'cooking_process' => $this->faker->text,
            'cooking_time' => $this->faker->time('H:i'),
        ];
    }
    public function states()
    {
        // TODO: Implement states() method.
    }
}
