<?php

namespace App\Domain\Semis\Database\Seeds;

use App\Domain\Semis\Entities\SemifinishedIngredient;
use Illuminate\Database\Seeder;

class SemifinishedIngredientSeeder extends Seeder
{
    public function run()
    {
        SemifinishedIngredient::factory()->count(100)->create();
    }
}
