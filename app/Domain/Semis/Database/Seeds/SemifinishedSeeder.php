<?php

namespace App\Domain\Semis\Database\Seeds;

use App\Domain\Semis\Entities\Semifinished;
use Illuminate\Database\Seeder;

class SemifinishedSeeder extends Seeder
{
    public function run()
    {
        Semifinished::factory()->count(100)->create();
    }
}
