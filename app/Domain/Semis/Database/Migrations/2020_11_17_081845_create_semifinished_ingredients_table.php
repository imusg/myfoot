<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemifinishedIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semifinished_ingredients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('semifinished_id');
            $table->foreignId('ingredient_id');
            $table->float('count')->default(0)->nullable();
            $table->float('brutto')->default(0);
            $table->float('netto')->default(0);
            $table->boolean('losses_cleaning_checked')->default(false);
            $table->boolean('losses_frying_checked')->default(false);
            $table->boolean('losses_cooking_checked')->default(false);
            $table->boolean('losses_baking_checked')->default(false);
            $table->boolean('losses_stew_checked')->default(false);
            $table->foreign('semifinished_id')
                ->references('id')
                ->on('semifinisheds')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('ingredient_id')
                ->references('id')
                ->on('ingredients')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semifinished_ingredients');
    }
}
