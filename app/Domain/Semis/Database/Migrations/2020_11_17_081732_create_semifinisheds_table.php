<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemifinishedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semifinisheds', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('cooking_process')->nullable();
            $table->time('cooking_time')->nullable();
            $table->string('barcode')->nullable();
            $table->string('workshop')->nullable();
            $table->string('tax')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semifinisheds');
    }
}
