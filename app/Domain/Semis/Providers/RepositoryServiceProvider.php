<?php
namespace App\Domain\Semis\Providers;

use App\Domain\Semis\Contracts\SemisIngredientRepository;
use App\Domain\Semis\Entities\SemifinishedIngredient;
use App\Domain\Semis\Repositories\EloquentSemisIngredientRepository;
use Illuminate\Support\ServiceProvider;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Semis\Contracts\SemisRepository;
use App\Domain\Semis\Repositories\EloquentSemisRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(SemisRepository::class, function() {
            return new EloquentSemisRepository(new Semifinished());
        });

        $this->app->singleton(SemisIngredientRepository::class, function() {
            return new EloquentSemisIngredientRepository(new SemifinishedIngredient());
        });
    }

    public function provides(): array
    {
        return [
            SemisRepository::class,
            SemisIngredientRepository::class
        ];
    }
}
