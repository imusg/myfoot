<?php

namespace App\Domain\Semis\Providers;

use App\Domain\Semis\Entities\SemifinishedIngredient;
use App\Domain\Semis\Listeners\Observers\SemisIngredientObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        SemifinishedIngredient::observe(SemisIngredientObserver::class);
    }
}
