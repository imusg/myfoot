<?php

namespace App\Domain\Semis\Providers;

use App\Domain\Semis\Http\Controllers\SemisController;
use App\Domain\Semis\Http\Controllers\SemisIngredientController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->apiResource('semifinisheds', SemisController::class)
            ->only(['store', 'index', 'update', 'destroy', 'show']);

        $router->post('semifinisheds/{semis}/ingredient', [SemisIngredientController::class, 'store']);

        $router->post('semifinisheds/{semifinisheds}/ingredient', [SemisIngredientController::class, 'store'])
            ->name('semifinisheds.store.ingredient');

        $router->put('semifinisheds/{semifinisheds}/ingredient/{ingredient}', [SemisIngredientController::class, 'update'])
            ->name('semifinisheds.update.ingredient');

        $router->delete('semifinisheds/{semifinisheds}/ingredient/{ingredient}', [SemisIngredientController::class, 'destroy'])
            ->name('semifinisheds.destroy.ingredient');
    }
}
