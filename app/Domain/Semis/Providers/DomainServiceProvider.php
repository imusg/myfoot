<?php
namespace App\Domain\Semis\Providers;

use App\Domain\Semis\Database\Factories\SemifinishedFactory;
use App\Domain\Semis\Database\Factories\SemifinishedIngredientFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'semis';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class,
    ];

    protected array $policies = [
    ];

    protected array $factories = [
       SemifinishedFactory::class,
       SemifinishedIngredientFactory::class
    ];
}
