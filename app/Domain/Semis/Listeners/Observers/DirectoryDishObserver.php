<?php

namespace App\Domain\Dishies\Listeners\Observers;

use App\Domain\Storage\Entities\Storage;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Storage\Exceptions\NotEnoughException;
use App\Domain\Storage\Exceptions\NotFoundIngredientException;

class DishIngredientObserver
{
    public function updating(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $netto = $dishIngredient->netto;
            $brutto = $dishIngredient->brutto;
            $ingredient = Ingredient::find($dishIngredient->ingredient_id);
            $body = request()->all();
            $storage = Storage::where('ingredient_id', $dishIngredient->ingredient_id)->first();
            foreach ($body as $nameMethod => $valueMethod) {
                switch ($nameMethod) {
                    case 'losses_cleaning_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_cleaning / 100));
                            if ($storage->count - $dishIngredient->netto < 0) {
                                throw new NotEnoughException();
                            }
                            return $storage->update([
                                'count' => $storage->count + $brutto - $dishIngredient->netto
                            ]);
                        }
                        $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_cleaning / 100));
                        return $storage->update([
                            'count' => $storage->count + $netto - $dishIngredient->netto
                        ]);
                    case 'losses_frying_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_frying / 100));

                            if ($storage->count - $dishIngredient->netto < 0) {
                                throw new NotEnoughException();
                            }

                            return $storage->update([
                                'count' => $storage->count + $brutto - $dishIngredient->netto
                            ]);
                        }
                        $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_frying / 100));
                        return $storage->update([
                            'count' => $storage->count + $netto - $dishIngredient->netto
                        ]);
                    case 'losses_cooking_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_cooking / 100));

                            if ($storage->count - $dishIngredient->netto < 0) {
                                throw new NotEnoughException();
                            }

                            return $storage->update([
                                'count' => $storage->count + $brutto - $dishIngredient->netto
                            ]);
                        }
                        $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_cooking / 100));
                        return $storage->update([
                            'count' => $storage->count + $netto - $dishIngredient->netto
                        ]);
                    case 'losses_baking_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_baking / 100));
                            if ($storage->count - $dishIngredient->netto < 0) {
                                throw new NotEnoughException();
                            }

                            return $storage->update([
                                'count' => $storage->count + $brutto - $dishIngredient->netto
                            ]);
                        }
                        $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_baking / 100));
                        return $storage->update([
                            'count' => $storage->count + $netto - $dishIngredient->netto
                        ]);
                    case 'losses_stew_checked':
                        if ($valueMethod) {
                            $dishIngredient->netto = $netto + ($brutto * ($ingredient->losses_stew / 100));

                            if ($storage->count - $dishIngredient->netto < 0) {
                                throw new NotEnoughException();
                            }

                            return $storage->update([
                                'count' => $storage->count + $brutto - $dishIngredient->netto
                            ]);
                        }
                        $dishIngredient->netto = $netto - ($brutto * ($ingredient->losses_stew / 100));
                        return $storage->update([
                            'count' => $storage->count + $netto - $dishIngredient->netto
                        ]);
                    default:
                        return null;
                }
            }
        }
    }

    public function creating(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $netto = $dishIngredient->netto / 1000;
            $count = $dishIngredient->count;

            $dishIngredient->netto = $dishIngredient->netto / 1000;
            $dishIngredient->brutto = $dishIngredient->brutto / 1000;

            $storage = Storage::where('ingredient_id', $dishIngredient->ingredient_id)->first();

            if (!$storage) {
                throw new NotFoundIngredientException();
            }

            if ($count) {
                if ($storage->count - $count < 0) {
                    throw new NotEnoughException();
                }
                return $storage->update([
                    'count' => $storage->count - $count
                ]);
            }

            if ($storage->count - $netto < 0) {
                throw new NotEnoughException();
            }

            return $storage->update([
                'count' => $storage->count - $netto
            ]);
        }
    }

    public function created(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $netto = $dishIngredient->netto * 1000;

            $calories = ($netto * $dishIngredient->ingredient->calories) / 100;
            $proteins = ($netto * $dishIngredient->ingredient->proteins) / 100;
            $fats = ($netto * $dishIngredient->ingredient->fats) / 100;
            $carbohydrates = ($netto * $dishIngredient->ingredient->carbohydrates) / 100;
            $dishIngredient->dish()->update([
                'calories' => $dishIngredient->dish->calories + $calories,
                'proteins' => $dishIngredient->dish->proteins + $proteins,
                'fats' => $dishIngredient->dish->fats + $fats,
                'carbohydrates' => $dishIngredient->dish->carbohydrates + $carbohydrates,
            ]);
        }
    }

    public function updated(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            if ($dishIngredient->isDirty('netto')) {
                $netto = $dishIngredient->netto * 1000;
                $initNetto = $dishIngredient->getOriginal('netto') * 1000;

                $calories = ($netto * $dishIngredient->ingredient->calories) / 100;
                $proteins = ($netto * $dishIngredient->ingredient->proteins) / 100;
                $fats = ($netto * $dishIngredient->ingredient->fats) / 100;
                $carbohydrates = ($netto * $dishIngredient->ingredient->carbohydrates) / 100;

                $initCalories = ($initNetto * $dishIngredient->ingredient->calories) / 100;
                $initProteins = ($initNetto * $dishIngredient->ingredient->proteins) / 100;
                $initFats = ($initNetto * $dishIngredient->ingredient->fats) / 100;
                $initCarbohydrates = ($initNetto * $dishIngredient->ingredient->carbohydrates) / 100;

                $dishIngredient->dish()->update([
                    'calories' => $dishIngredient->dish->calories - $initCalories + $calories,
                    'proteins' => $dishIngredient->dish->proteins - $initProteins + $proteins,
                    'fats' => $dishIngredient->dish->fats - $initFats + $fats,
                    'carbohydrates' => $dishIngredient->dish->carbohydrates - $initCarbohydrates + $carbohydrates,
                ]);
            }
        }
    }

    public function deleting(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $storage = Storage::where('ingredient_id', $dishIngredient->ingredient_id)->first();

            if ($storage) {
                $netto = $dishIngredient->netto;
                $count = $dishIngredient->count;

                if ($count) {
                    return $storage->update([
                        'count' => $storage->count + $count
                    ]);
                }

                return $storage->update([
                    'count' => $storage->count + $netto
                ]);
            }
        }
    }

    public function deleted(DishIngredient $dishIngredient)
    {
        if ($dishIngredient->ingredient_id) {
            $netto = $dishIngredient->netto * 1000;

            $calories = ($netto * $dishIngredient->ingredient->calories) / 100;
            $proteins = ($netto * $dishIngredient->ingredient->proteins) / 100;
            $fats = ($netto * $dishIngredient->ingredient->fats) / 100;
            $carbohydrates = ($netto * $dishIngredient->ingredient->carbohydrates) / 100;
            $dishIngredient->dish()->update([
                'calories' => $dishIngredient->dish->calories - $calories,
                'proteins' => $dishIngredient->dish->proteins - $proteins,
                'fats' => $dishIngredient->dish->fats - $fats,
                'carbohydrates' => $dishIngredient->dish->carbohydrates - $carbohydrates,
            ]);
        }
    }
}
