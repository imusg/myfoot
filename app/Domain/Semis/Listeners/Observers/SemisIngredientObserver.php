<?php

namespace App\Domain\Semis\Listeners\Observers;

use App\Domain\Storage\Entities\Storage;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Semis\Entities\SemifinishedIngredient;
use App\Domain\Storage\Exceptions\NotEnoughException;
use App\Domain\Storage\Exceptions\NotFoundIngredientException;

class SemisIngredientObserver
{
    public function updating(SemifinishedIngredient $semisIngredient)
    {
        if ($semisIngredient->ingredient_id) {
            $netto = $semisIngredient->netto;
            $brutto = $semisIngredient->brutto;
            $ingredient = Ingredient::find($semisIngredient->ingredient_id);
            $body = request()->all();
            foreach ($body as $nameMethod => $valueMethod) {
                switch ($nameMethod) {
                    case 'losses_cleaning_checked':
                        if ($valueMethod) {
                            $semisIngredient->netto = $netto - ($brutto * ($ingredient->losses_cleaning / 100));
                            $semisIngredient->semis()->update([
                                'netto' => $semisIngredient->semis->netto - $semisIngredient->getOriginal('netto') + $semisIngredient->netto
                            ]);
                            return;
                        }
                        $semisIngredient->netto = $netto + ($brutto * ($ingredient->losses_cleaning / 100));
                        $semisIngredient->semis()->update([
                                'netto' => $semisIngredient->semis->netto - $semisIngredient->getOriginal('netto') + $semisIngredient->netto
                        ]);
                        break;
                    case 'losses_frying_checked':
                        if ($valueMethod) {
                            $semisIngredient->netto = $netto - ($brutto * ($ingredient->losses_frying / 100));
                            $semisIngredient->semis()->update([
                                'netto' =>  $semisIngredient->netto  - $semisIngredient->getOriginal('netto') + $semisIngredient->semis->netto
                            ]);
                            return;
                        }
                        $semisIngredient->netto = $netto + ($brutto * ($ingredient->losses_frying / 100));
                        $semisIngredient->semis()->update([
                                'netto' => $semisIngredient->semis->netto - $semisIngredient->getOriginal('netto') + $semisIngredient->netto
                        ]);

                        break;
                    case 'losses_cooking_checked':
                        if ($valueMethod) {
                            $semisIngredient->netto = $netto - ($brutto * ($ingredient->losses_cooking / 100));
                            $semisIngredient->semis()->update([
                                'netto' =>  $semisIngredient->netto  - $semisIngredient->getOriginal('netto') + $semisIngredient->semis->netto
                            ]);
                            return;
                        }
                        $semisIngredient->netto = $netto + ($brutto * ($ingredient->losses_cooking / 100));
                        $semisIngredient->semis()->update([
                                'netto' => $semisIngredient->semis->netto - $semisIngredient->getOriginal('netto') + $semisIngredient->netto
                        ]);

                        break;
                    case 'losses_baking_checked':
                        if ($valueMethod) {
                            $semisIngredient->netto = $netto - ($brutto * ($ingredient->losses_baking / 100));
                            $semisIngredient->semis()->update([
                                'netto' =>  $semisIngredient->netto  - $semisIngredient->getOriginal('netto') + $semisIngredient->semis->netto
                            ]);
                            return;
                        }
                        $semisIngredient->netto = $netto + ($brutto * ($ingredient->losses_baking / 100));
                        $semisIngredient->semis()->update([
                                'netto' => $semisIngredient->semis->netto - $semisIngredient->getOriginal('netto') + $semisIngredient->netto
                        ]);

                        break;
                    case 'losses_stew_checked':
                        if ($valueMethod) {
                            $semisIngredient->netto = $netto - ($brutto * ($ingredient->losses_stew / 100));
                            $semisIngredient->semis()->update([
                                'netto' =>  $semisIngredient->netto  - $semisIngredient->getOriginal('netto') + $semisIngredient->semis->netto
                            ]);
                            return;
                        }
                        $semisIngredient->netto = $netto + ($brutto * ($ingredient->losses_stew / 100));
                        $semisIngredient->semis()->update([
                                'netto' => $semisIngredient->semis->netto - $semisIngredient->getOriginal('netto') + $semisIngredient->netto
                        ]);
                        break;
                    default:
                        return null;
                }
            }
        }
    }

    public function creating(SemifinishedIngredient $semisIngredient)
    {
        if ($semisIngredient->ingredient_id) {
            $semisIngredient->netto = $semisIngredient->netto / 1000;
            $semisIngredient->brutto = $semisIngredient->brutto / 1000;
        }
    }

    public function created(SemifinishedIngredient $semisIngredient)
    {
        if ($semisIngredient->ingredient_id) {
            $netto = $semisIngredient->netto * 1000;
            $calories = ($netto * $semisIngredient->ingredient->calories) / 100;
            $proteins = ($netto * $semisIngredient->ingredient->proteins) / 100;
            $fats = ($netto * $semisIngredient->ingredient->fats) / 100;
            $carbohydrates = ($netto * $semisIngredient->ingredient->carbohydrates) / 100;
            $semisIngredient->semis()->update([
                'calories' => $semisIngredient->semis->calories + $calories,
                'proteins' => $semisIngredient->semis->proteins + $proteins,
                'fats' => $semisIngredient->semis->fats + $fats,
                'carbohydrates' => $semisIngredient->semis->carbohydrates + $carbohydrates,
                'netto' => $semisIngredient->semis->netto - ($semisIngredient->getOriginal('netto') / 1000) + ($netto / 1000)
            ]);
        }
    }

    public function updated(SemifinishedIngredient $semisIngredient)
    {
        if ($semisIngredient->ingredient_id) {
            if ($semisIngredient->isDirty('netto')) {
                $netto = $semisIngredient->netto * 1000;
                $initNetto = $semisIngredient->getOriginal('netto') * 1000;

                $calories = ($netto * $semisIngredient->ingredient->calories) / 100;
                $proteins = ($netto * $semisIngredient->ingredient->proteins) / 100;
                $fats = ($netto * $semisIngredient->ingredient->fats) / 100;
                $carbohydrates = ($netto * $semisIngredient->ingredient->carbohydrates) / 100;

                $initCalories = ($initNetto * $semisIngredient->ingredient->calories) / 100;
                $initProteins = ($initNetto * $semisIngredient->ingredient->proteins) / 100;
                $initFats = ($initNetto * $semisIngredient->ingredient->fats) / 100;
                $initCarbohydrates = ($initNetto * $semisIngredient->ingredient->carbohydrates) / 100;

                $semisIngredient->semis()->update([
                    'calories' => $semisIngredient->semis->calories - $initCalories + $calories,
                    'proteins' => $semisIngredient->semis->proteins - $initProteins + $proteins,
                    'fats' => $semisIngredient->semis->fats - $initFats + $fats,
                    'carbohydrates' => $semisIngredient->semis->carbohydrates - $initCarbohydrates + $carbohydrates,
                    'netto' =>  $semisIngredient->semis->netto - ($initNetto / 1000) + ($netto / 1000)
                ]);
            }
        }
    }

    public function deleted(SemifinishedIngredient $semisIngredient)
    {
        if ($semisIngredient->ingredient_id) {
            $netto = $semisIngredient->netto * 1000;
            $calories = ($netto * $semisIngredient->ingredient->calories) / 100;
            $proteins = ($netto * $semisIngredient->ingredient->proteins) / 100;
            $fats = ($netto * $semisIngredient->ingredient->fats) / 100;
            $carbohydrates = ($netto * $semisIngredient->ingredient->carbohydrates) / 100;
            $semisIngredient->semis()->update([
                'calories' => $semisIngredient->semis->calories - $calories < 0 ? 0 : $semisIngredient->semis->calories - $calories,
                'proteins' => $semisIngredient->semis->proteins - $proteins < 0 ? 0 : $semisIngredient->semis->proteins - $proteins,
                'fats' => $semisIngredient->semis->fats - $fats < 0 ? 0 : $semisIngredient->semis->fats - $fats,
                'carbohydrates' => $semisIngredient->semis->carbohydrates - $carbohydrates < 0 ? 0 : $semisIngredient->semis->carbohydrates - $carbohydrates,
                'netto' => $semisIngredient->semis->netto - $netto /1000 < 0 ? 0 : $semisIngredient->semis->netto - $netto / 1000
            ]);
        }
    }
}
