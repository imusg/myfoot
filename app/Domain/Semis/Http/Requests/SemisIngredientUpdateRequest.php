<?php

namespace App\Domain\Semis\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class SemisIngredientUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'count' => 'nullable',
            'brutto' => 'nullable',
            'netto' => 'nullable',
            'losses_cleaning_checked' => 'nullable',
            'losses_frying_checked' => 'nullable',
            'losses_cooking_checked' => 'nullable',
            'losses_baking_checked' => 'nullable',
            'losses_stew_checked' => 'nullable'
        ];
    }
}
