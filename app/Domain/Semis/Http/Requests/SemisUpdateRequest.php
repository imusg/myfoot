<?php

namespace App\Domain\Semis\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class SemisUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => 'nullable|string',
            'cooking_process' => 'nullable|string',
            'cooking_time' => 'nullable|string',
            'barcode' => 'nullable|string',
            'tax' => 'nullable|string',
            'workshop' => 'nullable|string',
            'losses_cleaning' => 'nullable',
            'losses_frying' => 'nullable',
            'losses_cooking' => 'nullable',
            'losses_baking' => 'nullable',
        ];
    }
}
