<?php

namespace App\Domain\Semis\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

/**
 * App\Domain\Semis\Entities\Semifinished
 * @OA\Schema (
 *    schema="SemisRequest",
 *    type="object",
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Название полуфабриката",
 *      example="Пельмени",
 *    ),
 *     @OA\Property (
 *      property="cooking_process",
 *      type="string",
 *      description="Процесс приготовления",
 *      example="Берем мсясо ....",
 *    ),
 *    @OA\Property (
 *      property="cooking_time",
 *      type="string",
 *      example="14:20",
 *    )
 * )
*/
class SemisStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => 'nullable|string',
            'cooking_time' => 'nullable|string',
            'cooking_process' => 'nullable|string',
            'barcode' => 'nullable|string',
            'tax' => 'nullable|string',
            'workshop' => 'nullable|string',
        ];
    }
}
