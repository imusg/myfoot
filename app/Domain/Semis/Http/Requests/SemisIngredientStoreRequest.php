<?php

namespace App\Domain\Semis\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class SemisIngredientStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'ingredient_id' => 'required|exists:ingredients,id',
            'count' => 'nullable',
            'brutto' => 'nullable',
            'netto' => 'nullable',
            'losses_cleaning_checked' => 'boolean',
            'losses_frying_checked' => 'boolean',
            'losses_cooking_checked' => 'boolean',
            'losses_baking_checked' => 'boolean',
            'losses_stew_checked' => 'boolean'
        ];
    }
}
