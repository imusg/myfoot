<?php

namespace App\Domain\Semis\Http\Controllers;

use App\Domain\Ingredients\Http\Resources\IngredientResource;
use App\Domain\Semis\Contracts\SemisIngredientRepository;
use App\Domain\Semis\Http\Requests\SemisIngredientStoreRequest;
use App\Domain\Semis\Http\Requests\SemisIngredientUpdateRequest;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SemisIngredientController extends Controller
{
    private SemisIngredientRepository $semisIngredientsRepository;

    public function __construct(SemisIngredientRepository $semisIngredientsRepository)
    {
        $this->semisIngredientsRepository = $semisIngredientsRepository;
    }

    public function store(SemisIngredientStoreRequest $request, $id): \Illuminate\Http\JsonResponse
    {
        $data = $request->validated();
        $data['semifinished_id'] = $id;
        $response = $this->semisIngredientsRepository->store($data);
        return $this->respondWithCustomData(
            IngredientResource::make($response->ingredient),
            Response::HTTP_CREATED
        );
    }
    public function update(SemisIngredientUpdateRequest $request, $semifinishedId, $ingredientId)
    {
        $data = $request->validated();
        if (isset($data['netto'])) {
            $data['netto'] = $data['netto'] / 1000;
        }
        if (isset($data['brutto'])) {
            $data['brutto'] = $data['brutto'] / 1000;
        }
        $response = $this->semisIngredientsRepository->update(
            $this->semisIngredientsRepository->findOneBy([
                'ingredient_id' => $ingredientId ,
                'semifinished_id' => $semifinishedId
            ]),
            $data
        );
        return $this->respondWithCustomData(
            IngredientResource::make($response->ingredient),
            Response::HTTP_OK
        );
    }
    public function destroy($semifinishedId, $ingredientId)
    {
        $response = $this->semisIngredientsRepository->destroy(
            $this->semisIngredientsRepository->findOneBy([
                'ingredient_id' => $ingredientId ,
                'semifinished_id' => $semifinishedId
            ])
        );
        return $this->respondWithCustomData(
            ['deleted' => $response],
            Response::HTTP_OK
        );
    }
}
