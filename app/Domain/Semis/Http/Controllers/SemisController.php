<?php

namespace App\Domain\Semis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domain\Semis\Contracts\SemisRepository;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Semis\Http\Resources\SemisResource;
use App\Domain\Semis\Http\Resources\SemisCollection;
use App\Domain\Semis\Http\Requests\SemisStoreRequest;
use App\Domain\Semis\Http\Requests\SemisUpdateRequest;

class SemisController extends Controller
{
    private SemisRepository $semisRepository;

    public function __construct(SemisRepository $semisRepository)
    {
        $this->semisRepository = $semisRepository;
        $this->resourceItem = SemisResource::class;
        $this->resourceCollection = SemisCollection::class;
    }

    /**
     * @OA\Get(
     * path="/semifinisheds",
     * summary="Получение всех полуфабрикатов",
     * description="Получение всех полуфабрикатов",
     * operationId="indexSemifinisheds",
     * tags={"Semifinisheds"},
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех полуфабрикатов",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Semis"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *   )
     *  )
     * );
     */

    public function index()
    {
        $collection = $this->semisRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function show($id)
    {
        $response = $this->semisRepository->findOneById($id);
        return $this->respondWithItem($response);
    }
    /**
     * @OA\Post(
     * path="/semifinisheds",
     * summary="Создание полуфабриката",
     * description="Добавление полуфабриката",
     * operationId="storeSemifinisheds",
     * tags={"Semifinisheds"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для полуфабриката",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/SemisRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление полуфабриката",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Semis"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     */
    public function store(SemisStoreRequest $request)
    {
        $data = $request->validated();
        $semis = $this->semisRepository->store($data);
        return $this->respondWithCustomData(SemisResource::make($semis), Response::HTTP_CREATED);
    }

    /**
     * @OA\Put(
     * path="/semifinisheds/{semifinished}",
     * summary="Обновление полуфабриката",
     * description="Обновление полуфабриката",
     * operationId="updateSemifinisheds",
     * tags={"Semifinisheds"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="semifinished",
     *  in="path",
     *  required=true,
     *  description="Semifinished id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для обновления полуфабриката",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/SemisRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная обновление полуфабриката",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Semis"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     */
    public function update(SemisUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->semisRepository->update(
            $this->semisRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    /**
     * @OA\Delete (
     * path="/semifinisheds/{semifinished}",
     * summary="Удаление полуфабриката",
     * description="Удаление полуфабриката",
     * operationId="deleteSemifinisheds",
     * tags={"Semifinisheds"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="semifinished",
     *  in="path",
     *  required=true,
     *  description="Semifinished id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная удаление полуфабриката",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Semis"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     */
    public function destroy(int $id)
    {
        $response = $this->semisRepository->destroy($this->semisRepository->findOneById($id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function storeIngredient(Request $request, $semis)
    {

    }

    public function destroyIngredient($semis, $ingredient)
    {

    }
}
