<?php

namespace App\Domain\Semis\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SemisCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => SemisResource::collection($this->collection),
        ];
    }
}
