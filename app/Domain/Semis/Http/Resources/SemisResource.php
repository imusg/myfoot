<?php

namespace App\Domain\Semis\Http\Resources;

use App\Domain\Dishies\Http\Resources\IngredientCountResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SemisResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'cooking_process' => $this->cooking_process,
            'cooking_time' => $this->cooking_time,
            'tax' => $this->tax,
            'workshop' => $this->workshop,
            'barcode' => $this->barcode,
            'calories' => $this->calories,
            'proteins' => $this->proteins,
            'losses_cleaning' => $this->losses_cleaning,
            'losses_frying' => $this->losses_frying,
            'losses_cooking' => $this->losses_cooking,
            'losses_baking' => $this->losses_baking,
            'losses_stew' => $this->losses_stew,
            'fats' => $this->fats,
            'netto' => $this->netto * 1000,
            'carbohydrates' => $this->carbohydrates,
            'ingredients' => IngredientCountResource::collection($this->counts)
        ];
    }
}
