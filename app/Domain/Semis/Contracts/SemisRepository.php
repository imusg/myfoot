<?php

namespace App\Domain\Semis\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface SemisRepository extends BaseRepository
{
}
