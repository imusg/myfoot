<?php

namespace App\Domain\Semis\Entities;

use App\Domain\Ingredients\Entities\Ingredient;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Semis\Entities\Semifinished
 * @OA\Schema (
 *    schema="Semis",
 *    type="object",
 *    @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Название полуфабриката",
 *      example="Пельмени",
 *    ),
 *     @OA\Property (
 *      property="cooking_process",
 *      type="string",
 *      description="Процесс приготовления",
 *      example="Берем мсясо ....",
 *    ),
 *    @OA\Property (
 *      property="cooking_time",
 *      type="string",
 *      example="14:20",
 *    )
 * )
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Semis\Entities\SemifinishedIngredient[] $ingredients
 * @property-read int|null $ingredients_count
 * @method static \Illuminate\Database\Eloquent\Builder|Semifinished newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Semifinished newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Semifinished query()
 * @mixin \Eloquent
 */
class Semifinished extends Model
{
    protected $fillable = [
        'title',
        'cooking_process',
        'cooking_time',
        'barcode',
        'tax',
        'workshop',
        'calories',
        'proteins',
        'fats',
        'carbohydrates',
        'color',
        'netto',
        'brutto',
        'losses_cleaning',
        'losses_frying',
        'losses_cooking',
        'losses_baking',
        'losses_stew',
    ];

    public function ingredients()
    {
        return $this->belongsToMany(
            Ingredient::class,
            'semifinished_ingredients',
            'semifinished_id',
            'ingredient_id'
        );
    }

    public function counts()
    {
        return $this->hasMany(SemifinishedIngredient::class, 'semifinished_id', 'id');
    }
}
