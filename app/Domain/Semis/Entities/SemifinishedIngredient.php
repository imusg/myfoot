<?php

namespace App\Domain\Semis\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Semis\Entities\Semifinished;
use App\Domain\Ingredients\Entities\Ingredient;

/**
 * App\Domain\Semis\Entities\SemifinishedIngredient
 *
 * @property-read Ingredient|null $ingredient
 * @method static \Illuminate\Database\Eloquent\Builder|SemifinishedIngredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SemifinishedIngredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SemifinishedIngredient query()
 * @mixin \Eloquent
 */
class SemifinishedIngredient extends Model
{
    protected $fillable = [
        'semifinished_id', 'ingredient_id', 'count',
        'losses_cleaning_checked',
        'losses_frying_checked', 'losses_cooking_checked',
        'losses_baking_checked',
        'losses_stew_checked', 'brutto', 'netto'];

    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function semis()
    {
        return $this->hasOne(Semifinished::class, 'id', 'semifinished_id');
    }
}
