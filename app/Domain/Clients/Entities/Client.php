<?php
namespace App\Domain\Clients\Entities;

use App\Domain\Orders\Entities\Order;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Branches\Entities\Branch;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Domain\Clients\Entities\ClientType;

class Client extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'name', 'messenger', 'messenger_login' , 'balance', 'email', 'phone', 'sale',
        'amount_purchases', 'branch_id', 'balance_forecast', 'taxSystem', 'type_id',
        'timeDelivery', 'created_at'
    ];

    public function address()
    {
        return $this->hasMany(ClientDeliveryAddress::class, 'client_id', 'id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    public function messages()
    {
        return $this->hasMany(ClientMessage::class, 'client_id', 'id');
    }

    public function excluded()
    {
        return $this->hasMany(ClientExcludeIngredient::class, 'client_id', 'id');
    }
    public function histories()
    {
        return $this->hasMany(History::class, 'id', 'client_id');
    }

    public function type()
    {
        return $this->hasOne(ClientType::class, 'id', 'type_id');
    }


    public function orders()
    {
        return $this->belongsToMany(
            Order::class,
            'client_orders',
            'client_id',
            'order_id'
        );
    }
}
