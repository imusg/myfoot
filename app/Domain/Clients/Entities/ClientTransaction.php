<?php
namespace App\Domain\Clients\Entities;

use App\Domain\Orders\Entities\Order;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Clients\Entities\Client
 *
 * * @OA\Schema (
 *    schema="Client",
 *    type="object",
 *
 * @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="name",
 *      type="string",
 *      description="Имя клиента",
 *      example="Елена Ивановна Киселёваа",
 *    ),
 *     @OA\Property (
 *      property="messenger",
 *      type="string",
 *      description="Месенджер клиента",
 *      example="WhatsApp",
 *    ),
 *    @OA\Property (
 *      property="balance",
 *      type="integer",
 *      description="Баланс",
 *      example="100.0",
 *    ),
 *    @OA\Property (
 *      property="email",
 *      type="sting",
 *      example="test@test.com",
 *    ),
 *    @OA\Property (
 *      property="phone",
 *      type="string",
 *      example="8-800-268-2375",
 *    )
 * )
 * @property int $id
 * @property string $name
 * @property string $messenger
 * @property string|null $email
 * @property string|null $phone
 * @property string $balance
 * @property int $branch_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Clients\Entities\ClientDeliveryAddress[] $address
 * @property-read int|null $address_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Clients\Entities\ClientExcludeIngredient[] $excluded
 * @property-read int|null $excluded_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Clients\Entities\History[] $histories
 * @property-read int|null $histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Clients\Entities\ClientMessage[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Order[] $orders
 * @property-read int|null $orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereMessenger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientTransaction extends Model
{
    protected $fillable = ['client_id', 'value', 'order_id', 'type'];

    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }
}
