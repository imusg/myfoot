<?php

namespace App\Domain\Clients\Entities;

use App\Domain\Dishies\Entities\Dish;
use App\Domain\Ingredients\Entities\Ingredient;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Clients\Entities\ClientExcludeIngredient
 *
 * @property int $id
 * @property int|null $ingredient_id
 * @property int|null $dish_id
 * @property int $client_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Dish|null $dish
 * @property-read Ingredient|null $ingredient
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient whereIngredientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientExcludeIngredient whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientExcludeIngredient extends Model
{

    protected $fillable = [
        'ingredient_id',
        'replace_ingredient_id',
        'client_id',
        'description'
    ];
    public function ingredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    public function replaceIngredient()
    {
        return $this->hasOne(Ingredient::class, 'id', 'replace_ingredient_id');
    }

}
