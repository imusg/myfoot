<?php

namespace App\Domain\Clients\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Clients\Entities\District;

class ClientDeliveryAddress extends Model
{
    protected $fillable = [
        'region',
        'city',
        'street',
        'district',
        'house',
        'apartment',
        'entrance',
        'comment',
        'isMain'
    ];

    public function districts()
    {
        return $this->hasOne(District::class, 'id', 'district');
    }
}
