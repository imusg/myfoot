<?php

namespace App\Domain\Clients\Entities;

use App\Domain\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Clients\Entities\ClientMessage
 * @OA\Schema (
 *    schema="Messages",
 *    type="object",
 *    @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="text",
 *      type="string",
 *      description="Text Messages",
 *      example="Какой-то текст"
 *    ),
 *    @OA\Property (
 *      property="manager",
 *      ref="#/components/schemas/User"
 *    ),
 *    @OA\Property (
 *      property="images",
 *      type="array",
 *      @OA\Items(
 *         ref="#/components/schemas/MessagesImage"
 *      )
 *    ),
 * )
 * @property int $id
 * @property string $text
 * @property int $client_id
 * @property int $manager_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User|null $manager
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage wherePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientMessage extends Model
{
    protected $fillable = ['text', 'date', 'time' , 'manager_id', 'client_id'];


    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }

    public function images()
    {
        return $this->hasMany(ClientMessageImage::class, 'client_message_id', 'id');
    }
}
