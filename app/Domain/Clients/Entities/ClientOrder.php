<?php

namespace App\Domain\Clients\Entities;

use App\Domain\Orders\Entities\Order;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Clients\Entities\ClientOrder
 *
 * @property int $id
 * @property int $client_id
 * @property int $order_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Clients\Entities\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Order[] $orders
 * @property-read int|null $orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientOrder whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientOrder extends Model
{
    protected $fillable = ['client_id', 'order_id'];

    public function clients()
    {
        return $this->hasMany(Client::class, 'id', 'client_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }


    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'id', 'order_id');
    }
}
