<?php

namespace App\Domain\Clients\Entities;

use App\Domain\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Clients\Entities\ClientMessage
 * @OA\Schema (
 *    schema="MessagesImage",
 *    type="object",
 *    @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="url",
 *      type="string",
 *      description="Url Image",
 *      example="https://via.placeholder.com/640x480.png/004455?text=vero"
 *    ),
 * )
 * @property int $id
 * @property string $platform
 * @property string $text
 * @property string $image
 * @property int $client_id
 * @property int $manager_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User|null $manager
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage wherePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientMessageImage extends Model
{
    protected $fillable = ['url', 'client_message_id'];
}
