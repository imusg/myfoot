<?php

namespace App\Domain\Clients\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Clients\Entities\ClientHistoryOrder
 *
 * @property int $id
 * @property int $client_id
 * @property int $history_order_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder whereHistoryOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientHistoryOrder whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClientHistoryOrder extends Model
{
    protected $fillable = ['client_id', 'history_order_id'];
}
