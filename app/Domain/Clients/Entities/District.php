<?php
namespace App\Domain\Clients\Entities;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = ['title', 'branch_id'];
    public $timestamps = false;
}
