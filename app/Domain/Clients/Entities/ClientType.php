<?php
namespace App\Domain\Clients\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientType extends Model
{
    protected $fillable = ['title', 'branch_id'];
    public $timestamps = false;
}
