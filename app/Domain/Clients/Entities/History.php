<?php

namespace App\Domain\Clients\Entities;

use App\Domain\Users\Entities\User;
use App\Models\EventType;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Clients\Entities\History
 *
 * @property int $id
 * @property string $event
 * @property int $client_id
 * @property int $type_id
 * @property string $messenger
 * @property int $manager_id
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User|null $manager
 * @method static \Illuminate\Database\Eloquent\Builder|History newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|History newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|History query()
 * @method static \Illuminate\Database\Eloquent\Builder|History whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereMessenger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|History whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class History extends Model
{

    protected $fillable = [
        'event',
        'client_id',
        'type_id',
        'messenger',
        'manager_id',
        'date'
    ];

    public function type()
    {
        return $this->hasOne(EventType::class, 'id', 'type_id');
    }

    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }
}
