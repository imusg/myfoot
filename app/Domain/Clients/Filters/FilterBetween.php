<?php
namespace App\Domain\Clients\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterBetween implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereBetween($property, $value);
    }
}
