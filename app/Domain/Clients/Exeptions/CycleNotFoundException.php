<?php

namespace App\Domain\Clients\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CycleNotFoundException extends Exception
{
    public function report()
    {
        //
    }

    public function render(Request $request): JsonResponse
    {
        return response()->json([
            'error' => 'Not Found',
            'message' => $this->message
        ], Response::HTTP_NOT_FOUND);
    }
}
