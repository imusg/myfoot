<?php

namespace App\Domain\Clients\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryAddressResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'region' => $this->region,
            'city' => $this->city,
            'street' => $this->street,
            'house' => $this->house,
            'district' => $this->district,
            'districtName' => $this->districts ? $this->districts->title : null,
            'apartment' => $this->apartment,
            'entrance' => $this->entrance,
            'comment' => $this->comment,
            'isMain' => $this->isMain === 1
        ];
    }
}
