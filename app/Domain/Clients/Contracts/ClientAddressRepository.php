<?php

namespace App\Domain\Clients\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface ClientAddressRepository extends BaseRepository
{
}
