<?php

namespace App\Domain\Clients\Policies;

use App\Domain\Users\Entities\User;
use App\Domain\Clients\Entities\Client;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientsPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Client $client)
    {
        if ($user->branch_id === null) {
            return $user->can('view clients');
        }
        return $user->branch_id === $client->branch_id;
    }

    public function update(User $user, Client $client)
    {
        if ($user->branch_id === null) {
            return $user->can('update clients');
        }
        return $user->branch_id === $client->branch_id;
    }

    public function delete(User $user, Client $client)
    {
        if ($user->branch_id === null) {
            return $user->can('delete clients');
        }
        return $user->branch_id === $client->branch_id;
    }
}
