<?php

namespace App\Domain\Unit\Tests\Feature;

use App\Domain\Dishies\Entities\Dish;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class DishControllerTest extends TestCase
{
    private array $dish;
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
        $this->dish = [
            'id',
            'title',
            'barcode',
            'workshop',
            'tax',
            'image',
            'color',
            'cooking_process',
            'cooking_time',
            'ingredients',
            'category'
        ];
    }

    public function testIndexDish(): void
    {
        $this->get(route('dish.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    $this->dish
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    public function testShowDish(): void
    {
        $dish = factory(Dish::class)
            ->create();
        $response = $this->get(
            route('dish.show', $dish->id),
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => $this->dish,
                'meta',
            ]);
        $data = $response->json('data');
        $this->assertDatabaseHas('dishes', ['id' => $data['id']]);
    }

    public function testStoreDish() : void
    {
        $response = $this->post(
            route('dish.store'),
            [
                'title' => 'title',
                'barcode' => 'barcode',
                'workshop' => 'workshop',
                'tax' => 1,
                'image' => 'image',
                'color' => 'color',
                'cooking_process' => 'cooking_process',
                'cooking_time' => '14:20:20',
                'category_id' => 1,
                'ingredients'=> [['id' => 1]]
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => $this->dish,
                'meta',
            ]);
        $id = $response->json('data.id');
        $this->assertDatabaseHas('dishes', ['id' => $id]);
    }

    public function testUpdateDish() : void
    {
        $dish = factory(Dish::class)
            ->create();
        $response = $this->put(
            route('dish.update', $dish->id),
            [
                'title' => 'title',
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => $this->dish,
                'meta',
            ]);
        $data = $response->json('data');
        $this->assertDatabaseHas('dishes', ['title' => $data['title']]);
    }

    public function testDestroyDish() : void
    {
        $dish = factory(Dish::class)
            ->create();
        $this->delete(route('dish.destroy', $dish->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('dishes', ['id' => $dish->id]);
    }

}
