<?php

namespace App\Domain\Clients\Tests\Feature;

use App\Domain\Clients\Entities\Client;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class ClientsControllerTest extends TestCase
{
    private array $clients;
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
        $this->clients = [
            'id',
            'name',
            'messenger',
            'balance',
            'email',
            'phone'
        ];
    }

    public function testIndexClients(): void
    {
        $this->get(route('clients.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    $this->clients
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    public function testShowDirectory(): void
    {
        $client = factory(Client::class)
            ->create();
        $response = $this->get(
            route('clients.show', $client->id),
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'messenger',
                    'balance',
                    'email',
                    'phone',
                    'address',
                    'excluded'
                ],
                'meta',
            ]);
        $data = $response->json('data');
        $this->assertDatabaseHas('clients', ['id' => $data['id']]);
    }

    public function testStoreClient() : void
    {
        $response = $this->post(
            route('clients.store'),
            [
                'name' => 'test store client',
                'messenger' => 'vk',
                'email' => $this->faker->safeEmail,
                'phone' => "141241",
                'balance' => 1
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => $this->clients,
                'meta',
            ]);
        $id = $response->json('data.id');
        $this->assertDatabaseHas('clients', ['id' => $id]);
    }

    public function testUpdateClients() : void
    {
        $client = factory(Client::class)
            ->create();
        $response = $this->put(
            route('clients.update', $client->id),
            [
                'name' => 'test update client',
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => $this->clients,
                'meta',
            ]);
        $data = $response->json('data');
        $this->assertDatabaseHas('clients', ['name' => $data['name']]);
    }

    public function testDestroyClients() : void
    {
        $client = factory(Client::class)
            ->create();
        $this->delete(route('clients.destroy', $client->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('clients', ['id' => $client->id]);
    }

}
