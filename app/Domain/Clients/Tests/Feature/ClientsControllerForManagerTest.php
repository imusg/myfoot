<?php

namespace App\Domain\Clients\Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;
use App\Domain\Clients\Entities\Client;

class ClientsControllerForManagerTest extends TestCase
{
    private array $clients;
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'branch_id' => 1
        ]);
        $role = Role::where('name', Role::MANAGER)->first();
        $this->user->assignRole($role);
        $this->actingAs($this->user);
        $this->clients = [
            'id',
            'name',
            'messenger',
            'balance',
            'email',
            'phone'
        ];
    }

    /*
    * Успешное получения всех клиентов
    */
    public function testIndexClients(): void
    {
        factory(Client::class)->create(['branch_id' => 1]);
        $this->get(route('clients.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    $this->clients
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    /*
    * Ошибка получения всех клиентов другого филиала
    */
    public function testIndexClientsForOtherBranch(): void
    {
        $user = factory(User::class)->create([
            'branch_id' => 2
        ]);
        $role = Role::where('name', Role::MANAGER)->first();
        $user->assignRole($role);

        $this->actingAs($user)
            ->get(route('clients.index'))
            ->assertNoContent();
    }
    /*
        @test Получение информации по клиенту филиала менеджера
    */

    public function testShowInfoClient(): void
    {
        $client = factory(Client::class)
            ->create([
                'branch_id' => 1
            ]);
        $response = $this->get(
            route('clients.show', $client->id),
        )->assertSuccessful()
        ->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'messenger',
                'balance',
                'email',
                'phone',
                'address',
                'excluded'
            ],
            'meta',
        ]);

        $data = $response->json('data');
        $this->assertDatabaseHas('clients', ['id' => $data['id']]);
    }

    /*
       @test Ошибка получения информации по клиенту филиала менеджера
    */

    public function testShowInfoClientNoRights(): void
    {
        $client = factory(Client::class)
            ->create();
        $response = $this->get(
            route('clients.show', $client->id),
        )
            ->assertStatus(403);
    }
    /*
       @test Успешно добавление нового клиента
    */

    public function testStoreClient() : void
    {
        $response = $this->post(
            route('clients.store'),
            [
                'name' => 'test store client',
                'messenger' => 'vk',
                'messenger_login' => 'vk12421',
                'email' => 'testClient@example.com',
                'phone' => "141241",
                'balance' => 1
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => $this->clients,
                'meta',
            ]);
        $id = $response->json('data.id');
        $branch = $response->json('data.branch_id');
        $this->assertEquals($response->json('data.branch_id'), $this->user->branch_id);
        $this->assertDatabaseHas('clients', ['id' => $id]);
    }

    /*
       @test Успешно обновления клиента
    */
    public function testUpdateClients() : void
    {
        $client = factory(Client::class)
            ->create(['branch_id' => 1]);
        $response = $this->put(
            route('clients.update', $client->id),
            [
                'name' => 'test update client',
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => $this->clients,
                'meta',
            ]);
        $data = $response->json('data');
        $this->assertDatabaseHas('clients', ['name' => $data['name']]);
    }

    /*
       @test Ошибка обновления клиента, нет прав
    */

    public function testUpdateClientsNoRights() : void
    {
        $client = factory(Client::class)
            ->create(['branch_id' => 2]);
        $response = $this->put(
            route('clients.update', $client->id),
            [
                'name' => 'test update client',
            ]
        )
            ->assertStatus(403);
    }

    /*
       @test Успешно удаление клиента
    */

    public function testDestroyClients() : void
    {
        $client = factory(Client::class)
            ->create(['branch_id' => 1]);
        $this->delete(route('clients.destroy', $client->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('clients', ['id' => $client->id]);
    }

    /*
       @test Ошибка удаление клиента, нет прав
    */

    public function testDestroyClientsNoRights() : void
    {
        $client = factory(Client::class)
            ->create(['branch_id' => 2]);
        $this->delete(route('clients.destroy', $client->id))
            ->assertStatus(403);
    }
}
