<?php

namespace App\Domain\Clients\Tests\Feature;

use App\Domain\Clients\Entities\Client;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class ClientsOrdersControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testIndexClientsOrders(): void
    {
        $this->get(route('clients.orders.index', 1))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'orders'
                ],
                'meta'
            ]);
    }

    public function testStoreClient() : void
    {
       $this->post(
            route('clients.orders.store', 2),
            [
                'date' => 2,
                'meal_id' => 54
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data',
                'meta'
            ]);
    }

    public function testUpdateStoreClients() : void
    {
        $this->put(
            route('clients.orders.update', 2),
            [
                'directory_dish_id' => 2,
            ]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data',
                'meta',
            ]);
    }

    public function testDestroyClients() : void
    {
        $this->delete(route('clients.orders.destroy', 2))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data',
                'meta',
            ]);
    }

}
