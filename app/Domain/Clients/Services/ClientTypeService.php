<?php

namespace App\Domain\Clients\Services;

use App\Domain\Clients\Entities\ClientType;

class ClientTypeService
{

    public function findByFilters()
    {
        $ClientTypes = ClientType::when(request()->user()->branch_id, function ($query) {
            $query->where('branch_id', request()->user()->branch_id);
        })->paginate(20);
        return $ClientTypes;
    }

    public function store(array $data)
    {
        $ClientType = ClientType::create($data);
        return $ClientType;
    }

    public function update(int $id, array $data)
    {
        return tap(ClientType::find($id))->update($data);
    }

    public function destroy(int $id)
    {
        return tap(ClientType::find($id))->delete();
    }
}
