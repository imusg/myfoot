<?php

namespace App\Domain\Clients\Services;

use App\Domain\Clients\Entities\District;

class DistrictService
{

    public function findByFilters()
    {
        $districts = District::where('branch_id', request()->user()->branch_id)->paginate(20);
        return $districts;
    }

    public function store(array $data)
    {
        $district = District::create($data);
        return $district;
    }

    public function update(int $id, array $data)
    {
        return tap(District::find($id))->update($data);
    }

    public function destroy(int $id)
    {
        return tap(District::find($id))->delete();
    }
}
