<?php

namespace App\Domain\Clients\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Domain\Clients\Entities\InteractionType;

class InteractionTypesTableSeeder extends Seeder
{
    public function run()
    {
        $iteractions = collect([
            'Запрос информации',
            'Изменение условий доставки',
            'Изменение рациона',
            'Продление заказа',
            'Заморозка',
            'Обратная связь',
            'Жалоба'
        ]);
        $iteractions->each(function ($item) {
            InteractionType::create(['title' => $item]);
        });
    }
}
