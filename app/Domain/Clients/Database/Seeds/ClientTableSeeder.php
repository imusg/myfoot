<?php

namespace App\Domain\Clients\Database\Seeds;

use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Seeder;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class ClientTableSeeder extends Seeder
{
    public function run()
    {
        $fileName = storage_path('app/import/import_clients.xls');
        $reader = new Xls();

        $spreadsheet = $reader->load($fileName)->getActiveSheet();
        $collectClients = collect($spreadsheet->toArray());

        $collectClients->filter(function ($value) {
            return $value[1] !== null && $value[1] !== 'ФИО';
        })->each(function ($item) {
            $name = explode(' ', $item[1]);
            $messanger_login = null;

            $lastName = mb_substr($name[1], 0, 1) == '@' || mb_substr($name[1], 0, 1) == '+' ? '': $name[1];
            if ($name[1] === '') {
                $lastName = mb_substr($name[2], 0, 1) == '@' || mb_substr($name[2], 0, 1) == '+' ? '' : $name[2];
                if (mb_substr($name[2], 0, 1)  === '@') {
                    $messanger_login = $name[2];
                }
            } else {
                if (mb_substr($name[1], 0, 1) === '@') {
                    $messanger_login = $name[1];
                }
            }
            $firstName = $name[0];
            $name = $firstName.' '.$lastName;

            Client::create([
                'name' => $name,
                'phone' => $item[3],
                'sale' => preg_replace('/[^0-90-9]/', '', $item[5]) ? preg_replace('/[^0-90-9]/', '', $item[5]) : 0,
                'amount_purchases' => $item[6],
                'messenger_login' => $messanger_login
            ]);
        });
    }
}
