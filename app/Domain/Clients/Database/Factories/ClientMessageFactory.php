<?php

namespace App\Domain\Clients\Database\Factories;

use App\Domain\Clients\Entities\ClientMessage;
use App\Infrastructure\Abstracts\ModelFactory;

class ClientMessageFactory extends ModelFactory
{
    protected string $model = ClientMessage::class;

    public function fields() : array
    {

        return [
            'text' => $this->faker->text(100),
            'image' => $this->faker->imageUrl(),
            'manager_id' => 1,
            'client_id' => $this->faker->numberBetween(1, 2),
        ];
    }

    public function states()
    {
        // TODO: Implement fields() method.
    }
}
