<?php

namespace App\Domain\Clients\Database\Factories;

use App\Domain\Clients\Entities\Client;
use App\Infrastructure\Abstracts\ModelFactory;
use App\Domain\Clients\Entities\ClientItecation;

class ClientIteractionsFactory extends ModelFactory
{
    protected string $model = ClientItecation::class;

    public function fields(): array
    {
        $messenger = [
            'WhatsApp',
            'Viber',
            'Telegram',
            'Instagram Direct',
            'Facebook',
            'Vkontakte',
        ];
        return [
            'datetime' => $this->faker->datetime,
            'messenger' => $messenger[$this->faker->numberBetween(0, 5)],
            'client_id' => 1,
            'type_id' => 1,
            'manager_id' => 5,
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
