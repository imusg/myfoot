<?php

namespace App\Domain\Clients\Database\Factories;

use App\Domain\Clients\Entities\ClientDeliveryAddress;
use App\Infrastructure\Abstracts\ModelFactory;

class ClientDeliveryAddressFactory extends ModelFactory
{
    protected string $model = ClientDeliveryAddress::class;

    public function fields(): array
    {
        return [
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
