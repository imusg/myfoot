<?php

namespace App\Domain\Clients\Database\Factories;

use App\Domain\Clients\Entities\History;
use App\Infrastructure\Abstracts\ModelFactory;

class ClientHistoryOrderFactory extends ModelFactory
{
    protected string $model = History::class;

    public function states()
    {
    }

    public function fields()
    {
    }
}
