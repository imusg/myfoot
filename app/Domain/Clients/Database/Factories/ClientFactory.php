<?php

namespace App\Domain\Clients\Database\Factories;

use App\Domain\Clients\Entities\Client;
use App\Infrastructure\Abstracts\ModelFactory;

class ClientFactory extends ModelFactory
{
    protected string $model = Client::class;

    public function fields(): array
    {
        $messenger = [
            'WhatsApp',
            'Viber',
            'Telegram',
            'Instagram Direct',
            'Facebook',
            'Vkontakte',
        ];
        return [
            'name' => $this->faker->name,
            'messenger' => $messenger[$this->faker->numberBetween(0, 5)],
            'messenger_login' => $this->faker->name,
            'balance' => $this->faker->randomNumber(1),
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
