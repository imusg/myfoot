<?php

namespace App\Domain\Clients\Database\Factories;

use App\Domain\Clients\Entities\ItecationType;
use App\Infrastructure\Abstracts\ModelFactory;

class IteractionsTypeFactory extends ModelFactory
{
    protected string $model = ItecationType::class;

    public function fields(): array
    {
        return [
            'title' => $iteractions[$this->faker->numberBetween(0, 5)]
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
