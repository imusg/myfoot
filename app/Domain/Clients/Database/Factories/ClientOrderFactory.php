<?php

namespace App\Domain\Clients\Database\Factories;

use App\Domain\Clients\Entities\ClientOrder;
use App\Infrastructure\Abstracts\ModelFactory;

class ClientOrderFactory extends ModelFactory
{
    protected string $model = ClientOrder::class;

    public function fields(): array
    {
        return [
            'client_id' => $this->faker->numberBetween(1, 50),
            'order_id' => $this->faker->numberBetween(1, 50),
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
