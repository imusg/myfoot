<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('messenger')->nullable();
            $table->string('messenger_login')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('sale')->default(0);
            $table->decimal('balance')->default(0);
            $table->decimal('amount_purchases')->nullable();
            $table->foreignId('branch_id')->nullable();
            $table->foreign('branch_id')
                ->references('id')
                ->on('branches')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
