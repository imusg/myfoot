<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientExcludeIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_exclude_ingredients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ingredient_id')->nullable();
            $table->foreignId('dish_id')->nullable();
            $table->foreignId('client_id');
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('ingredient_id')
                ->references('id')
                ->on('ingredients')
                ->nullOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('dish_id')
                ->references('id')
                ->on('dishes')
                ->nullOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_exclude_ingredients');
    }
}
