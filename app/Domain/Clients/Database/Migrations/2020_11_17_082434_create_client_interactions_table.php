<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInteractionsTable extends Migration
{
    public function up()
    {
        Schema::create('client_interactions', function (Blueprint $table) {
            $table->id();
            $table->string('messenger')->nullable();
            $table->text('text')->nullable();
            $table->date('date')->nullable();
            $table->foreignId('client_id');
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreignId('type_id');
            $table->foreign('type_id')
                ->references('id')
                ->on('interaction_types')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreignId('manager_id')->default(5);
            $table->foreign('manager_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('client_interactions');
    }
}
