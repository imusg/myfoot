<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientMessageImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_message_images', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_message_id');
            $table->foreign('client_message_id')
                ->references('id')
                ->on('client_messages')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_message_images');
    }
}
