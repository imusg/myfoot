<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('client_transactions', function (Blueprint $table) {
            $table->id();
            $table->decimal('value')->default(0);
            $table->foreignId('client_id')->nullable();
            $table->foreignId('order_id')->nullable();
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('client_transactions');
    }
}
