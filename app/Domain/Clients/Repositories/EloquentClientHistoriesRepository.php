<?php

namespace App\Domain\Clients\Repositories;

use App\Domain\Orders\Entities\Order;
use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Clients\Entities\History;
use App\Domain\Clients\Entities\ClientOrder;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Domain\Clients\Contracts\ClientHistoriesRepository;

class EloquentClientHistoriesRepository extends EloquentRepository implements ClientHistoriesRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'amount',
        'paid',
        'history',
        'date',
        'channel',
        'branch_id',
        'meal_id',
        'cycle_data_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'name',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'manager', 'type'
    ];

    public function findByClientHistory($id): LengthAwarePaginator
    {
        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $orderIds = ClientOrder::where('client_id', $id)
            ->get()
            ->map(function ($item) {
                return $item->order_id;
            });

        return QueryBuilder::for(Order::class)
            ->whereIn('id', $orderIds)
            ->where('history', true)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }
}
