<?php

namespace App\Domain\Clients\Repositories;

use App\Domain\Clients\Contracts\ClientAddressRepository;
use App\Domain\Clients\Entities\ClientDeliveryAddress;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Infrastructure\Abstracts\EloquentRepository;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentClientAddressRepository extends EloquentRepository implements ClientAddressRepository
{

}
