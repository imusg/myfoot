<?php

namespace App\Domain\Clients\Repositories;

use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Clients\Entities\ClientMessage;
use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Entities\ClientTransaction;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Clients\Entities\ClientDeliveryAddress;
use App\Domain\Clients\Entities\ClientExcludeIngredient;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Domain\Clients\Contracts\InteractionTypeRepository;

class EloquentInteractionTypeRepository extends EloquentRepository implements InteractionTypeRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'name',
        'balance',
        'messenger_login',
        'messenger',
        'email',
        'phone',
        'branch_id',
        'created_at',
        'updated_at',
    ];
    private array $allowedFilters = [
        'name',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'orders',
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        if (request()->user()->branch_id) {
            return QueryBuilder::for(Client::class)
                ->where('branch_id', request()->user()->branch_id)
                ->select($this->defaultSelect)
                ->allowedFilters($this->allowedFilters)
                ->allowedIncludes($this->allowedIncludes)
                ->allowedSorts($this->allowedSorts)
                ->defaultSort($this->defaultSort)
                ->paginate($perPage);
        }

        return QueryBuilder::for(Client::class)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }
}
