<?php

namespace App\Domain\Clients\Repositories;

use App\Domain\Clients\Contracts\ClientOrderRepository;
use App\Domain\Clients\Entities\Client;
use App\Domain\Clients\Entities\ClientHistoryOrder;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Cycles\Entities\Cycle;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Orders\Entities\HistoryOrder;
use App\Domain\Orders\Entities\Order;
use App\Domain\Orders\Entities\OrderCycle;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Database\Eloquent\Model;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentClientOrderRepository extends EloquentRepository implements ClientOrderRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'name',
        'balance',
        'social',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'name',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'orders',
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(ClientOrder::class)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function findOneById(int $id): Model
    {
        $this->with([
            'orders']);
        return parent::findOneById($id);
    }

    public function findByCriteria(array $criteria)
    {
        $this->with([
            'orders',
            'orders.cycles.cycle'
        ]);
        return $this->model->with($this->with)
            ->where($criteria)
            ->orderByDesc('created_at')
            ->get();
    }

    private function updateClientBalance(int $clientId, int $id, $minus = true): bool
    {
        $dishPrice = DirectoryDish::find($id)->price;
        $client = Client::find($clientId);
        if ($minus) {
            $client->balance -= $dishPrice;
        } else {
            $client->balance += $dishPrice;
        }
        return $client->save();
    }


    public function storeOrder(int $id, array $data)
    {
        $cc = Cycle::where('current', 1);
        if ($cc->exists()) {
            $cycle = $cc->first()->id;
        } else {
            throw new \RuntimeException('Active Cycle not Found', );
        }
        $dishPrice = DirectoryDish::find($data['directory_dish_id'])->price;

        $data['cycle_id'] = $cycle;
        $orderCycle = OrderCycle::create($data);

        $order = Order::create([
            'amount' => $dishPrice,
            'paid' => $this->updateClientBalance($id, $data['directory_dish_id']),
            'order_cycle_id' => $orderCycle->id
        ]);
        ClientOrder::create([
            'client_id' => $id,
            'order_id' => $order->id
        ]);

        return $order;
    }

    public function updateOrder(int $id, array $data)
    {
        $dishPrice = DirectoryDish::find($data['directory_dish_id'])->price;
        $idOrderCycle = Order::where('id', $id)->first()->order_cycle_id;
        $oldDirectoryDish = OrderCycle::where('id', $idOrderCycle)
            ->first()->directory_dish_id;
        OrderCycle::where('id', $idOrderCycle)
            ->update($data);
        Order::where('id', $id)->update([
            'amount' => $dishPrice
        ]);
        $clientId = ClientOrder::where('order_id', $id)->first()->client_id;
        $this->updateClientBalance($clientId, $oldDirectoryDish, false);
        $this->updateClientBalance($clientId, $data['directory_dish_id']);
        return true;
    }

    public function deleteOrder(int $id)
    {
        $order = Order::where('id', $id)->first();
        $clientOrder = ClientOrder::where('order_id', $id)->first();
        $history = HistoryOrder::create($order->toArray());
        $orderCycle = OrderCycle::where('id', $order->order_cycle_id)->first();
        ClientHistoryOrder::create([
            'client_id' => $clientOrder->client_id,
            'history_order_id' => $history->id
        ]);
        $this->updateClientBalance($clientOrder->client_id, $orderCycle->directory_dish_id, false);
        Order::where('id', $id)->delete();
        ClientOrder::where('order_id', $id)->delete();
    }
}
