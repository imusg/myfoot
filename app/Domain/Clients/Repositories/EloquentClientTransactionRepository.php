<?php

namespace App\Domain\Clients\Repositories;

use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Clients\Entities\ClientTransaction;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Domain\Clients\Contracts\ClientTransactionRepository;

class EloquentClientTransactionRepository extends EloquentRepository implements ClientTransactionRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'value',
        'client_id',
        'type',
        'order_id',
        'created_at',
        'updated_at',
    ];
    private array $allowedFilters = [
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'order',
    ];

    public function findByFilter($id): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;
        $this->with(['order']);
        return QueryBuilder::for(ClientTransaction::class)
            ->where('client_id', $id)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }
}
