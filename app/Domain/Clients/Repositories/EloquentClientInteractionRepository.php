<?php

namespace App\Domain\Clients\Repositories;

use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Clients\Entities\ClientMessage;
use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Entities\ClientInteraction;
use App\Domain\Clients\Entities\ClientTransaction;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Clients\Entities\ClientDeliveryAddress;
use App\Domain\Clients\Entities\ClientExcludeIngredient;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Domain\Clients\Contracts\ClientInteractionsRepository;

class EloquentClientInteractionRepository extends EloquentRepository implements ClientInteractionsRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id','messenger', 'date', 'client_id', 'type_id', 'manager_id', 'text',
        'created_at',
        'updated_at',
    ];
    private array $allowedFilters = [
        'date',
        'type_id'
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'client',
        'type',
        'manager',
    ];

    public function findByClientId($id): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(ClientInteraction::class)
            ->where('client_id', $id)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }
}
