<?php

namespace App\Domain\Clients\Repositories;

use App\Domain\Clients\Filters\FilterBetween;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Clients\Entities\ClientMessage;
use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Entities\ClientTransaction;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Clients\Entities\ClientDeliveryAddress;
use App\Domain\Clients\Entities\ClientExcludeIngredient;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentClientRepository extends EloquentRepository implements ClientRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'name',
        'balance',
        'messenger_login',
        'messenger',
        'email',
        'balance_forecast',
        'phone',
        'taxSystem',
        'branch_id',
        'type_id',
        'created_at',
        'updated_at',
    ];
    private array $allowedFilters = [
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'orders',
        'branch'
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Client::class)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->select($this->defaultSelect)
            ->allowedFilters([
                'name',
                'messenger',
                'messenger_login',
                'phone',
                'branch.title',
                'type_id',
                'address.district',
                AllowedFilter::custom('created_at', new FilterBetween)
            ])
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function findOneById(int $id): Model
    {
        $this->with([
            'address',
            'excluded',
            'excluded.ingredient',
        ]);
        return parent::findOneById($id);
    }

    public function store(array $data): Model
    {
        $data['branch_id'] = request()->user()->branch_id;
        return parent::store($data);
    }

    public function findOneByIdWithOrders(int $id): Model
    {
        return Client::find($id)->load(['orders', 'orders.cycles.cycle']);
    }

    public function storeDeliveryAddress(string $id, array $data): Model
    {
        return $this->findOneById($id)
            ->hasOne(ClientDeliveryAddress::class, 'client_id', 'id')
            ->create($data);
    }

    public function getExcludeIngredients($id)
    {
        return ClientExcludeIngredient::where('client_id', $id)->get();
    }

    public function storeExclude($model, $data)
    {
        return $model->excluded()->create($data);
    }

    public function updateExclude(int $id, array $data)
    {
        return tap(ClientExcludeIngredient::find($id))->update($data);
    }

    public function destroyExclude($id): bool
    {
        return ClientExcludeIngredient::find($id)->delete();
    }

    public function changeBalance($model, $balance)
    {
        if ($balance < 0 && request()->user()->hasRole('manager')) {
            return $model;
        }
        ClientTransaction::create([
                    'type' => $balance > 0 ? 'Приход' : 'Cписание',
                    'client_id' => $model->id,
                    'value' => $balance
                ]);
        return tap($model)->update([
                    'balance' => $model->balance + $balance
                ]);
    }
}
