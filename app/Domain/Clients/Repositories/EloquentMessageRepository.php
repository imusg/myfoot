<?php

namespace App\Domain\Clients\Repositories;

use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Clients\Entities\ClientMessage;
use App\Domain\Clients\Contracts\MessageRepository;
use App\Domain\Clients\Entities\ClientMessageImage;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentMessageRepository extends EloquentRepository implements MessageRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'text',
        'date',
        'time',
        'manager_id',
        'created_at',
        'updated_at',
    ];
    private array $allowedFilters = [
        'name',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'manager',
        'images'
    ];

    public function findByFiltersCustom($client_id): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(ClientMessage::class)
            ->where('client_id', $client_id)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function store(array $data): Model
    {
        $stored = parent::store($data);
        if (isset($data['images'])) {
            $images = collect($data['images']);
            $images->each(function ($item) use ($stored) {
              ClientMessageImage::create([
                  'client_message_id' => $stored->id,
                  'url' => $item['url']
              ]);
            });
        }
        return $stored;
    }
    public function update(Model $model, array $data): Model
    {
        $model->images()->delete();
        if (isset($data['images'])) {
            $images = collect($data['images']);
            $images->each(function ($item) use ($model) {
                $model->images()->create([
                  'url' => $item['url']
              ]);
            });
        }
        return parent::update($model, $data);
    }

    public function destroy($model): bool
    {
        $model->images()->delete();
        return parent::destroy($model);
    }

}
