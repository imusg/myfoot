<?php
namespace App\Domain\Clients\Listeners\Observers;

use Exception;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Clients\Entities\ClientTransaction;

class ClientOrderObserver
{
    public function created(ClientOrder $clientOrder)
    {
        $balance = $clientOrder->client->balance;
        $amount = $clientOrder->order->amount;

        // if ($balance - $amount < 0) {
        //     $clientOrder->order()->update([
        //         'frozen' => true
        //     ]);
        //     throw new Exception('Не достаточно средств для оплаты заказа! Заказ заморожен!', 400);
        // }

        ClientTransaction::create([
            'type' => 'Заказ',
            'value' => -$amount,
            'order_id' => $clientOrder->order_id,
            'client_id' => $clientOrder->client_id
        ]);

        $clientOrder->client()->update([
            'balance' => $balance - $amount
        ]);

        $clientOrder->order()->update([
            'paid' => true
        ]);
    }
}
