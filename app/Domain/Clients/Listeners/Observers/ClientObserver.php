<?php
namespace App\Domain\Clients\Listeners\Observers;

use Exception;
use Carbon\Carbon;
use App\Domain\Orders\Entities\Order;
use App\Domain\Clients\Entities\Client;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Clients\Entities\ClientTransaction;

class ClientObserver
{
    public function updating(Client $client)
    {
        if ($client->isDirty('balance')) {
            $clientOrderIds = ClientOrder::where('client_id', $client->id)
                ->get()
                ->map(function ($item) {
                    return $item->order_id;
                });
            $orders = Order::whereIn('id', $clientOrderIds)->where('date', '>=', Carbon::now()->format('Y-m-d'))->get();
            if ($orders->count()) {
                $sumAmount = $orders->sum('amount');
                if ($sumAmount !== 0) {
                    $client->balance_forecast = floor($client->balance / $sumAmount);
                }
            } else {
                $client->balance_forecast = 0;
            }
        }
    }
}
