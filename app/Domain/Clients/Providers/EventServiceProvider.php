<?php

namespace App\Domain\Clients\Providers;

use App\Domain\Clients\Entities\Client;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Clients\Listeners\Observers\ClientObserver;
use App\Domain\Clients\Listeners\Observers\ClientOrderObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        ClientOrder::observe(ClientOrderObserver::class);
        Client::observe(ClientObserver::class);
    }
}
