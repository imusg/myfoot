<?php

namespace App\Domain\Clients\Providers;

use Illuminate\Routing\Router;
use App\Domain\Clients\Http\Controllers\ClientsController;
use App\Domain\Clients\Http\Controllers\DistrictController;
use App\Domain\Clients\Http\Controllers\ClientTypeController;
use App\Domain\Clients\Http\Controllers\ClientOrderController;
use App\Domain\Clients\Http\Controllers\ClientAddressController;
use App\Domain\Clients\Http\Controllers\ClientExcludeController;
use App\Domain\Clients\Http\Controllers\ClientHistoryController;
use App\Domain\Clients\Http\Controllers\ClientMessagesController;
use App\Domain\Clients\Http\Controllers\ClientInteractionController;
use App\Domain\Clients\Http\Controllers\ClientsTransactionController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->apiResource('clients', ClientsController::class)
            ->names('clients');

        $router
            ->apiResource('clients.excluded', ClientExcludeController::class)
            ->shallow();

        $router->put('clients/excluded/{id}', [ClientExcludeController::class, 'update']);

        $router
            ->apiResource('clients.address', ClientAddressController::class)
            ->shallow();

        $router->group(['prefix' => 'clients'], function ($router) {
            $router->get('{id}/messages', [ClientMessagesController::class, 'index']);
            $router->post('{id}/messages', [ClientMessagesController::class, 'store']);
            $router->delete('messages/{message_id}', [ClientMessagesController::class, 'destroy']);
            $router->put('messages/{message_id}', [ClientMessagesController::class, 'update']);
            $router->post('{id}/balance', [ClientsController::class, 'changeBalance']);
            $router->get('{id}/transactions', [ClientsTransactionController::class, 'index']);
            $router->post('{id}/transactions', [ClientsTransactionController::class, 'store']);

            $router->get('{id}/interactions', [ClientInteractionController::class, 'index']);
            $router->post('{id}/interactions', [ClientInteractionController::class, 'store']);

            $router->put('interactions/{id}', [ClientInteractionController::class, 'update']);
            $router->delete('interactions/{id}', [ClientInteractionController::class, 'destroy']);

            $router->get('{id}/history', [ClientHistoryController::class, 'index']);
        });

        $router->apiResource('districts', DistrictController::class)
            ->only(['index', 'store', 'update', 'destroy']);

        $router->apiResource('client/types', ClientTypeController::class)
            ->only(['index', 'store', 'update', 'destroy']);
    }
}
