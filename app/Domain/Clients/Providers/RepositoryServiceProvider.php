<?php

namespace App\Domain\Clients\Providers;

use App\Domain\Clients\Entities\Client;
use Illuminate\Support\ServiceProvider;
use App\Domain\Clients\Entities\History;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Clients\Entities\ClientMessage;
use App\Domain\Clients\Entities\InteractionType;
use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Entities\ClientInteraction;
use App\Domain\Clients\Entities\ClientTransaction;
use App\Domain\Clients\Contracts\MessageRepository;
use App\Domain\Clients\Entities\ClientDeliveryAddress;
use App\Domain\Clients\Contracts\ClientOrderRepository;
use App\Domain\Clients\Contracts\ClientAddressRepository;
use App\Domain\Clients\Contracts\ClientHistoriesRepository;
use App\Domain\Clients\Contracts\InteractionTypeRepository;
use App\Domain\Clients\Contracts\ClientTransactionRepository;
use App\Domain\Clients\Repositories\EloquentClientRepository;
use App\Domain\Clients\Contracts\ClientInteractionsRepository;
use App\Domain\Clients\Repositories\EloquentMessageRepository;
use App\Domain\Clients\Repositories\EloquentClientOrderRepository;
use App\Domain\Clients\Repositories\EloquentClientAddressRepository;
use App\Domain\Clients\Repositories\EloquentClientHistoriesRepository;
use App\Domain\Clients\Repositories\EloquentInteractionTypeRepository;
use App\Domain\Clients\Repositories\EloquentClientInteractionRepository;
use App\Domain\Clients\Repositories\EloquentClientTransactionRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(ClientRepository::class, function () {
            return new EloquentClientRepository(new Client());
        });
        $this->app->singleton(ClientAddressRepository::class, function () {
            return new EloquentClientAddressRepository(new ClientDeliveryAddress());
        });
        $this->app->singleton(MessageRepository::class, function () {
            return new EloquentMessageRepository(new ClientMessage());
        });

        $this->app->singleton(ClientOrderRepository::class, function () {
            return new EloquentClientOrderRepository(new ClientOrder());
        });
        $this->app->singleton(ClientHistoriesRepository::class, function () {
            return new EloquentClientHistoriesRepository(new History());
        });
        $this->app->singleton(ClientTransactionRepository::class, function () {
            return new EloquentClientTransactionRepository(new ClientTransaction());
        });
        $this->app->singleton(ClientInteractionsRepository::class, function () {
            return new EloquentClientInteractionRepository(new ClientInteraction());
        });
        $this->app->singleton(InteractionTypeRepository::class, function () {
            return new EloquentInteractionTypeRepository(new InteractionType());
        });
    }

    public function provides(): array
    {
        return [
            ClientRepository::class,
            ClientAddressRepository::class,
            MessageRepository::class,
            ClientOrderRepository::class,
            ClientHistoriesRepository::class,
            ClientTransactionRepository::class,
            ClientInteractionsRepository::class,
            InteractionTypeRepository::class,
        ];
    }
}
