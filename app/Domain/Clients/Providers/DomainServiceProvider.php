<?php
namespace App\Domain\Clients\Providers;

use App\Domain\Clients\Entities\Client;
use App\Domain\Clients\Policies\ClientsPolicy;
use App\Infrastructure\Abstracts\ServiceProvider;
use App\Domain\Clients\Database\Factories\ClientFactory;
use App\Domain\Clients\Database\Factories\ClientOrderFactory;
use App\Domain\Clients\Database\Factories\ClientMessageFactory;
use App\Domain\Clients\Database\Factories\ClientHistoryOrderFactory;
use App\Domain\Clients\Database\Factories\ClientDeliveryAddressFactory;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'clients';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class
    ];

    protected array $policies = [
        Client::class => ClientsPolicy::class
    ];

    protected array $factories = [
        ClientFactory::class,
        ClientDeliveryAddressFactory::class,
        ClientHistoryOrderFactory::class,
        ClientMessageFactory::class,
        ClientOrderFactory::class
    ];
}
