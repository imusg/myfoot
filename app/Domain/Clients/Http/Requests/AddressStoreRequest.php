<?php

namespace App\Domain\Clients\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'region' => 'nullable',
            'city' => 'nullable',
            'street' => 'nullable',
            'house' => 'nullable',
            'apartment' => 'nullable',
            'district' => 'nullable',
            'entrance' => 'nullable',
            'comment' => 'nullable',
            'isMain' => 'nullable'
        ];
    }
}
