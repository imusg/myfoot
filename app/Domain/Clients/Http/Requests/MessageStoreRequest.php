<?php
namespace App\Domain\Clients\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'text' => 'nullable',
            'images' => 'nullable',
            'date' => 'nullable',
            'time' => 'nullable',
        ];
    }
}
