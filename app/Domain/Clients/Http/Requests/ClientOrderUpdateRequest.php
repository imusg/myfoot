<?php


namespace App\Domain\Clients\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ClientOrderUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'directory_dish_id' => 'exists:directory_dishes,id',
        ];
    }
}
