<?php

namespace App\Domain\Clients\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'name' => 'string|nullable',
            'messenger' => 'string|nullable',
            'messenger_login' => 'string|nullable',
            'taxSystem' => 'string|nullable',
            'email' => 'email|string|nullable',
            'phone' => 'string|nullable',
            'balance' => 'numeric|nullable',
            'type_id' => 'nullable',
            'timeDelivery' => 'nullable',
            'created_at' => 'nullable'
        ];
    }
}
