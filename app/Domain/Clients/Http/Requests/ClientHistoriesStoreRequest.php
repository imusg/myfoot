<?php


namespace App\Domain\Clients\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ClientHistoriesStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            "type_id" => 'exists:event_types,id',
            "event" => "string",
            "messenger" => "string",
            "date" => "string",
            "manager_id" => 'exists:users,id'
        ];
    }
}
