<?php


namespace App\Domain\Clients\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ClientMessageStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'platform' => 'string',
            'text' => 'string',
            'image' => 'string',
        ];
    }
}
