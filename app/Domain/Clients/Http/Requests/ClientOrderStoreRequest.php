<?php


namespace App\Domain\Clients\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ClientOrderStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'day' => 'integer',
            'week' => 'integer',
            'directory_dish_id' => 'exists:directory_dishes,id',
        ];
    }
}
