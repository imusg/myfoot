<?php

namespace App\Domain\Clients\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema (
 *    schema="ClientStoreRequest",
 *    type="object",
 *    @OA\Property (
 *      property="name",
 *      type="string",
 *      description="Имя клиента",
 *      example="Иванова Валентина Ивановна",
 *    ),
 *    @OA\Property (
 *      property="messenger",
 *      type="sting",
 *      description="Месенджер используемый клиентом",
 *      example="WhatsApp",
 *    ),
 *    @OA\Property (
 *      property="email",
 *      type="string",
 *      description="Email клиента",
 *      example="example@example.com",
 *    ),
 *    @OA\Property (
 *      property="phone",
 *      type="string",
 *      example="+79999999900"
 *    ),
 *    @OA\Property (
 *      property="balance",
 *      type="integer",
 *      description="Баланс пользователя",
 *      example="100"
 *    )
 * )
 * */
class ClientInteractionStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'messenger' => 'required',
            'date' => 'required',
            'type_id' => 'required',
            'text' => 'string',
            'manager_id' => 'required'
        ];
    }
}
