<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => ClientResource::collection($this->collection),
        ];
    }
}
