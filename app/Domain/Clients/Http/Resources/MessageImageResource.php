<?php
namespace App\Domain\Clients\Http\Resources;

use App\Domain\Manager\Http\Resources\ManagerResources;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageImageResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'url' => $this->url,
        ];
    }
}
