<?php


namespace App\Domain\Clients\Http\Resources;


use Illuminate\Support\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Manager\Http\Resources\ManagerResources;

class MessageResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'date_format' => Carbon::make("{$this->date} {$this->time}")->locale('ru_RU')->isoFormat('DD MMMM YYYY h:mm'),
            'date' => $this->date,
            'time' => $this->time,
            'images' => MessageImageResource::collection($this->images),
            'manager' => ManagerResources::make($this->manager)
        ];
    }
}
