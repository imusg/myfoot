<?php


namespace App\Domain\Clients\Http\Resources;


use App\Domain\Orders\Http\Resources\OrderResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientOrderResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'orders' => OrderResource::collection($this->orders)
        ];
    }
}
