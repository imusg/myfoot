<?php

namespace App\Domain\Clients\Http\Resources;

use App\Domain\Orders\Http\Resources\OrderResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientHistoriesCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => OrderResource::collection($this->collection),
        ];
    }
}
