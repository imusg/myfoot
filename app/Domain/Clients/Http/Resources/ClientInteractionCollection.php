<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientInteractionCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => ClientInteractionResource::collection($this->collection),
        ];
    }
}
