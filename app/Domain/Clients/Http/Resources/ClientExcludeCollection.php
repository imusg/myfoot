<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientExcludeCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => ClientExcludeResource::collection($this->collection),
        ];
    }
}
