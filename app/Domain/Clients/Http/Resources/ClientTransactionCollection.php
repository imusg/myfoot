<?php
namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientTransactionCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => ClientTransactionResource::collection($this->collection),
        ];
    }
}
