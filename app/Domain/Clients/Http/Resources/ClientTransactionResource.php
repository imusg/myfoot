<?php
namespace App\Domain\Clients\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Orders\Http\Resources\OrderResource;

/**
 * @OA\Schema (
 *    schema="ClientWithAddressExcluded",
 *    type="object",
 *    @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="name",
 *      type="string",
 *      description="Имя клиента",
 *      example="Елена Ивановна Киселёваа",
 *    ),
 *     @OA\Property (
 *      property="messenger",
 *      type="string",
 *      description="Месенджер клиента",
 *      example="WhatsApp",
 *    ),
 *    @OA\Property (
 *      property="balance",
 *      type="integer",
 *      description="Баланс",
 *      example="100.0",
 *    ),
 *    @OA\Property (
 *      property="email",
 *      type="sting",
 *      example="test@test.com",
 *    ),
 *    @OA\Property (
 *      property="phone",
 *      type="string",
 *      example="8-800-268-2375",
 *    ),
 *    @OA\Property(
 *      property="address",
 *      type="array",
 *      @OA\Items(
 *          ref="#/components/schemas/Address"
 *      )
 *    ),
 *    @OA\Property(
 *      property="excluded",
 *      type="array",
 *      @OA\Items(
 *         ref="#/components/schemas/ExcludedIngredient"
 *      )
 *    ),
 * )
 */

class ClientTransactionResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'value' => $this->value,
            'type' => $this->type,
            'order' => $this->order ? ['date' => Carbon::make($this->order->date)->format('d.m.Y')] : null,
            'date' => $this->created_at->format('d.m.Y H:i')
        ];
    }
}
