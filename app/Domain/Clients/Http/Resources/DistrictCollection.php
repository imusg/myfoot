<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DistrictCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => DistrictResource::collection($this->collection),
        ];
    }
}
