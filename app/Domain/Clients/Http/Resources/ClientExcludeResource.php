<?php

namespace App\Domain\Clients\Http\Resources;

use App\Domain\Clients\Resources\DeliveryAddressResource;
use App\Domain\Dishies\Http\Resources\DishResource;
use App\Domain\Ingredients\Http\Resources\IngredientResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema (
 *    schema="ExcludedIngredient",
 *    type="object",
 *    @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="ingredient",
 *      ref="#/components/schemas/Ingredient"
 *    ),
 *    @OA\Property (
 *      property="dish",
 *      ref="#/components/schemas/Dish"
 *    )
 * )
 */

class ClientExcludeResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'ingredient' => $this->ingredient ? $this->ingredient->title : null,
            'description' => $this->description,
            'replaceIngredient' => $this->replaceIngredient ? $this->replaceIngredient->title : null,
            'excludedIngredientId' => $this->ingredient ? $this->ingredient->id : null,
            'replaceIngredientId' => $this->replaceIngredient ? $this->replaceIngredient->id : null
        ];
    }
}
