<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientTypeCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => ClientTypeResource::collection($this->collection),
        ];
    }
}
