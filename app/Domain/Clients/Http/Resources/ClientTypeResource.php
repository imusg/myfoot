<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientTypeResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
        ];
    }
}
