<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientOrderCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => ClientOrderResource::collection($this->collection),
        ];
    }
}
