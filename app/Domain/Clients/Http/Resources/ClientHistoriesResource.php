<?php

namespace App\Domain\Clients\Http\Resources;

use App\Domain\Event\Http\Resources\EventTypeResource;
use App\Domain\Manager\Http\Resources\ManagerResources;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientHistoriesResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'event' => $this->event,
            'messenger' => $this->messenger,
            'date' => $this->date,
            'type' => EventTypeResource::make($this->type),
            'manager' => ManagerResources::make($this->manager),
        ];
    }
}
