<?php

namespace App\Domain\Clients\Http\Resources;
use App\Domain\Clients\Http\Resources\MessageResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MessageCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => MessageResource::collection($this->collection),
        ];
    }
}
