<?php

namespace App\Domain\Clients\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Orders\Http\Resources\OrderResource;
use App\Domain\Branches\Http\Resources\BranchResource;
use App\Domain\Clients\Resources\DeliveryAddressResource;

/**
 * @OA\Schema (
 *    schema="ClientWithAddressExcluded",
 *    type="object",
 *    @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="name",
 *      type="string",
 *      description="Имя клиента",
 *      example="Елена Ивановна Киселёваа",
 *    ),
 *     @OA\Property (
 *      property="messenger",
 *      type="string",
 *      description="Месенджер клиента",
 *      example="WhatsApp",
 *    ),
 *    @OA\Property (
 *      property="balance",
 *      type="integer",
 *      description="Баланс",
 *      example="100.0",
 *    ),
 *    @OA\Property (
 *      property="email",
 *      type="sting",
 *      example="test@test.com",
 *    ),
 *    @OA\Property (
 *      property="phone",
 *      type="string",
 *      example="8-800-268-2375",
 *    ),
 *    @OA\Property(
 *      property="address",
 *      type="array",
 *      @OA\Items(
 *          ref="#/components/schemas/Address"
 *      )
 *    ),
 *    @OA\Property(
 *      property="excluded",
 *      type="array",
 *      @OA\Items(
 *         ref="#/components/schemas/ExcludedIngredient"
 *      )
 *    ),
 * )
 */

class ClientResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'messenger' => $this->messenger,
            'messenger_login' => $this->messenger_login,
            'balance' => $this->balance,
            'email' => $this->email,
            'phone' => $this->phone,
            'branch_id' => $this->branch_id,
            'taxSystem' => $this->taxSystem,
            'type_id' => $this->type_id,
            'balance_forecast' => $this->balance_forecast,
            'district' => count($this->address) ? $this->address[0]->districts ? $this->address[0]->districts->title: null : null,
            'branch' => BranchResource::make($this->branch),
            'type' => $this->type,
            'created_at' => $this->created_at->format('Y-m-d'),
            'created_at_formatted' => $this->created_at->locale('ru_RU')
                                ->isoFormat('DD MMMM YYYY'),
            'timeDelivery' => $this->timeDelivery,
            'histories' => ClientHistoriesResource::collection($this->whenLoaded('histories')),
            'orders' => OrderResource::collection($this->whenLoaded('orders')),
            'address' => DeliveryAddressResource::collection($this->whenLoaded('address')),
            'excluded' => ClientExcludeResource::collection($this->whenLoaded('excluded')),
        ];
    }
}
