<?php

namespace App\Domain\Clients\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Clients\Services\DistrictService;
use App\Domain\Clients\Http\Resources\DistrictResource;
use App\Domain\Clients\Http\Resources\DistrictCollection;
use App\Domain\Clients\Http\Requests\DistrictStoreRequest;
use App\Domain\Clients\Http\Requests\DistrictUpdateRequest;

class DistrictController extends Controller
{
    private DistrictService $districtService;

    public function __construct(DistrictService $districtService)
    {
        $this->districtService = $districtService;
        $this->resourceItem = DistrictResource::class;
        $this->resourceCollection = DistrictCollection::class;
    }

    public function index()
    {
        $response = $this->districtService->findByFilters();
        return $this->respondWithCollection($response);
    }

    public function store(DistrictStoreRequest $request)
    {
        $data = $request->validated();
        $data['branch_id'] = request()->user()->branch_id;
        $response = $this->districtService->store($data);
        return $this->respondWithItem($response);
    }

    public function update(int $id, DistrictUpdateRequest $request)
    {
        $data = $request->validated();
        $response = $this->districtService->update($id, $data);
        return $this->respondWithItem($response);
    }

    public function destroy(int $id)
    {
        $response = $this->districtService->destroy($id);
        return $this->respondWithItem($response);
    }
}
