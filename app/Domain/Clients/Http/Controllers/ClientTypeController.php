<?php

namespace App\Domain\Clients\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Clients\Services\ClientTypeService;
use App\Domain\Clients\Http\Resources\ClientTypeResource;
use App\Domain\Clients\Http\Resources\ClientTypeCollection;
use App\Domain\Clients\Http\Requests\ClientTypeStoreRequest;
use App\Domain\Clients\Http\Requests\ClientTypeUpdateRequest;

class ClientTypeController extends Controller
{
    private ClientTypeService $clientTypeService;

    public function __construct(ClientTypeService $clientTypeService)
    {
        $this->clientTypeService = $clientTypeService;
        $this->resourceItem = ClientTypeResource::class;
        $this->resourceCollection = ClientTypeCollection::class;
    }

    public function index()
    {
        $response = $this->clientTypeService->findByFilters();
        return $this->respondWithCollection($response);
    }

    public function store(ClientTypeStoreRequest $request)
    {
        $data = $request->validated();
        $data['branch_id'] = request()->user()->branch_id;
        $response = $this->clientTypeService->store($data);
        return $this->respondWithItem($response);
    }

    public function update(int $id, ClientTypeUpdateRequest $request)
    {
        $data = $request->validated();
        $response = $this->clientTypeService->update($id, $data);
        return $this->respondWithItem($response);
    }

    public function destroy(int $id)
    {
        $response = $this->clientTypeService->destroy($id);
        return $this->respondWithItem($response);
    }
}
