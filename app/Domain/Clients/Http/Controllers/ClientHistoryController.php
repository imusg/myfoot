<?php

namespace App\Domain\Clients\Http\Controllers;

use App\Domain\Clients\Contracts\ClientHistoriesRepository;
use App\Domain\Clients\Http\Requests\ClientHistoriesStoreRequest;
use App\Domain\Clients\Http\Resources\ClientHistoriesCollection;
use App\Domain\Clients\Http\Resources\ClientHistoriesResource;
use App\Domain\Clients\Resources\ClientCollection;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ClientHistoryController extends Controller
{
    private ClientHistoriesRepository $clientHistories;
    public function __construct(ClientHistoriesRepository $clientHistories)
    {
        $this->clientHistories = $clientHistories;
        $this->resourceItem = ClientHistoriesResource::class;
        $this->resourceCollection = ClientHistoriesCollection::class;
    }

    public function index($id)
    {
        $collection = $this->clientHistories->findByClientHistory($id);
        return $this->respondWithCollection($collection);
    }

    public function store(ClientHistoriesStoreRequest $request, $id)
    {
        $data = $request->validated();
        $data['client_id'] = $id;
        $response = $this->clientHistories->store($data);
        return $this->respondWithItem($response);
    }

    public function update(ClientHistoriesStoreRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->clientHistories->update($this->clientHistories->findOneById($id), $data);
        return $this->respondWithItem($response);
    }


    public function destroy($id)
    {
        $response = $this->clientHistories->destroy($this->clientHistories->findOneById($id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }
}
