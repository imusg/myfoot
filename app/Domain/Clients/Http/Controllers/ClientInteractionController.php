<?php

namespace App\Domain\Clients\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Clients\Http\Requests\ClientStoreRequest;
use App\Domain\Clients\Http\Requests\ClientUpdateRequest;
use App\Domain\Clients\Contracts\ClientInteractionsRepository;
use App\Domain\Clients\Http\Resources\ClientInteractionResource;
use App\Domain\Clients\Http\Resources\ClientInteractionCollection;
use App\Domain\Clients\Http\Requests\ClientInteractionStoreRequest;
use App\Domain\Clients\Http\Requests\ClientInteractionUpdateRequest;

class ClientInteractionController extends Controller
{
    private ClientInteractionsRepository $clientInteractionsRepository;

    public function __construct(ClientInteractionsRepository $clientInteractionsRepository)
    {
        $this->clientInteractionsRepository = $clientInteractionsRepository;
        $this->resourceItem = ClientInteractionResource::class;
        $this->resourceCollection = ClientInteractionCollection::class;
    }
    /**
     * @OA\Get(
     * path="/clients",
     * summary="Получение всех клиентов",
     * description="Получение всех клиентов",
     * operationId="indexClients",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех клиентов",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Client"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *        )
     *  )
     * );
     */
    public function index($id)
    {
        $collection = $this->clientInteractionsRepository->findByClientId($id);
        if ($collection->isEmpty()) {
            return $this->respondWithNoContent();
        }
        return $this->respondWithCollection($collection);
    }
    /**
     * @OA\Post(
     * path="/clients",
     * summary="Создание клиента",
     * description="Добавление клиента",
     * operationId="storeClients",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для создания клиента",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/ClientStoreRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Client"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  ClientStoreRequest  $request
     * @return JsonResponse
     */
    public function store(ClientInteractionStoreRequest $request, $id): JsonResponse
    {
        $data = $request->validated();
        $data['client_id'] = $id;
        $clientItecation = $this->clientInteractionsRepository->store($data);
        return $this->respondWithCustomData($clientItecation, Response::HTTP_CREATED);
    }

    /**
     * @OA\Put(
     * path="/clients/{client}",
     * summary="Обновление клиента",
     * description="Обновление клиента",
     * operationId="updateClient",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="client",
     *  in="path",
     *  required=true,
     *  description="Client id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для обновления клиента",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/ClientStoreRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная обновление клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Client"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  ClientUpdateRequest  $request
     * @param $id
     * @return mixed
     */
    public function update(ClientInteractionUpdateRequest $request, $id)
    {
        $clientItecation = $this->clientInteractionsRepository->findOneById($id);
        $data = $request->validated();
        $response = $this->clientInteractionsRepository->update(
            $clientItecation,
            $data
        );
        return $this->respondWithItem($response);
    }

    /**
     * @OA\Delete (
     * path="/clients/{client}",
     * summary="Удаление клиента",
     * description="Удаление клиента",
     * operationId="deleteClient",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="client",
     *  in="path",
     *  required=true,
     *  description="Client id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная удаление клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            @OA\Property(
     *              property="deleted",
     *              type="boolean",
     *              example="true"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        $clientItecation = $this->clientInteractionsRepository->findOneById($id);
        $response = $this->clientInteractionsRepository->destroy($clientItecation);
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }
}
