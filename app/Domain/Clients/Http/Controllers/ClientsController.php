<?php

namespace App\Domain\Clients\Http\Controllers;

use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Http\Requests\ClientStoreRequest;
use App\Domain\Clients\Http\Requests\ClientUpdateRequest;
use App\Domain\Clients\Http\Resources\ClientCollection;
use App\Domain\Clients\Http\Resources\ClientResource;
use App\Domain\Manager\Http\Requests\ManagerTaskUpdateRequest;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ClientsController extends Controller
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->resourceItem = ClientResource::class;
        $this->resourceCollection = ClientCollection::class;
    }
    /**
     * @OA\Get(
     * path="/clients",
     * summary="Получение всех клиентов",
     * description="Получение всех клиентов",
     * operationId="indexClients",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех клиентов",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Client"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *        )
     *  )
     * );
     */
    public function index()
    {
        $collection = $this->clientRepository->findByFilters();
        if ($collection->isEmpty()) {
            return $this->respondWithCustomData([]);
        }
        return $this->respondWithCollection($collection);
    }
    /**
     * @OA\Get (
     * path="/clients/{client}",
     * summary="Получение подробной информации о клиенте",
     * description="Получение подробной информации о клиенте",
     * operationId="getOneClient",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="client",
     *  in="path",
     *  required=true,
     *  description="Client id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешное получение информации о клиенте",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/ClientWithAddressExcluded",
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $client = $this->clientRepository->findOneById($id);
        $this->authorize('view', $client);
        return $this->respondWithItem($client);
    }

    /**
     * @OA\Post(
     * path="/clients",
     * summary="Создание клиента",
     * description="Добавление клиента",
     * operationId="storeClients",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для создания клиента",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/ClientStoreRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Client"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  ClientStoreRequest  $request
     * @return JsonResponse
     */
    public function store(ClientStoreRequest $request): JsonResponse
    {
        $data = $request->validated();
        $client = $this->clientRepository->store($data);
        return $this->respondWithCustomData($client, Response::HTTP_CREATED);
    }

    /**
     * @OA\Put(
     * path="/clients/{client}",
     * summary="Обновление клиента",
     * description="Обновление клиента",
     * operationId="updateClient",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="client",
     *  in="path",
     *  required=true,
     *  description="Client id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для обновления клиента",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/ClientStoreRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная обновление клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Client"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  ClientUpdateRequest  $request
     * @param $id
     * @return mixed
     */
    public function update(ClientUpdateRequest $request, $id)
    {
        $client = $this->clientRepository->findOneById($id);
        $this->authorize('update', $client);
        $data = $request->validated();
        $response = $this->clientRepository->update(
            $client,
            $data
        );
        return $this->respondWithItem($response);
    }

    /**
     * @OA\Delete (
     * path="/clients/{client}",
     * summary="Удаление клиента",
     * description="Удаление клиента",
     * operationId="deleteClient",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="client",
     *  in="path",
     *  required=true,
     *  description="Client id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная удаление клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            @OA\Property(
     *              property="deleted",
     *              type="boolean",
     *              example="true"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        $client = $this->clientRepository->findOneById($id);
        $this->authorize('delete', $client);
        $response = $this->clientRepository->destroy($client);
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function changeBalance(ClientUpdateRequest $request, $id)
    {
        $client = $this->clientRepository->findOneById($id);
        $this->authorize('update', $client);
        $data = $request->validated();
        $response = $this->clientRepository->changeBalance(
            $client,
            $data['balance']
        );
        return $this->respondWithItem($response);
    }
}
