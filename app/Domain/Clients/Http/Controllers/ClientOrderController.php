<?php

namespace App\Domain\Clients\Http\Controllers;

use App\Domain\Clients\Contracts\ClientOrderRepository;
use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Http\Requests\ClientOrderUpdateRequest;
use App\Domain\Clients\Http\Resources\ClientOrderResource;
use App\Domain\Clients\Http\Requests\ClientOrderStoreRequest;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ClientOrderController extends Controller
{
    private ClientOrderRepository $orderClientRepository;
    private ClientRepository $clientRepository;

    public function __construct(
        ClientOrderRepository $orderClientRepository,
        ClientRepository $clientRepository
    )
    {
        $this->orderClientRepository = $orderClientRepository;
        $this->clientRepository = $clientRepository;
    }

    public function index($id): JsonResponse
    {
        $data = $this->clientRepository->findOneByIdWithOrders($id);
        return $this->respondWithCustomData(ClientOrderResource::make($data));
    }

    public function store(ClientOrderStoreRequest $request, $id) : JsonResponse
    {
        $data = $request->validated();
        try {
            $response = $this->orderClientRepository->storeOrder($id, $data);
            return $this->respondWithCustomData($response, Response::HTTP_CREATED);
        } catch (\RuntimeException $e) {
            return $this->respondWithCustomData([
                'errors' => $e->getCode(),
                'message' => $e->getMessage()
            ], $e->getCode());
        }
    }

    public function update(ClientOrderUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $this->orderClientRepository->updateOrder($id, $data);
        return $this->respondWithCustomData(true);
    }


    public function destroy(int $id)
    {
        $this->orderClientRepository->deleteOrder($id);
        return $this->respondWithCustomData(true);
    }
}
