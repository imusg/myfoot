<?php

namespace App\Domain\Clients\Http\Controllers;

use Illuminate\Http\Response;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Clients\Contracts\MessageRepository;
use App\Domain\Clients\Http\Resources\MessageResource;
use App\Domain\Clients\Http\Resources\MessageCollection;
use App\Domain\Clients\Http\Requests\MessageStoreRequest;
use App\Domain\Clients\Http\Resources\ClientOrderResource;

class ClientMessagesController extends Controller
{
    private MessageRepository $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
        $this->resourceItem = MessageResource::class;
        $this->resourceCollection = MessageCollection::class;
    }

    public function index($id)
    {
        $response = $this->messageRepository->findByFiltersCustom($id);
        return $this->respondWithCollection($response);
    }

    public function store(MessageStoreRequest $request, $id)
    {
        $data = $request->validated();
        $data['client_id'] = intval($id);
        $data['manager_id'] = request()->user()->id;
        $responses = $this
            ->messageRepository
            ->store($data);
        return $this->respondWithCustomData($responses, Response::HTTP_CREATED);
    }

    public function update(MessageStoreRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->messageRepository->update(
            $this->messageRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }


    public function destroy($message_id)
    {
        $response = $this
            ->messageRepository
            ->destroy($this->messageRepository->findOneById($message_id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }
}
