<?php
namespace App\Domain\Clients\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Clients\Contracts\ClientTransactionRepository;
use App\Domain\Clients\Http\Resources\ClientTransactionResource;
use App\Domain\Clients\Http\Resources\ClientTransactionCollection;
use App\Domain\Clients\Http\Requests\ClientTransactionStoreRequest;

class ClientsTransactionController extends Controller
{
    private ClientTransactionRepository $clientTransactionRepository;

    public function __construct(ClientTransactionRepository $clientTransactionRepository)
    {
        $this->clientTransactionRepository = $clientTransactionRepository;
        $this->resourceItem = ClientTransactionResource::class;
        $this->resourceCollection = ClientTransactionCollection::class;
    }
    /**
     * @OA\Get(
     * path="/clients",
     * summary="Получение всех транзакций клиента",
     * description="Получение всех транзакций клиента",
     * operationId="indexClientsTransaction",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех транзакций клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Client"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *        )
     *  )
     * );
     */
    public function index($id)
    {
        $collection = $this->clientTransactionRepository->findByFilter($id);
        return $this->respondWithCollection($collection);
    }

    /**
     * @OA\Post(
     * path="/clients",
     * summary="Создание клиента",
     * description="Добавление клиента",
     * operationId="storeClients",
     * tags={"Clients"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для создания клиента",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/ClientStoreRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Client"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     * @param  ClientStoreRequest  $request
     * @return JsonResponse
     */
    public function store(ClientTransactionStoreRequest $request, $id): JsonResponse
    {
        $data = $request->validated();
        $data['client_id'] = $id;
        $client = $this->clientTransactionRepository->store($data);
        return $this->respondWithCustomData($client, Response::HTTP_CREATED);
    }
}
