<?php

namespace App\Domain\Clients\Http\Controllers;

use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Http\Requests\ClientStoreExcludeRequest;
use App\Domain\Clients\Http\Requests\ClientStoreRequest;
use App\Domain\Clients\Http\Resources\ClientExcludeCollection;
use App\Domain\Clients\Http\Resources\ClientExcludeResource;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ClientExcludeController extends Controller
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->resourceItem = ClientExcludeResource::class;
        $this->resourceCollection = ClientExcludeCollection::class;
    }

    public function index($id): ClientExcludeCollection
    {
        $collection = $this->clientRepository->getExcludeIngredients($id);
        return new ClientExcludeCollection($collection);
    }

    public function store(ClientStoreExcludeRequest $request, $id): JsonResponse
    {
        $data = $request->validated();
        $excluded = $this->clientRepository->storeExclude($this->clientRepository->findOneById($id), $data);
        return $this->respondWithCustomData($excluded, Response::HTTP_CREATED);
    }

    public function update(ClientStoreExcludeRequest $request, $id): JsonResponse
    {
        $data = $request->validated();
        $excluded = $this->clientRepository->updateExclude($id, $data);
        return $this->respondWithCustomData($excluded, Response::HTTP_CREATED);

    }

    public function destroy($id): JsonResponse
    {
        $response = $this->clientRepository->destroyExclude($id);
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

}
