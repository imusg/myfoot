<?php

namespace App\Domain\Clients\Http\Controllers;

use App\Domain\Clients\Contracts\ClientAddressRepository;
use App\Domain\Clients\Contracts\ClientRepository;
use App\Domain\Clients\Http\Requests\AddressStoreRequest;
use App\Domain\Clients\Resources\DeliveryAddressResource;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ClientAddressController extends Controller
{
    private ClientRepository $clientRepository;
    private ClientAddressRepository $clientAddressRepository;

    public function __construct(
        ClientRepository $clientRepository,
        ClientAddressRepository $clientAddressRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientAddressRepository = $clientAddressRepository;
        $this->resourceItem = DeliveryAddressResource::class;
    }

    public function store(AddressStoreRequest $request, $id)
    {
        
        $data = $request->validated();
        $responses = $this->clientRepository
            ->storeDeliveryAddress($id, $data);
        return $this->respondWithCustomData($responses, Response::HTTP_CREATED);
    }

    public function update(AddressStoreRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->clientAddressRepository->update(
            $this->clientAddressRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }


    public function destroy($id)
    {
        $response = $this
            ->clientAddressRepository
            ->destroy($this->clientAddressRepository->findOneById($id));
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }
}
