<?php

namespace App\Domain\Ingredients\Repositories;

use Spatie\QueryBuilder\QueryBuilder;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Infrastructure\Abstracts\EloquentRepository;
use App\Domain\Ingredients\Entities\IngredientCategory;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Domain\Ingredients\Contracts\IngredientRepository;

class EloquentIngredientRepository extends EloquentRepository implements IngredientRepository
{
    private string $defaultSort = 'id';

    private array $defaultSelect = [
        'id',
        'title',
        'barcode',
        'losses_cleaning',
        'losses_frying',
        'losses_cooking',
        'losses_baking',
        'unit_id',
        'category_id',
        'losses_stew',
        'created_at',
        'updated_at',
        'calories', 'proteins' ,'fats' ,'carbohydrates'
    ];

    private array $allowedFilters = [
        'title',
        'category.title',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [
        'units',
        'category'
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 ? $perPage : 20;

        return QueryBuilder::for(Ingredient::class)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function findByIngredientFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(IngredientCategory::class)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function storeCategoryIngredient($data)
    {
        return IngredientCategory::create($data);
    }

    public function updateCategoryIngredient($data, $id)
    {
        return tap(IngredientCategory::find($id))->update($data);
    }


    public function deleteCategoryIngredient($id)
    {
        return IngredientCategory::find($id)->delete();
    }
}
