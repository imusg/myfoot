<?php

namespace App\Domain\Ingredients\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Ingredients\Contracts\IngredientRepository;
use App\Domain\Ingredients\Http\Resources\IngredientResource;
use App\Domain\Ingredients\Http\Resources\IngredientCollection;
use App\Domain\Ingredients\Http\Requests\IngredientStoreRequest;
use App\Domain\Ingredients\Http\Requests\IngredientUpdateRequest;
use App\Domain\Ingredients\Http\Resources\IngredientCategoryCollection;

class IngredientsController extends Controller
{
    private IngredientRepository $ingredientRepository;

    public function __construct(IngredientRepository $ingredientRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->resourceItem = IngredientResource::class;
        $this->resourceCollection = IngredientCollection::class;
    }

    public function index()
    {
        $collection = $this->ingredientRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function store(IngredientStoreRequest $request): JsonResponse
    {
        $data = $request->validated();
        $ingredient = $this->ingredientRepository->store($data);
        return $this->respondWithCustomData(IngredientResource::make($ingredient), Response::HTTP_CREATED);
    }

    public function update(IngredientUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $response = $this->ingredientRepository->update(
            $this->ingredientRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy($id): JsonResponse
    {
        $response = $this->ingredientRepository->destroy(
            $this->ingredientRepository->findOneById($id)
        );
        return $this->respondWithCustomData([
            'deleted' => $response
        ], Response::HTTP_OK);
    }

    public function getIngredientCategory()
    {
        $collection = $this->ingredientRepository->findByIngredientFilters();
        return new IngredientCategoryCollection($collection);
    }
    public function storeIngredientCategory(Request $request)
    {
        $data = $request->only('title');
        $collection = $this->ingredientRepository->storeCategoryIngredient($data);
        return $this->respondWithCustomData($collection);
    }
    public function updateIngredientCategory(Request $request, $id)
    {
        $data = $request->only('title');
        $collection = $this->ingredientRepository->updateCategoryIngredient($data, $id);
        return $this->respondWithCustomData($collection);
    }
    public function deleteIngredientCategory($id)
    {
        $collection = $this->ingredientRepository->deleteCategoryIngredient($id);
        return $this->respondWithCustomData($collection);
    }
}
