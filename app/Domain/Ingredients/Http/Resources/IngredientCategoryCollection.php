<?php

namespace App\Domain\Ingredients\Http\Resources;

use App\Domain\Meal\Http\Resources\MealResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class IngredientCategoryCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => MealResource::collection($this->collection),
        ];
    }
}
