<?php

namespace App\Domain\Ingredients\Http\Resources;

use App\Domain\Category\Http\Resources\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class IngredientResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'barcode' => $this->barcode,
            'losses_cleaning' => $this->losses_cleaning,
            'losses_frying' => $this->losses_frying,
            'losses_cooking' => $this->losses_cooking,
            'losses_baking' => $this->losses_baking,
            'losses_stew' => $this->losses_stew,
            'calories' => $this->calories,
            'proteins' => $this->proteins,
            'fats' => $this->fats,
            'carbohydrates' => $this->carbohydrates,
            'unit' => CategoryResource::make($this->units),
            'category' => CategoryResource::make($this->category),
        ];
    }
}
