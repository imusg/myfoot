<?php

namespace App\Domain\Ingredients\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class IngredientCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => IngredientResource::collection($this->collection),
        ];
    }
}
