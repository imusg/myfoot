<?php

namespace App\Domain\Ingredients\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IngredientUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
           'title' => 'string',
           'barcode' => 'string|nullable',
           'losses_cleaning' => 'integer|nullable',
           'losses_frying' => 'integer|nullable',
           'losses_cooking' => 'integer|nullable',
           'losses_baking' => 'integer|nullable',
           'unit_id' => 'integer|exists:units,id|nullable',
           'category_id' => 'integer|nullable',
           'losses_stew' => 'integer|nullable',
           'calories' => 'nullable',
           'proteins' => 'nullable',
           'fats' => 'nullable|string',
           'carbohydrates' => 'nullable',
        ];
    }
}
