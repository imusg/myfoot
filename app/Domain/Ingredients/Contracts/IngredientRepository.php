<?php

namespace App\Domain\Ingredients\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface IngredientRepository extends BaseRepository
{
}
