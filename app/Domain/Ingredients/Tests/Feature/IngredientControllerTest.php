<?php
namespace App\Domain\Unit\Tests\Feature;

use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class IngredientControllerTest extends TestCase
{
    private array $ingredient;
    /*
        [x] Создаем ингредиент со всеми заполнеными полями
        [x] Ошибки валидации
        [x] Создание с не полными полями
        [x] Обновляем ингредиент
        [x] Удаляем ингредиент
        [x] Получение всех ингредиентов
        [x] Фильтрация ингредиентов по имени
        [x] Фильтрация ингредиентов по категории
    */
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testCreateAllFieldFill(): void
    {
        $data = [
            'title' => 'Foo ingredient',
            'barcode' => 'string|nullable',
            'losses_cleaning' => 10,
            'losses_frying' => 10,
            'losses_cooking' => 10,
            'losses_baking' => 10,
            'losses_stew' => 10,
            'unit_id' => 1,
            'category_id' => 1,
            'calories' => '36.2',
            'proteins' => '40',
            'fats' => '10',
            'carbohydrates' => '10.2',
        ];
        $response = $this->post(route('ingredient.store'), $data)
            ->assertSuccessful();
        $id = $response->json('data.id');
        $this->assertDatabaseHas('ingredients', ['id' => $id]);
    }

    public function testCreateWithErrorTitle() : void
    {
        $data = [
            'barcode' => 'string|nullable',
            'losses_cleaning' => 10,
            'losses_frying' => 10,
            'losses_cooking' => 10,
            'losses_baking' => 10,
            'losses_stew' => 10,
            'unit_id' => 1,
            'category_id' => 1,
            'calories' => '36.2',
            'proteins' => '40',
            'fats' => '10',
            'carbohydrates' => '10.2',
        ];

        $response = $this->post(
            route('ingredient.store'),
            $data
        )
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'title' => [
                        'validation.required'
                    ]
                ]
            ]);
    }

    public function testCreateWithErrorUnitId() : void
    {
        $data = [
            'title' => 'test ingredient',
            'barcode' => 'string|nullable',
            'losses_cleaning' => 10,
            'losses_frying' => 10,
            'losses_cooking' => 10,
            'losses_baking' => 10,
            'losses_stew' => 10,
            'category_id' => 1,
            'calories' => '36.2',
            'proteins' => '40',
            'fats' => '10',
            'carbohydrates' => '10.2',
        ];

        $response = $this->post(
            route('ingredient.store'),
            $data
        )
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'unit_id' => [
                        'validation.required'
                    ]
                ]
            ]);
    }

    public function testCreateWithErrorCategoryId() : void
    {
        $data = [
            'title' => 'test ingredient',
            'barcode' => 'string|nullable',
            'losses_cleaning' => 10,
            'losses_frying' => 10,
            'losses_cooking' => 10,
            'losses_baking' => 10,
            'losses_stew' => 10,
            'unit_id' => 1,
            'calories' => '36.2',
            'proteins' => '40',
            'fats' => '10',
            'carbohydrates' => '10.2',
        ];

        $response = $this->post(
            route('ingredient.store'),
            $data
        )
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'category_id' => [
                        'validation.required'
                    ]
                ]
            ]);
    }

    public function testCreateNotAllFieldFill(): void
    {
        $data = [
            'title' => 'Foo ingredient',
            'unit_id' => 1,
            'category_id' => 1
        ];
        $response = $this->post(route('ingredient.store'), $data)
            ->assertSuccessful();
        $id = $response->json('data.id');
        $this->assertDatabaseHas('ingredients', ['id' => $id]);
    }

    public function testUpdateIngredient() : void
    {
        $ingredient = factory(Ingredient::class)
            ->create();

        $response = $this->put(
            route('ingredient.update', $ingredient->id),
            [
                'title' => 'title test update'
            ]
        )
            ->assertSuccessful();

        $data = $response->json('data');
        $this->assertDatabaseHas('ingredients', ['title' => $data['title']]);
        $ingredient->delete();
    }

    public function testDeletesIngredient() : void
    {
        $ingredient = factory(Ingredient::class)
            ->create();

        $this->delete(route('ingredient.destroy', $ingredient->id))
            ->assertSuccessful();

        $this->assertDatabaseMissing('ingredients', ['id' => $ingredient->id]);
    }

    public function testGetAllIngredients() : void
    {
        $response = $this->get(route('ingredient.index'))
            ->assertSuccessful();
        $data = $response->json('data');
        $this->assertEquals(count($data), 20);
    }

    public function testGetIngredientsByFilterName() : void
    {
        $name = 'search ingredient';
        $ingredient = factory(Ingredient::class)
            ->create(['title' => $name]);

        $response = $this->get(route('ingredient.index', ['filter[title]' => $name]))
            ->assertSuccessful();
        $data = $response->json('data');

        $this->assertEquals(count($data), 1);

        $ingredient->delete();
    }

    public function testGetIngredientsByFilterCategory() : void
    {
        $category = 'Заморозка';
        $response = $this->get(route('ingredient.index', ['filter[category.title]' => $category]))
            ->assertSuccessful();
        $data = $response->json('data');
        $this->assertEquals(count($data), 14);
    }
}
