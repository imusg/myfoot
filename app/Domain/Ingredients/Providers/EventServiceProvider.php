<?php

namespace App\Domain\Ingredients\Providers;

use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Ingredients\Listeners\Observers\IngredientObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        Ingredient::observe(IngredientObserver::class);
    }
}
