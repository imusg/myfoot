<?php

namespace App\Domain\Ingredients\Providers;

use App\Domain\Clients\Http\Controllers\ClientHistoryController;
use App\Domain\Clients\Http\Controllers\ClientMessagesController;
use App\Domain\Clients\Http\Controllers\ClientOrderController;
use App\Domain\Clients\Http\Controllers\ClientsController;
use App\Domain\Clients\Http\Controllers\ClientAddressController;
use App\Domain\Ingredients\Http\Controllers\IngredientsController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->apiResource('ingredients', IngredientsController::class)
            ->names('ingredient');

        $router->get('ingredient/categories', [IngredientsController::class, 'getIngredientCategory']);
        $router->post('ingredient/categories', [IngredientsController::class, 'storeIngredientCategory']);
        $router->put('ingredient/categories/{id}', [IngredientsController::class, 'updateIngredientCategory']);
        $router->delete('ingredient/categories/{id}', [IngredientsController::class, 'deleteIngredientCategory']);
    }
}
