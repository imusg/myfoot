<?php


namespace App\Domain\Ingredients\Providers;


use App\Domain\Ingredients\Contracts\IngredientRepository;
use App\Domain\Ingredients\Entities\Ingredient;
use App\Domain\Dishies\Repositories\EloquentDishRepository;
use App\Domain\Ingredients\Repositories\EloquentIngredientRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(IngredientRepository::class, function () {
            return new EloquentIngredientRepository(new Ingredient());
        });
    }

    public function provides(): array
    {
        return [
            IngredientRepository::class
        ];
    }
}
