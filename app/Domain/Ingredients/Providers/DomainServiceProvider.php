<?php
namespace App\Domain\Ingredients\Providers;

use App\Domain\Clients\Database\Factories\ClientDeliveryAddresFactory;
use App\Domain\Clients\Database\Factories\ClientFactory;
use App\Domain\Clients\Database\Factories\ClientHistoryOrderFactory;
use App\Domain\Clients\Database\Factories\ClientMessageFactory;
use App\Domain\Clients\Database\Factories\ClientOrderFactory;
use App\Domain\Ingredients\Database\Factories\IngredientFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'ingredients';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        IngredientFactory::class
    ];
}
