<?php

namespace App\Domain\Ingredients\Entities;

use App\Domain\Unit\Entities\Unit;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Ingredients\Entities\Ingredient
 * * @OA\Schema (
 *    schema="Ingredient",
 *    type="object",
 *
 * @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 * @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Название ингредиента",
 *      example="Яйцо",
 *    ),
 *     @OA\Property (
 *      property="barcode",
 *      type="integer",
 *      description="Штрихкод",
 *      example="4703",
 *    ),
 *    @OA\Property (
 *      property="losses_cleaning",
 *      type="integer",
 *      description="Потери при отчистке",
 *      example="10",
 *    ),
 *    @OA\Property (
 *      property="losses_frying",
 *      type="integer",
 *      description="Потери при жарке",
 *      example="40",
 *    ),
 *    @OA\Property (
 *      property="losses_cooking",
 *      type="integer",
 *      description="Потери при готовке",
 *      example="20",
 *    ),
 *    @OA\Property (
 *      property="losses_baking",
 *      type="integer",
 *      description="Потери при выпечке",
 *      example="25",
 *    ),
 *    @OA\Property (
 *      property="unit",
 *      ref="#/components/schemas/Unit"
 *    ),
 * )
 * @property int $id
 * @property string $title
 * @property string|null $barcode
 * @property int $losses_cleaning
 * @property int $losses_frying
 * @property int $losses_cooking
 * @property int $losses_baking
 * @property int $unit_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Unit|null $units
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereLossesBaking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereLossesCleaning($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereLossesCooking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereLossesFrying($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IngredientCategory extends Model
{
    protected $fillable = [
        'title'
    ];
}
