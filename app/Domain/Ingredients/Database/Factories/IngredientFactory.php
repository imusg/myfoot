<?php

namespace App\Domain\Ingredients\Database\Factories;

use App\Domain\Ingredients\Entities\Ingredient;
use App\Infrastructure\Abstracts\ModelFactory;

class IngredientFactory extends ModelFactory
{
    protected string $model = Ingredient::class;

    public function fields()
    {
        return [
            'title' => $this->faker->title,
            'unit_id' => 1,
            'category_id' => 1
        ];
    }

    public function states()
    {
    }
}
