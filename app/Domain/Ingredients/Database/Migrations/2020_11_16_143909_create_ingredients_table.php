<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsTable extends Migration
{
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('barcode')->nullable();
            $table->integer('losses_cleaning')->default(0);
            $table->integer('losses_frying')->default(0);
            $table->integer('losses_cooking')->default(0);
            $table->integer('losses_baking')->default(0);
            $table->integer('losses_stew')->default(0);
            $table->double('calories', 8, 3)->default(0);
            $table->double('proteins', 8, 3)->default(0);
            $table->double('fats', 8, 3)->default(0);
            $table->double('carbohydrates', 8, 3)->default(0);
            $table->foreignId('unit_id');
            $table->foreignId('category_id');
            $table->foreign('category_id')
                ->references('id')
                ->on('ingredient_categories')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('unit_id')
                ->references('id')
                ->on('units')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}
