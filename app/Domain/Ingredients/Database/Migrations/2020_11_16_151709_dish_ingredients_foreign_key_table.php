<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DishIngredientsForeignKeyTable extends Migration
{
    public function up()
    {
        Schema::table('dish_ingredients', function (Blueprint $table) {
            $table->foreign('dish_id')->references('id')->on('dishes')->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->foreign('ingredient_id')->references('id')->on('ingredients')->cascadeOnDelete()
                ->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('dish_ingredients', function (Blueprint $table) {
            $table->dropForeign('dish_id');
            $table->dropForeign('ingredient_id');
        });
    }
}
