<?php

namespace App\Domain\Ingredients\Listeners\Observers;

use App\Domain\Dishies\Entities\Dish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Ingredients\Entities\Ingredient;

class IngredientObserver
{
    public function updated(Ingredient $ingredient)
    {
        if ($ingredient->isDirty('calories')) {
            $dishIngredients = DishIngredient::where('ingredient_id', $ingredient->id)->get();
            $dishIngredients->each(function ($dishIngredient) use ($ingredient) {
                $netto = $dishIngredient->netto * 1000; // гр
                $originalCalories = ($ingredient->getOriginal('calories') * $netto) / 100;
                $dish = Dish::find($dishIngredient->dish_id);
                $originalDishCalories = $dish->calories - $originalCalories;
                $calories = ($ingredient->calories * $netto) / 100;
                $dish->update([
                    'calories' => $originalDishCalories + $calories
                ]);
            });
        }
        if ($ingredient->isDirty('proteins')) {
            $dishIngredients = DishIngredient::where('ingredient_id', $ingredient->id)->get();
            $dishIngredients->each(function ($dishIngredient) use ($ingredient) {
                $netto = $dishIngredient->netto * 1000; // гр
                $originalProteins = ($ingredient->getOriginal('proteins') * $netto) / 100;
                $dish = Dish::find($dishIngredient->dish_id);
                $originalDishProteins = $dish->proteins - $originalProteins;
                $proteins = ($ingredient->proteins * $netto) / 100;
                $dish->update([
                    'proteins' => $originalDishProteins + $proteins
                ]);
            });
        }
        if ($ingredient->isDirty('fats')) {
            $dishIngredients = DishIngredient::where('ingredient_id', $ingredient->id)->get();
            $dishIngredients->each(function ($dishIngredient) use ($ingredient) {
                $netto = $dishIngredient->netto * 1000; // гр
                $originalFats = ($ingredient->getOriginal('fats') * $netto) / 100;
                $dish = Dish::find($dishIngredient->dish_id);
                $originalDishFats = $dish->fats - $originalFats;
                $fats = ($ingredient->fats * $netto) / 100;
                $dish->update([
                    'fats' => $originalDishFats + $fats
                ]);
            });
        }
        if ($ingredient->isDirty('carbohydrates')) {
            $dishIngredients = DishIngredient::where('ingredient_id', $ingredient->id)->get();
            $dishIngredients->each(function ($dishIngredient) use ($ingredient) {
                $netto = $dishIngredient->netto * 1000; // гр
                $originalCarbohydrates = ($ingredient->getOriginal('carbohydrates') * $netto) / 100;
                $dish = Dish::find($dishIngredient->dish_id);
                $originalDishCarbohydrates = $dish->carbohydrates - $originalCarbohydrates;
                $carbohydrates = ($ingredient->carbohydrates * $netto) / 100;
                $dish->update([
                    'carbohydrates' => $originalDishCarbohydrates + $carbohydrates
                ]);
            });
        }
    }
}
