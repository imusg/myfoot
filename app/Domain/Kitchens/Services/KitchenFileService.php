<?php

namespace App\Domain\Kitchens\Services;

use Carbon\Carbon;
use App\Domain\Orders\Entities\Order;
use Spatie\QueryBuilder\QueryBuilder;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Domain\Kitchens\Entities\Kitchen;
use App\Domain\Kitchens\Entities\KitchenFile;
use Storage;

class KitchenFileService
{
    public function generateFile($preview)
    {
        $date = request()->date ? request()->date : Carbon::now()->format('Y-m-d');
        $branchId = request()->user()->branch_id;
        $userId = request()->user()->id;

        $generalData = $this->generateGeneralData($date, $branchId);
        $additionalData = $this->generateAdditionData($date, $branchId);

        $path = $this->generateDocFile($generalData, $additionalData);
        if (!$preview) {
            KitchenFile::create([
                'url' => $path,
                'branch_id' => $branchId,
                'user_id' => $userId
            ]);
        }
        return [
            'url' => $path
        ];
    }

    public function destroy(int $id)
    {
        $file = KitchenFile::find($id);
        return $file->delete();
    }

    public function getAllFiles()
    {
        $branchId = request()->user()->branch_id;
        return QueryBuilder::for(KitchenFile::class)
            ->where('branch_id', $branchId)
            ->defaultSort('-created_at')
            ->paginate(20);
    }

    private function findClientExcludeIngredients($kitchen)
    {
        $clientExcludeIngredients = collect([]);
        if (isset($kitchen->clients[0])) {
            $kitchen->clients[0]->excluded->map(function ($exclude) use ($clientExcludeIngredients) {
                $clientExcludeIngredients->push($exclude->ingredient_id);
            });
        }
        if (isset($kitchen->position->data)) {
            $clientExcludeIngredientsTitle = $kitchen
                ->position
                ->data
                ->directory
                ->dish
                ->ingredients
                ->whereIn('id', $clientExcludeIngredients)
                ->values();

            $ingredientTitles = collect([]);
            $ingredientReplace = collect([]);

            $clientExcludeIngredientsTitle->map(function ($ingredient) use ($kitchen, $ingredientTitles, $ingredientReplace) {
                $repl = $kitchen->clients[0]->excluded->where('ingredient_id', $ingredient->id)->first();
                $title = $ingredient->title;
                $description = $repl->description;
                $t = $description ? "$title ($description)" : $title;
                $ingredientTitles->push($t);
                if (isset($repl->replaceIngredient)) {
                    $ingredientReplace->push($repl->replaceIngredient->title);
                }
            });
            return [
                'ingredientTitles' => $ingredientTitles,
                'ingredientReplace' => $ingredientReplace
            ];
        }
        return [
            'ingredientTitles' => [],
            'ingredientReplace' => []
        ];
    }

    private function generateGeneralData($date, $branchId)
    {
        $generalData = collect([]);

        $kitchens = Kitchen::where('date', $date)
            ->where('branch_id', $branchId)
            ->with(['position.data.directory.dish', 'meal', 'clients.excluded.ingredient'])
            ->get()
            ->groupBy('meal.title');

        $kitchenKeys = $kitchens->keys();
        $kitchenKeys->each(function ($key, $index) use ($kitchens, $generalData) {
            $clientExcludedCounts = collect([]);
            $clientCounts = collect([]);
            $kitchens[$key]->each(function ($kitchen) use ($generalData, $key, $clientCounts, $clientExcludedCounts) {
                $clientExcludeIngredients = $this->findClientExcludeIngredients($kitchen);
                if (isset($kitchen->position)) {
                    $clientCounts->push($kitchen->position->quantity);
                    if ($clientExcludeIngredients['ingredientTitles']->count()) {
                        $clientExcludedCounts->push($kitchen->position->quantity);
                        $generalData->push([
                            'category' => $key,
                            'count' => $kitchen->position->quantity,
                            'exceptions' => implode(', ', $clientExcludeIngredients['ingredientTitles']->toArray()),
                            'replacement' => implode(', ', $clientExcludeIngredients['ingredientReplace']->toArray())
                        ]);
                    }
                }
            });
            if ($clientCounts->sum() - $clientExcludedCounts->sum() > 0) {
                $generalData->push([
                    'category' => $key,
                    'count' => $clientCounts->sum() - $clientExcludedCounts->sum(),
                    'exceptions'=> '-',
                    'replacement' => '-'
                ]);
            }
        });
        return $generalData->toArray();
    }

    private function generateAdditionData($date, $branchId)
    {
        $additionalData = collect([]);
        $orders = Order::where('date', $date)
            ->where('branch_id', $branchId)
            ->get();
        $orders->map(function ($order) use ($additionalData) {
            $order->positions->map(function ($position) use ($additionalData) {
                if ($position->addition) {
                    $additionalData->push([
                        'additionalName' => $position->addition->title,
                        'additionalCount' => $position->quantity
                    ]);
                }
            });
        });
        return $additionalData->toArray();
    }

    private function generateDocFile(array $generalData, array $additionalData = [])
    {
        $templateProcessor = new TemplateProcessor(storage_path('app/templates/template_kitchen.docx'));
        $date = Carbon::now()->format('d.m.Y-H.i.s');
        $path = "task_kitchen_{$date}.docx";
        $d = request()->date ? Carbon::make(request()->date)->format('d.m.Y') : Carbon::now()->format('d.m.Y');
        $templateProcessor->setValue('date', $d);
        $templateProcessor->cloneRowAndSetValues('category', $generalData);
        $templateProcessor->cloneRowAndSetValues('additionalName', $additionalData);
        $file = $templateProcessor->save();
        Storage::disk('minio')->put("files/kitchens/$path", file_get_contents($file));
        return Storage::disk('minio')->url("files/kitchens/$path");
    }
}
