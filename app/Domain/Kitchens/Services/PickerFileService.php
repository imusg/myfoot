<?php

namespace App\Domain\Kitchens\Services;

use Carbon\Carbon;
use App\Domain\Orders\Entities\Order;
use Spatie\QueryBuilder\QueryBuilder;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Domain\Kitchens\Entities\Kitchen;
use App\Domain\Kitchens\Entities\PickerFile;
use Storage;

/**
 *
 * Example data for word file
 * ['client' =>
 *      ['clientName' => '','sumPay' => '', 'taxSystem' => ''],
 *      'general' => [['category' => 'foo 1']],
 *      'additions' => [['additionalName' => 'bar 1']]
 * ]
 * */

class PickerFileService
{
    public function generateFile($preview)
    {
        $date = request()->date ? request()->date : Carbon::now()->format('Y-m-d');
        $branchId = request()->user()->branch_id;
        $userId = request()->user()->id;

        $orders = Order::where('date', $date)->where('branch_id', $branchId)->get();
        $generalData = collect([]);

        $orders->each(function ($order) use ($generalData) {
            if (isset($order->clients[0])) {
            $clientName = $order->clients[0]->name;
            $clientAddressFind = $order->clients[0]->address->where('isMain', 1);

            $city = isset($order->clients[0]->address[0]) ? $order->clients[0]->address[0]->city : null;
            $street = isset($order->clients[0]->address[0]) ? $order->clients[0]->address[0]->street : null;
            $house =  isset($order->clients[0]->address[0]) ? $order->clients[0]->address[0]->house: null;
            $apartment =  isset($order->clients[0]->address[0]) ? $order->clients[0]->address[0]->apartment: null;
            $entrance =  isset($order->clients[0]->address[0]) ?$order->clients[0]->address[0]->entrance : null;

            if (!$clientAddressFind->isEmpty()) {
                $city = $clientAddressFind->first()->city;
                $street = $clientAddressFind->first()->street;
                $house = $clientAddressFind->first()->house;
                $apartment = $clientAddressFind->first()->apartment;
                $entrance = $clientAddressFind->first()->entrance;
                $comment = $clientAddressFind->first()->comment;
            }
            $clientAddress = "{$city}, {$street}, д.{$house}, подъезд {$entrance}, кв. {$apartment}";
            $sumPay = $order->amount;
            $taxSystem = $order->clients[0]->taxSystem ? $order->clients[0]->taxSystem : '-';
            $phone = $order->clients[0] ? $order->clients[0]->phone : '-';
            $general = $this->generateGeneralData($order->id);
            $additions = $this->generateAdditionData($order);
            $generalData->push([
                'client' => [
                    'clientName' => $clientName,
                    'sumPay' => $sumPay,
                    'taxSystem' => $taxSystem,
                    'clientAddress' => $clientAddress,
                    'clientPhone' => $phone
                ],
                'general' => $general,
                'additions' => $additions
            ]);
                }
        });

        $path = $this->generateDocFile($generalData->toArray());
        if (!$preview) {
            PickerFile::create([
                'url' => $path,
                'branch_id' => $branchId,
                'user_id' => $userId
            ]);
        }
        return [
            'url' => $path
        ];
    }

    public function getAllFiles()
    {
        $branchId = request()->user()->branch_id;
        return QueryBuilder::for(PickerFile::class)
            ->where('branch_id', $branchId)
            ->defaultSort('-created_at')
            ->paginate(20);
    }

    private function findClientExcludeIngredients($kitchen)
    {
        $clientExcludeIngredients = collect([]);
        $kitchen->clients[0]->excluded->map(function ($exclude) use ($clientExcludeIngredients) {
            $clientExcludeIngredients->push($exclude->ingredient_id);
        });
        if (isset($kitchen
                ->position
                ->data)) {
            $clientExcludeIngredientsTitle = $kitchen
                ->position
                ->data
                ->directory
                ->dish
                ->ingredients
                ->whereIn('id', $clientExcludeIngredients)
                ->values();

            $ingredientTitles = collect([]);
            $ingredientReplace = collect([]);

            $clientExcludeIngredientsTitle->map(function ($ingredient) use ($kitchen, $ingredientTitles, $ingredientReplace) {
                $repl = $kitchen->clients[0]->excluded->where('ingredient_id', $ingredient->id)->first();
                $title = $ingredient->title;
                $description = $repl->description;
                $t = $description ? "$title ($description)" : $title;
                $ingredientTitles->push($t);
                if (isset($repl->replaceIngredient)) {
                    $ingredientReplace->push("{$title} -> {$repl->replaceIngredient->title}");
                }
            });
            return [
                'ingredientTitles' => $ingredientTitles,
                'ingredientReplace' => $ingredientReplace
            ];
        }
        return [
            'ingredientTitles' => [],
            'ingredientReplace' => []
        ];
    }

    private function generateGeneralData($orderId)
    {
        $generalData = collect([]);
        $kitchens = Kitchen::where('order_id', $orderId)
            ->with(['position.data.directory.dish', 'meal', 'clients.excluded.ingredient'])
            ->get()
            ->groupBy('meal.title');

        $kitchenKeys = $kitchens->keys();
        $kitchenKeys->each(function ($key, $index) use ($kitchens, $generalData) {
            $clientExcludedCounts = collect([]);
            $clientCounts = collect([]);
            $kitchens[$key]->each(function ($kitchen) use ($generalData, $key, $clientCounts, $clientExcludedCounts) {
                $clientExcludeIngredients = $this->findClientExcludeIngredients($kitchen);
                if (isset($kitchen->position)) {
                    $clientCounts->push($kitchen->position->quantity);
                    if ($clientExcludeIngredients['ingredientTitles']->count()) {
                        $clientExcludedCounts->push($kitchen->position->quantity);
                        $generalData->push([
                            'category' => $key,
                            'count' => $kitchen->position->quantity,
                            'exceptions' => implode(', ', $clientExcludeIngredients['ingredientTitles']->toArray()),
                            'replacement' => implode(', ', $clientExcludeIngredients['ingredientReplace']->toArray())
                        ]);
                    }
                }
            });
            if ($clientCounts->sum() - $clientExcludedCounts->sum() > 0) {
                $generalData->push([
                    'category' => $key,
                    'count' => $clientCounts->sum() - $clientExcludedCounts->sum(),
                    'exceptions'=> '-',
                    'replacement' => '-'
                ]);
            }
        });
        return $generalData->toArray();
    }

    private function generateAdditionData($order)
    {
        $additionalData = collect([]);
        $order->positions->map(function ($position) use ($additionalData) {
            if ($position->addition) {
                $additionalData->push([
                    'additionalName' => $position->addition->title,
                    'additionalCount' => $position->quantity
                ]);
            }
        });
        return $additionalData->toArray();
    }

    public function destroy(int $id)
    {
        $file = PickerFile::find($id);
        return $file->delete();
    }

    private function generateDocFile(array $generalData)
    {
        $templateProcessor = new TemplateProcessor(storage_path('app/templates/template_picker.docx'));
        $date = request()->date ? Carbon::make(request()->date)->format('d.m.Y') : Carbon::now()->format('d.m.Y');
        $time = Carbon::now()->format('H.i.s');
        $dateFormat = request()->date ? Carbon::make(request()->date)->format('d.m.Y') : Carbon::now()->format('d.m.Y');
        $path = "task_picker_{$date}-{$time}.docx";
        $templateProcessor->setValue('date', $dateFormat);
        $templateProcessor->cloneBlock('clientBlock', count($generalData), true, true);
        foreach ($generalData as $key => $value) {
            $columnNumber = $key + 1;
            $templateProcessor->setValue("clientName#{$columnNumber}", $value['client']['clientName']);
            $templateProcessor->setValue("sumPay#{$columnNumber}", $value['client']['sumPay']);
            $templateProcessor->setValue("taxSystem#{$columnNumber}", $value['client']['taxSystem']);
            $templateProcessor->setValue("clientAddress#{$columnNumber}", $value['client']['clientAddress']);
            $templateProcessor->setValue("clientPhone#{$columnNumber}", $value['client']['clientPhone']);
            $general = collect($value['general']);
            $general = $general->map(function ($item) use ($columnNumber) {
                return [
                    "category#{$columnNumber}" => $item['category'],
                    "count#{$columnNumber}" => $item['count'],
                    "exceptions#{$columnNumber}" => $item['exceptions'],
                    "replacement#{$columnNumber}" => $item['replacement'],
                ];
            });
            $additions = collect($value['additions']);
            $additions = $additions->map(function ($item) use ($columnNumber) {
                return [
                    "additionalName#{$columnNumber}" => $item['additionalName'],
                    "additionalCount#{$columnNumber}" => $item['additionalCount'],
                ];
            });
            $templateProcessor->cloneRowAndSetValues("category#{$columnNumber}", $general->toArray());
            $templateProcessor->cloneRowAndSetValues("additionalName#{$columnNumber}", $additions->toArray());
        }
        $file = $templateProcessor->save();
        Storage::disk('minio')->put("files/pickers/$path", file_get_contents($file));
        return Storage::disk('minio')->url("files/pickers/$path");
    }
}
