<?php
namespace App\Domain\Kitchens\Database\Factories;

use App\Domain\Kitchens\Entities\Kitchen;
use App\Infrastructure\Abstracts\ModelFactory;

class KitchenFactory extends ModelFactory
{
    protected string $model = Kitchen::class;

    public function fields(): array
    {
        return [];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
