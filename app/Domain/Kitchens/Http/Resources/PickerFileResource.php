<?php

namespace App\Domain\Kitchens\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Dishies\Http\Resources\DirectoryResource;

class PickerFileResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'url' => $this->url,
           'created_at' => $this->created_at->locale('ru_RU')->isoFormat('DD MMMM YYYY H:m')
        ];
    }
}
