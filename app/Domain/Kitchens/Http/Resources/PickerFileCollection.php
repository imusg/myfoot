<?php

namespace App\Domain\Kitchens\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PickerFileCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => PickerFileResource::collection($this->collection)
        ];
    }
}
