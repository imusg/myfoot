<?php

namespace App\Domain\Kitchens\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class KitchenFileCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => KitchenFileResource::collection($this->collection)
        ];
    }
}
