<?php

namespace App\Domain\Orders\Http\Resources;

use App\Domain\Dishies\Http\Resources\DirectoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderCycleResource extends JsonResource
{
    public function toArray($request)
    {
        return [
           'start' => $this->cycle->start,
           'end' => $this->cycle->end,
           'day' => $this->day,
           'week' => $this->week,
           'dish' => DirectoryResource::make($this->directory)
        ];
    }
}
