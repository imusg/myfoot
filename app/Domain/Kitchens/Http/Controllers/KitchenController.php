<?php

namespace App\Domain\Kitchens\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Orders\Http\Resources\OrderResource;
use App\Domain\Kitchens\Contracts\KitchenRepository;
use App\Domain\Orders\Http\Resources\OrderCollection;
use App\Domain\Kitchens\Http\Requests\KitchenUpdateRequest;

class KitchenController extends Controller
{
    private KitchenRepository $kitchenRepository;

    public function __construct(KitchenRepository $kitchenRepository)
    {
        $this->kitchenRepository = $kitchenRepository;
        $this->resourceCollection = OrderCollection::class;
        $this->resourceItem = OrderResource::class;
    }

    public function index()
    {
        $response = $this->kitchenRepository->findKitchens(request()->date);
        return $this->respondWithCustomData($response);
    }

    public function update(KitchenUpdateRequest $request)
    {
        $data = $request->validated();
        $model = $this->kitchenRepository->updateState(request()->date, $data['meals'], $data);
        return $this->respondWithCustomData($model);
    }

    public function updateKitchen($id, KitchenUpdateRequest $request)
    {
        $data = $request->validated();
        $model = $this->kitchenRepository->findOneById($id);
        $response = $this->kitchenRepository->update($model, $data);
        return $this->respondWithCustomData($response);
    }

    public function writeOff(Request $request)
    {
        $response = $this->kitchenRepository->writeOff($request->positions);
        return $this->respondWithCustomData(true);
    }
}
