<?php

namespace App\Domain\Kitchens\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Kitchens\Services\PickerFileService;
use App\Domain\Kitchens\Http\Resources\PickerFileResource;
use App\Domain\Kitchens\Http\Resources\PickerFileCollection;

class PickerFileController extends Controller
{
    private PickerFileService $pickerFileService;

    public function __construct(PickerFileService $pickerFileService)
    {
        $this->pickerFileService = $pickerFileService;
        $this->resourceCollection = PickerFileCollection::class;
        $this->resourceItem = PickerFileResource::class;
    }

    public function generate()
    {
        $preview = request()->preview === 'true' ? true : false;
        return $this->respondWithCustomData($this->pickerFileService->generateFile($preview));
    }

    public function index()
    {
        return $this->respondWithCollection($this->pickerFileService->getAllFiles());
    }

    public function destroy(int $id)
    {
        return $this->respondWithCustomData($this->pickerFileService->destroy($id));
    }
}
