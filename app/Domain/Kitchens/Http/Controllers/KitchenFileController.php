<?php

namespace App\Domain\Kitchens\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Kitchens\Services\KitchenFileService;
use App\Domain\Kitchens\Http\Resources\KitchenFileResource;
use App\Domain\Kitchens\Http\Resources\KitchenFileCollection;

class KitchenFileController extends Controller
{
    private KitchenFileService $kitchenFileService;

    public function __construct(KitchenFileService $kitchenFileService)
    {
        $this->kitchenFileService = $kitchenFileService;
        $this->resourceCollection = KitchenFileCollection::class;
        $this->resourceItem = KitchenFileResource::class;
    }

    public function generate()
    {
        $preview = request()->preview === 'true' ? true : false;
        return $this->respondWithCustomData($this->kitchenFileService->generateFile($preview));
    }

    public function index()
    {
        return $this->respondWithCollection($this->kitchenFileService->getAllFiles());
    }

    public function destroy(int $id)
    {
        return $this->respondWithCustomData($this->kitchenFileService->destroy($id));
    }
}
