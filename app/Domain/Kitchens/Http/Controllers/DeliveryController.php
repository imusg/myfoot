<?php

namespace App\Domain\Kitchens\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Orders\Http\Resources\OrderResource;
use App\Domain\Kitchens\Contracts\KitchenRepository;
use App\Domain\Orders\Http\Resources\OrderCollection;
use App\Domain\Orders\Http\Requests\OrderStoreRequest;
use App\Domain\Orders\Http\Requests\OrderUpdateRequest;
use App\Domain\Kitchens\Http\Requests\DeliveryStoreRequest;
use App\Domain\Kitchens\Http\Requests\KitchenUpdateRequest;

class DeliveryController extends Controller
{
    private KitchenRepository $kitchenRepository;

    public function __construct(KitchenRepository $kitchenRepository)
    {
        $this->kitchenRepository = $kitchenRepository;
        $this->resourceCollection = OrderCollection::class;
        $this->resourceItem = OrderResource::class;
    }

    /**
     * @OA\Get(
     * path="/orders",
     * summary="Получение заказов пользователя",
     * description="Получение заказов пользователя",
     * operationId="indexOrders",
     * tags={"Orders"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="date",
     *  in="query",
     *  description="Дата на которую нужно получить заказы",
     *  @OA\Schema(
     *    type="string"
     *  )
     * ),
     * @OA\Parameter(
     *  name="client",
     *  in="query",
     *  required=true,
     *  description="Клиент у которого нужно получить заказы",
     *  @OA\Schema(
     *    type="string"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех заказаов",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Order"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *   )
     *  )
     * );
     */

    public function index()
    {
        $response = $this->kitchenRepository->findClientDelivery();
        return $this->respondWithCustomData($response);
    }


    /**
     * @OA\Put(
     * path="/orders/{order}",
     * summary="Обновление заказа клиента",
     * description="Обновление заказа клиента",
     * operationId="updateOrders",
     * tags={"Orders"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="order",
     *  in="path",
     *  required=true,
     *  description="Order id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для обновления заказа клиента",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/OrderRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная обновление заказа клиента",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Order"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     */

    public function store(DeliveryStoreRequest $request)
    {
        $data = $request->validated();
        $model = $this->kitchenRepository->storeDelivery($data);
        return $this->respondWithCustomData($model);
    }

    /**
         * @OA\Delete (
         * path="/orders/{order}",
         * summary="Удаление заказа клиента",
         * description="Удаление заказа клиента",
         * operationId="deleteOrders",
         * tags={"Orders"},
         * security={{"bearerAuth":{}}},
         * @OA\Parameter(
         *  name="order",
         *  in="path",
         *  required=true,
         *  description="Order id",
         *  @OA\Schema(
         *    type="integer"
         *  )
         * ),
         * @OA\Response(
         *    response=200,
         *    description="Успешная удаление заказа клиента",
         *    @OA\JsonContent(
         *       @OA\Property(
         *            property="data",
         *            type="object",
         *            ref="#/components/schemas/Order"
         *          ),
         *       @OA\Property(
         *         property="meta",
         *         type="object",
         *         @OA\Property(
         *            property="timestamp",
         *            type="integer",
         *            example="1608213575077"
         *         )
         *       )
         *      )
         *   )
         * )
         */

    public function update(Request $request)
    {
        $response = $this->kitchenRepository->updateDeliveryState($request->all());
        return $this->respondWithCustomData($response);
    }

    public function getKitchen()
    {
        $response = $this->orderRepository->findByDateForKitchen(request()->date);
        return $this->respondWithCollection($response);
    }
}
