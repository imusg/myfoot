<?php

namespace App\Domain\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
* @OA\Schema (
 *    schema="OrderRequest",
 *    type="object",
 *    @OA\Property (
 *      property="date",
 *      type="string",
 *      example="2020-12-20",
 *    ),
 *   @OA\Property (
 *      property="client_id",
 *      type="integer",
 *      description="Ид клиента",
 *      example="1",
 *    ),
 * @OA\Property (
 *      property="meal_id",
 *      type="integer",
 *      description="Ид категории приема пищи",
 *      example="1",
 *    ),
 * )
*/
class OrderStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'date' => 'required|date_format:Y-m-d',
            'client_id' => 'required|exists:clients,id',
            'longer' => 'nullable',
            'meal_id' => 'required|exists:meals,id',
        ];
    }
}
