<?php

namespace App\Domain\Kitchens\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KitchenUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'state' => 'string|nullable',
            'actual_quantity' => 'integer',
            'meals' => 'array|nullable',
        ];
    }
}
