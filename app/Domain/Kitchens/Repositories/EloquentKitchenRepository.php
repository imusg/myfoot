<?php
namespace App\Domain\Kitchens\Repositories;

use Exception;
use Carbon\Carbon;
use App\Domain\Orders\Entities\Order;
use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Kitchens\Entities\Kitchen;
use App\Domain\Kitchens\Entities\Delivery;
use App\Domain\Clients\Entities\ClientOrder;
use App\Domain\Orders\Entities\OrderPosition;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Domain\Kitchens\Contracts\KitchenRepository;
use App\Infrastructure\Abstracts\EloquentRepository;

class EloquentKitchenRepository extends EloquentRepository implements KitchenRepository
{
    public function findKitchens(string $date)
    {
        $orders = Order::where('date', $date)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->get();

        $orders->each(function ($order) {
            $order->positions->each(function ($position) use ($order) {
                if (isset($position->data)) {
                    Kitchen::firstOrCreate([
                        'order_id' => $order->id,
                        'client_id' => count($order->clients) ? $order->clients[0]->id : null,
                        'position_id' => $position->id,
                        'meal_id' => $position->data->directory->dish->meal_id,
                        'date' => $order->date,
                        'branch_id' => $order->branch_id,
                    ]);
                }
            });
        });

        $response = collect([]);

        $kitchens = Kitchen::where('date', $date)
                ->when(request()->user()->branch_id, function ($query) {
                    return $query->where('branch_id', request()->user()->branch_id);
                })
                ->get()
                ->groupBy('position.data.directory.dish.title');

        $keys = $kitchens->keys();

        $keys->each(function ($key) use ($kitchens, $response, $date) {
            $response->push($kitchens[$key]->map(function ($kit) use ($key, $kitchens, $date) {
                $counts = collect([]);
                $clients = collect([]);
                $positions = collect([]);
                $orders = collect([]);
                $excluded = collect([]);
                $kitchens[$key]->each(function ($kitchen) use ($counts, $clients, $positions, $orders, $excluded) {
                    $excludedIngredientIds = count($kitchen->clients) ?
                        $kitchen->clients[0]->excluded->map(function ($exc) {
                            return $exc->ingredient_id;
                        }) : [];
                    if (isset($kitchen->position->data))
                    {
                        $kitchen->position->data->directory->dish->ingredients->whereIn('id', $excludedIngredientIds)
                            ->each(function ($ex) use ($excluded, $kitchen) {
                                $title = $ex->title;
                                $excluded1 = $kitchen->clients[0]->excluded->where('ingredient_id', $ex->id)->first();
                                $excluded->push([
                                    'title' => $title ,
                                    'description' => $excluded1->description ,
                                    'count' => $kitchen->position->quantity,
                                    'id' => $kitchen->clients[0]->id,
                                    'replace' => isset( $excluded1->replaceIngredient) ? $excluded1->replaceIngredient->title : null
                                ]);
                            });
                        $clients->push(['name' => count($kitchen->clients) ? $kitchen->clients[0]->name : null, 'quantity' => $kitchen->position->quantity, 'key' => rand()]);
                        $counts->push($kitchen->position->quantity);
                        $positions->push($kitchen->position->id);
                        $orders->push($kitchen->order_id);
                    }

                });
                $exc = collect([]);
                $keys = $excluded->groupBy('id')->keys();
                $keys->each(function ($k) use ($exc, $excluded) {
                    $count = $excluded->groupBy('id')[$k]->map(function ($e) use ($exc) {
                        $title = $e['title'];
                        $desc = $e['description'];
                        $t = $desc ? "$title($desc)" : $title;
                        $replace = isset($e['replace']) ? $e['replace'] : null;
                        return ['title' => $t , 'count' => $e['count'], 'replace' => $replace];
                    });
                    $str = [];
                    foreach ($count->toArray() as $key => $value) {
                        if ($value['replace']) {
                            $title = $value['title'];
                            $replace = $value['replace'];
                            array_push($str, "$title -> $replace");
                        } else {
                            $title = $value['title'];
                            array_push($str, $title);
                        }
                    }
                    $k = implode(', ', $str);
                    $exc->push([
                        $k => $count->unique('count')->sum('count')
                    ]);
                });
                $newEx = (object) $exc;
                return [
                    'id' => $kit->id,
                    'actual_quantity' => $kit->actual_quantity,
                    'clients' => $clients->values()->all(),
                    'category' => $key,
                    'count' => $counts->sum(),
                    'excluded' =>  $newEx->values()->all(),
                    'state' => $kit->state,
                    'meal_id' => $kit->meal_id,
                    'meal' => $kit->meal->title,
                    'orders' => $orders,
                    'date' => $kit->date,
                    'positions' => $positions
                ];
            })[0]);
        });
        return $response;
    }

    public function updateState(string $date, array $meals, $actual)
    {
        if (isset($actual['actual_quantity'])) {
            $data = [
                'actual_quantity' => $actual['actual_quantity']
            ];
        } else {
            $data = ['state' => $actual['state']];
        }
        $this->model->where('date', $date)
            ->whereIn('meal_id', $meals)
            ->update($data);
        return true;
    }

    public function storeDelivery(array $data)
    {
        $orders = collect();
        $response = collect([]);
        $list = collect([]);

        $kitchens = Kitchen::whereIn('order_id', $data['orders'])
                ->where('branch_id', request()->user()->branch_id)
                ->where('state', 'complete')
                ->get()
                ->groupBy('meal_id');

        $keys = $kitchens->keys();

        $keys->each(function ($key) use ($response, $kitchens, $list) {
            $kitchens[$key]->each(function ($kitchen) use ($key, $list, $kitchens) {
                Delivery::firstOrCreate([
                    'kitchen_id' => $kitchen->id,
                    'order_id' => $kitchen->order_id,
                    'meal_id' => $key,
                    'branch_id' => $kitchen->branch_id,
                    'client_id' => $kitchen->client_id,
                    'date' => $kitchen->date
                ]);
                $ordersPositions = '';
                $positions = collect([]);

                Kitchen::where('client_id', $kitchen->client_id)
                    ->where('date', $kitchen->date)
                    ->where('branch_id', $kitchen->branch_id)
                    ->get()
                    ->each(function ($k) use ($kitchen, $positions) {
                        $positions->push($k->position);
                    });
                foreach ($positions as $position) {
                    if ($position->data) {
                        $ordersPositions .= 'Блюдо '.$position->data->directory->dish->title.'. Кол-во: '.$position->quantity.', ';
                    } else {
                        $ordersPositions .= 'Доп меню '.$position->addition->title.'. Кол-во: '.$position->quantity.', ';
                    }
                }
                $address = '';
                foreach ($kitchen->clients[0]->address as $a) {
                    $address .= $a['region'].', '.$a['city'].', '.$a['street'].', '.$a['house'].', '.$a['apartment'].', '.$a['entrance'].'. Комментарий: '.$a['comment'];
                }
                $list->push([
                    $kitchen->clients[0]->name, $kitchen->clients[0]->phone, $address , $ordersPositions
                ]);
            });
        });
        $header = ['Имя', 'Телефон', 'Адрес', 'Cостав заказа'];
        $list->prepend($header);
        $filename = '/upload/export_delivery_'.time().'.csv';
        $fp = fopen(public_path().$filename, 'w');
        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
        foreach ($list->unique()->toArray() as $fields) {
            $this->myFputcsv($fp, $fields);
        }
        fclose($fp);
        return ['url' => 'http://myfoot-b1.amics-tech.ru/'.$filename];
    }

    private function myFputcsv($fp, $csv_arr, $delimiter = ';', $enclosure = '"')
    {
        // проверим, что на  входе массив
        if (!is_array($csv_arr)) {
            return(false);
        }
        // обойдем все  элемены массива
        for ($i = 0, $n = count($csv_arr); $i < $n;  $i ++) {
            // если это не  число
            if (!is_numeric($csv_arr[$i])) {
                // вставим символ  ограничения и продублируем его в теле элемента
                $csv_arr[$i] =  $enclosure.str_replace($enclosure, $enclosure.$enclosure, $csv_arr[$i]).$enclosure;
            }
            // если  разделитель - точка, то числа тоже экранируем
            if (($delimiter == '.') && (is_numeric($csv_arr[$i]))) {
                $csv_arr[$i] =  $enclosure.$csv_arr[$i].$enclosure;
            }
        }
        // сольем массив в строку, соединив разделителем
        $str = implode($delimiter, $csv_arr)."\n";
        fwrite($fp, $str);
        // возвращаем  количество записанных данных
        return strlen($str);
    }

    public function findClientDelivery()
    {
        $response = collect([]);

        $delivery = Delivery::where('date', request()->date)
            ->when(auth()->user()->branch_id, function ($query) {
                return $query->where('branch_id', auth()->user()->branch_id);
            })
            ->get()
            ->groupBy('client_id');
        if ($delivery->count()) {
            $keys = $delivery->keys();
            $keys->each(function ($key) use ($delivery, $response) {
                $response->push($delivery[$key]->map(function ($d) use ($delivery, $key) {
                    $meals = collect([]);
                    $delivery[$key]->each(function ($item) use ($meals) {
                        $meals->push(['name' => $item->kitchen->position->data->directory->dish->title." ({$item->meal->title})", 'quantity' => $item->kitchen->position->quantity]);
                    });
                    $address = '';
                    if ($d->client->address->count()) {
                        $a = $d->client->address[0];
                        $address = $a['region'].', '.$a['city'].', '.$a['street'].', '.$a['house'].', '.$a['apartment'].', '.$a['entrance'];
                    }
                    return [
                        'client_id' => $key,
                        'id' => $d->id,
                        'client' => $d->client->name,
                        'state' => $d->state,
                        'meals' => $meals,
                        'address' => $address,
                        'date' => $d->date,
                        'comment' => $d->client->address->count() ? $a['comment'] : null
                    ];
                })[0]);
            });
        }
        return $response;
    }

    public function findDeliveryById($id): Model
    {
        return Delivery::find($id);
    }

    public function updateDeliveryState($data)
    {
        Delivery::whereIn('id', $data['ids'])
            ->update([
                'state' => $data['state']
            ]);
        return true;
    }

    public function writeOff(array $positions)
    {
        $kitchens = Kitchen::whereIn('position_id', $positions)
            ->where('state', 'complete')
            ->get();
        $ids = collect([]);
        $kitchens->each(function ($kitchen) use ($ids) {
            $dishId = $kitchen->position->data->directory->dish_id;
            $ingredients = DishIngredient::where('dish_id', $dishId)->get();
            $ingredients->each(function ($ingredient) use ($kitchen, $ids) {
                $storage = Storage::where('ingredient_id', $ingredient->ingredient_id)
                    ->where('branch_id', request()->user()->branch_id)
                    ->first();
                if ($ingredient->ingredient) {
                    if (!$storage) {
                        throw new Exception("Нет ингредиентов на складе {$ingredient->ingredient->title}");
                    }
                    if ($storage->count - $ingredient->netto < 0) {
                        throw new Exception("Нехватает ингредиента {$storage->ingredient->title}");
                    }
                    $ids->push(['id' => $ingredient->ingredient_id, 'netto' => $ingredient->netto ]);
                } else {
                    $ingredient->semis->counts->each(function ($item) use ($ids) {
                        $storage = Storage::where('ingredient_id', $item->ingredient_id)
                            ->where('branch_id', request()->user()->branch_id)
                            ->first();
                        if (!$storage) {
                            throw new Exception("Нет ингредиентов на складе {$item->ingredient->title}");
                        }
                        if ($storage->count - $item->netto < 0) {
                            throw new Exception("Нехватает ингредиента {$storage->ingredient->title}");
                        }
                        $ids->push(['id' => $item->ingredient_id, 'netto' => $item->netto]);
                    });
                }
            });
        });
        $ids->each(function ($ing) {
            $storage = Storage::where('ingredient_id', $ing['id'])
                    ->where('branch_id', request()->user()->branch_id)
                    ->first();
            $storage->update([
                'count' => $storage->count - $ing['netto']
            ]);
        });
        return true;
    }
}
