<?php
namespace App\Domain\Kitchens\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Kitchens\Contracts\KitchenRepository;
use App\Infrastructure\Abstracts\EloquentRepository;

class EloquentKitchenRepository extends EloquentRepository implements KitchenRepository
{

}
