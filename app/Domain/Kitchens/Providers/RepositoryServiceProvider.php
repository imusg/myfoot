<?php

namespace App\Domain\Kitchens\Providers;

use Illuminate\Support\ServiceProvider;
use App\Domain\Kitchens\Entities\Kitchen;
use App\Domain\Kitchens\Contracts\KitchenRepository;
use App\Domain\Kitchens\Repositories\EloquentKitchenRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(KitchenRepository::class, function () {
            return new EloquentKitchenRepository(new Kitchen());
        });
    }

    public function provides(): array
    {
        return [
            KitchenRepository::class
        ];
    }
}
