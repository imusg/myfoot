<?php
namespace App\Domain\Kitchens\Providers;

use Illuminate\Routing\Router;
use App\Domain\Kitchens\Http\Controllers\KitchenController;
use App\Domain\Kitchens\Http\Controllers\DeliveryController;
use App\Domain\Kitchens\Http\Controllers\PickerFileController;
use App\Domain\Kitchens\Http\Controllers\KitchenFileController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'namespace'  => $this->namespace,
                'prefix'     => 'api/v1',
                'middleware' => ['auth:api'],
            ], function (Router $router) {
                $this->mapRoutesWhenManager($router);
                $this->mapRoutesKitchenFile($router);
                $this->mapRoutesPickerFile($router);
            });
    }

    private function mapRoutesWhenManager(Router $router) : void
    {
        $router->get('kitchens', [KitchenController::class, 'index'])
            ->name('kitchens.index');

        $router->put('kitchens', [KitchenController::class, 'update'])
            ->name('kitchens.update');

        $router->put('kitchens/{id}', [KitchenController::class, 'updateKitchen']);

        $router->get('delivery', [DeliveryController::class, 'index']);

        $router->post('delivery', [DeliveryController::class, 'store']);

        $router->put('delivery', [DeliveryController::class, 'update']);

        $router->post('writeoff', [KitchenController::class, 'writeOff']);
    }

    private function mapRoutesKitchenFile(Router $router): void
    {
        $router->get('kitchens/file', [KitchenFileController::class, 'generate']);
        $router->delete('kitchens/file/{id}', [KitchenFileController::class, 'destroy']);
        $router->get('kitchens/files', [KitchenFileController::class, 'index']);
    }

    private function mapRoutesPickerFile(Router $router): void
    {
        $router->get('pickers/file', [PickerFileController::class, 'generate']);
        $router->delete('pickers/file/{id}', [PickerFileController::class, 'destroy']);
        $router->get('pickers/files', [PickerFileController::class, 'index']);
    }
}
