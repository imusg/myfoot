<?php
namespace App\Domain\Kitchens\Providers;

use App\Domain\Kitchens\Database\Factories\KitchenFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'kitchens';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        KitchenFactory::class
    ];
}
