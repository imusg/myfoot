<?php
namespace App\Domain\Kitchens\Entities;

use App\Domain\Meal\Entities\Meal;
use App\Domain\Orders\Entities\Order;
use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Kitchens\Entities\Kitchen;

class Delivery extends Model
{
    protected $table = 'delivery';
    protected $fillable = ['meal_id', 'order_id', 'client_id' ,'date', 'state', 'branch_id', 'kitchen_id'];

    protected $attributes = ['state' => 'not'];

    public function orders()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function kitchen()
    {
        return $this->hasOne(Kitchen::class, 'id', 'kitchen_id');
    }

    public function meal()
    {
        return $this->hasOne(Meal::class, 'id', 'meal_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }
}
