<?php
namespace App\Domain\Kitchens\Entities;

use App\Domain\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;

class KitchenFile extends Model
{
    protected $fillable = [
        'url',
        'branch_id',
        'user_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
