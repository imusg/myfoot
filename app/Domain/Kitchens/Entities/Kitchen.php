<?php
namespace App\Domain\Kitchens\Entities;

use App\Domain\Meal\Entities\Meal;
use App\Domain\Orders\Entities\Order;
use App\Domain\Clients\Entities\Client;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Kitchens\Entities\Delivery;
use App\Domain\Orders\Entities\OrderPosition;

class Kitchen extends Model
{
    protected $fillable = [
        'meal_id',
        'order_id',
        'date',
        'state',
        'branch_id',
        'position_id',
        'actual_quantity',
        'client_id'
    ];

    protected $attributes = ['state' => 'not'];

    public function orders()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function position()
    {
        return $this->hasOne(OrderPosition::class, 'id', 'position_id');
    }

    public function meal()
    {
        return $this->hasOne(Meal::class, 'id', 'meal_id');
    }

    public function kitchenInDelivery()
    {
        return $this->hasMany(Delivery::class, 'kitchen_id', 'id');
    }

    public function clients()
    {
        return $this->belongsToMany(
            Client::class,
            'client_orders',
            'order_id',
            'client_id',
            'order_id'
        );
    }
}
