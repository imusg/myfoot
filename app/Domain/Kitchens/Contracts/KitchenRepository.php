<?php

namespace App\Domain\Kitchens\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface KitchenRepository extends BaseRepository
{
}
