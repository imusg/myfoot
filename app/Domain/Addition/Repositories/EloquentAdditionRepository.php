<?php

namespace App\Domain\Addition\Repositories;

use App\Domain\Addition\Contracts\AdditionRepository;
use App\Domain\Addition\Entities\Addition;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentAdditionRepository extends EloquentRepository implements AdditionRepository
{
    private string $defaultSort = 'id';

    private array $defaultSelect = [
        'id',
        'title',
        'price',
        'purchase_price',
        'branch_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'title',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Addition::class)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }
}
