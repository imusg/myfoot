<?php

namespace App\Domain\Addition\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use App\Domain\Storage\Entities\Storage;
use App\Domain\Addition\Entities\Addition;
use App\Domain\Dishies\Entities\DirectoryDish;
use App\Domain\Dishies\Entities\DishIngredient;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Addition\Contracts\AdditionRepository;
use App\Domain\Addition\Http\Resources\AdditionResource;
use App\Domain\Addition\Http\Resources\AdditionCollection;
use App\Domain\Addition\Http\Requests\AdditionStoreRequests;
use App\Domain\Addition\Http\Requests\AdditionUpdateRequests;

class AdditionController extends Controller
{
    private AdditionRepository $AdditionRepository;

    public function __construct(AdditionRepository $AdditionRepository)
    {
        $this->AdditionRepository = $AdditionRepository;
        $this->resourceItem = AdditionResource::class;
        $this->resourceCollection = AdditionCollection::class;
    }
    /**
     * @OA\Get(
     * path="/additions",
     * summary="Получение всех позиций доп меню",
     * description="Получение всех позиций доп меню",
     * operationId="indexAdditions",
     * tags={"Additions"},
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     *    response=200,
     *    description="Успешная получение всех позиций доп меню",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *              ref="#/components/schemas/Addition"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         ref="#/components/schemas/Meta"
     *       )
     *        )
     *  )
     * );
     */
    public function index()
    {
        $collection = $this->AdditionRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    /**
     * @OA\Post(
     * path="/additions",
     * summary="Создание доп меню",
     * description="Добавление доп меню",
     * operationId="storeAdditions",
     * tags={"Additions"},
     * security={{"bearerAuth":{}}},
     * @OA\RequestBody(
     *    required=true,
     *    description="Передайте данные для создания доп меню",
     *    @OA\JsonContent(
     *       ref="#/components/schemas/AdditionRequest"
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная добавление доп меню",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            ref="#/components/schemas/Addition"
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     */
    public function store(AdditionStoreRequests $request)
    {
        $data = $request->validated();
        $response = $this->AdditionRepository->store($data);
        return $this->respondWithItem($response);
    }
    /**
         * @OA\Put(
         * path="/additions/{addition}",
         * summary="Обновление доп меню",
         * description="Обновление доп меню",
         * operationId="updateAddition",
         * tags={"Additions"},
         * security={{"bearerAuth":{}}},
         * @OA\Parameter(
         *  name="addition",
         *  in="path",
         *  required=true,
         *  description="Addition id",
         *  @OA\Schema(
         *    type="integer"
         *  )
         * ),
         * @OA\RequestBody(
         *    required=true,
         *    description="Передайте данные для обновления доп меню",
         *    @OA\JsonContent(
         *       ref="#/components/schemas/AdditionRequest"
         *    ),
         * ),
         * @OA\Response(
         *    response=200,
         *    description="Успешная обновление доп меню",
         *    @OA\JsonContent(
         *       @OA\Property(
         *            property="data",
         *            type="object",
         *            ref="#/components/schemas/Addition"
         *          ),
         *       @OA\Property(
         *         property="meta",
         *         type="object",
         *         @OA\Property(
         *            property="timestamp",
         *            type="integer",
         *            example="1608213575077"
         *         )
         *       )
         *      )
         *   )
         * )
         */
    public function update(AdditionUpdateRequests $request, $id)
    {
        $data = $request->validated();
        $response = $this->AdditionRepository->update(
            $this->AdditionRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }
    /**
     * @OA\Delete (
     * path="/additions/{addition}",
     * summary="Удаление доп меню",
     * description="Удаление доп меню",
     * operationId="deleteAddition",
     * tags={"Additions"},
     * security={{"bearerAuth":{}}},
     * @OA\Parameter(
     *  name="addition",
     *  in="path",
     *  required=true,
     *  description="Addition id",
     *  @OA\Schema(
     *    type="integer"
     *  )
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешная удаление доп меню",
     *    @OA\JsonContent(
     *       @OA\Property(
     *            property="data",
     *            type="object",
     *            @OA\Property(
     *              property="deleted",
     *              type="boolean",
     *              example="true"
     *            )
     *          ),
     *       @OA\Property(
     *         property="meta",
     *         type="object",
     *         @OA\Property(
     *            property="timestamp",
     *            type="integer",
     *            example="1608213575077"
     *         )
     *       )
     *      )
     *   )
     * )
     */
    public function destroy($id): JsonResponse
    {
        $response = $this->AdditionRepository->destroy(
            $this->AdditionRepository->findOneById($id)
        );
        return $this->respondWithCustomData([
            'deleted' => $response
        ]);
    }
}
