<?php

namespace App\Domain\Addition\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Branches\Http\Resources\BranchResource;

class AdditionResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
            'purchase_price' => $this->purchase_price,
            'branch' => BranchResource::make($this->branch)
        ];
    }
}
