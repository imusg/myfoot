<?php

namespace App\Domain\Addition\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AdditionCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => AdditionResource::collection($this->collection),
        ];
    }
}
