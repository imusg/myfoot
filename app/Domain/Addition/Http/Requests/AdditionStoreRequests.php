<?php

namespace App\Domain\Addition\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

/**
* @OA\Schema (
 *    schema="AdditionRequest",
 *    type="object",
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Названии позиции",
 *      example="Pepsi",
 *    ),
 *     @OA\Property (
 *      property="price",
 *      type="integer",
 *      description="Цена",
 *      example="220.00",
 *    ),
*     @OA\Property (
 *      property="purchase_price",
 *      type="integer",
 *      description="Закупочная цена",
 *      example="220.00",
 *    )
 * )
*/
class AdditionStoreRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'max:250',
            ],
            'price' => [
                'required'
            ],
            'purchase_price' => 'required',
            'branch_id' => 'nullable',
        ];
    }
}
