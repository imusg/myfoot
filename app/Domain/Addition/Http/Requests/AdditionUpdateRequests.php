<?php

namespace App\Domain\Addition\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class AdditionUpdateRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'string',
                'max:250',
            ],
            'price' => [
                'integer'
            ],
            'purchase_price' => 'integer',
            'branch_id' => 'nullable',
        ];
    }
}
