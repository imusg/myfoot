<?php

namespace App\Domain\Addition\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Branches\Entities\Branch;

/**
 * App\Domain\Addition\Entities\Addition
 * @OA\Schema (
 *    schema="Addition",
 *    type="object",
 * @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Названии позиции",
 *      example="Pepsi",
 *    ),
 *     @OA\Property (
 *      property="price",
 *      type="integer",
 *      description="Цена",
 *      example="220.00",
 *    ),
*     @OA\Property (
 *      property="purchase_price",
 *      type="integer",
 *      description="Закупочная цена",
 *      example="220.00",
 *    )
 * )
 * @property int $id
 * @property string $title
 * @property string $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Addition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Addition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Addition query()
 * @method static \Illuminate\Database\Eloquent\Builder|Addition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Addition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Addition wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Addition whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Addition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Addition extends Model
{
    protected $fillable = ['title', 'price', 'purchase_price', 'branch_id'];

    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }
}
