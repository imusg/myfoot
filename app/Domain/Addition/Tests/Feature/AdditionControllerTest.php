<?php

namespace App\Domain\Addition\Tests\Feature;

use App\Domain\Addition\Entities\Addition;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class AdditionControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testIndexAddition(): void
    {
        $this->get(route('addition.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    ['id', 'title', 'price']
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    public function testStoreAddition() : void
    {
        $response = $this->post(
            route('addition.store'),
            ['title' => $this->faker->jobTitle, 'price' => 20]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'price'
                ],
                'meta',
            ]);
        $id = $response->json('data.id');
        $this->assertDatabaseHas('additions', ['id' => $id]);
    }

    public function testUpdateAddition() : void
    {
        $addition = factory(Addition::class)
            ->create();

        $response = $this->put(route('addition.update', $addition->id), ['price' => 25])
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'price'
                ],
                'meta',
            ]);

        $data = $response->json('data');
        $this->assertDatabaseHas('additions', $data);
    }

    public function testDestroyAddition() : void
    {
        $addition = factory(Addition::class)
            ->create();

        $this->delete(route('addition.destroy', $addition->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('additions', ['id' => $addition->id]);
    }

}
