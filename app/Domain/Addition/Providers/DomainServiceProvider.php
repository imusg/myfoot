<?php
namespace App\Domain\Addition\Providers;

use App\Domain\Addition\Database\Factories\AdditionFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'additions';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        AdditionFactory::class
    ];
}
