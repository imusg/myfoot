<?php
namespace App\Domain\Addition\Providers;

use App\Domain\Addition\Contracts\AdditionRepository;
use App\Domain\Addition\Entities\Addition;
use App\Domain\Addition\Repositories\EloquentAdditionRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(AdditionRepository::class, function () {
            return new EloquentAdditionRepository(new Addition());
        });
    }

    public function provides(): array
    {
        return [
            AdditionRepository::class
        ];
    }
}
