<?php

namespace App\Domain\Addition\Database\Factories;

use App\Domain\Addition\Entities\Addition;
use App\Infrastructure\Abstracts\ModelFactory;

class AdditionFactory extends ModelFactory
{
    protected string $model = Addition::class;

    public function fields(): array
    {
        return [
            'title' => $this->faker->jobTitle,
            'price' => $this->faker->numberBetween(0, 100),
            'purchase_price' => $this->faker->numberBetween(0, 100),
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
