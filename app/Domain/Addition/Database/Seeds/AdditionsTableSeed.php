<?php

namespace App\Domain\Addition\Database\Seeds;

use App\Domain\Addition\Entities\Addition;
use Illuminate\Database\Seeder;

class AdditionsTableSeed extends Seeder
{
    public function run()
    {
        factory(Addition::class, 10)->create();
    }
}
