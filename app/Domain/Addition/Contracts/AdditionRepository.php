<?php

namespace App\Domain\Addition\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface AdditionRepository extends BaseRepository
{
}
