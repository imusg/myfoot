<?php

namespace App\Domain\Users\Repositories;

use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Users\Contracts\UserRepository;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentUserRepository extends EloquentRepository implements UserRepository
{
    private string $defaultSort = '-created_at';

    private array $defaultSelect = [
        'id',
        'email',
        'name',
        'branch_id',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];

    private array $allowedIncludes = [
        'roles',
        'branch'
    ];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(User::class)
            ->when(request()->user()->branch_id, function ($query) {
                return $query->where('branch_id', request()->user()->branch_id);
            })
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }

    public function store(array $data): Model
    {
        $user = parent::store($data);
        $role = Role::find($data['role_id']);
        $user->assignRole($role);
        return $user;
    }

    public function update(Model $model, array $data): Model
    {
        $user = parent::update($model, $data);
        if (isset($data['role_id'])) {
            $role = Role::find($user->roles[0]->id);
            $user->removeRole($role);
            $newRole = Role::find($data['role_id']);
            $user->assignRole($newRole);
        }
        return $user;
    }
}
