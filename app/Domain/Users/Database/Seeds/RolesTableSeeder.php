<?php

namespace App\Domain\Users\Database\Seeds;

use App\Domain\Users\Entities\Permission;
use App\Domain\Users\Entities\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run(): void
    {
        ############
        # SUPER_ADMIN
        ############

        $role_super_admin = Role::create(['name' => Role::SUPER_ADMIN]);
        $role_super_admin->givePermissionTo(Permission::all());

        ############
        # MASTER_ADMIN
        ############

        $role_master_admin = Role::create(['name' => Role::MASTER_ADMIN]);
        $role_master_admin->givePermissionTo(Permission::all());

        ############
        # ADMINISTRATOR
        ############

        $role_administrator = Role::create(['name' => Role::ADMINISTRATOR]);
        $role_administrator->givePermissionTo(Permission::where('name', 'like', '%kitchens%')->get());
        $role_administrator->givePermissionTo(Permission::where('name', 'like', '%delivery%')->get());
        $role_administrator->givePermissionTo(Permission::where('name', 'like', '%manager%')->get());
        $role_administrator->givePermissionTo(Permission::where('name', 'like', '%orders%')->get());
        $role_administrator->givePermissionTo(Permission::where('name', 'like', '%clients%')->get());

        ############
        # ADMIN
        ############

        $role_admin = Role::create(['name' => Role::ADMIN]);
        $role_admin->givePermissionTo(Permission::where('name', 'like', '%kitchens%')->get());
        $role_admin->givePermissionTo(Permission::where('name', 'like', '%delivery%')->get());
        $role_admin->givePermissionTo(Permission::where('name', 'like', '%manager%')->get());
        $role_admin->givePermissionTo(Permission::where('name', 'like', '%orders%')->get());
        $role_admin->givePermissionTo(Permission::where('name', 'like', '%clients%')->get());
        $role_admin->givePermissionTo(Permission::where('name', 'like', '%storage%')->get());

        ############
        # MANAGER
        ############

        $role_manager = Role::create(['name' => Role::MANAGER]);
        $role_manager->givePermissionTo(Permission::where('name', 'like', '%kitchens%')->get());
        $role_manager->givePermissionTo(Permission::where('name', 'like', '%delivery%')->get());
        $role_manager->givePermissionTo(Permission::where('name', 'like', '%manager%')->get());
        $role_manager->givePermissionTo(Permission::where('name', 'like', '%orders%')->get());
        $role_manager->givePermissionTo(Permission::where('name', 'like', '%clients%')->get());
    }
}
