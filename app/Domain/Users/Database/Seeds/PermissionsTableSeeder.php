<?php

namespace App\Domain\Users\Database\Seeds;

use App\Domain\Users\Entities\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run() :void
    {
        collect([
            'permissions',
            'users',
            'roles',
            'units',
            'dishes',
            'clients',
            'orders',
            'storage',
            'manager',
            'semis',
            'cycles',
            'category',
            'meals',
            'branches',
            'ingredients',
            'kitchens',
            'delivery',
        ])->each(function ($type) {
            $this->createPermissions($type);
        });
    }

    private function createPermissions($type): void
    {
        Permission::create(['name' => "view any {$type}"]);
        Permission::create(['name' => "view {$type}"]);
        Permission::create(['name' => "create {$type}"]);
        Permission::create(['name' => "update {$type}"]);
        Permission::create(['name' => "delete {$type}"]);
        Permission::create(['name' => "restore {$type}"]);
        Permission::create(['name' => "force delete {$type}"]);
    }
}
