<?php

namespace App\Domain\Users\Database\Seeds;

use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;
use Illuminate\Database\Seeder;

class UsersTableSeed extends Seeder
{
    public function run()
    {
        $userSuperAdmin = factory(User::class)->create([
            'email' => 'super_admin@example.com'
        ]);
        $roleSuperAdmin = Role::where('name', Role::SUPER_ADMIN)->first();
        $userSuperAdmin->assignRole($roleSuperAdmin);

        $userMasterAdmin = factory(User::class)->create([
            'email' => 'master_admin@example.com',
        ]);
        $roleMasterAdmin = Role::where('name', Role::MASTER_ADMIN)->first();
        $userMasterAdmin->assignRole($roleMasterAdmin);


        $userAdmin = factory(User::class)->create([
            'email' => 'admin@example.com',
            'branch_id' => 1
        ]);
        $roleAdmin = Role::where('name', Role::ADMIN)->first();
        $userAdmin->assignRole($roleAdmin);

        $userAdministrator = factory(User::class)->create([
            'email' => 'administrator@example.com',
            'branch_id' => 1
        ]);
        $roleAdministrator = Role::where('name', Role::ADMINISTRATOR)->first();
        $userAdministrator->assignRole($roleAdministrator);

        $userManager = factory(User::class)->create([
            'email' => 'manager@example.com',
            'branch_id' => 1
        ]);
        $roleManager = Role::where('name', Role::MANAGER)->first();
        $userManager->assignRole($roleManager);
    }
}
