<?php

namespace App\Domain\Users\Database\Factories;

use App\Domain\Users\Entities\User;
use App\Infrastructure\Abstracts\ModelFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends ModelFactory
{
    protected string $model = User::class;

    public function fields(): array
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => 12345678,
            'branch_id' => null,
            'remember_token' => Str::random(10),
        ];
    }

    public function states(): void
    {
    }
}
