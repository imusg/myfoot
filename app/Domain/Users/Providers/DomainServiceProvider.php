<?php

namespace App\Domain\Users\Providers;

use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;
use App\Domain\Users\Policies\UserPolicy;
use App\Domain\Users\Entities\Permission;
use App\Infrastructure\Abstracts\ServiceProvider;
use App\Domain\Users\Database\Factories\UserFactory;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'users';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
        EventServiceProvider::class,
    ];

    protected array $policies = [
//        Permission::class       => PermissionPolicy::class,
//        Role::class             => RolePolicy::class,
       User::class             => UserPolicy::class,
    ];

    protected array $factories = [
        UserFactory::class,
    ];
}
