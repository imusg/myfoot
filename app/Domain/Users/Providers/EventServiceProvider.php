<?php

namespace App\Domain\Users\Providers;

use App\Domain\Users\Entities\Permission;
use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;
use App\Domain\Users\Listeners\Observers\PermissionObserver;
use App\Domain\Users\Listeners\Observers\RoleObserver;
use App\Domain\Users\Listeners\Observers\UserObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Neves\Events\Contracts\TransactionalEvent;

class EventServiceProvider extends ServiceProvider implements TransactionalEvent
{
    protected $listen = [];

    public function boot()
    {
        parent::boot();
        Permission::observe(PermissionObserver::class);
        Role::observe(RoleObserver::class);
        User::observe(UserObserver::class);
    }
}
