<?php

namespace App\Domain\Users\Providers;

use Illuminate\Routing\Router;
use App\Domain\Users\Entities\User;
use App\Domain\Users\Http\Controllers\UserController;
use App\Domain\Users\Http\Controllers\LoginController;
use App\Domain\Users\Http\Controllers\EmployesController;
use App\Domain\Users\Http\Controllers\RegisterController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function map(Router $router): void
    {
        if (config('register.api_routes')) {
            $this->mapApiRoutes($router);
        }
    }

    protected function mapApiRoutes(Router $router): void
    {
        $router
            ->group([
                'prefix'     => 'api/v1/auth',
            ], function (Router $router) {
                $this->mapRoutesWhenGuest($router);
            });

        $router->group([
            'prefix'     => 'api/v1',
        ], function (Router $router) {
            $router
                ->group(['middleware' => 'auth:api'], function () use ($router) {
                    $router
                        ->get('users', [UserController::class, 'index'])
                        ->name('users.get');

                    $router
                        ->post('users', [UserController::class, 'store'])
                        ->name('users.create');

                    $router
                        ->put('users/{id}', [UserController::class, 'update'])
                        ->name('users.update');

                    $router
                        ->delete('users/{id}', [UserController::class, 'destroy'])
                        ->name('users.delete');
                    $router->get('me', [LoginController::class, 'me'])->name('api.me');
                });
        });
    }

    private function mapRoutesWhenGuest(Router $router): void
    {
        $router
            ->group(['middleware' => 'guest'], function () use ($router) {
                $router
                    ->post('sign-in', [LoginController::class, 'login'])
                    ->name('api.auth.login');
            });
    }
}
