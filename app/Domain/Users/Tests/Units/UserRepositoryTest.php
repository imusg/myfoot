<?php

namespace App\Domain\Users\Tests\Unit;

use Tests\TestCase;
use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;
use App\Domain\Users\Repositories\EloquentUserRepository;

class UserRepositoryTest extends TestCase
{
    private User $mockUser;
    private $repositoryMock;
    private array $mockData;

    public function setUp(): void
    {
        parent::setUp();
        $this->mockData = [
            'name' => 'test',
            'email' => 'foo@bar.com',
            'password' => '12345678'
        ];
        $this->mockUser = factory(User::class)->make($this->mockData);
        $this->repositoryMock = $this->createMock(EloquentUserRepository::class);
    }

    public function testStoreUserRepository(): void
    {
        $this->repositoryMock
            ->method('store')
            ->willReturn($this->mockUser);
        $this->assertSame($this->mockUser, $this->repositoryMock->store($this->mockData));
    }

    public function testUpdateUserRepository(): void
    {
        $newMockData = ['name' => 'new-test'];

        $this->mockUser->name = 'new-test';

        $this->assertSame($this->mockUser, $this->repositoryMock->update($this->mockUser, $newMockData));
    }
}
