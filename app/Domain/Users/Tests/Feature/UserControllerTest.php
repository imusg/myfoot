<?php

namespace App\Domain\Users\Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;

class UserControllerTest extends TestCase
{
    private User $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $role = Role::where('name', Role::SUPER_ADMIN)->first();
        $this->user->assignRole($role);
    }
    /*
    * Получение все пользователя
    */
    public function testGetAllUsers() : void
    {
        $response = $this
            ->actingAs($this->user)
            ->get(route('users.get'));
        $response
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    ['id', 'name', 'email', 'roles']
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    /*
    * Создание нового пользователя с выбранной ролью
    */
    public function testStoreNewUserWhenRole(): void
    {
        $mockData = [
            'name' => 'test',
            'email' => 'test@test.com',
            'password' => '12345678',
            'branch_id' => 1,
            'role_id' => 5
        ];
        $response = $this
            ->actingAs($this->user)
            ->postJson(route('users.create'), $mockData);
        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'email', 'roles', 'branch'
                ]
            ]);
        $this->assertDatabaseHas('users', ['id' => $response->json('data.id')]);
    }

    /*
    * Обновление информаци пользователя
    */
    public function testUpdateUser() : void
    {
        $mockData = [
            'name' => 'new-test',
        ];
        $user = factory(User::class)->create();
        $response = $this
            ->actingAs($this->user)
            ->putJson(route('users.update', $user->id), $mockData);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'email', 'roles', 'branch'
                ]
            ]);
        $this->assertDatabaseHas('users', $mockData);
    }

    /*
    * Удаление пользователя
    */
    public function testDeleteUser() : void
    {
        $user = factory(User::class)->create();
        $response = $this
            ->actingAs($this->user)
            ->delete(route('users.delete', $user->id));
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data'
            ]);
        $this->assertDeleted('users', $user->toArray());
    }
}
