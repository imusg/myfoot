<?php

namespace App\Domain\Users\Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Entities\User;
use Illuminate\Support\Facades\Log;

class LoginControllerTest extends TestCase
{
    private User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testLogin(): void
    {
        $response = $this
            ->postJson(route('api.auth.login'), [
                'email'    => $this->user->email,
                'password' => '12345678',
            ])
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'token',
                    'tokenType',
                ],
                'meta',
            ]);
    }

    public function testFetchTheCurrentUser()
    {
        $this
            ->actingAs($this->user)
            ->getJson(route('api.me'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'email', 'roles', 'branch'
                ]
            ]);
    }
}
