<?php

namespace App\Domain\Users\Tests\Feature;

use Tests\TestCase;
use App\Domain\Users\Entities\Role;
use App\Domain\Users\Entities\User;

class UsersPermissionTest extends TestCase
{
    private User $user;
    private $role;

    private function changeRole(string $role):void
    {
        $this->user->removeRole($this->role);
        $this->role = Role::where('name', $role)->first();
        $this->user->assignRole($this->role);
    }

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->role = Role::where('name', Role::SUPER_ADMIN)->first();
        $this->user->assignRole($this->role);
    }

    public function testCanNotGetUserForTheManager(): void
    {
        $this->changeRole(Role::MANAGER);

        $this
            ->actingAs($this->user)
            ->get(route('users.get'))
            ->assertStatus(403);
    }

    public function testCanNotGetUserForTheAdmin(): void
    {
        $this->changeRole(Role::ADMIN);

        $this
            ->actingAs($this->user)
            ->get(route('users.get'))
            ->assertStatus(403);
    }

    public function testCanNotGetUserForTheAdministrator(): void
    {
        $this->changeRole(Role::ADMINISTRATOR);

        $this
            ->actingAs($this->user)
            ->get(route('users.create'))
            ->assertStatus(403);
    }

    public function testCanNotCreateNewUserForTheManager(): void
    {
        $this->changeRole(Role::MANAGER);

        $this
            ->actingAs($this->user)
            ->postJson(route('users.create'), [])
            ->assertStatus(403);
    }

    public function testCanNotCreateNewUserForTheAdmin(): void
    {
        $this->changeRole(Role::ADMIN);

        $this
            ->actingAs($this->user)
            ->postJson(route('users.create'), [])
            ->assertStatus(403);
    }

    public function testCanNotCreateNewUserForTheAdministrator(): void
    {
        $this->changeRole(Role::ADMINISTRATOR);

        $this
            ->actingAs($this->user)
            ->postJson(route('users.create'), [])
            ->assertStatus(403);
    }

    public function testCanNotUpdateUserForTheManager(): void
    {
        $this->changeRole(Role::MANAGER);

        $this
            ->actingAs($this->user)
            ->put(route('users.update', 1), [])
            ->assertStatus(403);
    }

    public function testCanNotUpdateUserForTheAdmin(): void
    {
        $this->changeRole(Role::ADMIN);

        $this
            ->actingAs($this->user)
            ->put(route('users.update', 1), [])
            ->assertStatus(403);
    }

    public function testCanNotUpdateUserForTheAdministrator(): void
    {
        $this->changeRole(Role::ADMINISTRATOR);

        $this
            ->actingAs($this->user)
            ->put(route('users.update', 1), [])
            ->assertStatus(403);
    }

    public function testCanNotDeleteUserForTheManager(): void
    {
        $this->changeRole(Role::MANAGER);

        $this
            ->actingAs($this->user)
            ->delete(route('users.delete', 1))
            ->assertStatus(403);
    }

    public function testCanNotDeleteUserForTheAdmin(): void
    {
        $this->changeRole(Role::ADMIN);

        $this
            ->actingAs($this->user)
            ->delete(route('users.delete', 1))
            ->assertStatus(403);
    }

    public function testCanNotDeleteUserForTheAdministrator(): void
    {
        $this->changeRole(Role::ADMINISTRATOR);

        $this
            ->actingAs($this->user)
            ->delete(route('users.delete', 1))
            ->assertStatus(403);
    }
}
