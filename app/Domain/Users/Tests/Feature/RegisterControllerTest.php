<?php

namespace App\Domain\Users\Tests\Feature;

use App\Domain\Users\Entities\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $user = factory(User::class)->create();
        $this->actingAs($user);
    }

    public function testCanRegister()
    {
        $this->postJson(route('api.auth.register'), [
            'name'                  => 'test',
            'email'                 => $this->faker->unique()->safeEmail,
            'password'              => '12345678',
        ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'data' => [
                    'token',
                    'tokenType',
                ],
                'meta',
            ]);
    }

    public function testCannotRegisterBecausePasswordIsTooShort()
    {
        $this->postJson(route('api.auth.register'), [
            'name'                  => 'test',
            'email'                 => $this->faker->unique()->safeEmail,
            'password'              => 'secret',
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure([
                'data' => [
                    'message',
                    'errors',
                ],
                'meta'
            ]);
    }

    public function testCannotRegisterBecauseEmailAlreadyRegistered()
    {
        $this->postJson(route('api.auth.register'), [
            'email'                 => 'test@test.com',
            'password'              => 'secretxxx-test',
            'password_confirmation' => 'secretxxx-test',
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure([
                'data' => [
                    'message',
                    'errors',
                ],
                'meta'
            ]);
    }
}
