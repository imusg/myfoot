<?php

namespace App\Domain\Users\Policies;

use App\Domain\Users\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('view any users');
    }

    public function view(User $user)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->can('create users');
    }

    public function update(User $user, User $updateUser)
    {
        return $user->can('update users');
    }

    public function delete(User $user)
    {
        return $user->can('delete users');
    }
}
