<?php

namespace App\Domain\Users\Listeners\Observers;

use App\Domain\Users\Entities\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class PermissionObserver
{
    public function creating(Model $model)
    {
        $model->guard_name = 'api';
    }

    public function updated(Permission $permission)
    {
        $this->created($permission);
    }

    public function created(Permission $permission)
    {
        Cache::forget('spatie.permission.cache');
    }
}
