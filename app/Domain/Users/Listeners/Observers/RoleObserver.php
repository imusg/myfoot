<?php

namespace App\Domain\Users\Listeners\Observers;

use App\Domain\Users\Entities\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class RoleObserver
{
    public function creating(Model $model)
    {
        $model->guard_name = 'api';
    }

    public function updated(Role $role)
    {
        $this->created($role);
    }

    public function created(Role $role)
    {
        Cache::forget('spatie.permission.cache');
    }
}
