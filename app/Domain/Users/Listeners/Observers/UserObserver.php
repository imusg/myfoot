<?php

namespace App\Domain\Users\Listeners\Observers;

use Illuminate\Database\Eloquent\Model;
use Neves\Events\Contracts\TransactionalEvent;

class UserObserver implements TransactionalEvent
{
    public function creating(Model $model)
    {
    }
}
