<?php

namespace App\Domain\Users\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UnauthorizedException extends Exception
{
    public function report()
    {
        //
    }

    public function render(Request $request): JsonResponse
    {
        return response()->json([
            'error' => 'Ошибка авторизации',
            'message' => $this->message
        ], Response::HTTP_UNAUTHORIZED);
    }
}
