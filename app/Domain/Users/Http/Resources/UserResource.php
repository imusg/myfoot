<?php

namespace App\Domain\Users\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Domain\Branches\Http\Resources\BranchResource;

class UserResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'roles' => RoleResource::collection($this->roles),
            'branch' => BranchResource::make($this->branch)
        ];
    }
}
