<?php

namespace App\Domain\Users\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Domain\Users\Entities\User;
use App\Domain\Users\Contracts\UserRepository;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Users\Http\Resources\UserResource;
use App\Domain\Users\Http\Resources\UserCollection;
use App\Domain\Users\Http\Requests\UserStoreRequest;
use App\Domain\Users\Http\Requests\UserUpdateRequest;

/**
 * @OA\Get (
 * path="/user",
 * summary="Пользователь",
 * description="Получение авторизованного пользователя",
 * operationId="authUser",
 * tags={"Users"},
 * @OA\Response(
 *    response=200,
 *    description="Успешное получение авторизованного пользователя",
 *    @OA\JsonContent(
 *       @OA\Property(
 *            property="data",
 *            type="object",
 *            ref="#/components/schemas/User"
 *          ),
 *       @OA\Property(
 *         property="meta",
 *         type="object",
 *         @OA\Property(
 *            property="timestamp",
 *            type="integer",
 *            example="1608213575077"
 *         )
 *      )
 *        )
 *   )
 * )
 */
class UserController extends Controller
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->resourceItem = UserResource::class;
        $this->resourceCollection = UserCollection::class;
    }

    public function index() : UserCollection
    {
        //$this->authorize('viewAny', User::class);
        $collection = $this->userRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function store(UserStoreRequest $request) : JsonResponse
    {
        //$this->authorize('create', User::class);
        $data = $request->validated();
        $response = $this->userRepository->store($data);
        return $this->respondWithCustomData(
            UserResource::make($response),
            201
        );
    }

    public function update(UserUpdateRequest $request, string $id) : UserResource
    {
        $user = $this->userRepository->findOneById($id);
        //$this->authorize('update', $user);
        $response = $this->userRepository->update($user, $request->validated());
        return $this->respondWithItem($response);
    }

    public function destroy(string $id) : JsonResponse
    {
        $user = $this->userRepository->findOneById($id);
        //$this->authorize('delete', $user);
        $response = $this->userRepository->destroy($user);
        return $this->respondWithCustomData($response);
    }
}
