<?php

namespace App\Domain\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Domain\Users\Entities\User;
use Illuminate\Support\Facades\Hash;
use App\Interfaces\Http\Controllers\Controller;
use App\Domain\Users\Http\Requests\LoginRequest;
use App\Domain\Users\Http\Resources\UserResource;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Domain\Users\Exceptions\UnauthorizedException;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * @OA\Post(
 * path="/auth/sign-in",
 * summary="Авторизация",
 * description="Авторизация с помощью email and password",
 * operationId="authLogin",
 * tags={"Users"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Передайте учетные данные пользователя",
 *    @OA\JsonContent(
 *       ref="#/components/schemas/LoginRequest"
 *    ),
 * ),
 * @OA\Response(
 *    response=200,
 *    description="Успешная авторизация пользователя",
 *    @OA\JsonContent(
 *       @OA\Property(
 *            property="data",
 *            type="object",
 *            @OA\Property(
 *              property="token",
 *              type="string",
 *              example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
 *            ),
 *            @OA\Property(
 *              property="tokenType",
 *              type="string",
 *              example="Bearer"
 *            ),
 *            @OA\Property(
 *              property="expiresIn",
 *              type="integer",
 *              example="10800"
 *            )
 *          ),
 *       @OA\Property(
 *         property="meta",
 *         type="object",
 *         @OA\Property(
 *            property="timestamp",
 *            type="integer",
 *            example="1608213575077"
 *         )
 *      )
 *        )
 *   )
 * )
 */

class LoginController extends Controller
{
    use AuthenticatesUsers;
    use ValidatesRequests;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * {@inheritdoc}
     */
    protected function attemptLogin(Request $request): bool
    {
        $token = $this->guard()->attempt($this->credentials($request));

        if ($token) {
            $this->guard()->setToken($token);
            return true;
        }

        return false;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $user = auth()->user();

        $this->clearLoginAttempts($request);

        $token = (string)$this->guard()->getToken();
        $expiration = $this->guard()->getPayload()->get('exp');

        return $this->respondWithCustomData([
            'token'     => $token,
            'tokenType' => 'Bearer',
            'expiresIn' => $expiration - time(),
            'role' => $user->roles[0]->name
        ]);
    }

    public function signIn(LoginRequest $request): JsonResponse
    {
        $data = $request->validated();
        $user = User::where('email', $data['email'])->first();
        if (!$user || !Hash::check($data['password'], $user->password)) {
            throw new UnauthorizedException('Неверный email или пароль');
        }
        $token = $user->createToken($data['email'])->plainTextToken;
        return $this->respondWithCustomData([
            'token' => $token,
            'tokenType' => 'Bearer',
        ]);
    }

    public function me() : JsonResponse
    {
        return $this->respondWithCustomData(UserResource::make(auth()->user()));
    }
}
