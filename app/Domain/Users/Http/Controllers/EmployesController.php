<?php
namespace App\Domain\Users\Http\Controllers;

use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class EmployesController extends Controller
{
    public function user() : JsonResponse
    {
        return $this->respondWithCustomData(auth()->user());
    }
}
