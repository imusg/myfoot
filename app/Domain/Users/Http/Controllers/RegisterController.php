<?php

namespace App\Domain\Users\Http\Controllers;

use App\Domain\Users\Entities\User;
use App\Domain\Users\Http\Requests\RegistrationRequest;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function signUp(RegistrationRequest $request)
    {
        $input = $request->validated();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $token = $user->createToken($input['email'])->plainTextToken;
        return $this->respondWithCustomData([
            'token' => $token,
            'tokenType' => 'Bearer'
        ], Response::HTTP_CREATED);
    }

}
