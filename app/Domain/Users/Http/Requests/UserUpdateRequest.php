<?php

namespace App\Domain\Users\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

/**
 * @OA\Schema(
 *    schema="LoginRequest",
 *    type="object",
 * )
 * @OA\Property(
 *   property="email",
 *   type="string",
 *   description="Email",
 *   example="test1@example.com",
 * )
 * @OA\Property(
 *   property="password",
 *   type="string",
 *   description="Password min 8 char",
 *   example="12345678",
 * )
 */

class UserUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'string|max:255',
            'email' => 'string|email',
            'role_id' => 'integer',
            'password' => 'string',
            'branch_id' => 'nullable',
        ];
    }
}
