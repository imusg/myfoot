<?php

namespace App\Domain\Users\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

/**
 * @OA\Schema(
 *    schema="LoginRequest",
 *    type="object",
 * )
 * @OA\Property(
 *   property="email",
 *   type="string",
 *   description="Email",
 *   example="test1@example.com",
 * )
 * @OA\Property(
 *   property="password",
 *   type="string",
 *   description="Password min 8 char",
 *   example="12345678",
 * )
 */

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                'max:250',
            ],
            'password' => [
                'required',
                'string',
                'min:8',
                'max:20',
            ],
        ];
    }
}
