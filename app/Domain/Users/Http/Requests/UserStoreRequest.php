<?php

namespace App\Domain\Users\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

/**
 * @OA\Schema(
 *    schema="LoginRequest",
 *    type="object",
 * )
 * @OA\Property(
 *   property="email",
 *   type="string",
 *   description="Email",
 *   example="test1@example.com",
 * )
 * @OA\Property(
 *   property="password",
 *   type="string",
 *   description="Password min 8 char",
 *   example="12345678",
 * )
 */

class UserStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'role_id' => 'required',
            'branch_id' => 'nullable',
        ];
    }
}
