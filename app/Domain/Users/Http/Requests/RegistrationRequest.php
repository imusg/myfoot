<?php

namespace App\Domain\Users\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class RegistrationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:250',
            ],
            'email' => [
                'required',
                'email',
                'max:250',
                'unique:users,email'
            ],
            'password' => [
                'required',
                'string',
                'min:8',
                'max:20',
            ],
        ];
    }
}
