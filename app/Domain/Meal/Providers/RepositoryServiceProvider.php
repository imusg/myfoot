<?php


namespace App\Domain\Meal\Providers;

use App\Domain\Meal\Contracts\MealRepository;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Meal\Repositories\EloquentMealRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(MealRepository::class, function () {
            return new EloquentMealRepository(new Meal());
        });
    }

    public function provides(): array
    {
        return [
            MealRepository::class
        ];
    }
}
