<?php
namespace App\Domain\Meal\Providers;

use App\Domain\Meal\Database\Factories\MealFactory;
use App\Infrastructure\Abstracts\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    protected string $alias = 'categories';

    protected bool $hasMigrations = true;

    protected bool $hasTranslations = true;

    protected bool $hasFactories = true;

    protected bool $hasPolicies = true;

    protected array $providers = [
        RouteServiceProvider::class,
        RepositoryServiceProvider::class,
    ];

    protected array $policies = [
    ];

    protected array $factories = [
        MealFactory::class
    ];
}
