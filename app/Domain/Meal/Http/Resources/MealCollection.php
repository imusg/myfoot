<?php

namespace App\Domain\Meal\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MealCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => MealResource::collection($this->collection),
        ];
    }
}
