<?php

namespace App\Domain\Meal\Http\Controllers;

use App\Domain\Meal\Contracts\MealRepository;
use App\Domain\Meal\Entities\Meal;
use App\Domain\Meal\Http\Requests\MealStoreRequests;
use App\Domain\Meal\Http\Requests\MealUpdateRequests;
use App\Domain\Meal\Http\Resources\MealCollection;
use App\Domain\Meal\Http\Resources\MealResource;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class MealController extends Controller
{
    private MealRepository $MealRepository;

    public function __construct(MealRepository $MealRepository)
    {
        $this->MealRepository = $MealRepository;
        $this->resourceItem = MealResource::class;
        $this->resourceCollection = MealCollection::class;
    }

    public function index()
    {
        $collection = $this->MealRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function store(MealStoreRequests $request)
    {
        $data = $request->validated();
        $response = $this->MealRepository->store($data);
        return $this->respondWithItem($response);
    }

    public function update(MealUpdateRequests $request, $id)
    {
        $data = $request->validated();
        $response = $this->MealRepository->update(
            $this->MealRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy($id): JsonResponse
    {
        $response = $this->MealRepository->destroy(
            $this->MealRepository->findOneById($id)
        );
        return $this->respondWithCustomData([
            'deleted' => $response
        ]);
    }
}
