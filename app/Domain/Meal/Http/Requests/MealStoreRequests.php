<?php

namespace App\Domain\Meal\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class MealStoreRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'max:250',
            ]
        ];
    }
}
