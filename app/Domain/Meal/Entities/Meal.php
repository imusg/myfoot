<?php

namespace App\Domain\Meal\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Meal\Entities\Meal
 * @OA\Schema (
 *    schema="Meal",
 *    type="object",
 *  @OA\Property (
 *      property="id",
 *      type="integer",
 *      description="ID",
 *      example="1"
 *    ),
 *  @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Категория приема пищи",
 *      example="Завтрак"
 *    ),
 * )
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Meal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Meal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Meal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Meal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Meal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Meal whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Meal whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Meal extends Model
{
    protected $fillable = ['title'];
}
