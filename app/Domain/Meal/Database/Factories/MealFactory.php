<?php

namespace App\Domain\Meal\Database\Factories;

use App\Domain\Meal\Entities\Meal;
use App\Infrastructure\Abstracts\ModelFactory;

class MealFactory extends ModelFactory
{
    protected string $model = Meal::class;

    public function fields(): array
    {
        return [
            'title' => $this->faker->jobTitle
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }


}
