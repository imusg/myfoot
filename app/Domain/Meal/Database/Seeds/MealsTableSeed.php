<?php

namespace App\Domain\Meal\Database\Seeds;

use App\Domain\Meal\Entities\Meal;
use Illuminate\Database\Seeder;

class MealsTableSeed extends Seeder
{
    public function run()
    {
        $data = collect(['Завтрак', 'Обед', 'Ужин']);
        $data->each(function ($item) {
            Meal::create([
                'title' => $item
            ]);
        });
    }
}
