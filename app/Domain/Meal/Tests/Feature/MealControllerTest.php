<?php

namespace App\Domain\Meal\Tests\Feature;

use App\Domain\Meal\Entities\Meal;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class MealControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testIndexMeal(): void
    {
        $this->get(route('meal.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    ['id', 'title']
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    public function testStoreMeal() : void
    {
        $response = $this->post(
            route('meal.store'),
            ['title' => $this->faker->jobTitle]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);
        $id = $response->json('data.id');
        $this->assertDatabaseHas('meals', ['id' => $id]);
    }

    public function testUpdateMeal() : void
    {
        $Meal = factory(Meal::class)
            ->create();

        $response = $this->put(route('meal.update', $Meal->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);

        $data = $response->json('data');
        $this->assertDatabaseHas('meals', $data);
    }

    public function testDestroyMeal() : void
    {
        $Meal = factory(Meal::class)
            ->create();

        $this->delete(route('meal.destroy', $Meal->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('meals', ['id' => $Meal->id]);
    }

}
