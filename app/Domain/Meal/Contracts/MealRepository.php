<?php

namespace App\Domain\Meal\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface MealRepository extends BaseRepository
{
}
