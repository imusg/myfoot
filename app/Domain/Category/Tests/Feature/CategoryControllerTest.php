<?php

namespace App\Domain\Category\Tests\Feature;

use App\Domain\Category\Entities\Category;
use App\Domain\Users\Entities\User;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(User::class)->create());
    }

    public function testIndexCategory(): void
    {
        $this->get(route('category.index'))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    ['id', 'title']
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'per_page',
                    'total'
                ],
            ]);
    }

    public function testStoreCategory() : void
    {
        $response = $this->post(
            route('category.store'),
            ['title' => $this->faker->jobTitle]
        )
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);
        $id = $response->json('data.id');
        $this->assertDatabaseHas('categories', ['id' => $id]);
    }

    public function testUpdateCategory() : void
    {
        $Category = factory(Category::class)
            ->create();

        $response = $this->put(route('category.update', $Category->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title'
                ],
                'meta',
            ]);

        $data = $response->json('data');
        $this->assertDatabaseHas('categories', $data);
    }

    public function testDestroyCategory() : void
    {
        $Category = factory(Category::class)
            ->create();

        $this->delete(route('category.destroy', $Category->id))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'deleted',
                ],
                'meta',
            ]);
        $this->assertDatabaseMissing('categories', ['id' => $Category->id]);
    }
}
