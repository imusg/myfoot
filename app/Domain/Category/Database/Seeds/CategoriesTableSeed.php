<?php

namespace App\Domain\Category\Database\Seeds;

use App\Domain\Category\Entities\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeed extends Seeder
{
    public function run()
    {
        $data = collect(['суп', 'второе']);
        $data->each(function ($item) {
            Category::create([
                'title'=>$item
            ]);
        });
    }
}
