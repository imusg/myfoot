<?php

namespace App\Domain\Category\Database\Factories;

use App\Domain\Category\Entities\Category;
use App\Infrastructure\Abstracts\ModelFactory;

class CategoryFactory extends ModelFactory
{
    protected string $model = Category::class;

    public function fields(): array
    {
        return [
            'title' => $this->faker->jobTitle
        ];
    }

    public function states()
    {
        // TODO: Implement states() method.
    }
}
