<?php


namespace App\Domain\Category\Providers;

use App\Domain\Category\Contracts\CategoryRepository;
use App\Domain\Category\Entities\Category;
use App\Domain\Category\Repositories\EloquentCategoryRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    public function register(): void
    {
        $this->app->singleton(CategoryRepository::class, function () {
            return new EloquentCategoryRepository(new Category());
        });
    }

    public function provides(): array
    {
        return [
            CategoryRepository::class
        ];
    }
}
