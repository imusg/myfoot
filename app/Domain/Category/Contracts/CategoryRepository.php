<?php

namespace App\Domain\Category\Contracts;

use App\Infrastructure\Contracts\BaseRepository;

interface CategoryRepository extends BaseRepository
{
}
