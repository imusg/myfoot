<?php

namespace App\Domain\Category\Repositories;

use App\Domain\Category\Contracts\CategoryRepository;
use App\Domain\Category\Entities\Category;
use App\Infrastructure\Abstracts\EloquentRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\QueryBuilder;

class EloquentCategoryRepository extends EloquentRepository implements CategoryRepository
{
    private string $defaultSort = 'id';

    private array $defaultSelect = [
        'id',
        'title',
        'created_at',
        'updated_at',
    ];

    private array $allowedFilters = [
        'title',
    ];

    private array $allowedSorts = [
        'updated_at',
        'created_at',
    ];
    private array $allowedIncludes = [];

    public function findByFilters(): LengthAwarePaginator
    {
        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        return QueryBuilder::for(Category::class)
            ->select($this->defaultSelect)
            ->allowedFilters($this->allowedFilters)
            ->allowedIncludes($this->allowedIncludes)
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->paginate($perPage);
    }
}
