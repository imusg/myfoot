<?php

namespace App\Domain\Category\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Category\Entities\Category
 *
 * @OA\Schema (
 *    schema="Category",
 *    type="object",
 *    @OA\Property (
 *     property="id",
 *     type="integer",
 *     description="ID",
 *     example="1",
 *    ),
 *    @OA\Property (
 *      property="title",
 *      type="string",
 *      description="Название категории",
 *      example="суп",
 *    )
 * )
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    protected $fillable = ['title'];
}
