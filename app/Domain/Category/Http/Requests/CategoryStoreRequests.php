<?php

namespace App\Domain\Category\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class CategoryStoreRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'max:250',
            ]
        ];
    }
}
