<?php

namespace App\Domain\Category\Http\Requests;

use App\Interfaces\Http\Controllers\FormRequest;

class CategoryUpdateRequests extends FormRequest
{
    public function authorize(): bool
    {
        return auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => [
                'string',
                'max:250',
            ]
        ];
    }
}
