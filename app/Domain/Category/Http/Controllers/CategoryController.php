<?php

namespace App\Domain\Category\Http\Controllers;

use App\Domain\Category\Contracts\CategoryRepository;
use App\Domain\Category\Entities\Category;
use App\Domain\Category\Http\Requests\CategoryStoreRequests;
use App\Domain\Category\Http\Requests\CategoryUpdateRequests;
use App\Domain\Category\Http\Resources\CategoryCollection;
use App\Domain\Category\Http\Resources\CategoryResource;
use App\Interfaces\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class CategoryController extends Controller
{
    private CategoryRepository $CategoryRepository;

    public function __construct(CategoryRepository $CategoryRepository)
    {
        $this->CategoryRepository = $CategoryRepository;
        $this->resourceItem = CategoryResource::class;
        $this->resourceCollection = CategoryCollection::class;
    }

    public function index()
    {
        $collection = $this->CategoryRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    public function store(CategoryStoreRequests $request)
    {
        $data = $request->validated();
        $response = $this->CategoryRepository->store($data);
        return $this->respondWithItem($response);
    }

    public function update(CategoryUpdateRequests $request, $id)
    {
        $data = $request->validated();
        $response = $this->CategoryRepository->update(
            $this->CategoryRepository->findOneById($id),
            $data
        );
        return $this->respondWithItem($response);
    }

    public function destroy($id): JsonResponse
    {
        $response = $this->CategoryRepository->destroy(
            $this->CategoryRepository->findOneById($id)
        );
        return $this->respondWithCustomData([
            'deleted' => $response
        ]);
    }
}
