<?php

namespace App\Domain\Audits\Listeners\Observers;

use Illuminate\Database\Eloquent\Model;
use Neves\Events\Contracts\TransactionalEvent;

class AuditObserver implements TransactionalEvent
{
    public function creating(Model $model)
    {
    }
}
