<?php

namespace App\Domain\Audits\Entities;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model implements \OwenIt\Auditing\Contracts\Audit
{
    use \OwenIt\Auditing\Audit;

    public const UPDATED_AT = null;

    protected static $unguarded = true;

    protected $keyType = 'integer';

    protected $casts = [
        'id'         => 'integer',
        'old_values' => 'json',
        'new_values' => 'json',
    ];

    public function getTable(): string
    {
        return 'audits';
    }

    public function getConnectionName()
    {
        return config('database.default');
    }
}
