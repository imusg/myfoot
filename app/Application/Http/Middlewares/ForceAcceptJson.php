<?php

namespace App\Application\Http\Middlewares;

use Closure;

class ForceAcceptJson
{
    public function handle($request, Closure $next)
    {
        $request->headers->set('Accept', 'application/json');
        return $next($request);
    }
}
