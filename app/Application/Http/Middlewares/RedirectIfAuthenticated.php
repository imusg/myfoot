<?php

namespace App\Application\Http\Middlewares;

use App\Interfaces\Http\Controllers\ResponseTrait;
use Closure;

class RedirectIfAuthenticated
{
    use ResponseTrait;

    public function handle($request, Closure $next, $guard = null)
    {
        return $next($request);
    }
}
